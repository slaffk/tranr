<?php
/**
 * @var $this Controller
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo CHtml::encode($this->getTitle()); ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=0.9">
	<meta property="og:locale" content="ru_RU" />
	<meta property="og:site_name" content="Tranr.ru тренеры/спорт/танцы" />
	<meta property="og:title" content="<?php echo CHtml::encode($this->getTitle()); ?>" />
	<?php Yii::app()->clientScript->registerCssFile('/css/bootstrap.min.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish('css/main.css')); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish('css/bootstrap-theme.css'));?>
	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	<?php Yii::app()->clientScript->registerScriptFile('/js/bootstrap.min.js');?>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<div class="container">

		<?php if (Yii::app()->user->isGuest):?>
			<?=$this->renderPartial('application.views.site.auth_modal');?>
		<?php endif?>
		<?= $this->renderPartial('application.views.site.sms_modal'); ?>
		<div class="navbar navbar-default main-menu" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Меню</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand blue-caption" href="/" title="Бета версия портала ТРЭНР">
						<img width="90" height="20" src="/images/v1/logo-90x20.png"/>
					</a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="<?=Yii::app()->request->getRequestUri() == '/' ? 'active' : ''?>">
							<?=CHtml::link('Подбор тренера', '/')?>
						</li>
						<li class="<?=substr(Yii::app()->request->getRequestUri(), 0, 9) == '/articles' ? 'active' : ''?>">
							<?=CHtml::link('Статьи', '/articles/')?>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<?php if (Yii::app()->user->isGuest):?>
							<li><?=CHtml::link('Вы &mdash; тренер?', '#auth-dialog', array('style' => 'color: #03C03C', 'role' => 'button', 'data-toggle' => 'modal'))?></li>
						<?php else:?>
							<li class="dropdown <?=substr(Yii::app()->request->getRequestUri(), 0, 8) == '/manager' ? 'active' : ''?>">
								<a href="#" class="dropdown-toggle green-caption logo-nav" data-toggle="dropdown">
									<?php if(Yii::app()->user->hasRole(Role::ROLE_TRAINER)):?>
										<img class="img-circle logo-nav" src="<?=Yii::app()->user->getModel()->trainer->getThumb(40, 40, 'crop')?>" width="40" height="40" alt=""/>
									<?php else:?>
										<img class="img-circle logo-nav" src="<?=Yii::app()->user->getModel()->getThumb(40, 40, 'crop')?>" width="40" height="40" alt=""/>
									<?php endif?>
									<?=Yii::app()->user->getModel()->firstname?> <?=Yii::app()->user->getModel()->lastname?> <b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<?php if (Yii::app()->user->hasRole(Role::ROLE_ADMIN)):?>
										<li><a href="/manager/page/index"><span class="glyphicon glyphicon-cog"></span> Администрирование</a></li>
									<?php endif?>
									<li><a href="<?= CHtml::normalizeUrl(array('/cabinet/trainer')); ?>"><span class="glyphicon glyphicon-user"></span> Кабинет тренера</a></li>
									<li><a href="/site/logout"><i class="glyphicon glyphicon-heart"></i> Мне понравилось</a></li>
									<li><a href="/site/logout"><i class="glyphicon glyphicon-wrench"></i> Настройки</a></li>
									<li><a href="/site/logout"><i class="glyphicon glyphicon-log-out"></i> Выход</a></li>
								</ul>
							</li>
						<?php endif?>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<?php if ($this->page):?><?php $this->page->renderWidgets($this, Widget::ZONE_HEADER_1)?><?php endif;?>
		</div>

		<?php /* if(isset($this->breadcrumbs)):?>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
				'homeLink' => false,
				'links' => $this->breadcrumbs,
				'separator' => ' <span class="divider">/</span> ',
				'htmlOptions' => array('class' => 'breadcrumb'),
			));?>
		<?php endif */ ?>

		<div class="content row-fluid">
			<?php echo $content; ?>
		</div>

		<div class="clearfix"></div>
		<div class="footer well well-sm">
			&copy; 2014 <a href="http://tranr.ru">Tranr.ru</a>,
			<a href="mailto:info@tranr.ru">info@tranr.ru</a>
			<?php $this->widget('application.widgets.CommonWidget', array('action' => 'analytics'));?>
			<?php if ($this->page):?><?php $this->page->renderWidgets($this, Widget::ZONE_FOOTER_1)?><?php endif;?>
		</div>
	</div>
</body>
</html>