<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="col-lg-12">
	<?php if ($this->page):?><?php $this->page->renderWidgets($this, Widget::ZONE_CONTENT)?><?php endif;?>
	<?php echo $content?>
</div>
<?php $this->endContent(); ?>