<?php
/**
 * @var $this Controller
 */
?>
<?php $this->beginContent('//layouts/main'); ?>
	<div class="col-lg-8">
		<?php if ($this->page):?><?php $this->page->renderWidgets($this, Widget::ZONE_CONTENT)?><?php endif;?>
		<?php echo $content; ?>
	</div>
	<div class="col-lg-4">
		<?php if ($this->page):?><?php $this->page->renderWidgets($this, Widget::ZONE_SIDEBAR_1)?><?php endif;?>
		<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title' => 'Меню',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
		?>
	</div>
<?php $this->endContent(); ?>