<?php
/**
 * @var $this Controller
 */
?>
<?php $this->beginContent('//layouts/main'); ?>
	<div class="col-lg-4">
		<?php if ($this->page):?><?php $this->page->renderWidgets($this, Widget::ZONE_SIDEBAR_1)?><?php endif;?>
	</div>
	<div class="col-lg-4">
		<?php if ($this->page):?><?php $this->page->renderWidgets($this, Widget::ZONE_CONTENT)?><?php endif;?>
		<?php echo $content; ?>
	</div>
	<div class="col-lg-4">
		<?php if ($this->page):?><?php $this->page->renderWidgets($this, Widget::ZONE_SIDEBAR_2)?><?php endif;?>
		<?php echo $content; ?>
	</div>
<?php $this->endContent(); ?>