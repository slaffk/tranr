<?php
/**
 * @var $this SiteController
 * @var $model LoginForm
 * @var $form CActiveForm
 */
$this->pageTitle = Yii::app()->name . ' - Авторизация';
$this->breadcrumbs = array(
	'Авторизация',
);
?>

<h1>Авторизация</h1>

<?php if (Yii::app()->user->hasFlash('success')):?>
	<div class="alert">
		<?=Yii::app()->user->getFlash('success')?>
		<a class="closeBtn flashCloseBtn" title="Закрыть">&nbsp;</a>
	</div>
<?php endif?>

<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'login-form',
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
	'htmlOptions' => array(
		'class' => 'form-horizontal',
	),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username', array('style' => 'width: 200px')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password', array('style' => 'width: 200px')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<label></label>
		<?=$form->checkBox($model, 'rememberMe', array('class'=>'ajax_field'))?>
		<?php echo $form->labelEx($model,'rememberMe', array('class' => 'right')); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row" style="padding-left: 189px">
		<a href="/remind/">забыли пароль?</a>
	</div>

	<?php echo CHtml::submitButton('Войти'); ?>

	<?php $this->endWidget(); ?>
</div>
