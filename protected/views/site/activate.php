<h1>Активация учетной записи</h1>

<div class="form">
	<p>
		Активация учетной записи прошла успешно. Теперь вы можете зайти в <a href="/cabinet/">личный кабинет</a> и
		<?php if (Yii::app()->user->hasRole(Role::ROLE_EMPLOYER)):?>
			<a href="/cabinet/myvacancies/add/">разместить вакансию</a>
		<?php endif?>
		<?php if (Yii::app()->user->hasRole(Role::ROLE_APPLICANT)):?>
			<a href="/cabinet/myresume/add/">разместить резюме</a>
		<?php endif?>
		<?php if (Yii::app()->user->hasRole(Role::ROLE_HR)):?>
			<a href="/cabinet/myresume/add/">разместить резюме</a> или <a href="/cabinet/myvacancies/add/">разместить вакансию</a>
		<?php endif?>
	</p>
</div>
