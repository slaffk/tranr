<div class="modal fade" id="auth-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Вход в кабинет тренера</h4>
			</div>
			<form class="form-horizontal" role="form" id="login-form" action="/site/login/" method="post">
				<div class="modal-body">

					<div class="form-group">
						<label for="LoginForm_username" class="col-sm-3 control-label required">Email <span class="required">*</span></label>
						<div class="col-sm-6">
							<input class="form-control" name="LoginForm[username]" id="LoginForm_username" type="text">
						</div>
					</div>

					<div class="form-group">
						<label for="LoginForm_password" class="col-sm-3 control-label required">Пароль <span class="required">*</span></label>
						<div class="col-sm-6">
							<input class="form-control" name="LoginForm[password]" id="LoginForm_password" type="password">
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-6">
							<div class="checkbox">
								<label>
									<input id="ytLoginForm_rememberMe" type="hidden" value="0" name="LoginForm[rememberMe]">
									<input name="LoginForm[rememberMe]" id="LoginForm_rememberMe" value="1" type="checkbox">
									Запомнить меня
								</label>
								<a class="pull-right" href="/remind/">забыли пароль?</a>
							</div>
						</div>
					</div>

					<?php /*
					<div class="form-group">
						<label class="col-sm-3 control-label"></label>
						<div class="col-sm-6">
							или через социальные сети
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-3 control-label"></label>
						<div class="col-sm-6">
							<?php $this->widget('ext.eauth.EAuthWidget', array('action' => 'site/login'));?>
						</div>
					</div>
					*/ ?>

				</div>
				<div class="modal-footer">
					<a href="/registration/" class="btn btn-info pull-left">Зарегистрировать личный кабинет</a>
					<input type="submit" class="btn btn-success" value="Войти">
					<button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
				</div>
			</form>
		</div>
	</div>
</div>
