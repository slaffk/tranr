<div class="modal-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'sms-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'class' => 'form-horizontal'
	),
)); ?>
<p class="note">Поля со звездочкой <span class="required">*</span> обязательны.</p>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->hiddenField($model, 'trainer_id'); ?>
<div class="modal-body">
	<div class="form-group">
		<?php echo $form->labelEx($model, 'user_fio', array('class'=>'col-sm-3 control-label')); ?>
		<div class="col-sm-6">
			<?php echo $form->textField($model, 'user_fio', array('class'=>'form-control')); ?>
			<?php echo $form->error($model, 'user_fio'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model, 'user_phone', array('class'=>'col-sm-3 control-label')); ?>
		<div class="col-sm-6">
			<?php echo $form->textField($model, 'user_phone', array('class'=>'form-control')); ?>
			<?php echo $form->error($model, 'user_phone'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model, 'text', array('class'=>'col-sm-3 control-label')); ?>
		<div class="col-sm-6">
			<?php echo $form->textArea($model, 'text', array('class'=>'form-control')); ?>
			<?php echo $form->error($model, 'text'); ?>
		</div>
	</div>

</div>
<div class="modal-footer">
	<input type="button" id="smsSubmitButton"  class="btn btn-success" value="Отправить">
	<button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
</div>
<?php $this->endWidget(); ?>
</div>
