<div class="modal fade" id="sms-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Отправить sms тренеру</h4>
			</div>
			<div class="smsInfo"></div>
			<div id="smsContent">
				
			</div>
		</div>
	</div>
</div>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('js/sms.js')); ?>
