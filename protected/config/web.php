<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

return CMap::mergeArray(
	require(dirname(__FILE__) . '/web.main.php'),
	array(
		'name' => 'Tranr',
		'language' => 'ru',
	    
		'modules' => array(
			//на продакшене выключаем
			'gii' => array(
				'class' => 'system.gii.GiiModule',
				'generatorPaths' => array('application.gii'),
				'password' => 'password',
				'ipFilters' => array('127.0.0.1', '::1'),
			),
		),

		'components' => array(
		    'sms' => array(
				'class'=>'application.components.Sms',
				'user'=>'slava.volynets@gmail.com',
				'password'=>'q1w2e3r4t5y6',
		    ),
		    'log' => array(
			    'class' => 'CLogRouter',
			    'routes' => array(
				    array(
					    'class' => 'CFileLogRoute',
					    'levels' => 'error, warning',
				    ),
				    #array('class' => 'CWebLogRoute', 'levels' => 'error, warning, profile'), //треш лог
			    ),
		    ),
		    'session' => array(
			    'cookieParams' => array('domain' => '.tranr.local'),
		    ),
		    'user' => array(
			    'identityCookie' => '.tranr.local',
		    ),
		),
		// using Yii::app()->params['paramName']
		'params' => array(
			'adminEmail' => 'slava.volynets@gmail.com',
			'adminName' => 'Поддержка Tranr.ru',
			'mainDomain' => 'tranr.local',
		),
	)
);
