<?php

return array(
	'connectionString' => 'mysql:host=localhost;dbname=tranr',
	'emulatePrepare' => true,
	'username' => 'tranr',
	'password' => 'tranr',
	'charset' => 'utf8',
	'enableProfiling' => true,
	'enableParamLogging' => true,
);
