<?php
return array(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'Tranr console',
	'preload' => array('log'),
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.components.helpers.*',
		'application.extensions.LinkPager',
	),
	'commandMap' => array(
		'migrate' => array(
			'class' => 'system.cli.commands.MigrateCommand',
			'migrationPath' => 'application.migrations',
			'migrationTable' => 'migration',
		),
	),
	'components' => array(
		'sms' => array(
			'class'=>'application.components.Sms',
			'user'=>'slava.volynets@gmail.com',
			'password'=>'q1w2e3r4t5y6',
		),
		'db' => require(dirname(__FILE__).'/db.php'),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),
	'params' => array(
		'adminEmail' => 'slava.volynets@gmail.com',
		'adminName' => 'Поддержка Tranr.ru',
		'mainDomain' => 'tranr.local',
	),
);