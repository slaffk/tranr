set :application, 'tranr'
set :repo_url, 'git@bitbucket.org:vvolynets/tranr.git'

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

set :scm, :git

# set :format, :pretty
# set :log_level, :debug
# set :pty, true

# set :linked_files, %w{config/database.yml}
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# set :keep_releases, 5

namespace :deploy do

  desc 'Migrate'
  task :migrate do
    on roles(:web), in: :groups, wait: 5 do
      execute release_path.join('protected/yiic migrate --interactive=0')
    end
  end

  desc 'Create SymLinks'
  task :createLinks do
    on roles(:web), in: :groups, wait: 0 do
      execute 'chmod', '+x', release_path.join('protected/deploy.sh')
      execute release_path.join('protected/deploy.sh'), deploy_to
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after :finishing, 'deploy:createLinks'
  after :finishing, 'deploy:migrate'
  after :finishing, 'deploy:cleanup'
end
