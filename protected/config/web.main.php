<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'id' => md5(php_uname().'constant'),
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'Tranr',
	'preload' => array('log'),
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.components.helpers.*',
		'application.extensions.LinkPager',
		/** авторизация через социалки */
		'application.extensions.eoauth.*',
		'application.extensions.eoauth.lib.*',
		'application.extensions.lightopenid.*',
		'application.extensions.eauth.*',
		'application.extensions.eauth.services.*',
		'application.extensions.EScriptBoost.*',
	),
	'modules' => array(
		'manager' => array(),
		'cabinet' => array(),
	),
	'components' => array(
		'image'=>array(
			'class' => 'application.extensions.image.CImageComponent',
			'driver' => 'GD',
		),
		'metadata' => array(
			'class' => 'Metadata'
		),
		'city' => array(
			'class' => 'AppCity',
		),
		'user' => array(
			'class' => 'AppUser',
			'loginUrl' => array('site/login'),
			'allowAutoLogin' => true,
			'autoUpdateFlash' => false,
		),
		'db' => require(dirname(__FILE__).'/db.php'),
		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false,
			'rules' => array(
//				'<controller:\w+>/<id:\d+>' => '<controller>/view',
//				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
//				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
				'<_m:(manager|gii|cabinet)>' => '<_m>/default/index',
				'<_m:(manager|gii|cabinet)>/<_c>' => '<_m>/<_c>/index',
				'<_m:(manager|gii|cabinet)>/<_c>/<_a>' => '<_m>/<_c>/<_a>',
				'<_c:(site|ajax)>/<_a>' => '<_c>/<_a>',
				'<_m>' => 'site/default',
				'<_m>/<_c>' => 'site/default',
				'<_m>/<_c>/<_a>' => 'site/default',
				'<_m>/<_c>/<_a>/<id:.+>' => 'site/default',
				'<_m>/<_c>/<_a>/<id:.+>/<p:.+>' => 'site/default',
			),
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'clientScript' => array(
			'class' => 'EClientScriptBoost',
			'cacheDuration' => 30,
		),
		'assetManager' => array(
			'class' => 'EAssetManagerBoost',
			'minifiedExtensionFlags' => array('min.css', 'min.js','minified.js','packed.js')
		),
		/** авторизация через социалки */
		'loid' => array(
			'class' => 'ext.lightopenid.loid',
		),
		'eauth' => array(
			'class' => 'ext.eauth.EAuth',
			'popup' => true,
			'cache' => false,
			'cacheExpire' => 0,
			'services' => array(
//				'google' => array(
//					'class' => 'GoogleOpenIDService',
//				),
				'vkontakte' => array(
					// register your app here: https://vk.com/editapp?act=create&site=1
					'class' => 'VKontakteOAuthService',
					'client_id' => '4502890',
					'client_secret' => 'Ir93kE8vUfqKD8GaL8L6',
				),
				'facebook' => array(
					// register your app here: https://developers.facebook.com/apps/
					'class' => 'FacebookOAuthService',
					'client_id' => '1490915494486722',
					'client_secret' => '7b39ccd8e39d528b4ed24425777d1af6',
				),
				'twitter' => array(
					'class' => 'TwitterOAuthService',
					'key' => 'T3cKBZueuQ0uuhGGmaXjAZLas', // register your app here: https://dev.twitter.com/apps/new
					'secret' => 'PEvP1uXYnHh5JvslawGVIwivDFRtomi0hk4EhwuJrFe0Q01xxq',
				),
				'google_oauth' => array(
					// register your app here: https://code.google.com/apis/console/
					'class' => 'GoogleOAuthService',
					'client_id' => '265735021168-6097njjht1v34u28asgg6vqp2r4f5lgd.apps.googleusercontent.com',
					'client_secret' => 'N4EyKgofO-XMkz4GiI-DHD5M',
					'title' => 'Google (OAuth)',
				),
				'mailru' => array(
					// register your app here: http://api.mail.ru/sites/my/add
					'class' => 'MailruOAuthService',
					'client_id' => '723727',
					'client_secret' => '6db8d636996692fdb8d73dc8f7c2564e',
				),
				'odnoklassniki' => array(
					// register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
					// ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
					'class' => 'OdnoklassnikiOAuthService',
					'client_id' => '1098223872',
					'client_public' => 'CBACJGHCEBABABABA',
					'client_secret' => '9235587E51CA25F983F48DD8',
					'title' => 'Одноклассники',
				),
			),
		),
	),
);