<?php
/**
 * Class SiteController
 */
class SiteController extends Controller {

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(

			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
				'testLimit' => '1'
			),
//			'page'=>array(
//				'class' => 'CViewAction',
//			),
		);
	}

	
	public function actionSms() {
		if(!Yii::app()->request->isAjaxRequest) {
			throw new CHttpException(404, "Страница не найдена.");
		}
		
		$model = new SmsModel();
		// Выдает просто форму
		if(Yii::app()->request->getPost('trainer_id') != null) {
			$model->trainer_id = Yii::app()->request->getPost('trainer_id');
			$content = $this->renderPartial('_form_sms', array(
				'model'=>$model,
			), true);
			echo CJSON::encode(array('success'=>true, 'content'=>$content));
			Yii::app()->end();
		}
		// Валидация формы и отправка sms
		else if(Yii::app()->request->getPost('data') != null) {
			$data = array();
			parse_str(Yii::app()->request->getPost('data'), $data);
			$model->attributes = $data['SmsModel'];
			
			$trainer = Trainer::model()->findByPk($model->trainer_id);
			if($trainer === null) {
				throw new CHttpException(404, "Страница не найдена.");
			}
			$model->phone = $trainer->phone;
			
			if(!$model->validate()) {
				$content = $this->renderPartial('_form_sms', array(
					'model'=>$model,
				), true);
				echo CJSON::encode(array('success'=>false, 'content'=>$content));
				Yii::app()->end();
			} else {
				$id = Yii::app()->sms->send($model->phone, $model->text);
				if($id !== false) {
					$model->sms_id = $id;
					$model->save();
					echo CJSON::encode(array('success'=>true, 'message'=>"Ваше сообщение отправлено тренеру. В ближайшее время он с вами свяжется."));
					Yii::app()->end();
				} else {
					echo CJSON::encode(array('success'=>false, 'message'=>implode("<br />", Yii::app()->sms->getErrors())));
					Yii::app()->end();
				}
			}
		}
	}
	
	/**
	 * index
	 */
	public function actionIndex() {

		$this->page = Page::model()->find('path = "-"'); #главная
		if ($this->page) {

			header('Content-Type: text/html', true, 200);
			$this->layout = $this->page->layout;
			$this->setPageTitle($this->page->title);

			Yii::app()->clientScript->registerMetaProperty('website', 'og:type');

			if ($this->page->meta_keywords) {
				Yii::app()->clientScript->registerMetaTag($this->page->meta_keywords, 'keywords');
			}
			if ($this->page->meta_description) {
				Yii::app()->clientScript->registerMetaProperty($this->page->meta_description, 'og:description');
				Yii::app()->clientScript->registerMetaTag($this->page->meta_description, 'description');
			}

			$this->render('dummy', array());
			Yii::app()->end();
		} else {
			$this->render('index');
		}
	}

	/**
	 * @param $path
	 * @param $slash
	 * @return CDbCriteria
	 */
	private function makePageCriteria($path, $slash) {

		$criteria = new CDbCriteria();
		$criteria->compare('page.path', $path);
		$criteria->addInCondition(
			'slash_id',
			$slash ? array(Page::SLASH_ALLOWED, Page::SLASH_REQUIRED) : array(Page::SLASH_NOT_ALLOWED, Page::SLASH_ALLOWED)
		);
		return $criteria;
	}

	/**
	 * @throws CHttpException
	 */
	public function actionDefault() {
		
		$request = Yii::app()->request->getRequestUri();
		$path = Yii::app()->request->getPathInfo();
		$slash = @$request[strlen($path) + 1] == '/';

		$criteria = $this->makePageCriteria($path, $slash);

		$this->page = Page::model()->find($criteria);
		if (!$this->page) {
			foreach (Page::$regexp as $key => $value) {
				if (preg_match($key, $path, $this->vars)) {
					$path = implode('/', array_filter(explode('/', $value)));
					break;
				}
			}
			$criteria = $this->makePageCriteria($path, $slash);
			$this->page = Page::model()->find($criteria);
		}

		if ($this->page) {

			header('Content-Type: text/html', true, 200);
			$this->layout = $this->page->layout;
			$this->setPageTitle($this->page->title);

			Yii::app()->clientScript->registerMetaProperty('object', 'og:type');

			if ($this->page->meta_keywords) {
				Yii::app()->clientScript->registerMetaTag($this->page->meta_keywords, 'keywords');
			}
			if ($this->page->meta_description) {
				Yii::app()->clientScript->registerMetaProperty($this->page->meta_description, 'og:description');
				Yii::app()->clientScript->registerMetaTag($this->page->meta_description, 'description');
			}

			$this->render('dummy', array());
			Yii::app()->end();
		} else {
			$file = File::model()->thisCity()->findByAttributes(array('filename' => $path));
			if ($file) {
				if ($file->attach) {
					Yii::app()->request->sendFile($file->filename, $file->data, $file->file_type);
				} else {
					header('Pragma: public');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Content-Transfer-Encoding: binary');
					header('Content-length: '.$file->file_size);
					header('Content-Type: '.$file->file_type);
					echo $file->data;
				}
				Yii::app()->end();
			}
			throw new CHttpException(404, 'Страница не найдена');
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {

		$error = Yii::app()->errorHandler->error;

		if ($error) {
			if ($error['code'] != 404) {
				Mailer::mail(MailTemplate::EVENT_ERROR, Yii::app()->params['errorEmail'], $error);
			}
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->page = new Page();
				$this->page->widgets = array(
					WidgetHelper::createWidget('TrainerWidget', Widget::ZONE_SIDEBAR_1, array('action' => 'category'))
				);
				$this->render($error['code'] == 404 ? '404' : 'error', $error);
			}
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact() {

		$model = new ContactForm;

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('ContactForm');
			if($model->validate()) {
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * login page
	 */
	public function actionLogin() {

		$serviceName = Yii::app()->request->getQuery('service');
		if (isset($serviceName)) {
			/** @var $eauth EAuthServiceBase */
			$eauth = Yii::app()->eauth->getIdentity($serviceName);
			$eauth->redirectUrl = Yii::app()->user->returnUrl;
			$eauth->cancelUrl = $this->createAbsoluteUrl('site/login');

			try {
				if ($eauth->authenticate()) {
					//var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes());
					$identity = new EAuthUserIdentity($eauth);

					// successful authentication
					if ($identity->authenticate()) {
						Yii::app()->user->login($identity);
						//var_dump($identity->id, $identity->name, Yii::app()->user->id);exit;

						// special redirect with closing popup window
						$eauth->redirect();
					}
					else {
						// close popup window and redirect to cancelUrl
						$eauth->cancel();
					}
				}

				// Something went wrong, redirect to login page
				$this->redirect(array('site/login'));
			}
			catch (EAuthException $e) {
				// save authentication error to session
				Yii::app()->user->setFlash('error', 'EAuthException: '.$e->getMessage());

				// close popup window and redirect to cancelUrl
				$eauth->redirect($eauth->getCancelUrl());
			}
		}

		$model = new LoginForm;
		if (Yii::app()->request->getPost('ajax') === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('LoginForm');
			if($model->validate() && $model->login()) {
				$this->redirect(array('/cabinet/trainer/index'));
			}
		}
		$this->render('login', array('model' => $model));
	}

	/**
	 * Logs out
	 */
	public function actionLogout() {

		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	/**
	 * tag autocompleter
	 */
	public function actionTagAutoComplete() {

		$response['tags'] = array();
		$criteria = new CDbCriteria;
		foreach (Tag::model()->findAll($criteria) as $tag) {
			$response['tags'][] = array('tag' => $tag->name);
		}
		echo json_encode($response);
	}

	/**
	 * @throws CHttpException
	 */
	public function actionUpload() {

		Yii::app()->user->requireAuthorized();

		if ($_FILES['upload']) {
			$full_path = '';
			if ($_FILES['upload'] == 'none' || empty($_FILES['upload']['name'])) {

				$message = 'Вы не выбрали файл';
			} else if ($_FILES['upload']['size'] == 0 || $_FILES['upload']['size'] > 2050000) {

				$message = 'Размер файла не соответствует';
			} else if (!in_array($_FILES['upload']["type"], array('image/jpeg', 'image/gif', 'image/png'))) {

				$message = 'Допускается загрузка только картинок JPG, GIF и PNG.';
			} else if (!is_uploaded_file($_FILES['upload']['tmp_name'])) {

				$message = 'Файл не загрузился. Попробуйте еще раз.';
			} else {

				Yii::import('application.extensions.ETranslitFilter');
				$filename = rand(1, 1000) . '-' . ETranslitFilter::cyrillicToLatin($_FILES['upload']['name'], false, true); # мамочка.jpg = mamochka.jpg
				move_uploaded_file($_FILES['upload']['tmp_name'], 'uploads/'.$filename);
				$full_path = Yii::app()->request->getHostInfo() . '/uploads/' . $filename;
				$message = 'Файл ' . $_FILES['upload']['name'] . ' загружен';
			}
			$callback = $_REQUEST['CKEditorFuncNum'];
			echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'.$callback.'", "'.$full_path.'", "'.$message.'" );</script>';
		}
		Yii::app()->end();
	}

	public function actionActivate() {

		$code = Yii::app()->request->getQuery('code');
		if ($code && $user = User::model()->findByAttributes(array('activation_code' => $code))) {
			/** @var $user User */
			$user->activated = 1;
			$user->save();

			if (Yii::app()->user->isGuest) {
				$identity = new UserIdentity($user->email, null, false);
				if ($identity->authenticate()){
					Yii::app()->user->login($identity);
				}
			}

			$this->render('activate', array('model' => $user));
		} else {
			throw new CHttpException(403, 'Некорректный код');
		}
	}
}