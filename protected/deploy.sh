#!/usr/bin/env bash
echo $1

if [ ! -f $1/db.php ]
then
  cp $1/current/protected/config/db.php $1/db.php
fi

if [ ! -f $1/web.php ]
then
  cp $1/current/protected/config/web.php $1/web.php
fi

if [ ! -f $1/console.php ]
then
  cp $1/current/protected/config/console.php $1/console.php
fi

if [ ! -d $1/uploads/ ]
then
  mkdir $1/uploads/
  chmod 777 $1/uploads/
fi

if [ ! -d $1/runtime/ ]
then
  mkdir $1/runtime/
  chmod 777 $1/runtime/
fi

rm $1/current/protected/config/web.php
rm $1/current/protected/config/db.php
rm $1/current/protected/config/console.php
rm -rf $1/current/uploads/
ln -s $1/db.php $1/current/protected/config/db.php
ln -s $1/web.php $1/current/protected/config/web.php
ln -s $1/console.php $1/current/protected/config/console.php
ln -s $1/uploads $1/current/uploads

rm -rf $1/current/protected/runtime
ln -s $1/runtime $1/current/protected/runtime

chmod 777 $1/current/assets
