<?php
/**
 * Class PhotoController
 */
class PhotoController extends TrainerGroundController {

	/**
	 * @return void
	 */
	public function actionIndex() {

		$model = new TrainerPhoto('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getPost('TrainerPhoto');
		$model->trainer_id = Yii::app()->user->getModel()->trainer->id;

		$this->render('index', ['model' => $model]);
	}

	/**
	 * @return void
	 */
	public function actionCreate() {

		$model = new TrainerPhoto();
		$model->trainer_id = Yii::app()->user->getModel()->trainer->id;

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('TrainerPhoto');
			if ($model->save()) {
				$this->redirect(array('/cabinet/photo/index'));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 * @throws CHttpException
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if(Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('TrainerPhoto');
			if ($model->save()) {
				$this->redirect(array('/cabinet/photo/index'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * @param $id
	 * @return TrainerPhoto
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = TrainerPhoto::model()->inArray(['trainer_id' => Yii::app()->user->getModel()->trainer->id])->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}

