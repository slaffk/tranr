<?php
/**
 * Class VideoController
 */
class VideoController extends TrainerGroundController {


	/**
	 * @return void
	 */
	public function actionIndex() {

		$model = new TrainerVideo('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getPost('TrainerVideo');
		$model->trainer_id = Yii::app()->user->getModel()->trainer->id;

		$this->render('index', ['model' => $model]);
	}

	/**
	 * @return void
	 */
	public function actionCreate() {

		$model = new TrainerVideo();
		$model->trainer_id = Yii::app()->user->getModel()->trainer->id;

		if(Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('TrainerVideo');
			if($model->save()) {
				$this->redirect(array('/cabinet/video/index'));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 * @throws CHttpException
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);
		
		if(Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('TrainerVideo');
			if($model->save()) {
				$this->redirect(array('/cabinet/video/index'));
			}
		}
		
		$this->render('update', array(
			'model'=>$model,
		));
	}

	/**
	 * @param $id
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * @param $id
	 * @return TrainerVideo
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = TrainerVideo::model()->inArray(['trainer_id' => Yii::app()->user->getModel()->trainer->id])->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}

?>
