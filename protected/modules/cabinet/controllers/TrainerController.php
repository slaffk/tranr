<?php
/**
 * Class TrainerController
 */
class TrainerController extends TrainerGroundController {

	/**
	 * @return void
	 */
	public function actionIndex() {

		$this->render('index', ['model' => $this->loadModel()]);
	}

	/**
	 * @throws CHttpException
	 */
	public function actionUpdate() {

		$model = $this->loadModel();

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('Trainer');;
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}
		$model->dob = Yii::app()->dateFormatter->format('yyyy-MM-dd', $model->dob);
		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @return Trainer
	 * @throws CHttpException
	 */
	public function loadModel() {

		$model = Trainer::model()->findByPk(Yii::app()->user->getModel()->trainer->id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}