<?php
/**
 * Class CabinetModule
 */
class CabinetModule extends CWebModule {

	public function init() {

		$this->setImport(array(
			'cabinet.models.*',
			'cabinet.components.*',
		));
	}

	public function beforeControllerAction($controller, $action) {

		return parent::beforeControllerAction($controller, $action);
	}
}
