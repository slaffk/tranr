<?php
/**
 * @var TrainerPhoto $model
 */
?>
<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'video-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
			'class' => 'form-horizontal'
		),
	))?>

	<?=$form->errorSummary($model)?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model, 'title', array('placeholder'=>'Название фото')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<?php if ($model->getIsNewRecord()):?>
		<div class="row">
			<?php echo $form->labelEx($model,'photo'); ?>
			<?php echo $form->fileField($model, 'photo', array('style' => 'width: 230px'));?>
			<?php echo $form->error($model,'photo'); ?>
		</div>
	<?php endif?>

	<?php if ($model->photo):?>
		<div class="row showImage">
			<label></label>
			<img src="<?=$model->getThumb(220, 220, 'crop')?>" width="220" style="float: left; margin: 5px 5px 5px 0" /> <br/>
		</div>
	<?php endif?>

	<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-success')); ?>

	<?php $this->endWidget(); ?>
</div>