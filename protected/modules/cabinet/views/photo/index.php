<?php
/**
 * @var TrainerPhoto $model
 * @var PhotoController $this
 */
?>

<br/>

<ol class="breadcrumb">
	<li><a href="/">Главная</a></li>
	<li><a href="/cabinet/trainer">Кабинет тренера</a></li>
	<li class="active">Мои фото</li>
</ol>

<div class="col-sm-12">
	<?php $this->widget('zii.widgets.CListView', array(
		'id' => 'resume-list',
		'dataProvider' => $model->search(),
		'itemView' => '_view',
		'template' => "{sorter}\n{items}&nbsp;{pager}",
		'afterAjaxUpdate' => 'js:function(id, data) {window.scrollTo(0,0)}',
		'emptyText' => '',
		'htmlOptions' => array('class' => 'gorjob-list-view'),
	))?>
	<div class="col-sm-2 text-center">
		<a href="/cabinet/photo/create" class="btn btn-success">Добавить фото</a>
		<br/>
		<br/>
	</div>
</div>
<div class="clearfix"></div>

<div class="modal" id="picture-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				fuck
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.click-image').on('click', function(e) {
		e.preventDefault();
		$('#picture-dialog .modal-body').html('<img src="' + $(this).attr('href') + '"/>');
		$('#picture-dialog').modal();
	});
</script>
