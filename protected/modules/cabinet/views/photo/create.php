<br/>

<ol class="breadcrumb">
	<li><a href="/">Главная</a></li>
	<li><a href="/cabinet/trainer">Кабинет тренера</a></li>
	<li><a href="/cabinet/photo/index">Мои фото</a></li>
	<li class="active">Добавить фото</li>
</ol>

<?php $this->renderPartial('_form', array('model' => $model)); ?>