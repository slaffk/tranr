<?php
/**
 * @var $data TrainerPhoto
 */
?>

<div class="col-sm-3 text-center image-item">
	<?=CHtml::link('<i class="glyphicon glyphicon-trash"></i>', array('/cabinet/photo/delete', 'id' => $data->id), ['class' => 'delete-img', 'title' => 'Удалить фото', 'onclick' => 'return confirm("Вы уверены, что хотите удалить это фото?")']); ?>
	<?=CHtml::link('<i class="glyphicon glyphicon-cog"></i>', array('/cabinet/photo/update', 'id' => $data->id), ['class' => 'modify-img', 'title' => 'Редактировать фото']); ?>
	<a href="<?=$data->getThumb(568, 415, 'crop')?>" class="click-image">
		<?=CHtml::image($data->getThumb(220, 165, 'crop'), CHtml::encode($data->title), array('width' => 220, 'height' => 165, 'class' => 'img-thumbnail')); ?>
	</a>
	<p class="small"><?=$data->title;?></p>
</div>
