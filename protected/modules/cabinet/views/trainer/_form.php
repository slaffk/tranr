<?php
/**
 * @var $this TrainerController
 * @var $model Trainer
 * @var $form CActiveForm
 */
?>

<br/>

<ol class="breadcrumb">
	<li><a href="/">Главная</a></li>
	<li><a href="/cabinet/trainer">Кабинет тренера</a></li>
	<li><a href="/cabinet/trainer/index">Личная информация</a></li>
	<li class="active">Изменить</li>
</ol>

<div class="col-sm-3">
	<ul class="nav nav-pills nav-stacked thumbnail" role="tablist">
		<li role="presentation" class="active">
			<a href="#home" role="tab" data-toggle="tab">
				<i class="glyphicon glyphicon-paperclip"></i>
				Основная информация
			</a>
		</li>
		<li role="presentation">
			<a href="#education" role="tab" data-toggle="tab">
				<i class="glyphicon glyphicon-briefcase"></i>
				Образование и опыт
			</a>
		</li>
		<li role="presentation">
			<a href="#rubrics" role="tab" data-toggle="tab">
				<i class="glyphicon glyphicon-check"></i>
				Размещение в рубриках
			</a>
		</li>
		<li role="presentation">
			<a href="#contacts" role="tab" data-toggle="tab">
				<i class="glyphicon glyphicon-envelope"></i>
				Контактные данные
			</a>
		</li>
	</ul>
</div>
<div class="col-sm-9 form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id' => 'trainer-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
			'class' => 'form-horizontal'
		),
	)); ?>
	<?=$form->errorSummary($model)?>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home">
			<div class="row">
				<?php echo $form->labelEx($model,'trainer_type'); ?>
				<?php echo $form->dropDownList($model, 'trainer_type', Trainer::$trainerTypes); ?>
				<?php echo $form->error($model,'trainer_type'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'name'); ?>
				<?php echo $form->textField($model, 'name', array('maxlength' => 512, 'placeholder' => 'Поле для секций')); ?>
				<?php echo $form->error($model, 'name'); ?>
			</div>

			<div class="row radio">
				<?php echo $form->labelEx($model,'gender'); ?>
				<?php echo $form->radioButtonList($model, 'gender', User::$genders, array('separator' => '&nbsp;&nbsp;&nbsp;', 'class' => 'radio-group'));?>
				<?php echo $form->error($model,'gender'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'lastname'); ?>
				<?php echo $form->textField($model, 'lastname', array('maxlength' => 128, 'placeholder' => 'напр., Сидоров')); ?>
				<?php echo $form->error($model, 'lastname'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'firstname'); ?>
				<?php echo $form->textField($model, 'firstname', array('maxlength' => 128, 'placeholder' => 'напр., Иван')); ?>
				<?php echo $form->error($model, 'firstname'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'middlename'); ?>
				<?php echo $form->textField($model, 'middlename', array('maxlength' => 128, 'placeholder' => 'напр., Петрович')); ?>
				<?php echo $form->error($model, 'middlename'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model, 'dob'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'model' => $model,
					'attribute' => 'dob',
					'language' => 'ru',
					'value' => $model->dob,
					'options' => array(
						'showAnim' => 'show',
						'showOn' => 'focus',
						'dateFormat' => 'yy-mm-dd'
					),
					'htmlOptions'=>array(
						'style' => 'width: 300px;',
					),
				));?>
				<style>.ui-datepicker-trigger {display: none}</style>
				<?php echo $form->error($model, 'dob'); ?>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="contacts">
			<div class="row">
				<?php echo $form->labelEx($model,'city_id'); ?>
				<?php echo $form->dropDownList($model, 'city_id', CHtml::listData(City::model()->root()->findAll(), 'id', 'name')); ?>
				<?php echo $form->error($model,'city_id'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'area_id'); ?>
				<?php echo $form->dropDownList($model, 'area_id', array(null => '') + CHtml::listData(Area::model()->findAll(), 'id', 'name')); ?>
				<?php echo $form->error($model,'area_id'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'metro_id'); ?>
				<?php echo $form->dropDownList($model, 'metro_id', array(null => '') + CHtml::listData(Metro::model()->findAll(), 'id', 'name')); ?>
				<?php echo $form->error($model,'metro_id'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'address'); ?>
				<?php echo $form->textField($model, 'address', array('maxlength' => 128)); ?>
				<?php echo $form->error($model, 'address'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'phone'); ?>
				<?php echo $form->textField($model, 'phone', array('maxlength' => 128)); ?>
				<?php echo $form->error($model, 'phone'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'phone_additional'); ?>
				<?php echo $form->textField($model, 'phone_additional', array('maxlength' => 128)); ?>
				<?php echo $form->error($model, 'phone_additional'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'email'); ?>
				<?php echo $form->textField($model, 'email', array('maxlength' => 128)); ?>
				<?php echo $form->error($model, 'email'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model, 'vk_link'); ?>
				<?php echo $form->textField($model, 'vk_link', array('maxlength' => 128)); ?>
				<?php echo $form->error($model, 'vk_link'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model, 'fb_link'); ?>
				<?php echo $form->textField($model, 'fb_link', array('maxlength' => 128)); ?>
				<?php echo $form->error($model, 'fb_link'); ?>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="rubrics">
			<div class="row">
				<?php echo $form->labelEx($model,'category_id'); ?>
				<?php echo $form->dropDownList($model, 'category_id', CHtml::listData(Category::model()->findAll(), 'id', 'dottedName')); ?>
				<?php echo $form->error($model,'category_id'); ?>
			</div>

			<div class="row radio">
				<?php echo $form->labelEx($model,'categoryIds'); ?>
				<?php echo $form->checkBoxList($model, 'categoryIds', CHtml::listData(Category::model()->notRoot()->findAll(), 'id', 'name'));?>
				<?php echo $form->error($model,'categoryIds'); ?>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="education">
			<div class="row">
				<?php echo $form->labelEx($model,'edu_level'); ?>
				<?php echo $form->dropDownList($model, 'edu_level', array(null => '') + Trainer::$eduLevels); ?>
				<?php echo $form->error($model,'edu_level'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'edu_college'); ?>
				<?php echo $form->textField($model, 'edu_college', array('maxlength' => 128)); ?>
				<?php echo $form->error($model, 'edu_college'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'edu_year'); ?>
				<?php echo $form->textField($model, 'edu_year', array('maxlength' => 4)); ?>
				<?php echo $form->error($model, 'edu_year'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'edu_other'); ?>
				<?php echo $form->textArea($model, 'edu_other'); ?>
				<?php echo $form->error($model, 'edu_other'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'experience'); ?>
				<?php echo $form->dropDownList($model, 'experience', array(null => '') + Trainer::$experiences); ?>
				<?php echo $form->error($model,'experience'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'experience_text'); ?>
				<?php echo $form->textArea($model, 'experience_text'); ?>
				<?php echo $form->error($model, 'experience_text'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'additional'); ?>
				<?php echo $form->textArea($model, 'additional'); ?>
				<?php echo $form->error($model, 'additional'); ?>
			</div>

		</div>
	</div>

	<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-success')); ?>
	<?php $this->endWidget(); ?>
</div>
