<?php
/**
 * @var Trainer $model
 * @var TrainerController $this
 */
?>

<br/>

<ol class="breadcrumb">
	<li><a href="/">Главная</a></li>
	<li><a href="/cabinet/trainer">Кабинет тренера</a></li>
	<li class="active">Личная информация</li>
</ol>

<div class="col-sm-3 text-center">
	<?=CHtml::image($model->getThumb(220, 220, 'crop'), $model->image_alt, array('class' => 'img-circle', 'width' => 220, 'height' => 220))?>
	<div class="clear"></div>

	<a id="changeAvatarBtn" href="#" title="Сменить аватар">
		<i class="glyphicon glyphicon-upload"></i>
		Сменить аватар
	</a>

	<?php $form = $this->beginWidget('CActiveForm', array (
		'action' => '/cabinet/trainer/update',
		'htmlOptions' => array(
			'id' => 'chAvFrm',
			'style' => 'display: none',
			'enctype' => 'multipart/form-data',
		)
	))?>
		<?=$form->fileField($model, 'image')?>
	<?php $this->endWidget(); ?>

	<script type="text/javascript">
		$('#changeAvatarBtn').click(function(e) {
			e.preventDefault();
			$('#Trainer_image').click();
			if (confirm('Загрузить выбранный логотип?')) {
				$('#chAvFrm').submit();
			};
		})
	</script>
</div>
<div class="col-sm-9">
	<?=CHtml::link('<i class="glyphicon glyphicon-pencil"></i>', array('/cabinet/trainer/update'), ['class' => 'btn btn-sm btn-info pull-right', 'style' => 'margin-top: 15px']) ?>

	<?php if (!$model->active):?>
		<p class="text-danger">Ваш аккаунт отключен модератором!</p>
	<?php endif?>

	<?php if ($model->name || $model->firstname || $model->lastname):?>
		<h1><?=$model->name?></h1>
	<?php endif?>

	<address>
		<strong><?=$model->firstname?> <?=$model->lastname?></strong><br>
		<?=Trainer::$trainerTypes[$model->trainer_type]?><br>
	</address>

	<address>
		<?=CHtml::link(
			'<strong>' . ($model->category->parent ? $model->category->parent->name . ' / ' : '') . $model->category->name . '</strong>',
			$model->category->url,
			array('class' => 'btn btn-xs btn-success disabled')
		)?>
		<?php if (!empty($model->categories)):?>
			<?php foreach ($model->categories as $category):?>
				<?=CHtml::link($category->name, $category->url, array('class' => 'btn btn-xs btn-default disabled'))?>
			<?php endforeach?>
		<?php endif?>
	</address>


	<address>
		<i class="glyphicon glyphicon-map-marker"></i>

		<?php if ($model->address):?>
			<?=$model->address?>,
		<?php endif?>

		<?php if ($model->metro):?>
			метро &laquo;<?=$model->metro->name?>&raquo;,
		<?php endif?>

		<?php if ($model->area):?>
			<?=$model->area->name?> район,
		<?php endif?>

		<?=$model->city->name?>, Российская Федерация<br>

		<i class="glyphicon glyphicon-phone"></i>
		<?=CommonHelper::formatPhone($model->phone)?><?=$model->phone_additional ? ', ' . CommonHelper::formatPhone($model->phone_additional) : ''?><br/>

		<i class="glyphicon glyphicon-envelope"></i>
		<?=CHtml::link($model->email, 'mailto:' . $model->email)?>
	</address>

	<address>
		<?php if ($model->fb_link):?>
			<?=CHtml::image(Yii::app()->image->makeThumb('images/fb.png', 16, 16), 'ссылка на профиль facebook', ['class' => 'img-rounded'])?>
			<a href="<?=$model->fb_link?>">
				<?=$model->firstname?> <?=$model->lastname?><br/>
			</a>
		<?php endif?>
		<?php if ($model->vk_link):?>
			<?=CHtml::image(Yii::app()->image->makeThumb('images/vk.png', 16, 16), 'ссылка на профиль vk', ['class' => 'img-rounded'])?>
			<a href="<?=$model->vk_link?>">
				<?=$model->firstname?> <?=$model->lastname?><br/>
			</a>
		<?php endif?>
	</address>
</div>

