<?php
$this->breadcrumbs = array(
	'Главная' => '/',
	'Кабинет' => array('index'),
	'Редактировать',
);
$this->menu = array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<?php $this->renderPartial('_form', array('model' => $model)); ?>