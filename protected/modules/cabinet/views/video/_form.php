<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id' => 'video-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
			'class' => 'form-horizontal'
		),
	)); ?>

	<?=$form->errorSummary($model)?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model, 'title', array('placeholder' => 'Название видео')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'link'); ?>
		<?php echo $form->textField($model, 'link', array('placeholder' => 'http://www.youtube.com/watch?v=*')); ?>
		<?php echo $form->error($model,'link'); ?>
	</div>

	<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-success')); ?>

	<?php $this->endWidget(); ?>
</div>