<br/>

<ol class="breadcrumb">
	<li><a href="/">Главная</a></li>
	<li><a href="/cabinet/trainer">Кабинет тренера</a></li>
	<li><a href="/cabinet/video/index">Мои видео</a></li>
	<li class="active">Добавить видео</li>
</ol>

<?php $this->renderPartial('_form', array('model' => $model)); ?>