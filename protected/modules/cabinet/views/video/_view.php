<?php
/**
 * @var $data TrainerVideo
 */
?>

<div class="col-sm-3 text-center image-item">
	<?=CHtml::link('<i class="glyphicon glyphicon-trash"></i>', array('/cabinet/video/delete', 'id' => $data->id), ['class' => 'delete-img', 'title' => 'Удалить видео', 'onclick' => 'return confirm("Вы уверены, что хотите удалить это видео?")']); ?>
	<?=CHtml::link('<i class="glyphicon glyphicon-cog"></i>', array('/cabinet/video/update', 'id' => $data->id), ['class' => 'modify-img', 'title' => 'Редактировать видео']); ?>
	<a href="<?=$data->getVideoId()?>" class="click-video">
		<?=CHtml::image($data->getVideoScreenshot(TrainerVideo::SCREENSHOT_SIZE_BIG), CHtml::encode($data->title), array('width' => 220, 'height' => 220, 'class' => 'img-thumbnail')); ?>
	</a>
	<p class="small"><?=$data->title;?></p>
</div>
