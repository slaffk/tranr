<?php
/**
 * @var $this TrainerController
 * @var $model Trainer
 * @var $form CActiveForm
 */
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'trainer-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'form-horizontal'
	),
)); ?>

<p class="note">Поля со звездочкой <span class="required">*</span> обязательны.</p>

<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'trainer_id'); ?>
		<?php echo $form->dropDownList($model, 'trainer_id', CHtml::listData(Trainer::model()->findAll(), 'id', function($model) {
			return $model->getFullName();
		})); ?>
		<?php echo $form->error($model, 'trainer_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'type'); ?>
		<?php echo $form->dropDownList($model, 'type', TrainerReview::getTypeList()); ?>
		<?php echo $form->error($model, 'type'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'name'); ?>
		<?php echo $form->textField($model, 'name'); ?>
		<?php echo $form->error($model, 'name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'email'); ?>
		<?php echo $form->textField($model, 'email'); ?>
		<?php echo $form->error($model, 'email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'text'); ?>
		<?php echo $form->textArea($model, 'text'); ?>
		<?php echo $form->error($model, 'text'); ?>
	</div>

<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-success')); ?>

<?php $this->endWidget(); ?>

</div>