<?php
/**
 * @var $this TrainerController
 * @var $model Trainer
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Отзывы' => array('index'),
	'Редактировать',
);
$this->menu = array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Редактировать отзыв</h1>
<?php $this->renderPartial('_form', array('model' => $model)); ?>