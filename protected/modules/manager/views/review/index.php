<?php
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Отзывы',
);
?>

<br/>
<?=CHtml::link('<i class="glyphicon glyphicon-plus"></i> Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-success'))?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trainer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns'=>array(
		array(
			'name' => 'name',
			'htmlOptions' => array(),
			'filter' => CHtml::activeTextField($model, 'name', array('placeholder' => 'Поиск по имени')),
			'type' => 'raw',
			'value' => '$data->name',
		),
		array(
			'name' => 'email',
			'htmlOptions' => array(),
			'filter' => CHtml::activeTextField($model, 'name', array('placeholder' => 'Поиск по email')),
			'type' => 'raw',
			'value' => '$data->email',
		),
		array(
			'name' => 'ip',
			'htmlOptions' => array(),
			'filter' => CHtml::activeTextField($model, 'ip', array('placeholder' => 'Поиск по ip')),
			'type' => 'raw',
			'value' => '$data->ip',
		),
		array(
			'name' => 'type',
			'htmlOptions' => array('style' => 'width: 100px;'),
			'filter' => TrainerReview::getTypeList(),
			'value' => 'TrainerReview::getTypeList()[$data->type]',
		),
		array(
			'name' => 'date',
			'htmlOptions' => array(),
			'filter' => false,
			'type' => 'raw',
			'value' => '$data->date',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
		),
	),
));
