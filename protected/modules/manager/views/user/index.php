<?php
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Пользователи',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'user-grid',
	'dataProvider' => $model->search($options),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		array(
			'name' => 'active',
			'header' => 'Вкл/выкл',
			'htmlOptions' => array('style' => 'width: 60px; text-align: center'),
			'filter' => CHtml::activeDropDownList($model, 'active', array(null => '', 0 => 'Выкл', 1 => 'Вкл')),
			'type' => 'raw',
			'value' => '$data->active ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'begin_date',
			'header' => 'Дата регистрации',
			'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy", $data->begin_date);',
			'htmlOptions' => array('style' => 'width: 100px; text-align: center'),
		),
		array(
			'name' => 'lastname',
			'type' => 'raw',
			'header' => 'Пользователь',
			'value' => '$data->firstname . " " . $data->lastname . "<br/>" . CHtml::link($data->email, $data->email)',
		),
		array(
			'name' => 'last_login',
			'header' => 'Последний вход',
			'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy hh:mm", $data->last_login);',
			'htmlOptions' => array('style' => 'width: 100px; text-align: center'),
		),
		array(
			'name' => 'roles.name',
			'type' => 'raw',
			'header' => 'Роли',
			'value' => 'implode(", ", CHtml::listData($data->roles, "id", "name"))',
			'filter' => CHtml::activeDropDownList($model, 'id', array(''=>'') + CHtml::listData(Role::model()->findAll(), 'id', 'name')),
			'htmlOptions' => array('style' => 'width: 160px; text-align: center'),
		),
		array(
			'class' => 'CButtonColumn',
			'template'=>'{update} {delete}',
//			'cssClassExpression' => function($row, $data, $component) {
//
//				$component->template = (strtotime($data->end_date) < time()+1) ? '{view}' : '{view} {update} {delete}';
//			},
		),
	),
));
?>
<?=CHtml::link('Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-default'))?>
