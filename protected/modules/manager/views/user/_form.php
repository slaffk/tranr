<?php
/**
 * @var CActiveForm $form
 * @var Controller $controller
 * @var User $model
 */
?>
<?=$form->errorSummary($model)?>
<div class="form">
	<h1>Профиль пользователя</h1>
	<div class="row">
		<?php echo $form->labelEx($model,'dummy'); ?>
		<?=$form->checkBox($model, 'active')?>
		<?php echo $form->label($model,'active', array('class' => 'right')); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'lastname'); ?>
		<?php echo $form->textField($model, 'lastname');?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'firstname'); ?>
		<?php echo $form->textField($model, 'firstname');?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'middlename'); ?>
		<?php echo $form->textField($model, 'middlename');?>
		<?php echo $form->error($model,'middlename'); ?>
	</div>
	<div class="row radio">
		<?php echo $form->labelEx($model,'gender'); ?>
		<?php echo $form->radioButtonList($model, 'gender', User::$genders, array('separator' => '&nbsp;&nbsp;&nbsp;', 'class' => 'radio-group'));?>
		<?php echo $form->error($model,'gender'); ?>
	</div>
	<?php /* <div class="row">
		<?php echo $form->labelEx($model,'birth_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'name' => 'User[birth_date]',
			'language' => 'ru',
			'value' => $model->birth_date,
			'options' => array(
				'showAnim' => 'show',
				'showOn' => 'focus',
			),
			'htmlOptions' => array(
				'class' => 'inp_340',
			),
		));?>
		<style>.ui-datepicker-trigger {display: none}</style>
		<?php echo $form->error($model, 'birth_date'); ?>
	</div>
 	*/ ?>
	<div class="row">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo $form->fileField($model, 'image', array('style' => 'width: 230px'));?>
		<?php echo $form->error($model,'image'); ?>
	</div>
	<?php echo $form->hiddenField($model, 'deleteImage'); ?>
	<?php if ($model->image):?>
		<div class="row showImage">
			<label></label>
			<img src="<?php echo $model->getThumb(100, 100, 'crop');?>" style="float: left; margin: 5px 5px 5px 0" /> <br/>
			<a class="trash trash-btn" href="#">Удалить</a><br/>
		</div>
		<script>
			$('.trash-btn').click(function(e) {
				e.preventDefault();
				if (confirm('Вы уверены, что хотите удалить аватар?')) {
					$('#User_deleteImage').val('1');
					$('.showImage').remove();
				}
			});
		</script>
	<?php endif?>

	<div class="row">
		<h3>Контактная информация</h3>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'email', array('label' => 'E-mail / Логин')); ?>
		<?php echo $form->textField($model, 'email');?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model, 'phone');?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<h3>Служебное</h3>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size' => 60,'maxlength' => 256)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	<div class="row radio">
		<?php echo $form->labelEx($model,'roleIds'); ?>
		<?php echo $form->checkBoxList($model, 'roleIds', CHtml::listData(Role::model()->findAll(), 'id', 'name'));?>
		<?php echo $form->error($model,'roleIds'); ?>
	</div>
</div>
