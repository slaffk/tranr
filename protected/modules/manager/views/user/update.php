<?php
/**
 * @var Controller $this
 * @var User $model
 * @var CActiveForm $form
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Пользователи' => array('index'),
	$model->email,
);
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'user-form',
	'htmlOptions' => array(
		'autocomplete' => 'off',
		'enctype' => 'multipart/form-data',
		'class' => 'form-horizontal'
	),
)); ?>

<?php $this->renderPartial('_form', array('model' => $model, 'form' => $form)); ?>

<div class="rform_footer">
	<p><span class="button"><?php echo CHtml::submitButton('Сохранить'); ?></span></p>
</div>

<?php $this->endWidget(); ?>