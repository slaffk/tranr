<?php
$this->breadcrumbs = array(
	'Менеджер' => '/admin/default/index',
	'Пользователи' => array('index'),
	$model->email,
);

$this->menu = array(
	array('label' => 'Действия'),
	array('label' => 'Вернуться', 'url' => array('index')),
	array('label' => 'Добавить', 'url' => array('create')),
	array('label' => 'Редактировать', 'url' => array('update', 'id' => $model->id)),
);
?>

<h1><?=$model->email?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
'data' => $model,
'htmlOptions' => array('style' => 'text-align: left'),
'attributes' => array(
	'email',
	'lastname',
	'firstname',
	'middlename',
	array(
		'name' => 'active',
		'label' => 'Активен',
		'value' => $model->active ? "Да" : "Нет",
	),
	array (
		'name' => 'begin_date',
		'label' => 'Дата регистрации',
		'value' => Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $model->begin_date),
	),
	array (
		'name' => 'last_login',
		'value' => Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $model->last_login),
	),
	array (
		'label' => 'Роли',
		'type' => 'html',
		'value' => implode(", ", CHtml::listData($model->roles, 'id', 'name')),
	),
),
)); ?>
