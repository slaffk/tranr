<?php
/**
 * @var $this CityController
 * @var $model City
 * @var $form CActiveForm
 */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'city-form',
	'htmlOptions' => array(
		'class' => 'form-horizontal',
	)
)); ?>

<p class="note">Поля со звездочкой <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->label($model,'active');?>
		<?=$form->checkBox($model, 'active');?>
		<?php echo $form->error($model,'active');?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'default');?>
		<?=$form->checkBox($model, 'default');?>
		<?php echo $form->error($model,'default');?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parent_id'); ?>
		<?php echo $form->dropDownList(
			$model,
			'parent_id',
			[null => ''] + CHtml::listData(City::model()->root()->not(['id' => $model->id])->findAll(), 'id', 'name')
		);?>
		<?php echo $form->error($model,'parent_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alias'); ?>
		<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'alias'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'timezone'); ?>
		<?php echo $form->dropDownList($model, 'timezone', City::$timezones); ?>
		<?php echo $form->error($model,'timezone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_pril'); ?>
		<?php echo $form->textField($model,'name_pril',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name_pril'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_pril_f'); ?>
		<?php echo $form->textField($model,'name_pril_f',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name_pril_f'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_pril_mul'); ?>
		<?php echo $form->textField($model,'name_pril_mul',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name_pril_mul'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_pril_where'); ?>
		<?php echo $form->textField($model,'name_pril_where',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name_pril_where'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_pril_on'); ?>
		<?php echo $form->textField($model,'name_pril_on',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name_pril_on'); ?>
	</div>

	<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-complete')); ?>

<?php $this->endWidget(); ?>

</div>
<script>
	$('#City_parent_id').live('change', function(e) {
		if ($(this).val()) {
			$('.root-attr').hide();
		} else {
			$('.root-attr').show();
		}
	});
	$('#City_parent_id').change();
</script>
