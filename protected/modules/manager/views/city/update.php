<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Менеджер' => '/manag/default/index',
	'Города' => array('index'),
	'Редактировать',
);
$this->menu = array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>
<h1>Редактировать город</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>