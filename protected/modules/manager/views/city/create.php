<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Города' => array('index'),
	'Добавить город',
);
$this->menu = array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Добавить город</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>