<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Менеджер' => '/manag/default/index',
	'Города' => array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List City', 'url'=>array('index')),
	array('label'=>'Create City', 'url'=>array('create')),
	array('label'=>'Update City', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete City', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage City', 'url'=>array('admin')),
);
?>

<h1>Просмотр #<?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'alias',
		'timezone',
		'default',
		'seo_text',
		'metro_count',
		'area_count',
		'partner_form',
		'partner_name',
		'inn',
		'kpp',
		'account',
		'bank',
		'bic',
		'director',
		'director_position',
		'director_email',
		'responsible',
		'responsible_position',
		'responsible_email',
		'phone',
		'fax',
		'email',
		'email_additional',
		'wmr',
		'ym',
		'hear_from',
		'password',
		'name_pril',
		'name_pril_f',
		'name_pril_mul',
		'name_pril_where',
		'name_pril_on',
		'active',
		'timestamp',
	),
)); ?>
