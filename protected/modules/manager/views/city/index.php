<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index/',
	'Города',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'city-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		array(
			'name' => 'active',
			'value' => '$data->active ? "Да" : "Нет"',
		),
		array(
			'name' => 'name',
			'type' => 'raw',
			'value' => '$data->name . ($data->parent ? " (" . $data->parent->name . ")" : "")',
		),
		array(
			'name' => 'alias',
			'value' => '$data->alias . "." . Yii::app()->params["mainDomain"]',
		),
		'timezone',
		array(
			'class' => 'CButtonColumn',
			'template' => '{update}',
		),
	),
));
?>
<?=CHtml::link('Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-default'))?>
