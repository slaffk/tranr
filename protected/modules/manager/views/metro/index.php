<?php
/**
 * @var $this MetroController
 * @var $model Metro
 */
$this->breadcrumbs=array(
	'Менеджер' => '/manag/default/index',
	'Метро',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'metro-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		array(
			'name' => 'city_id',
			'value' => '$data->city ? $data->city->name : "Не определен"',
			'filter' => CHtml::activeDropDownList($model, 'city_id', array(null => '') + CHtml::listData(City::model()->findAll(), 'id', 'name')),
			'htmlOptions' => array('style' => 'width: 200px; text-align: center'),
		),
		'name',
		array(
			'class' => 'CButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
));
?>
<?=CHtml::link('Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-default'))?>
