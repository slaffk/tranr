<?php
/**
 * @var $this MetroController
 * @var $model Metro
 * @var $form CActiveForm
 */
?>
<div class="wide form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id' => 'metro-form',
		'enableAjaxValidation' => false,
	)); ?>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'city_id'); ?>
		<?php echo $form->dropDownList($model, 'city_id', CHtml::listData(City::model()->region()->findAll(), 'id', 'name')); ?>
		<?php echo $form->error($model,'city_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<div class="rform_footer" style="position: relative; width: 430px">
		<p><span class="button"><?php echo CHtml::submitButton('Сохранить'); ?></span></p>
	</div>
	<?php $this->endWidget(); ?>
</div>