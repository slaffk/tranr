<?php
/**
 * @var $this MetroController
 * @var $model Metro
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Метро' => array('index'),
	'Добавить',
);
?>
<h1>Добавить метро</h1>
<?php $this->renderPartial('_form', array('model' => $model)); ?>