<?php
/**
 * @var $this MetroController
 * @var $model Metro
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Метро' => array('index'),
	'Редактировать',
);
?>
<h1>Редактировать метро</h1>
<?php $this->renderPartial('_form', array('model' => $model)); ?>