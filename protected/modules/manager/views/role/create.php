<?php
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Роли' => array('index'),
	'Добавить роль',
);
?>

<h1>Добавить роль</h1>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'role-form',
	'htmlOptions' => array('style' => 'margin: 0;')
)); ?>

<div class="wide form">
	<?=$this->renderPartial('_form', array('model' => $model, 'form' => $form))?>
</div>

<div style="margin-left: 190px">
	<?=$this->renderPartial('_resources', array('model' => $model, 'form' => $form))?>
</div>

<div class="rform_footer">
	<p><span class="button"><?php echo CHtml::submitButton('Сохранить'); ?></span></p>
</div>

<?php $this->endWidget();?>
