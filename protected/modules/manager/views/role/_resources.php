<?php
/**
 * @var Role $model
 */
?>
<style>
	[class^="icon-"], [class*=" icon-"] {
		display: inline-block;
		width: 14px;
		height: 14px;
		margin-top: 1px;
		line-height: 14px;
		vertical-align: text-top;
		background-image: url('/images/glyphicons-halflings.png');
		background-position: 14px 14px;
		background-repeat: no-repeat;
	}
	.icon-plus {
		background-position: -408px -96px;
	}
	.icon-minus {
		background-position: -433px -96px;
	}
	.single_margin {
		margin-left: 20px;
	}
	.double_margin {
		margin-left: 40px;
	}
	.hide {
		display: none;
	}
	label.checkbox {
		margin-top: 0;
		margin-bottom: 0;
		padding-top: 0 !important;
		min-height: 20px !important;
	}
	label.checkbox label {
		padding-left: 0;
	}
</style>

<div class='resource-list'>
	<?php
	$modulesChecked = $controllersChecked = array();
	$clicks = array();
	$moduleId = $controllerId = null;

	foreach (Resource::model()->sortAcl()->findAll() as $resource) {
		/** @var Resource $resource */
		if ($resource->action) {
			$text = 'Действие &laquo;' . $resource->action . '&raquo;';
		} elseif ($resource->controller) {
			$text = 'Контроллер &laquo;' . $resource->controller . '&raquo;';
		} else {
			$text = 'Модуль &laquo;' . $resource->module . '&raquo;';
		}

		$checked = in_array($resource->id, is_array($model->resourcesIds) ? $model->resourcesIds : array());
		$disabled = false;
		$rel = null;
		$id = 'role_resource_'.$resource->id;

		if ($resource->module && $resource->controller && $resource->action) {

			//action resource
			$details = $text;
			$class = ' hide double_margin m_'.$resource->module.'_c_'.$resource->controller;

			$disabled = in_array($resource->module, $modulesChecked)
				|| in_array($resource->module.'_'.$resource->controller, $controllersChecked);

			if ($checked) {
				array_push($clicks, $moduleId);
				array_push($clicks, $controllerId);
			}
		} elseif ($resource->module && $resource->controller) {

			//controller resource
			$controllerId = $id;
			$class = ' hide single_margin m_'.$resource->module;
			$rel = 'm_'.$resource->module.'_c_'.$resource->controller;
			$details = '<a rel="'.$rel.'"><i class="icon-plus"></i>'.$text.'</a>';

			$disabled = in_array($resource->module, $modulesChecked);
			if ($checked) {
				array_push($controllersChecked, $resource->module.'_'.$resource->controller);
				array_push($clicks, $moduleId);
			}
		} elseif ($resource->module) {

			//module resource
			$moduleId = $id;
			$rel = 'm_'.$resource->module;
			$details = '<a rel="'.$rel.'"><i class="icon-plus"></i>'.$text.'</a>';
			$class = '';
			if ($checked) array_push($modulesChecked, $resource->module);
		} else {

			continue;
		}

		?>
		<div>
		<label class="checkbox<?=$class?>">
			<input type="checkbox" name="Role[resourcesIds][]" id="<?=$id?>" rel="<?=$rel?>"
				value="<?=$resource->id?>" <?=(($checked || $disabled) ? 'checked ' : '')?> <?=($disabled ? 'disabled ' : '')?> />

			<label for="role_resource_<?=$resource->id?>"><?=$details?></label>
		</label>
		</div>
		<?php
	}
	?>
</div>
<script>
	$('.resource-list a').click(function() {

		$(this).find('i').toggleClass('icon-minus').toggleClass('icon-plus');

		if ($(this).find('i').hasClass('icon-minus')) {
			$('.'+this.rel).removeClass('hide');
		} else {
			$('label[class*="'+this.rel+'"]').find('i').removeClass('icon-minus').addClass('icon-plus');
			$('label[class*="'+this.rel+'"]').addClass('hide');
		}

		return false;
	});

	$('.resource-list input').change(function() {

		var rel = $(this).attr('rel');

		if ($(this).is(':checked')) {
			$('label[class*="'+rel+'"]').find('input').attr('checked', true).attr('disabled', true);
		} else {
			$('label[class*="'+rel+'"]').find('input').attr('checked', false).attr('disabled', false);
		}
	});
	<?php foreach (array_unique($clicks) as $click):?>
		$('#<?=$click?>').parent().find('a').click();
	<?php endforeach;?>
</script>
