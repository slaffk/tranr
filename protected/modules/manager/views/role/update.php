<?php
/**
 * @var Role $model
 * @var CActiveForm $form
 * @var RoleController $this
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Роли' => array('index'),
	'Редактировать',
);

$this->menu = array(
	array('label' => 'Действия'),
	array('label' => 'Вернуться', 'url' => array('index')),
	array('label' => 'Добавить', 'url' => array('create')),
	array('label' => 'Просмотреть', 'url' => array('view', 'id' => $model->id)),
);
?>

<h1>Редактировать &laquo;<?=$model->name?>&raquo;</h1>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'role-form',
	'htmlOptions' => array('style' => 'margin: 0;', 'class' => 'form-horizontal')
));?>

	<?=$this->renderPartial('_form', array('model' => $model, 'form' => $form))?>

<div style="margin-left: 220px">
	<?=$this->renderPartial('_resources', array('model' => $model, 'form' => $form))?>
</div>

<div class="rform_footer">
	<p><span class="button"><?php echo CHtml::submitButton('Сохранить'); ?></span></p>
</div>

<?php $this->endWidget();?>
