<?php
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Роли',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'role-grid',
	'dataProvider' => $model->search($options),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		'name',
		'alias',
		array(
			'name' => 'id',
			'type' => 'raw',
			'header' => 'Модули',
			'value' => 'implode(", ", $data->resourcesImplode)',
		),
		array(
			'class' => 'CButtonColumn',
			'template'=>'{view} {update} {delete}',
		),
	),

));
?>
<?=CHtml::link('Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-default'))?>
