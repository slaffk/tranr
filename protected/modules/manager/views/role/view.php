<?php
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Роли' => array('index'),
	$model->name,
);
?>

<h1>Просмотр &laquo;<?=$model->name?>&raquo;</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'htmlOptions' => array('style' => 'text-align: left'),
	'attributes' => array(
		'name',
		'alias',
		array (
			'label' => 'Ресурсы',
			'type' => 'html',
			'value' => implode(", ", $model->resourcesImplode),
		),
	),
));?>
