<?php
/**
 * @var Role $model
 * @var CActiveForm $form
 */
?>

<?=$form->errorSummary($model)?>
<div class="form">

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size' => 60, 'maxlength' => 256)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alias'); ?>
		<?php echo $form->textField($model,'alias',array('size' => 60, 'maxlength' => 256)); ?>
		<?php echo $form->error($model,'alias'); ?>
	</div>
</div>
