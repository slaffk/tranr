<?php
/**
 * @var $this TrainerController
 * @var $model Trainer
 * @var $form CActiveForm
 */
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'trainer-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'form-horizontal'
	),
)); ?>

<p class="note">Поля со звездочкой <span class="required">*</span> обязательны.</p>

<?php echo $form->errorSummary($model); ?>

<div class="row checkbox">
	<?php echo $form->labelEx($model,'active'); ?>
	<?php echo $form->checkBox($model, 'active'); ?>
	<?php echo $form->error($model, 'active'); ?>
</div>

<div class="row checkbox">
	<?php echo $form->labelEx($model,'moderated'); ?>
	<?php echo $form->checkBox($model, 'moderated'); ?>
	<?php echo $form->error($model, 'moderated'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'parent_id'); ?>
	<?php echo $form->dropDownList($model, 'parent_id', [null => 'Нет'] + CHtml::listData(Trainer::model()->club()->findAll(), 'id', 'name')); ?>
	<?php echo $form->error($model,'parent_id'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'tariff_id'); ?>
	<?php echo $form->dropDownList($model, 'tariff_id', CHtml::listData(Tariff::model()->findAll(), 'id', 'name')); ?>
	<?php echo $form->error($model,'tariff_id'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'category_id'); ?>
	<?php echo $form->dropDownList($model, 'category_id', CHtml::listData(Category::model()->findAll(), 'id', 'dottedName')); ?>
	<?php echo $form->error($model,'category_id'); ?>
</div>

<div class="row radio">
	<?php echo $form->labelEx($model,'categoryIds'); ?>
	<?php echo $form->checkBoxList($model, 'categoryIds', CHtml::listData(Category::model()->notRoot()->findAll(), 'id', 'name'));?>
	<?php echo $form->error($model,'categoryIds'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'user_id'); ?>
	<?php echo $form->dropDownList($model, 'user_id', CHtml::listData(User::model()->findAll(), 'id', 'email')); ?>
	<?php echo $form->error($model,'user_id'); ?>
</div>

<?php echo $form->hiddenField($model, 'deleteImage'); ?>
<?php if ($model->image):?>
	<div class="row showImage">
		<?php echo $form->labelEx($model,'image'); ?>
		<?=CHtml::image($model->getThumb(100, 100, 'crop'), '', array('class' => 'img-thumbnail'))?>
		<?=CHtml::button('Удалить', array('class' => 'deleteImageBtn btn btn-xs btn-warning'));?>
	</div>
<?php endif;?>

<div class="row uploadImage file">
	<?php echo $form->labelEx($model,'image', array('label' => 'Загрузить изображение')); ?>
	<?php echo $form->fileField($model,'image'); ?>
	<?php echo $form->error($model,'image'); ?>
</div>

<script language="javascript">
	$('.deleteImageBtn').click(function(e) {
		e.preventDefault();
		if (confirm('Вы уверены, что хотите удалить текущее изображение?')) {
			$('.showImage').remove();
			$('#Trainer_deleteImage').val('1');
		}
	});
	$('#Trainer_image_alt').change(function(e) {
		CKEDITOR.defaultAlt = $(this).val();
	});
	CKEDITOR.defaultAlt = $('#Trainer_image_alt').val();
</script>

<h2>Основное</h2>

<div class="row">
	<?php echo $form->labelEx($model,'trainer_type'); ?>
	<?php echo $form->dropDownList($model, 'trainer_type', Trainer::$trainerTypes); ?>
	<?php echo $form->error($model,'trainer_type'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'name'); ?>
	<?php echo $form->textField($model, 'name', array('maxlength' => 512, 'placeholder' => 'Поле для секций')); ?>
	<?php echo $form->error($model, 'name'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'alias'); ?>
	<?php echo $form->textField($model, 'alias', array('maxlength' => 512, 'placeholder' => 'Генерируется автоматически')); ?>
	<?php echo $form->error($model, 'alias'); ?>
</div>

<div class="row radio">
	<?php echo $form->labelEx($model,'gender'); ?>
	<?php echo $form->radioButtonList($model, 'gender', User::$genders, array('separator' => '&nbsp;&nbsp;&nbsp;', 'class' => 'radio-group'));?>
	<?php echo $form->error($model,'gender'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'lastname'); ?>
	<?php echo $form->textField($model, 'lastname', array('maxlength' => 128, 'placeholder' => 'напр., Сидоров')); ?>
	<?php echo $form->error($model, 'lastname'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'firstname'); ?>
	<?php echo $form->textField($model, 'firstname', array('maxlength' => 128, 'placeholder' => 'напр., Иван')); ?>
	<?php echo $form->error($model, 'firstname'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'middlename'); ?>
	<?php echo $form->textField($model, 'middlename', array('maxlength' => 128, 'placeholder' => 'напр., Петрович')); ?>
	<?php echo $form->error($model, 'middlename'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model, 'dob'); ?>
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
		'model' => $model,
		'name' => 'Trainer[dob]',
		'language' => 'ru',
		'value' => $model->dob,
		'options' => array(
			'showAnim' => 'show',
			'showOn' => 'focus',
		),
		'htmlOptions'=>array(
			'style' => 'width: 300px;',
		),
	));?>
	<style>.ui-datepicker-trigger {display: none}</style>
	<?php echo $form->error($model, 'dob'); ?>
</div>

<h2>Образование и опыт</h2>

<div class="row">
	<?php echo $form->labelEx($model,'edu_level'); ?>
	<?php echo $form->dropDownList($model, 'edu_level', array(null => '') + Trainer::$eduLevels); ?>
	<?php echo $form->error($model,'edu_level'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'edu_college'); ?>
	<?php echo $form->textField($model, 'edu_college', array('maxlength' => 128)); ?>
	<?php echo $form->error($model, 'edu_college'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'edu_year'); ?>
	<?php echo $form->textField($model, 'edu_year', array('maxlength' => 4)); ?>
	<?php echo $form->error($model, 'edu_year'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'edu_other'); ?>
	<?php echo $form->textArea($model, 'edu_other'); ?>
	<?php echo $form->error($model, 'edu_other'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'experience'); ?>
	<?php echo $form->dropDownList($model, 'experience', array(null => '') + Trainer::$experiences); ?>
	<?php echo $form->error($model,'experience'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'experience_text'); ?>
	<?php echo $form->textArea($model, 'experience_text'); ?>
	<?php echo $form->error($model, 'experience_text'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'additional'); ?>
	<?php echo $form->textArea($model, 'additional'); ?>
	<?php echo $form->error($model, 'additional'); ?>
</div>


<h2>Контактные данные</h2>

<div class="row">
	<?php echo $form->labelEx($model,'city_id'); ?>
	<?php echo $form->dropDownList($model, 'city_id', CHtml::listData(City::model()->root()->findAll(), 'id', 'name')); ?>
	<?php echo $form->error($model,'city_id'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'area_id'); ?>
	<?php echo $form->dropDownList($model, 'area_id', array(null => '') + CHtml::listData(Area::model()->findAll(), 'id', 'name')); ?>
	<?php echo $form->error($model,'area_id'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'metro_id'); ?>
	<?php echo $form->dropDownList($model, 'metro_id', array(null => '') + CHtml::listData(Metro::model()->findAll(), 'id', 'name')); ?>
	<?php echo $form->error($model,'metro_id'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'address'); ?>
	<?php echo $form->textField($model, 'address', array('maxlength' => 128)); ?>
	<?php echo $form->error($model, 'address'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'phone'); ?>
	<?php echo $form->textField($model, 'phone', array('maxlength' => 128)); ?>
	<?php echo $form->error($model, 'phone'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'phone_additional'); ?>
	<?php echo $form->textField($model, 'phone_additional', array('maxlength' => 128)); ?>
	<?php echo $form->error($model, 'phone_additional'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'email'); ?>
	<?php echo $form->textField($model, 'email', array('maxlength' => 128)); ?>
	<?php echo $form->error($model, 'email'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model, 'vk_link'); ?>
	<?php echo $form->textField($model, 'vk_link', array('maxlength' => 128)); ?>
	<?php echo $form->error($model, 'vk_link'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model, 'fb_link'); ?>
	<?php echo $form->textField($model, 'fb_link', array('maxlength' => 128)); ?>
	<?php echo $form->error($model, 'fb_link'); ?>
</div>

<h2>Мета данные</h2>

<div class="row">
	<?php echo $form->labelEx($model,'image_alt'); ?>
	<?php echo $form->textField($model, 'image_alt', array('size' => 60, 'maxlength' => 256)); ?>
	<?php echo $form->error($model, 'image_alt'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'meta_title'); ?>
	<?php echo $form->textField($model, 'meta_title'); ?>
	<?php echo $form->error($model, 'meta_title'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'meta_keywords'); ?>
	<?php echo $form->textField($model, 'meta_keywords'); ?>
	<?php echo $form->error($model, 'meta_keywords'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'meta_description'); ?>
	<?php echo $form->textField($model, 'meta_description'); ?>
	<?php echo $form->error($model, 'meta_description'); ?>
</div>

<div class="row">
	<label>Ключевые слова</label>
	<input type="text" name="tags" id="Trainer_tags" class="tm-input" style="width: 80px"  value="" autocomplete="off" placeholder="Введите тег">
	<?php $this->widget('ext.tagedit.Tagedit',array(
		'model' => $model,
		'options' => array(
			'prefilled' => explode(',', $model->tag),
		)
	))?>
</div>

<div class="row">
	<?php echo $form->labelEx($model, 'moderator_comment'); ?>
	<?php echo $form->textArea($model, 'moderator_comment'); ?>
	<?php echo $form->error($model, 'moderator_comment'); ?>
</div>

<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-success')); ?>

<?php $this->endWidget(); ?>

</div>