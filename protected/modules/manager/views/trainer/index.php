<?php
/**
 * @var $this TrainerController
 * @var $model Trainer
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Тренера',
);
?>

<br/>
<?=CHtml::link('<i class="glyphicon glyphicon-plus"></i> Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-success'))?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trainer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns'=>array(
		array(
			'name' => 'active',
			'header' => 'Вкл/выкл',
			'htmlOptions' => array('style' => 'width: 60px; text-align: center'),
			'filter' => CHtml::activeDropDownList($model, 'active', array(null => '', 0 => 'Выкл', 1 => 'Вкл')),
			'type' => 'raw',
			'value' => '$data->active ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'moderated',
			'header' => 'Мод.',
			'htmlOptions' => array('style' => 'width: 60px; text-align: center'),
			'filter' => CHtml::activeDropDownList($model, 'moderated', array(null => '', 0 => 'Выкл', 1 => 'Вкл')),
			'type' => 'raw',
			'value' => '$data->moderated ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'name',
			'filter' => CHtml::activeTextField($model, 'name', array('placeholder' => 'Поиск по ФИО / позиции')),
			'type' => 'raw',
			'value' => 'CHtml::image($data->getThumb(34, 34, "crop"), $data->image_alt, array("style" => "float: left; margin-right: 5px", "class" => "img-circle bordered lightblue-border")) . CHtml::link($data->firstname . " " . $data->middlename . " " . $data->lastname . "<br/>" . $data->name, $data->url, array("target" => "_blank"))',
		),
		array(
			'name' => 'tariff_id',
			'header' => 'Тариф',
			'filter' => CHtml::listData(Tariff::model()->findAll(), 'id', 'name'),
			'type' => 'raw',
			'value' => '$data->tariff ? $data->tariff->name : "Не установлен"',
		),
		array(
			'name' => 'trainer_type',
			'filter' => Trainer::$trainerTypes,
			'type' => 'raw',
			'value' => '@Trainer::$trainerTypes[$data->trainer_type]',
		),
		array(
			'name' => 'city_id',
			'filter' => CHtml::listData(City::model()->findAll(), 'id', 'name'),
			'value' => '$data->city ? $data->city->name : ""',
		),
		array(
			'name' => 'category_id',
			'filter' => CHtml::listData(Category::model()->findAll(), 'id', 'dottedName'),
			'value' => '$data->category ? $data->category->name : ""',
		),
		array(
			'name' => 'timestamp',
			'filter' => false,
			'htmlOptions' => array('style' => 'width: 90px; text-align: center'),
			'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data->timestamp);',
		),
		array(
			'name' => 'meta_title',
			'header' => 'mt',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->meta_title ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'meta_keywords',
			'header' => 'mk',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->meta_keywords ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'meta_description',
			'header' => 'md',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->meta_description ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),

		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
		),
	),
));
