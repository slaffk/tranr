<?php
/**
 * @var $this CategoryController
 * @var $model Category
 * @var $form CActiveForm
 */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'company-category-form',
	'enableAjaxValidation'=>false,
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
			'class' => 'form-horizontal'
		),
)); ?>

	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->hiddenField($model,'returnUrl', array('value' => $model->returnUrl ? $model->returnUrl : Yii::app()->request->urlReferrer)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'parent_id'); ?>
		<?php echo $form->dropDownList($model, 'parent_id', array(null => 'Нет') + CHtml::listData(Category::model()->root()->not(array('id' => $model->id))->findAll(), 'id', 'name')); ?>
		<?php echo $form->error($model,'parent_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alias'); ?>
		<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'alias'); ?>
	</div>

	<?php echo $form->hiddenField($model, 'deleteImage'); ?>
	<?php if ($model->image):?>
		<div class="row showImage">
			<?php echo $form->labelEx($model,'image'); ?>
			<?=CHtml::image($model->getThumb(30, 30, 'crop'), '', array('class' => 'img-thumbnail'))?>
			<?=CHtml::button('Удалить', array('class' => 'deleteImageBtn btn btn-mini'));?>
		</div>
	<?php endif;?>

	<div class="row uploadImage">
		<?php echo $form->labelEx($model,'image', array('label' => 'Загрузить иконку')); ?>
		<?php echo $form->fileField($model,'image'); ?>
		<?php echo $form->error($model,'image'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'image_alt'); ?>
		<?php echo $form->textField($model, 'image_alt', array('size' => 60, 'maxlength' => 256)); ?>
		<?php echo $form->error($model, 'image_alt'); ?>
	</div>

	<script language="javascript">
		$('.deleteImageBtn').click(function(e) {
			e.preventDefault();
			if (confirm('Вы уверены, что хотите удалить текущее изображение?')) {
				$('.showImage').remove();
				$('#Article_deleteImage').val('1');
			}
		});
		$('#Article_image_alt').change(function(e) {
			CKEDITOR.defaultAlt = $(this).val();
		});
		CKEDITOR.defaultAlt = $('#Article_image_alt').val();
	</script>

	<div class="row">
		<?php echo $form->labelEx($model,'sort'); ?>
		<?php echo $form->textField($model,'sort',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'sort'); ?>
	</div>
	<h2>Мета для тренеров</h2>
	<div class="row">
		<?php echo $form->labelEx($model,'h1'); ?>
		<?php echo $form->textField($model,'h1'); ?>
		<?php echo $form->error($model,'h1'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'meta_title'); ?>
		<?php echo $form->textField($model,'meta_title', ['placeholder' => '60 символов', 'onchange' => '$(".titlecount").text($(this).val().length);']); ?>
		<?php echo $form->error($model,'meta_title'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'meta_keywords'); ?>
		<?php echo $form->textField($model,'meta_keywords'); ?>
		<?php echo $form->error($model,'meta_keywords'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'meta_description'); ?>
		<?php echo $form->textField($model,'meta_description', ['placeholder' => '160 символов', 'onchange' => '$(".descriptioncount").text($(this).val().length);']); ?>
		<?php echo $form->error($model,'meta_description'); ?>
	</div>

	<h2>Мета для статей</h2>
	<div class="row">
		<?php echo $form->labelEx($model,'a_h1'); ?>
		<?php echo $form->textField($model,'a_h1'); ?>
		<?php echo $form->error($model,'a_h1'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'a_meta_title'); ?>
		<?php echo $form->textField($model,'a_meta_title', ['placeholder' => '60 символов', 'onchange' => '$(".atitlecount").text($(this).val().length);']); ?>
		<?php echo $form->error($model,'a_meta_title'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'a_meta_keywords'); ?>
		<?php echo $form->textField($model,'a_meta_keywords'); ?>
		<?php echo $form->error($model,'a_meta_keywords'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'a_meta_description'); ?>
		<?php echo $form->textField($model,'a_meta_description', ['placeholder' => '160 символов', 'onchange' => '$(".adescriptioncount").text($(this).val().length);']); ?>
		<?php echo $form->error($model,'a_meta_description'); ?>
	</div>

	<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-complete')); ?>

<?php $this->endWidget(); ?>

</div>