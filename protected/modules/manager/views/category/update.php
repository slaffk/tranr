<?php
/**
 * @var $this CategoryController
 * @var $model Category
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Виды спорта' => array('index'),
	'Редактировать',
);

$this->menu = array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Редактировать вид спорта</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>