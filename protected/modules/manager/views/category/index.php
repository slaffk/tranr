<?php
/**
 * @var $this CategoryController
 * @var $model Category
 */
$this->breadcrumbs=array(
	'Менеджер' => '/manager/default/index',
	'Виды спорта',
);
?>
<br/>
<?=CHtml::link('<i class="glyphicon glyphicon-plus"></i> Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-success'))?>
<?=CHtml::link('<i class="glyphicon glyphicon-sort-by-alphabet"></i> Выстроить по алфавиту', Chtml::normalizeUrl(array('sort')), array('class' => 'btn btn-default', 'onclick' => 'return confirm("Вы уверены? Текущая сортировка сбросится и выстроится по алфавиту")'))?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'company-category-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'ajaxUpdate' => false,
	'columns' => array(
		array(
			'name' => 'name',
			'type' => 'html',
			'value' => '($data->parent ? "<span style=\"margin-left: 30px\"/>" : "") . CHtml::image($data->getThumb(34, 34, "crop"), $data->image_alt, array("class" => "img-circle bordered lightblue-border")) . " " . ($data->parent ? $data->parent->name . " => " . $data->name : $data->name)',
		),
		'alias',
		'sort',
		array(
			'name' => 'article_count',
			'htmlOptions' => array('style' => 'width: 80px; text-align: center'),
			'type' => 'raw',
			'value' => 'CHtml::link(intval($data->article_count) . " статей", $data->articleUrl, array("target" => "_blank"))',
		),
		array(
			'name' => 'trainer_count',
			'htmlOptions' => array('style' => 'width: 80px; text-align: center'),
			'type' => 'raw',
			'value' => 'CHtml::link(intval($data->trainer_count) . " тренеров", $data->url, array("target" => "_blank"))',
		),
		array(
			'name' => 'h1',
			'header' => 'h1',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->h1 ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'meta_title',
			'header' => 'mt',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->meta_title ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'meta_keywords',
			'header' => 'mk',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->meta_keywords ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'meta_description',
			'header' => 'md',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->meta_description ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'a_h1',
			'header' => 'ah1',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->a_h1 ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'a_meta_title',
			'header' => 'amt',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->a_meta_title ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'a_meta_keywords',
			'header' => 'amk',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->a_meta_keywords ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'a_meta_description',
			'header' => 'amd',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->a_meta_description ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'class' => 'CButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
));
?>
