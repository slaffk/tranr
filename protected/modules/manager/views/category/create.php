<?php
/**
 * @var $this CategoryController
 * @var $model Category
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Виды спорта' => array('index'),
	'Добавить',
);
$this->menu=array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Добавить вид спорта</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>