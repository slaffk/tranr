<?php
/**
 * @var $this PageController
 * @var $model Page
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Страницы',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'page-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		'path',
		'title',
		array(
			'name' => 'slash_id',
			'header' => 'Слеш',
			'filter' => Page::$slash,
			'value' => '@Page::$slash[$data->slash_id]',
		),
		array(
			'name' => 'id',
			'header' => 'Виджеты',
			'filter' => false,
			'value' => '$data->widgets ? implode(",", CHtml::listData($data->widgets, "class", "class"))  : ""',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
));
?>
<?=CHtml::link('Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-default'))?>
