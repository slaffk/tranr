<?php
/**
 * @var $this PageController
 * @var $model Page
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Страницы' => array('index'),
	'Редактировать',
);
$this->menu=array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Редактировать</h1>

<?php $this->renderPartial('_form', array('model' => $model, 'newWidget' => $newWidget)); ?>