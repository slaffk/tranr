<?php
/**
 * @var Resource $model
 * @var array $options
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Ресурсы',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'resource-grid',
	'dataProvider' => $model->search($options),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		'module',
		'controller',
		'action',
		'description',
		array(
			'class' => 'CButtonColumn',
			'template'=>'{view} {update}',
		),
	),
));
?>
<?=CHtml::link('Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-default'))?> &nbsp;
<?=CHtml::link('Сканировать ресурсы', CHtml::normalizeUrl(array('synchronize')), array('class' => 'btn btn-default'))?>
