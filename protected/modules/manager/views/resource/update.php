<?php
/**
 * @var Controller $this
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Ресурсы' => array('index'),
	'Редактировать ресурс',
);
?>

<h1>Редактировать ресурс &laquo;<?=$model->module ? $model->module : null;?><?=$model->controller ? '/'.$model->controller : null;?><?=$model->action ? '/'.$model->action : null;?>&raquo;</h1>

<div class="wide form">
	<?$form = $this->beginWidget('CActiveForm', array(
		'id' => 'resource-form',
		'htmlOptions' => array('style' => 'margin: 0;')
	)); ?>

	<?=$form->errorSummary($model)?>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size' => 60,'maxlength' => 256)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="rform_footer" style="position: relative; width: 430px">
		<p><span class="button"><?php echo CHtml::submitButton('Сохранить'); ?></span></p>
	</div>

	<?$this->endWidget();?>
</div>