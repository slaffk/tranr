<?php
/**
 * @var Controller $this
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Ресурсы' => array('index'),
	'Ресурс '.($model->module ? $model->module : '').($model->controller ? '/'.$model->controller : '').($model->action ? '/'.$model->action : ''),
);

$this->menu = array(
	array('label' => 'Действия'),
	array('label' => 'Вернуться', 'url' => array('index')),
	array('label' => 'Редактировать', 'url' => array('update', 'id' => $model->id)),
);
?>

<h1>Просмотр &laquo;<?=$model->module ? $model->module : null;?><?=$model->controller ? '/'.$model->controller : null;?><?=$model->action ? '/'.$model->action : null;?>&raquo;</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'htmlOptions' => array('style' => 'text-align: left'),
	'attributes' => array(
		'module',
		'controller',
		'action',
		'description',
	),
));?>
