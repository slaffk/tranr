<?php
/**
 * @var $this AreaController
 * @var $model Area
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Районы' => array('index'),
	'Добавить',
);
?>
<h1>Добавить район</h1>
<?php $this->renderPartial('_form', array('model' => $model)); ?>