<?php
/**
 * @var $this NewsCategoryController
 * @var $model NewsCategory
 */
$this->breadcrumbs=array(
	'Менеджер' => '/manag/default/index',
	'Категории новостей',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'news-category-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		array(
			'name' => 'name',
			'value' => '$data->parent ? $data->parent->name . " => " . $data->name : $data->name',
		),
		'alias',
//		array(
//			'name' => 'section_id',
//			'filter' => CHtml::listData(Section::model()->findAll(), 'id', 'name'),
//			'value' => '$data->section ? $data->section->name : ""',
//		),
		array(
			'class' => 'CButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
));
?>
<?=CHtml::link('Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-default'))?>
