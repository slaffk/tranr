<?php
/**
 * @var $this NewsCategoryController
 * @var $model NewsCategory
 * @var $form CActiveForm
 */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-category-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'parent_id'); ?>
		<?php echo $form->dropDownList($model, 'parent_id', array(null => 'Нет') + CHtml::listData(NewsCategory::model()->root()->not(['id' => $model->id])->findAll(), 'id', 'name')); ?>
		<?php echo $form->error($model,'parent_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'section_id'); ?>
		<?php echo $form->dropDownList($model, 'section_id', CHtml::listData(Section::model()->findAll(), 'id', 'name'), array('style' => 'width: 393px')); ?>
		<?php echo $form->error($model,'section_id'); ?>
	</div>
*/ ?>
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alias'); ?>
		<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'alias'); ?>
	</div>

	<h3>META поля</h3>
	<div class="row">
		<?php echo $form->labelEx($model,'h1'); ?>
		<?php echo $form->textArea($model,'h1'); ?>
		<?php echo $form->error($model,'h1'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'meta_title'); ?>
		<?php echo $form->textArea($model,'meta_title'); ?>
		<?php echo $form->error($model,'meta_title'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'meta_keywords'); ?>
		<?php echo $form->textArea($model,'meta_keywords'); ?>
		<?php echo $form->error($model,'meta_keywords'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'meta_description'); ?>
		<?php echo $form->textArea($model,'meta_description'); ?>
		<?php echo $form->error($model,'meta_description'); ?>
	</div>

	<div class="rform_footer">
		<p><span class="button"><?php echo CHtml::submitButton('Сохранить'); ?></span></p>
	</div>

<?php $this->endWidget(); ?>

</div>