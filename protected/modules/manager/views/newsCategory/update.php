<?php
/**
 * @var $this NewsCategoryController
 * @var $model NewsCategory
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Категории новостей' => array('index'),
	'Редактировать',
);

$this->menu = array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Редактировать категорию</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>