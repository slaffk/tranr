<?php
/**
 * @var $this NewsCategoryController
 * @var $model NewsCategory
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Категории новостей' => array('index'),
	'Добавить',
);
$this->menu=array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Добавить категорию новостей</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>