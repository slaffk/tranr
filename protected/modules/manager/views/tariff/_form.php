<?php
/**
 * @var $this TariffController
 * @var $model Tariff
 * @var $form CActiveForm
 */
?>
<div class="wide form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id' => 'tariff-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
			'class' => 'form-horizontal'
		),
	)); ?>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size' => 60, 'maxlength' => 256)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'price_month'); ?>
		<?php echo $form->textField($model,'price_month',array('size' => 60, 'maxlength' => 256)); ?>
		<?php echo $form->error($model,'price_month'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'price_quarter'); ?>
		<?php echo $form->textField($model,'price_quarter',array('size' => 60, 'maxlength' => 256)); ?>
		<?php echo $form->error($model,'price_quarter'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'price_half_year'); ?>
		<?php echo $form->textField($model,'price_half_year',array('size' => 60, 'maxlength' => 256)); ?>
		<?php echo $form->error($model,'price_half_year'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'price_year'); ?>
		<?php echo $form->textField($model,'price_year',array('size' => 60, 'maxlength' => 256)); ?>
		<?php echo $form->error($model,'price_year'); ?>
	</div>
	<div class="row checkbox">
		<?php echo $form->labelEx($model,'stick'); ?>
		<?php echo $form->checkBox($model, 'stick'); ?>
		<?php echo $form->error($model, 'stick'); ?>
	</div>
	<div class="row checkbox">
		<?php echo $form->labelEx($model,'main_page'); ?>
		<?php echo $form->checkBox($model, 'main_page'); ?>
		<?php echo $form->error($model, 'main_page'); ?>
	</div>
	<div class="row checkbox">
		<?php echo $form->labelEx($model,'hide_related'); ?>
		<?php echo $form->checkBox($model, 'hide_related'); ?>
		<?php echo $form->error($model, 'hide_related'); ?>
	</div>
	<div class="row checkbox">
		<?php echo $form->labelEx($model,'color'); ?>
		<?php echo $form->checkBox($model, 'color'); ?>
		<?php echo $form->error($model, 'color'); ?>
	</div>
	<div class="row checkbox">
		<?php echo $form->labelEx($model,'recommend'); ?>
		<?php echo $form->checkBox($model, 'recommend'); ?>
		<?php echo $form->error($model, 'recommend'); ?>
	</div>
	<div class="row checkbox">
		<?php echo $form->labelEx($model,'verify'); ?>
		<?php echo $form->checkBox($model, 'verify'); ?>
		<?php echo $form->error($model, 'verify'); ?>
	</div>
	<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-success')); ?>
	<?php $this->endWidget(); ?>
</div>
