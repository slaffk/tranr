<?php
/**
 * @var $this TariffController
 * @var $model Tariff
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Тарифы' => array('index'),
	'Добавить',
);
?>
<h1>Добавить тариф</h1>
<?php $this->renderPartial('_form', array('model' => $model)); ?>
