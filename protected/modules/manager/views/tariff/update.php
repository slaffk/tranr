<?php
/**
 * @var $this TariffController
 * @var $model Tariff
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Тарифы' => array('index'),
	'Редактировать',
);
?>
<h1>Редактировать тариф</h1>
<?php $this->renderPartial('_form', array('model' => $model)); ?>
