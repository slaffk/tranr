<?php
/**
 * @var $this TariffController
 * @var $model Tariff
 */
$this->breadcrumbs=array(
	'Менеджер' => '/manag/default/index',
	'Тарифы',
);
?>

<br/>
<?=CHtml::link('<i class="glyphicon glyphicon-plus"></i> Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-success'))?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'area-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		'name',
		array(
			'name' => 'price_month',
			'header' => 'Цена',
			'type' => 'raw',
			'value' => '$data->price_month . " / " . $data->price_quarter . " / " . $data->price_half_year . " / " . $data->price_year',
		),
		array(
			'name' => 'stick',
			'header' => 'Опции',
			'type' => 'raw',
			'value' => 'implode(", ", $data->options)',
		),
		array(
			'class' => 'CButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
));
