<?php
/**
 * @var $this MailTemplateController
 * @var $model MailTemplate
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Шаблоны писем' => array('index'),
	'Добавить',
);
$this->menu=array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Добавить шаблон</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>