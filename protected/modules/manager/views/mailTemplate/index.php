<?php
/**
 * @var $this MailTemplateController
 * @var $model MailTemplate
 * @var $form MailTemplateForm
 */
$this->breadcrumbs=array(
	'Менеджер' => '/manag/default/index',
	'Шаблоны писем',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'template-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		array(
			'name' => 'event',
			'filter' => MailTemplate::$events,
			'value' => '@MailTemplate::$events[$data->event]',
		),
		array(
			'name' => 'title',
		),
		array(
			'name' => 'content',
		),
		array(
			'class' => 'CButtonColumn',
			'template'=>'{update} {delete} {test}',
			'buttons' => array (
				'test' => array (
					'options' => array('class' => 'gj-btn testEmailBtn', 'title' => 'Тестовое сообщение'),
					'label' => '',
					'url' => '$data->event',
				),
			),
		),
	),
));
?>
<button type="submit" data-url="<?=CHtml::normalizeUrl(array('create'))?>" name="reset" class="small-button addBtn"><span>добавить</span></button>
<script>
	$('.addBtn').click(function(e) {
		e.preventDefault();
		document.location.href = $(this).attr('data-url');
	});
	$('.testEmailBtn').live('click', function(e) {
		e.preventDefault();
		var event = $(this).attr('href');
		jPrompt('Укажите e-mail для тестовой рассылки', '', 'E-mail', function(email) {
			if (email) {
				$.get('<?=CHtml::normalizeUrl(array('test'))?>?to=' + email + '&event=' + event, function(text) {
					alert(text);
				});
			}
		});
	});
</script>
<div class="clearfix"></div>

<?php if (Yii::app()->user->hasFlash('success')):?>
	<div class="alert">
		<?=Yii::app()->user->getFlash('success')?>
		<a class="closeBtn flashCloseBtn" title="Закрыть">&nbsp;</a>
	</div>
<?php endif?>

<?php echo CHtml::beginForm()?>
<div class="wide form">
	<h2>Настройки шапки и подвала для писем</h2>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'header'); ?>
		<?php echo CHtml::activeTextArea($form, 'header'); ?>
		<?php echo CHtml::error($form, 'header'); ?>
	</div>
	<div class="form">
		<?php echo CHtml::activeLabel($form, 'header_html'); ?>
	</div>
	<div class="row" style="margin-left: 189px">
		<?php echo CHtml::activeTextArea($form, 'header_html'); ?>
		<?php echo CHtml::error($form, 'header_html'); ?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'footer'); ?>
		<?php echo CHtml::activeTextArea($form, 'footer'); ?>
		<?php echo CHtml::error($form, 'footer'); ?>
	</div>
	<div class="form">
		<?php echo CHtml::activeLabel($form, 'footer_html'); ?>
	</div>
	<div class="row" style="margin-left: 189px">
		<?php echo CHtml::activeTextArea($form, 'footer_html'); ?>
		<?php echo CHtml::error($form, 'footer_html'); ?>
	</div>
</div>
<div class="rform_footer" style="position: relative; width: 430px">
	<p><span class="button"><?php echo CHtml::submitButton('Сохранить'); ?></span></p>
</div>
<?php echo CHtml::endForm()?>

<?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getRequest()->getBaseUrl().'/js/redaktor/redaktor.js'); ?>
<script>
	CKEDITOR.replace('MailTemplateForm_header_html', {
		forcePasteAsPlainText: true,
		toolbar: [
			['Source', 'Preview', 'TextColor', 'BGColor'],
			['Format'],
			['Bold', 'Italic', 'Underline'],
			['Link', 'Unlink', 'Anchor'],
			['Smiley', 'Image', 'TextColor'],
			['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
			['Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight']
		],
		enterMode : CKEDITOR.ENTER_BR,
		shiftEnterMode: CKEDITOR.ENTER_P,
		height: '80px'
	});
	CKEDITOR.replace('MailTemplateForm_footer_html', {
		forcePasteAsPlainText: true,
		toolbar: [
			['Source', 'Preview', 'TextColor', 'BGColor'],
			['Format'],
			['Bold', 'Italic', 'Underline'],
			['Link', 'Unlink', 'Anchor'],
			['Smiley', 'Image', 'TextColor'],
			['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
			['Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight']
		],
		enterMode : CKEDITOR.ENTER_BR,
		shiftEnterMode: CKEDITOR.ENTER_P,
		height: '80px'
	});
</script>
