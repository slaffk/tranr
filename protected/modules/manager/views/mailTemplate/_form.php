<?php
/**
 * @var $this MailTemplateController
 * @var $model MailTemplate
 * @var $form CActiveForm
 */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'template-form',
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'event'); ?>
		<?php echo $form->dropDownList($model, 'event', MailTemplate::$events); ?>
		<?php echo $form->error($model,'event'); ?>
	</div>
	<div class="row radio">
		<label>Параметры</label>
		<span class="template-hints"></span>
	</div>
	<script>
		$('#MailTemplate_event').live('change', function(e) {
			$.getJSON('/ajax/mailTemplateHints?event=' + $(this).val(), function(data, status, xhr) {
				if (status == 'success') {
					var $div = $('.template-hints');
					$div.empty();
					if (data.length <= 0) {
						$div.append('Нет параметров');
						return;
					}
					for (var key in data) {
						$div.append('{'+key+'} - ' + data[key]);
						$div.append($('<br>'));
					}
				} else {
					alert('Произошла ошибка');
				}
			});
		});
		$('#MailTemplate_event').change();
	</script>
	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size' => 60, 'maxlength' => 128)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content'); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'content_html'); ?>
	</div>
	<div class="row" style="padding-left: 190px; width: 500px">
		<?php echo $form->textArea($model,'content_html'); ?>
	</div>
	<?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getRequest()->getBaseUrl().'/js/redaktor/redaktor.js'); ?>
	<script>
		CKEDITOR.replace('MailTemplate_content_html', {
			forcePasteAsPlainText: true,
			toolbar: [
				['Source', 'Preview', 'TextColor', 'BGColor'],
				['Format'],
				['Bold', 'Italic', 'Underline'],
				['Link', 'Unlink', 'Anchor'],
				['Smiley', 'Image', 'TextColor'],
				['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
				['Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight']
			],
			enterMode : CKEDITOR.ENTER_BR,
			shiftEnterMode: CKEDITOR.ENTER_P,
			height: '80px'
		});
	</script>
	<div class="row">
		<?php echo $form->labelEx($model,'content_sms'); ?>
		<?php echo $form->textArea($model,'content_sms'); ?>
		<?php echo $form->error($model,'content_sms'); ?>
	</div>
	<div class="rform_footer" style="position: relative; width: 430px">
		<p><span class="button"><?php echo CHtml::submitButton('Сохранить'); ?></span></p>
	</div>
<?php $this->endWidget(); ?>
</div>