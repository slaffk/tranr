<?php
/**
 * @var $this FileController
 * @var $model File
 * @var $form CActiveForm
 */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'file-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'form-horizontal'
	),
));?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'city_id'); ?>
		<?php echo $form->dropDownList($model, 'city_id', CHtml::listData(City::model()->root()->findAll(), 'id', 'name')); ?>
		<?php echo $form->error($model,'city_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data'); ?>
		<?php echo $form->fileField($model,'data'); ?>
		<?php echo $form->error($model,'data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dummy'); ?>
		<?=$form->checkBox($model, 'attach')?>
		<?php echo $form->label($model,'attach', array('class' => 'right')); ?>
		<?php echo $form->error($model,'attach'); ?>
	</div>

	<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-complete')); ?>

<?php $this->endWidget(); ?>

</div>