<?php
/**
 * @var $this FileController
 * @var $model File
 */
$this->breadcrumbs=array(
	'Менеджер' => '/manag/default/index',
	'Файлы',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'file-grid',
	'dataProvider' => $model->search(),
	'filter' => null,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		array(
			'name' => 'timestamp',
			'type' => 'raw',
			'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm", $data->timestamp)',
			'htmlOptions' => array('style' => 'width: 110px; text-align: center'),
		),

		array(
			'name' => 'city_id',
			'value' => '$data->city->name',
			'htmlOptions' => array('style' => 'width: 110px; text-align: center'),
		),
		array(
			'name' => 'filename',
			'type' => 'raw',
			'value' => '$data->filename . " | <a href=\'".$data->url."\'>".$data->url."</a>".($data->attach ? " (аттачмент)" : "")'
		),
		array(
			'name' => 'file_size',
			'type' => 'raw',
			'value' => '$data->file_size . " байт"',
			'htmlOptions' => array('style' => 'width: 110px; text-align: center'),
		),
		array(
			'name' => 'file_type',
			'htmlOptions' => array('style' => 'width: 110px; text-align: center'),
		),
		array(
			'class' => 'CButtonColumn',
			'template'=>'{delete}',
		),
	),
));
?>
<?=CHtml::link('Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-default'))?>
