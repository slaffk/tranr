<?php
/**
 * @var $this FileController
 * @var $model File
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Файлы' => array('index'),
	'Добавить',
);
$this->menu=array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Добавить файл</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>