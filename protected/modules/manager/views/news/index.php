<?php
/**
 * @var $this NewsController
 * @var $model News
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Новости',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'news-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns' => array(
		array(
			'name' => 'date',
			'value' => '$data->date ? Yii::app()->dateFormatter->format("dd.MM.yyyy", $data->date) : ""',
		),
		array(
			'name' => 'status',
			'value' => '@News::$statuses[$data->status]',
		),
		array(
			'name' => 'city_id',
			'value' => '$data->city ? $data->city->name : ""',
		),
		array(
			'name' => 'category_id',
			'value' => '$data->category ? $data->category->name : ""',
		),
		'title',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
		),
	),
));
?>
<?=CHtml::link('Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-default'))?>
