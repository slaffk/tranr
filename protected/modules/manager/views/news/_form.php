<?php
/**
 * @var $this NewsController
 * @var $model News
 * @var $form CActiveForm
 */
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'news-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

	<p class="note">Поля со звездочкой <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'name' => 'News[date]',
			'language' => 'ru',
			'value' => $model->date,
			'options' => array(
				'showAnim' => 'show',
				'showOn' => 'focus',
			),
			'htmlOptions'=>array(
				'class' => 'style_input1 h28',
				'style' => 'width: 188px; padding-left: 10px',
			),
		));?>
		<style>.ui-datepicker-trigger {display: none}</style>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<?php echo $form->hiddenField($model, 'deleteImage'); ?>
	<div class="row showImage">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php if ($model->image):?>
			<?=CHtml::button('Удалить изображение', array('class' => 'deleteImageBtn'));?><br/>
		<?php endif;?>
		<?=CHtml::image($model->image)?>
	</div>

	<div class="row uploadImage">
		<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'image'); ?>
	</div>

	<script language="javascript">
		$('.deleteImageBtn').click(function(e) {
			e.preventDefault();
			if (confirm('Вы уверены, что хотите удалить текущее изображение?')) {
				$('.showImage').remove();
				$('#News_deleteImage').val('1');
			}
		});
	</script>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getRequest()->getBaseUrl().'/js/redaktor/redaktor.js'); ?>
	<script>
		CKEDITOR.replace('News_content', {
			forcePasteAsPlainText: true,
			toolbar: [
				['Source', 'Maximize', 'Preview'],
				['Format', 'Bold', 'Italic', 'Underline'],
				['Link', 'Unlink', 'Anchor'],
				['Smiley', 'Image', 'TextColor', 'BGColor'],
				['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
				['Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight']
			],
			enterMode : CKEDITOR.ENTER_BR,
			shiftEnterMode: CKEDITOR.ENTER_P,
			height: '240px'
		});
	</script>

	<div class="row">
		<?php echo $form->labelEx($model,'city_id'); ?>
		<?php echo $form->dropDownList($model, 'city_id', CHtml::listData(City::model()->findAll(), 'id', 'name'), array('style' => 'width: 393px')); ?>
		<?php echo $form->error($model,'city_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model, 'category_id', CHtml::listData(NewsCategory::model()->notRoot()->findAll(), 'id', 'name', 'parent.name'), array('style' => 'width: 393px')); ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company_id'); ?>
		<?php echo $form->dropDownList($model, 'company_id', array(null => 'Нет') + CHtml::listData(Company::model()->findAll(), 'id', 'name'), array('style' => 'width: 393px')); ?>
		<?php echo $form->error($model,'company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model, 'status', News::$statuses, array('style' => 'width: 393px')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<label>Теги</label>
		<input type="text" name="tags" id="News_tags" class="tm-input span8"  value="" autocomplete="off" placeholder="Введите тег">
		<?php $this->widget('ext.tagedit.Tagedit',array(
		'model' => $model,
		'options' => array(
			'prefilled' => explode(',', $model->tag),
		)
		))?>
	</div>

	<div class="row" style="border: 1px dotted #000000; padding: 5px">
		<label>Тексты для SEO</label>
		<div class="row">
			<?php echo $form->textArea($model, 'meta_title', array('size' => 60, 'maxlength' => 256, 'placeholder'=> 'Заголовок (TITLE)')); ?>
			<?php echo $form->error($model, 'meta_title'); ?>
		</div>
		<div class="row">
			<?php echo $form->textArea($model, 'meta_keywords', array('size' => 60,'maxlength' => 256, 'placeholder' => 'Ключевые слова (KEYWORDS)')); ?>
			<?php echo $form->error($model, 'meta_keywords'); ?>
		</div>
		<div class="row">
			<?php echo $form->textArea($model, 'meta_description', array('size' => 60, 'maxlength' => 256, 'placeholder'=> 'Метаописание (DESCRIPTION)', 'style' => 'width: 386px')); ?>
			<?php echo $form->error($model, 'meta_description'); ?>
		</div>
		<div class="row">
			<?php echo $form->textField($model, 'image_alt', array('size' => 60, 'maxlength' => 256, 'placeholder' => 'Ключевые первого изображения (ALT)')); ?>
			<?php echo $form->error($model, 'image_alt'); ?>
		</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div>