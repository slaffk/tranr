<?php
/**
 * @var $this NewsController
 * @var $model News
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Новости' => array('index'),
	'Добавить',
);
$this->menu = array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Добавить новость</h1>
<?php $this->renderPartial('_form', array('model' => $model)); ?>