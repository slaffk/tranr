<?php
/**
 * @var $this NewsController
 * @var $model News
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Новости' => array('index'),
	'Редактировать',
);
$this->menu = array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Редактировать новость</h1>
<?php $this->renderPartial('_form', array('model' => $model)); ?>