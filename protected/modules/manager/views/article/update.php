<?php
/**
 * @var $this ArticleController
 * @var $model Article
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Статьи' => array('index'),
	'Редактировать',
);
$this->menu = array(
	array('label' => 'Вернуться', 'url' => array('index')),
);
?>

<h1>Редактировать статью</h1>
<?php $this->renderPartial('_form', array('model' => $model)); ?>