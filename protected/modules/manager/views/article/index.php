<?php
/**
 * @var $this ArticleController
 * @var $model Article
 */
$this->breadcrumbs = array(
	'Менеджер' => '/manag/default/index',
	'Статьи',
);
?>
<br/>
<?=CHtml::link('<i class="glyphicon glyphicon-plus"></i> Добавить', CHtml::normalizeUrl(array('create')), array('class' => 'btn btn-success'))?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'article-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows' => 0,
	'template' => "{items} {pager}",
	'enableSorting' => false,
	'rowCssClass' => null,
	'columns'=>array(
		array(
			'name' => 'title',
			'filter' => CHtml::activeTextField($model, 'title', array('placeholder' => 'Поиск по заголовку')),
			'type' => 'raw',
			'value' => 'CHtml::image($data->getThumb(34, 34, "crop"), $data->image_alt, array("class" => "img-rounded bordered lightblue-border")) . " " . ($data->status == Article::STATUS_PUBLISHED ? CHtml::link($data->title, $data->url, array("target" => "_blank")) : $data->title)',
		),
		array(
			'name' => 'status',
			'filter' => Article::$statuses,
			'type' => 'raw',
			'value' => '"<span class=\"".($data->status == Article::STATUS_PUBLISHED ? "green-caption" : "text-muted")."\">" . @Article::$statuses[$data->status] . "</span>"',
		),
		array(
			'name' => 'city_id',
			'filter' => CHtml::listData(City::model()->findAll(), 'id', 'name'),
			'value' => '$data->city ? $data->city->name : "Все"',
		),
		array(
			'name' => 'category_id',
			'filter' => CHtml::listData(Category::model()->findAll(), 'id', 'dottedName'),
			'value' => '$data->category ? $data->category->name : ""',
		),
		array(
			'name' => 'timestamp',
			'filter' => false,
			'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data->timestamp);',
			'htmlOptions' => array('style' => 'width: 90px; text-align: center'),
		),
		array(
			'name' => 'meta_title',
			'header' => 'mt',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->meta_title ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'meta_keywords',
			'header' => 'mk',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->meta_keywords ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'name' => 'meta_description',
			'header' => 'md',
			'htmlOptions' => array('style' => 'width: 30px; text-align: center'),
			'filter' => false,
			'type' => 'html',
			'value' => '$data->meta_description ? "<i class=active_circle></i>" : "<i class=inactive_circle></i>"',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
		),
	),
));
