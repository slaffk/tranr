<?php
/**
 * @var $this ArticleController
 * @var $model Article
 * @var $form CActiveForm
 */
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'article-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'form-horizontal'
	),
)); ?>

	<p class="note">Поля со звездочкой <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'timestamp'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'name' => 'Article[timestamp]',
			'language' => 'ru',
			'value' => $model->timestamp,
			'options' => array(
				'showAnim' => 'show',
				'showOn' => 'focus',
			),
			'htmlOptions'=>array(
				'style' => 'width: 300px;',
			),
		));?>
		<style>.ui-datepicker-trigger {display: none}</style>
		<?php echo $form->error($model, 'timestamp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'city_id'); ?>
		<?php echo $form->dropDownList($model, 'city_id', array(null => 'Все') + CHtml::listData(City::model()->root()->findAll(), 'id', 'name')); ?>
		<?php echo $form->error($model,'city_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model, 'category_id', CHtml::listData(Category::model()->findAll(), 'id', 'dottedName')); ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model, 'status', Article::$statuses); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_title'); ?>
		<?php echo $form->textField($model, 'meta_title'); ?>
		<?php echo $form->error($model, 'meta_title'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'meta_keywords'); ?>
		<?php echo $form->textField($model, 'meta_keywords'); ?>
		<?php echo $form->error($model, 'meta_keywords'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'meta_description'); ?>
		<?php echo $form->textField($model, 'meta_description'); ?>
		<?php echo $form->error($model, 'meta_description'); ?>
	</div>

	<?php echo $form->hiddenField($model, 'deleteImage'); ?>
	<?php if ($model->image):?>
		<div class="row showImage">
			<?php echo $form->labelEx($model,'image'); ?>
			<?=CHtml::image($model->getThumb(100, 100, 'crop'), '', array('class' => 'img-polaroid'))?>
			<?=CHtml::button('Удалить', array('class' => 'deleteImageBtn btn btn-mini'));?>
		</div>
	<?php endif;?>

	<div class="row uploadImage">
		<?php echo $form->labelEx($model,'image', array('label' => 'Загрузить изображение')); ?>
		<?php echo $form->fileField($model,'image'); ?>
		<?php echo $form->error($model,'image'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'image_alt'); ?>
		<?php echo $form->textField($model, 'image_alt', array('size' => 60, 'maxlength' => 256)); ?>
		<?php echo $form->error($model, 'image_alt'); ?>
	</div>

	<script language="javascript">
		$('.deleteImageBtn').click(function(e) {
			e.preventDefault();
			if (confirm('Вы уверены, что хотите удалить текущее изображение?')) {
				$('.showImage').remove();
				$('#Article_deleteImage').val('1');
			}
		});
		$('#Article_image_alt').change(function(e) {
			CKEDITOR.defaultAlt = $(this).val();
		});
		CKEDITOR.defaultAlt = $('#Article_image_alt').val();
	</script>

	<div class="row">
		<label>Теги</label>
		<input type="text" name="tags" id="Article_tags" class="tm-input" style="width: 80px"  value="" autocomplete="off" placeholder="Введите тег">
		<?php $this->widget('ext.tagedit.Tagedit',array(
			'model' => $model,
			'options' => array(
				'prefilled' => explode(',', $model->tag),
			)
		))?>
	</div>

	<?php echo $form->labelEx($model,'content'); ?>
	<div class="row">
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getRequest()->getBaseUrl().'/js/redaktor/redaktor.js'); ?>
	<script>
		CKEDITOR.replace('Article_content', {
			forcePasteAsPlainText: true,
			toolbar: [
				['Source', 'Maximize', 'Preview'],
				['Format', 'Bold', 'Italic', 'Underline'],
				['Link', 'Unlink', 'Anchor'],
				['Smiley', 'Image', 'TextColor', 'BGColor'],
				['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
				['Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight']
			],
//			enterMode : CKEDITOR.ENTER_BR,
//			shiftEnterMode: CKEDITOR.ENTER_P,
			height: '240px'
		});
	</script>

	<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-large btn-complete')); ?>

	<?php $this->endWidget(); ?>

</div>