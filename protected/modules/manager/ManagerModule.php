<?php
/**
 * Class ManagerModule
 */
class ManagerModule extends CWebModule {

	public function init() {

		$this->setImport(array('manager.models.*', 'manager.components.*'));
	}

	public function beforeControllerAction($controller, $action) {

		return parent::beforeControllerAction($controller, $action);
	}
}
