<?php
/**
 * Class NewsCategoryController
 */
class NewsCategoryController extends AdminController {

	const CTRL_NAME = 'Категории новостей';

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	/**
	 * create
	 */
	public function actionCreate() {

		$model = new NewsCategory;

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('NewsCategory');
			if($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('create',array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('NewsCategory');
			if($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new NewsCategory('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('NewsCategory');

		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 * @return NewsCategory
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = NewsCategory::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}
