<?php
/**
 * Class PageController
 */
class PageController extends AdminController {

	const CTRL_NAME = 'Страницы';

	/**
	 * @var string
	 */
	public $layout='//layouts/column2';

	/**
	 * Create
	 */
	public function actionCreate() {

		$model = new Page;
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('Page');
			if ($model->save()) {
				foreach (Yii::app()->request->getPost('Widget', array()) as $widgetAttr) {
					$widget = new Widget();
					$widget->attributes = $widgetAttr;
					$widget->action = @$widgetAttr['action'];
					$widget->caption = @$widgetAttr['caption'];
					$widget->page_id = $model->id;
					$widget->save();
				}
				$this->redirect(array('index'));
			}
		}

		$this->render('create',array(
			'model' => $model,
			'newWidget' => new Widget(),
		));
	}

	/**
	 * @param $id
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('Page');
			if ($model->save()) {
				$widgets = Yii::app()->request->getPost('Widget', array());
				$update = array_keys(array_intersect_key($widgets, CHtml::listData($model->widgets, 'id', 'id')));
				$insert = array_keys(array_diff_key($widgets, $update, CHtml::listData($model->widgets, 'id', 'id')));
				$delete = array_diff(array_keys(CHtml::listData($model->widgets, 'id', 'id')), $update, $insert);

				Widget::model()->deleteAllByAttributes(array('id' => $delete));
				foreach (Widget::model()->findAllByAttributes(array('id' => $update)) as $widget) {
					/** @var $widget Widget */
					$widget->attributes = $widgets[$widget->id];
					$widget->action = @$widgets[$widget->id]['action'];
					$widget->caption = @$widgets[$widget->id]['caption'];
					$widget->page_id = $model->id;
					$widget->save();
				}
				foreach ($insert as $key) {
					$widget = new Widget();
					$widget->attributes = $widgets[$key];
					$widget->action = @$widgets[$key]['action'];
					$widget->caption = @$widgets[$key]['caption'];
					$widget->page_id = $model->id;
					$widget->save();
				}
				$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model' => $model,
			'newWidget' => new Widget(),
		));
	}

	/**
	 * @param $id
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new Page('search');
		$model->unsetAttributes();

		if(isset($_GET['Page']))
			$model->attributes = $_GET['Page'];

		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * @param $widget
	 */
	public function actionWidgetActions($widget) {

		echo json_encode(WidgetHelper::actions($widget));
		Yii::app()->end();
	}

	/**
	 * @param $id
	 * @return Page
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = Page::model()->findByPk($id);
		if($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		return $model;
	}
}
