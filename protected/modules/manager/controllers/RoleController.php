<?php
/**
 * Class RoleController
 */
class RoleController extends AdminController {

	const CTRL_NAME = 'Роли';

	/**
	 * @param integer $id ID сущности
	 */
	public function actionView($id) {

		$this->render('view', array('model' => $this->loadModel($id)));
	}

	/**
	 * Создание
	 */
	public function actionCreate() {

		$model = new Role;

		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('Role');
			$model->attributes = $data;
			$model->resourcesIds = @$data['resourcesIds'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Создана роль ' . $model->name);
				$this->redirect(array('index'));
			}
		}

		$this->render('create', array('model' => $model));
	}

	/**
	 * @param integer $id
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('Role');
			$model->attributes = $data;
			$model->resourcesIds = @$data['resourcesIds'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Изменена роль ' . $model->name);
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array('model' => $model));
	}

	/**
	 * @param integer $id
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax')) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * Листинг
	 */
	public function actionIndex() {

		$model = new Role('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Role');

		$options = array(
			'pagination' => array(
				'pageSize' => self::RECORDS_ON_PAGE,
			),
		);
		$this->render('index', array(
			'model' => $model,
			'options' => $options,
		));
	}

	/**
	 * @param integer $id
	 * @throws CHttpException
	 * @return Role
	 */
	public function loadModel($id) {

		$model = Role::model()->with('resources')->together()->findByPk($id);

		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена');
		}
		return $model;
	}
}
