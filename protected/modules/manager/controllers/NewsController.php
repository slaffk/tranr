<?php
/**
 * Class NewsController
 */
class NewsController extends AdminController {

	const CTRL_NAME = 'Новости';

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	/**
	 * create
	 */
	public function actionCreate() {

		$model = new News;
		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('News');
			$data['date'] = date('Y-m-d H:i:s', strtotime(@$data['date']));
			$model->attributes = $data;
			$model->tag = $data['tag'];
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$model->date = Yii::app()->dateFormatter->format('dd.MM.yyyy', $model->date);
		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('News');
			$data['date'] = date('Y-m-d H:i:s', strtotime(@$data['date']));
			$model->attributes = $data;
			$model->tag = $data['tag'];
			$model->deleteImage = $data['deleteImage'];
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}
		$model->date = Yii::app()->dateFormatter->format('dd.MM.yyyy', $model->date);
		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new News('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('News');

		$this->render('index',array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 * @return News
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = News::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}
