<?php
/**
 * Class CityController
 */
class CityController extends AdminController {

	const CTRL_NAME = 'Города';

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	/**
	 * @param integer
	 */
	public function actionView($id) {

		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * create
	 */
	public function actionCreate() {

		$model = new City;
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('City');
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}
		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('City');
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}
		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
//	public function actionDelete($id) {
//
//		$this->loadModel($id)->delete();
//
//		if (!Yii::app()->request->getQuery('ajax', false)) {
//			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
//		}
//	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new City('search');
		$model->unsetAttributes();
		$model->attributes=Yii::app()->request->getQuery('City');
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 * @return City
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = City::model()->findByPk($id);
		if($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}
