<?php
/**
 * Class TrainerController
 */
class TrainerController extends AdminController {

	const CTRL_NAME = 'Тренера';

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	/**
	 * create
	 */
	public function actionCreate() {

		$model = new Trainer();
		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('Trainer');
			if (@$data['dob'] && strtotime(@$data['dob'])) {
				$data['dob'] = date('Y-m-d', strtotime(@$data['dob'])) . ' ' . date('H:i:s');
			}
			$model->attributes = $data;
			$model->tag = $data['tag'];
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('Trainer');
			if (@$data['dob'] && strtotime(@$data['dob'])) {
				$data['dob'] = date('Y-m-d', strtotime(@$data['dob'])) . ' ' . date('H:i:s');
			}
			$model->attributes = $data;
			$model->tag = $data['tag'];
			$model->deleteImage = $data['deleteImage'];
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}
		$model->dob = Yii::app()->dateFormatter->format('dd.MM.yyyy', $model->dob);
		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new Trainer('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Trainer');

		$this->render('index',array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 * @return Trainer
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = Trainer::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}
