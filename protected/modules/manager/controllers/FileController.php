<?php
/**
 * Class FileController
 */
class FileController extends AdminController {

	const CTRL_NAME = 'Файлы';

	/**
	 * create
	 */
	public function actionCreate() {

		$model = new File;

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('File');
			if($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('create',array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('File');
			if($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new File('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('File');

		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 * @return File
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = File::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}
