<?php
/**
 * Class TariffController
 */
class TariffController extends AdminController {

	const CTRL_NAME = 'Тарифы';

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	/**
	 * create
	 */
	public function actionCreate() {

		$model = new Tariff('create');

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('Tariff');
			if($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('create',array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('Tariff');
			if($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new Tariff('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Tariff');

		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 * @return Tariff
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = Tariff::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}
