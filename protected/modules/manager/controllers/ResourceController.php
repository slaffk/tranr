<?php
/**
 * Class ResourceController
 */
class ResourceController extends AdminController {

	const CTRL_NAME = 'Ресурсы';
	const RECORDS_ON_PAGE = 50;

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	/**
	 * init
	 */
	public function init() {

		parent::init();

		if (Yii::app()->request->isAjaxRequest) {
			$this->layout = false;
		}
	}

	/**
	 * @return void
	 */
	public function actionSynchronize() {

		$codeResources = Resource::getAllCodeResources();
		$dbResources = Resource::getAllDbResources();

		//бежим по ресурсам из кода, запинывая недостоющие в базу
		foreach ($codeResources as $module => $controllers) {

			if (!array_key_exists($module, $dbResources)) {
				$m = new Resource();
				$m->module = $module;
				$m->save();
				$dbResources[$module] = array();
			}

			foreach ($controllers as $controller => $actions) {

				if (!array_key_exists($controller, $dbResources[$module])) {
					$m = new Resource();
					$m->module = $module;
					$m->controller = $controller;
					$m->save();
					$dbResources[$module][$controller] = array();
				}

				foreach ($actions as $action) {

					if (!in_array($action, $dbResources[$module][$controller])) {
						$m = new Resource();
						$m->module = $module;
						$m->controller = $controller;
						$m->action = $action;
						$m->save();
						array_push($dbResources[$module][$controller], $action);
					}
				}
			}
		}

		Yii::app()->user->setFlash('success', 'Ресурсы синхронизированы');
		$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
	}


	/**
	 * Просмотр сущности
	 * @param integer $id ID сущности
	 */
	public function actionView($id) {

		$this->render('view',array('model' => $this->loadModel($id)));
	}

	/**
	 * @param integer $id ID сущности
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('Resource');
			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Ресурс сохранен');
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array('model' => $model));
	}

	/**
	 * @return void
	 */
	public function actionIndex() {

		$model = new Resource('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Resource');

		$options = array(
			'pagination' => array (
				'pageSize' => self::RECORDS_ON_PAGE,
			),
			'criteria' => array (
				'order' => 'module, controller, action',
			)
		);

		$this->render('index', array(
			'model' => $model,
			'options' => $options,
		));
	}

	/**
	 * @param integer $id ID сущности
	 * @throws CHttpException
	 * @return Resource
	 */
	public function loadModel($id) {

		$model = Resource::model()->findByPk($id);

		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена');
		}
		return $model;
	}
}
