<?php

class ReviewController extends AdminController {

	const CTRL_NAME = 'Отзывы';

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	public function actionIndex() {
		$model = new TrainerReview('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('TrainerReview');

		$this->render('index',array(
			'model' => $model,
		));
	}
	
	public function actionCreate() {

		$model = new TrainerReview();
		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('TrainerReview');
			$model->attributes = $data;
			
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('TrainerReview');
			$model->attributes = $data;
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}
		$this->render('update', array(
			'model' => $model,
		));
	}
	
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}
	
	public function loadModel($id) {

		$model = TrainerReview::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}

?>
