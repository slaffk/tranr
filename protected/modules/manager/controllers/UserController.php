<?php
/**
 * Class UserController
 */
class UserController extends AdminController {

	const CTRL_NAME = 'Пользователи';

	/**
	 * @param integer $id
	 */
	public function actionView($id) {

		$this->render('view',array('model' => $this->loadModel($id)));
	}

	/**
	 * Создание
	 */
	public function actionCreate() {

		$model = new User();

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('User');
			$model->password = $model->validate() ? User::cryptPassword($model->password) : $model->password;

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Вы добавили пользователя &laquo;' . $model->email . '&raquo;');
				$this->redirect(array('index'));
			}
		}

		$this->render('create', array('model' => $model));
	}

	/**
	 * @param integer $id ID сущности
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('User');
			if (!@$data['password'] || $model->password == @$data['password']) {
				unset($data['password']);
				$model->attributes = $data;
			} else {
				$model->attributes = $data;
				$model->password = $model->validate() ? User::cryptPassword($data['password']) : $model->password;
			}

			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Вы изменили пользователя &laquo;' . $model->email . '&raquo;');
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array('model' => $model));
	}

	/**
	 * @param integer $id ID сущности
	 * @throws CHttpException
	 */
	public function actionDelete($id) {

		$model = $this->loadModel($id);
		$model->delete();

		if (!isset($_GET['ajax'])) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * Листинг
	 */
	public function actionIndex() {

		$model = new User('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('User');

		$options = array(
			'pagination' => array(
				'pageSize' => self::RECORDS_ON_PAGE,
			),
		);

		$this->render('index', array(
			'model' => $model,
			'options' => $options,
		));
	}

	/**
	 * @param integer $id ID сущности
	 * @throws CHttpException
	 * @return User
	 */
	public function loadModel($id) {

		$model = User::model()->with('roles')->together()->findByPk($id);

		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена');
		}
		return $model;
	}
}
