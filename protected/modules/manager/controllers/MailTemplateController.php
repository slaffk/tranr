<?php
/**
 * Class MailTemplateController
 */
class MailTemplateController extends AdminController {

	const CTRL_NAME = 'Шаблоны писем';

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	/**
	 * create
	 */
	public function actionCreate() {

		$model = new MailTemplate();

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('MailTemplate');
			if($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('create',array(
			'model' => $model,
		));
	}

	/**
	 * @param $to
	 * @param $event
	 */
	public function actionTest($to, $event) {

		if (Yii::app()->request->isAjaxRequest) {

			foreach (explode(',', $to) as $email) {
				Mailer::mail($event, $email, 'test');
			}
			echo 'Рассылка отправлена на емейл(ы) '. $to;
		}
	}

	/**
	 * @param integer
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('MailTemplate');
			if($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new MailTemplate('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('MailTemplate');

		$form = new MailTemplateForm();
		if (Yii::app()->request->isPostRequest) {
			$form->attributes = Yii::app()->request->getPost('MailTemplateForm');
			if ($form->save()) {
				Yii::app()->user->setFlash('success', 'Данные сохранены.');
			}
		}

		$this->render('index', array(
			'model' => $model,
			'form' => $form,
		));
	}

	/**
	 * @param $id
	 * @return MailTemplate
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = MailTemplate::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}
