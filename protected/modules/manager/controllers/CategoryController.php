<?php
/**
 * Class CategoryController
 */
class CategoryController extends AdminController {

	const CTRL_NAME = 'Категории компаний';

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	/**
	 * create
	 */
	public function actionCreate() {

		$model = new Category;

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('Category');
			if ($model->save()) {
				$this->redirect($model->returnUrl ? $model->returnUrl : array('index'));
			}
		}

		$this->render('create',array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('Category');
			if ($model->save()) {
				$this->redirect($model->returnUrl ? $model->returnUrl : array('index'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new Category('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Category');

		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * сортировка по алфовиту
	 */
	public function actionSort() {

		$sort = 1;
		foreach (Category::model()->root()->sortByName()->findAll() as $root) {
			/** @var $root Category */
			$root->sort = $sort++;
			$root->save();

			$children = Category::model()->sortByName()->findAllByAttributes(array('parent_id' => $root->id));
			if (empty($children)) continue;

			foreach ($children as $sub) {
				/** @var $sub Category */
				$sub->sort = $sort++;
				$sub->save();

				$childs = Category::model()->sortByName()->findAllByAttributes(array('parent_id' => $sub->id));
				if (empty($childs)) continue;

				foreach ($childs as $cat) {
					/** @var $cat Category */
					$cat->sort = $sort++;
					$cat->save();
				}
			}
		}
		$this->redirect(!empty($returnUrl) ? $returnUrl : array('index'));
	}

	/**
	 * @param $id
	 * @return Category
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = Category::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}
