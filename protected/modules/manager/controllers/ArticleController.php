<?php
/**
 * Class ArticleController
 */
class ArticleController extends AdminController {

	const CTRL_NAME = 'Статьи';

	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';

	/**
	 * create
	 */
	public function actionCreate() {

		$model = new Article;
		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('Article');
			if (@$data['timestamp'] && strtotime(@$data['timestamp'])) {
				$data['timestamp'] = date('Y-m-d', strtotime(@$data['timestamp'])) . ' ' . date('H:i:s');
			}
			$model->attributes = $data;
			$model->tag = $data['tag'];
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}
		$model->timestamp = date('d.m.Y');
		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * @param integer
	 */
	public function actionUpdate($id) {

		$model = $this->loadModel($id);

		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('Article');
			if (@$data['timestamp'] && strtotime(@$data['timestamp'])) {
				$data['timestamp'] = date('Y-m-d', strtotime(@$data['timestamp'])) . ' ' . date('H:i:s');
			}
			$model->attributes = $data;
			$model->tag = $data['tag'];
			$model->deleteImage = $data['deleteImage'];
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}
		$model->timestamp = Yii::app()->dateFormatter->format('dd.MM.yyyy', $model->timestamp);
		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 */
	public function actionDelete($id) {

		$this->loadModel($id)->delete();
		if (!Yii::app()->request->getQuery('ajax', false)) {
			$this->redirect(Yii::app()->request->getPost('returnUrl', array('index')));
		}
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new Article('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Article');

		$this->render('index',array(
			'model' => $model,
		));
	}

	/**
	 * @param $id
	 * @return Article
	 * @throws CHttpException
	 */
	public function loadModel($id) {

		$model = Article::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Страница не найдена.');
		}
		return $model;
	}
}
