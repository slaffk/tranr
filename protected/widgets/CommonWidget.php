<?php
/**
 * Class CommonWidget
 */
class CommonWidget extends AppWidget {

	const VIEW_PATH = 'application.widgets.common';

	/**
	 * @return string
	 */
	public static function getName() {

		return 'Общие виджеты';
	}

	/**
	 * @return array
	 */
	public static function getActionList() {

		return array(
			'analytics' => 'Google Analytics (футер)',
			'salesDept' => 'Отдел по работе с кл (сайдбар)',
			'benefits' => 'Преимущества портала (сайдбар)',
			'facilities' => 'Возможности портала (контент)',
			'citySeo' => 'Городской SEO текст (контент)',
			'banner1' => 'Баннер 220х400 (сайдбар)',
			'socialSharing' => 'Кнопки шеринга в социалки (контент)',
			'contacts' => 'Контактная инфа партнера (контент)',
			'banner336x280' => 'Adsense 336 x 280 (сайдбар)',
			'banner728x90' => 'Adsense 728 x 90 (контент)',
		);
	}

	/**
	 * benefits
	 */
	public function actionAnalytics() {

		$this->render(self::VIEW_PATH . '.analytics');
	}

	/**
	 * benefits
	 */
	public function actionSalesDept() {

		$this->render(self::VIEW_PATH . '.sales_dept');
	}

	/**
	 * benefits
	 */
	public function actionBenefits() {

		$this->render(self::VIEW_PATH . '.benefits');
	}

	/**
	 * benefits
	 */
	public function actionFacilities() {

		$this->render(self::VIEW_PATH . '.facilities', array(
			'sliders' => AdvertSlider::model()->this()->findAll(),
			'left' => Benefit::model()->findAllByAttributes(array('type' => Benefit::TYPE_SLIDER_LEFT)),
			'right' => Benefit::model()->findAllByAttributes(array('type' => Benefit::TYPE_SLIDER_RIGHT)),
		));
	}

	/**
	 * city SEO
	 */
	public function actionCitySeo() {

		$this->render(self::VIEW_PATH . '.city_seo');
	}

	/**
	 * city SEO
	 */
	public function actionBanner1() {

		$this->render(self::VIEW_PATH . '.banner1');
	}

	/**
	 * city SEO
	 */
	public function actionSocialSharing() {

		$this->render(self::VIEW_PATH . '.social_sharing');
	}

	/**
	 * @return void
	 */
	public function actionContacts() {

		$this->render(self::VIEW_PATH . '.contacts');
	}

	/**
	 * @throws CException
	 */
	public function actionBanner336x280() {

		$this->render(self::VIEW_PATH . '.banner336x280');
	}

	/**
	 * @throws CException
	 */
	public function actionBanner728x90() {

		$this->render(self::VIEW_PATH . '.banner728x90');
	}
}
