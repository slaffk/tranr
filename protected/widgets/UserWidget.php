<?php
/**
 * Class UserWidget
 */
class UserWidget extends AppWidget {

	const VIEW_PATH = 'application.widgets.user';

	/**
	 * @return string
	 */
	public static function getName() {

		return 'Пользователи';
	}

	/**
	 * @return array
	 */
	public static function getActionList() {

		return array(
			//'index' => 'Список статей (контент)',
			'register' => 'Регистрация пользователя',
			'benefits' => 'Преимущества регистрации (сайдбар)',
			'profile' => 'Профиль пользователя',
			'profileMenu' => 'Меню в профиле (сайдбар)',
			'profileEdit' => 'Профиль (редактирование)',
			'favorite' => 'Избранное',
			'blacklist' => 'Черный список',
			'remind' => 'Восстановление пароля',
			'reset' => 'Установка нового пароля',
			'password' => 'Смена пароля в кабинете',
			'unsubscribe' => 'Форма отписки от рассылок',
			'subscribe' => 'Мои подписки (контент)',
		);
	}

	/**
	 * benefits
	 */
	public function actionBenefits() {

		$this->render(self::VIEW_PATH . '.benefits', array(
			'app' => Benefit::model()->findAllByAttributes(array('type' => Benefit::TYPE_EMPLOYER)),
			'emp' => Benefit::model()->findAllByAttributes(array('type' => Benefit::TYPE_COMPANY)),
			'ka' => Benefit::model()->findAllByAttributes(array('type' => Benefit::TYPE_HR)),
		));
	}

	/**
	 * index
	 */
	public function actionRegister() {

		if (!Yii::app()->user->isGuest) {
			$this->controller->redirect('/cabinet');
			return;
		}

		$userForm = new RegistrationForm();
		$companyForm = new CompanyRegistrationForm();
		$hrForm = new HrRegistrationForm();
		$tab = 1;

		if (Yii::app()->request->getPost('ajax') === 'reg-form-user') {
			echo CActiveForm::validate($userForm);
			Yii::app()->end();
		}
		if (Yii::app()->request->getPost('ajax') === 'reg-form-company') {
			echo CActiveForm::validate($companyForm);
			Yii::app()->end();
		}

		if (Yii::app()->request->isPostRequest) {
			$userFormData = Yii::app()->request->getPost('RegistrationForm');
			$companyFormData = Yii::app()->request->getPost('CompanyRegistrationForm');
			$hrFormData = Yii::app()->request->getPost('HrRegistrationForm');
			$tab = $userFormData ? 1 : ($companyFormData ? 2 : 3);
			$model = $userFormData ? $userForm : ($companyFormData ? $companyForm : $hrForm);
			$model->attributes = $userFormData ? $userFormData : ($companyFormData ? $companyFormData : $hrFormData);
			if ($model->register()) {
				$identity = new UserIdentity($model->email, $model->passwordConfirm);
				if ($identity->authenticate()){
					Yii::app()->user->login($identity);
				}

				if (!Yii::app()->user->isGuest) {

					Yii::app()->user->setFlash('success', 'Регистрация прошла успешно!<br/>'.
						'Теперь вы можете разместить '.($userFormData ? 'резюме ' : 'вакансию ').
						'и заполнить <a href="/cabinet/settings/">профиль '.($userFormData ? 'соискателя' : 'компании').'</a>, '.
						'чтобы '.($userFormData ? 'работодатели' : 'соискатели').' могли больше узнать о вас!');

					$this->getController()->redirect('/cabinet/'.($userFormData ? 'myresume' : 'myvacancies').'/add/');
				}
				$this->render(self::VIEW_PATH.'.register_ok', array('model' => $model));
				return;
			}
		}

		$this->render(self::VIEW_PATH.'.register', array(
			'userForm' => $userForm,
			'companyForm' => $companyForm,
			'hrForm' => $hrForm,
			'tab' => $tab,
		));
	}

	/**
	 * profile
	 */
	public function actionProfile() {

		Yii::app()->user->requireAuthorized();

		$c = $this->getController();
		$model = Yii::app()->user->getModel();

		if (@$c->vars[1] == 'sendAct') {
			if (!$model->activated) {
				Mailer::mail(MailTemplate::EVENT_USER_REG, $model->email, $model);
				Yii::app()->user->setFlash('success', 'Мы отправили вам URL для активации. Проверьте ваш почтовый ящик ('.$model->email.')');
			}
			$c->redirect('/cabinet/');
		}

		$this->render(self::VIEW_PATH . '.profile', array(
			'model' => $model,
		));
	}

	public function actionProfileMenu() {

		if (Yii::app()->user->isGuest || !Yii::app()->user->getModel()) {
			return;
		}

		$this->render(self::VIEW_PATH . '.profile_menu');
	}

	public function actionProfileEdit() {

		Yii::app()->user->requireAuthorized();
		$user = Yii::app()->user->getModel();

		if (Yii::app()->request->isPostRequest) {
			$userData = Yii::app()->request->getPost('User');
			$companyData = Yii::app()->request->getPost('Company');
			$model = $companyData && $user->company ? $user->company : $user;
			if ($userData) {
				$userData['birth_date'] = date('Y-m-d H:i:s', strtotime(@$userData['birth_date']));
			}
			$model->attributes = $companyData ? $companyData : $userData;
			if ($model->save()) {
				if ($userData && $model instanceof User && $model->phone != $model->currentPhone && $model->phone) {
					Yii::app()->user->setFlash('success', 'Теперь вы сможете авторизоваться используя телефон ' . $model->phone);
				}
				$this->render(self::VIEW_PATH.'.profile_edit_ok', array('model' => $model));
				return;
			}
		}

		if ((Yii::app()->user->hasRole(Role::ROLE_HR) || Yii::app()->user->hasRole(Role::ROLE_EMPLOYER)) && $user->company) {
			$this->render(self::VIEW_PATH . '.profile_edit_company', array('company' => $user->company));
		} else {
			$user->birth_date = Yii::app()->dateFormatter->format('dd.MM.yyyy', $user->birth_date != '0000-00-00 00:00:00' ? $user->birth_date : strtotime('01.01.2000'));
			$this->render(self::VIEW_PATH . '.profile_edit_user', array('user' => $user));
		}
	}

	public function actionFavorite() {

		Yii::app()->user->requireAuthorized();

		$model = new Favorite('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Favorite');

		$this->render(self::VIEW_PATH.'.favorite', array('model' => $model->my()));
	}

	public function actionBlacklist() {

		Yii::app()->user->requireAuthorized();

		$c = $this->getController();
		if (@$c->vars[1] && @$c->vars[2] && $c->vars[2] == 'delete') {
			Blacklist::model()->my()->deleteByPk(explode(',', $c->vars[1]));
			Yii::app()->end();
		}

		$model = new Blacklist('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Blacklist');

		$this->render(self::VIEW_PATH.'.blacklist', array('model' => $model->my()));
	}

	/**
	 * remind
	 */
	public function actionRemind() {

		if (!Yii::app()->user->isGuest) {
			$this->controller->redirect('/cabinet/');
			return;
		}

		$post = Yii::app()->request->getPost('RemindForm');
		$model = new RemindForm(@$post['required'] ? null : 'step1');
		$model->unsetAttributes();
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $post;
			if ($model->remind()) {
				if (strpos($model->email, '@') === false) {
					Yii::app()->user->setFlash('success', 'На номер '.$model->email.', в течение десяти минут придет SMS-сообщение с кодом, который необходимо ввести в форму ниже');
					Yii::app()->user->setFlash('info', $model->email);
					$this->getController()->redirect('/remind/reset/');
				} else {
					Yii::app()->user->setFlash('success', 'На e-mail '.$model->email.', в течение десяти минут придет письмо с инструкцией по смене пароля.');
				}
			}
		}

		$this->render(self::VIEW_PATH.'.remind', array('model' => $model));
	}

	/**
	 * reset
	 */
	public function actionReset() {

		if (!Yii::app()->user->isGuest) {
			$this->controller->redirect('/cabinet/');
			return;
		}

		$c = $this->getController();

		$data = Yii::app()->request->getPost('ResetPasswordForm');
		$model = new ResetPasswordForm(@$data['password'] ? 'step2' : null);
		$model->unsetAttributes();
		$model->email = @$c->vars[1];
		$model->resetCode = @$c->vars[2];
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $data;
			if ($model->scenario == 'step2' && $model->reset()) {
				Yii::app()->user->setFlash('success', 'Новый пароль успешно установлен. Теперь вы можете авторизоваться');
				$c->redirect('/site/login/');
			}
			if ($model->validateStep1()) {
				$model->scenario = 'step2';
			}
		}

		$this->render(self::VIEW_PATH.'.reset', array('model' => $model));
	}

	/**
	 * смена пароля
	 */
	public function actionPassword() {

		Yii::app()->user->requireAuthorized();

		$model = new ChangePasswordForm();
		$model->unsetAttributes();
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('ChangePasswordForm');
			if ($model->change()) {
				Yii::app()->user->setFlash('success', 'Вы успешно изменили Ваш пароль! С этого момента вы можете авторизоваться, используя его.');
			}
		}

		$this->render(self::VIEW_PATH.'.password', array('model' => $model));
	}

	/**
	 * unsubscribe
	 */
	public function actionUnsubscribe() {

		$model = new Unsubscribe();
		$model->attributes = Yii::app()->request->getQuery('Unsubscribe');
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('Unsubscribe');
			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'E-mail '.$model->email.' был отписан от рассылок.');
			}
		}
		$this->render(self::VIEW_PATH.'.unsubscribe', array('model' => $model));
	}

	public function actionSubscribe() {

		$u = Yii::app()->request->getQuery('u');
		CommonHelper::autoAuth(Yii::app()->request->getQuery('c'));

		/** @var $model User */
		$model = Yii::app()->user->getModel();
		if (!$model) {
			throw new CHttpException(403, 'Неверная ссылка!');
		}
		if (Yii::app()->request->isPostRequest || $u) {
			$data = Yii::app()->request->getPost('User', array());
			if (is_array(@$data['subscribeIds'])) {
				$model->subscribeIds = array_unique(@$data['subscribeIds']);
				$model->saveSubscribes();
				Yii::app()->user->setFlash('success', 'Изменения сохранены');
			}
			if (array_key_exists('subscribed', $data) || $u) {
				User::model()->updateByPk($model->id, array('subscribed' => @$data['subscribed'] ? 1 : 0));
				Yii::app()->user->setFlash('success', @$data['subscribed'] ? 'Вы подписаны на рассылку уведомлений' : 'Вы отписаны от рассылок уведомлений!');
				SubscribeLog::log($model->id, @$data['subscribed'] ? SubscribeLog::EVENT_SUBSCRIBE : SubscribeLog::EVENT_UNSUBSCRIBE, Yii::app()->request->userHostAddress);
			}
			$this->getController()->redirect('/cabinet/subscribes/');
		}
		$this->render(self::VIEW_PATH.'.subscribe', array('model' => $model));
	}
}
