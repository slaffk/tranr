<?php
/**
 * @var $this TrainerWidget
 * @var $record Trainer
 */
Yii::app()->clientScript->registerLinkTag('canonical', null, $record->url);
Yii::app()->clientScript->registerLinkTag('image_src', null, $record->getThumb(200, 200, 'crop'));
Yii::app()->clientScript->registerMetaProperty('article', 'og:type', null, [], null, true);
Yii::app()->clientScript->registerMetaProperty($record->meta_description, 'og:description', null, [], null, true);
Yii::app()->clientScript->registerMetaProperty($record->url, 'og:url');
Yii::app()->clientScript->registerMetaProperty($record->getThumb(200, 200, 'crop'), 'og:image');
if ($record->meta_keywords) {
	foreach (explode(',', $record->meta_keywords) as $keyword) {
		if (trim($keyword))
			Yii::app()->clientScript->registerMetaProperty(trim($keyword), 'article:tag', null, [], null, true, true);
	}
}
if ($record->category) {
	Yii::app()->clientScript->registerMetaProperty($record->category->name, 'article:section');
}
$labels = $record->attributeLabels();

?>

<div class="thumbnail">
	<div class="col-sm-4 text-center">
		<?php if ($record->tariff && $record->tariff->recommend):?>
			<i class="glyphicon glyphicon-star-empty pull-left" style="position: absolute; margin-top: 10px; color: green" title="Мы рекомендуем этого тренера!"></i>
		<?php elseif ($record->tariff && $record->tariff->verify):?>
			<i class="glyphicon glyphicon-ok pull-left" style="position: absolute; margin-top: 10px; color: green" title="Это проверенный тренер!"></i>
		<?php endif?>
		<?=	CHtml::image($record->getThumb(220, 220, 'crop'), $record->image_alt, array('class' => 'img-circle', 'width' => 220, 'height' => 220))?>
		<div class="caption">
			<p class=""><i class="glyphicon glyphicon-map-marker"></i>
				Россия, <?=$record->city->name?><?=$record->area ? ', ' . $record->area->name : ''?><?=$record->metro ? ', ' . $record->metro->name : ''?>
			</p>
			<?php if ($record->phone):?>
				<p><i class="glyphicon glyphicon-phone"></i> <?=$record->phone?></p>
			<?php endif?>
			<?php if ($record->phone_additional):?>
				<p><i class="glyphicon glyphicon-earphone"></i> <?=$record->phone_additional?></p>
			<?php endif?>
		</div>
	</div>
	<div class="col-sm-8">
		<div class="caption">
			<div class="row">
				<div class="col-xs-8">
					<h4><?=$record->name?></h4>
				</div>
				<?php
					$positive = $record->getReviewAmount(TrainerReview::TYPE_POSITIVE);
					$negative = $record->getReviewAmount(TrainerReview::TYPE_NEGATIVE);
				?>
				<div class="col-xs-4 text-right">
					<h4>
						<a href="#" class="text-muted text-info" title="Положительных отзывов: <?= $positive ?>">
							<i class="glyphicon glyphicon-thumbs-up" style="margin-right: 3px"></i><?= $positive ?></a>
						<a href="#" class="text-muted text-danger" title="Отрицательных отзывов: <?= $negative ?>" style="margin-left: 10px">
							<i class="glyphicon glyphicon-thumbs-down" style="margin-right: 3px"></i><?= $negative ?></a>
					</h4>
				</div>
			</div>


			<?php if ($record->firstname || $record->lastname):?>
				<p class="lead text-capitalize" style="margin-bottom: 10px"><?=$record->firstname?> <?=$record->middlename?> <?=$record->lastname?></p>
			<?php endif?>
			<p style="margin-bottom: 20px">
				<?=CHtml::link(
					'<strong>' . ($record->category->parent ? $record->category->parent->name . ' / ' : '') . $record->category->name . '</strong>',
					$record->category->url,
					array('class' => 'btn btn-xs btn-success disabled')
				)?>
				<?php if (!empty($record->categories)):?>
					<?php foreach ($record->categories as $category):?>
						<?=CHtml::link($category->name, $category->url, array('class' => 'btn btn-xs btn-default disabled'))?>
					<?php endforeach?>
				<?php endif?>
			</p>
			<dl>
				<?php if ($record->edu_level):?>
					<dt>Образование</dt>
					<dd><?=@Trainer::$eduLevels[$record->edu_level]?><?=$record->edu_college ? ', '.$record->edu_college : ''?><?=$record->edu_year ? ', '.$record->edu_year : ''?></dd>
				<?php endif?>
			</dl> 
			<dl>
				<?php if ($record->experience):?>
					<dt>Опыт работы</dt>
					<dd><?=@Trainer::$experiences[$record->experience]?></dd>
				<?php endif?>
			</dl>
			<?php
				$attributes = array('edu_other', 'experience_text', 'additional', 'address', 'vk_link', 'fb_link');
			?>
			<?php foreach($attributes as $attr): ?>
				<?php if($record->$attr != ""): ?>
				<dl>
					<dt><?= $labels[$attr]; ?></dt>
					<dd><?= $record->$attr ?></dd>
				</dl>
				<?php endif; ?>
			<?php endforeach; ?>

			<div class="row">
				<div class="col-xs-8">
					<a href="#" title="Позвонить" onclick="alert('Not implended yet!'); return false;" class="btn btn-default"><i class="glyphicon glyphicon-earphone"></i></a>
					<a href="#sms-dialog" data-toggle="modal"  title="Написать SMS" data-trainer="<?= $record->id; ?>" class="sendSms btn btn-default"><i class="glyphicon glyphicon-send"></i></a>
				</div>

				<div class="col-xs-4">
					<a href="<?=$record->url?>" class="btn btn-link pull-right" onclick="$(this).find(':first-child').toggleClass('glyphicon-heart-empty'); $(this).find(':first-child').toggleClass('glyphicon-heart');return false">
						<i class="glyphicon glyphicon-heart-empty small" style="margin-right: 5px"></i><span class="hidden-xs">Мне нравится</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php $this->widget('application.widgets.CommonWidget', array('action' => 'socialSharing'));?>
