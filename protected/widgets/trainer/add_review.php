<h1>Добавить отзыв</h1>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id' => 'add-review-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'class' => 'form-horizontal'
	),
)); ?>

	<p class="note">Поля со звездочкой <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'type'); ?>
		<?php echo $form->dropDownList($model, 'type', TrainerReview::getTypeList()); ?>
		<?php echo $form->error($model, 'type'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'name'); ?>
		<?php echo $form->textField($model, 'name'); ?>
		<?php echo $form->error($model, 'name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'email'); ?>
		<?php echo $form->textField($model, 'email'); ?>
		<?php echo $form->error($model, 'email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'text'); ?>
		<?php echo $form->textArea($model, 'text'); ?>
		<?php echo $form->error($model, 'text'); ?>
	</div>
	<div class="row">
		<div><?php $this->widget('CCaptcha') ?></div>
		<?php echo $form->labelEx($model, 'verifyCode') ?>
		<?php echo $form->textField($model, 'verifyCode') ?>
		<?php echo $form->error($model, 'verifyCode'); ?>
	</div>
	<?php echo CHtml::submitButton('Добавить отзыв', array('class' => 'btn btn-large')); ?>
<?php $this->endWidget(); ?>
</div>