<?php
/**
 * @var $this TrainerWidget
 * @var $model Trainer
 * @var $category Category
 */
?>

<div class="well well-sm widget-title blue-bg">
	
	<h4><i class="glyphicon glyphicon-link"></i> <?=$category->h1 ? $this->controller->translate($category->h1) : (($category->parent ? $category->parent->name . ' / ' : '') . $category->name)?></h4>
</div>

<?php $this->widget('zii.widgets.CListView', array(
	'id' => 'trainer-list',
	'dataProvider' => $model->search(),
	'itemView' => TrainerWidget::VIEW_PATH . '._trainer',
	'template' => "{items} {pager}",
	'emptyText' => '<p class="text-warning">К сожалению, сейчас в категории &laquo;'.$category->name.'&raquo; пусто :(<br/> Попробуйте сделать запрос позже, или выберите другую категорию в меню справа</p>',
	'pager' => array('class' => 'LinkPager', 'url' => '/'.$this->controller->page->path.'/', 'pageName' => 'Trainer_page'),
	'htmlOptions' => array('class' => ''),
))?>