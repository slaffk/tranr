<?php
/**
 * @var $models Trainer[]
 */
?>

<div class="well well-sm widget-title green-bg">
	<h4><?=$this->caption ? $this->caption : 'Похожие тренеры'?></h4>
</div>

<?php foreach ($models as $data):?>
	<?php $this->render(TrainerWidget::VIEW_PATH . '._trainer_small', array('data' => $data))?>
<?php endforeach?>
