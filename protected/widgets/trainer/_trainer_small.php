<?php
/**
 * @var $data Trainer
 */
?>
<div class="thumbnail">
	<div class="col-sm-12">
		<?php if ($data->tariff && $data->tariff->recommend):?>
			<i class="glyphicon glyphicon-star-empty pull-left" style="position: absolute; margin-top: 10px; color: green" title="Мы рекомендуем этого тренера!"></i>
		<?php elseif ($data->tariff && $data->tariff->verify):?>
			<i class="glyphicon glyphicon-ok pull-left" style="position: absolute; margin-top: 10px; color: green" title="Это проверенный тренер!"></i>
		<?php endif?>

		<h4 class="text-center">
			<?=CHtml::link($data->name, $data->url, array('class' => ''))?>
		</h4>
		<?=CHtml::link(
			CHtml::image($data->getThumb(220, 220, 'crop'), $data->image_alt, array('class' => 'img-circle', 'width' => 220, 'height' => 220)),
			$data->url,
			array('title' => $data->name . ' ' . $data->firstname . ' ' . $data->lastname)
		)?>

		<div class="caption">
			<p style="float: left"><i class="glyphicon glyphicon-map-marker"></i>
				<?=$data->city->name?><?=$data->area ? ', ' . $data->area->name : ''?>
			</p>

			<a href="#" class="text-muted text-danger" title="Отрицательных отзывов: 0" style="margin-left: 10px; float: right;">
				<i class="glyphicon glyphicon-thumbs-down" style="margin-right: 3px"></i>0</a>
			<a href="#" class="text-muted text-info" title="Положительных отзывов: 0" style="float: right;">
				<i class="glyphicon glyphicon-thumbs-up" style="margin-right: 3px"></i>0</a>

			<div class="clearfix"></div>

			<a href="<?=$data->url?>" class="btn btn-default pull-left" title="Смотреть полную анкету">Смотреть</a>

			<a href="<?=$data->url?>" class="btn btn-link pull-right" onclick="$(this).find(':first-child').toggleClass('glyphicon-heart-empty'); $(this).find(':first-child').toggleClass('glyphicon-heart');return false">
				<i class="glyphicon glyphicon-heart-empty small" style="margin-right: 5px"></i><span class="hidden-xs">Мне нравится</span>
			</a>
			<div class="clearfix"></div>

		</div>
	</div>
	<div class="clearfix"></div>
</div>

