<?php
/**
 * @var $this TrainerWidget
 * @var $data Category[]
 */
$active = $this->getController()->model;
if ($active instanceof Trainer) {
	$active = $active->category;
}
if (!($active instanceof Category)) {
	$active = null;
}
?>

<div class="well well-sm widget-title blue-bg">
	<h4><?=$this->caption ? $this->caption : 'Хотите найти тренера?'?></h4>
</div>
<ul class="nav nav-pills nav-stacked sidebar-menu">
	<?php foreach ($data as $category):?>
		<?php if ($active && ($active->id == $category->id || ($active->parent_id == $category->id))):?>
			<li <?=$active->id == $category->id ? 'class="active"' : ''?>>
				<?=CHtml::link(
					CHtml::image($category->getThumb(34, 34, 'crop'), $category->image_alt, array('class' => 'img-circle', 'width' => 34, 'height' => 34)).
						' ' . $category->name . ' <span class="text-muted small">('.$category->trainer_count.' '.CommonHelper::pluralForm($category->trainer_count, 'тренер', 'тренера', 'тренеров').')</span>',
					$category->getUrl()
				)?>
			</li>
			<?php if (!empty($category->childsWithTrainers)):?>
				<ul class="nav nav-pills nav-stacked sidebar-submenu">
					<?php foreach ($category->childsWithTrainers as $child):?>
						<li <?=$active->id == $child->id ? 'class="active"' : ''?>>
							<?=CHtml::link(
								CHtml::image($child->getThumb(34, 34, 'crop'), $child->image_alt, array('class' => 'img-circle', 'width' => 34, 'height' => 34)).
									' ' . $child->name . ' <span class="text-muted small">('.$child->trainer_count.' '.CommonHelper::pluralForm($child->trainer_count, 'тренер', 'тренера', 'тренеров').')</span>',
								$child->getUrl()
							)?>
						</li>
					<?php endforeach?>
				</ul>
			<?php endif?>
		<?php else:?>
			<li>
				<?=CHtml::link(
					CHtml::image($category->getThumb(34, 34, 'crop'), $category->image_alt, array('class' => 'img-circle', 'width' => 34, 'height' => 34)).
						' ' . $category->name . ' <span class="text-muted small">('.$category->trainer_count.' '.CommonHelper::pluralForm($category->trainer_count, 'тренер', 'тренера', 'тренеров').')</span>',
					$category->getUrl()
				)?>
			</li>
		<?php endif?>
	<?php endforeach?>
</ul>
<?php /*
<div class="panel panel-info widget-title">
	<div class="panel-heading">
		<h4><i class="glyphicon glyphicon-link"></i> Хотите найти тренера?</h4>
	</div>
	<div class="panel-body">
		Panel content
	</div>
</div>
*/ ?>
