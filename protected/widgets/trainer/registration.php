<?php
/**
 * @var $model RegistrationForm
 * @var $form CActiveForm
 */
?>
<div class="well well-sm">Регистрация тренера</div>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'add-review-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'class' => 'form-horizontal',
		'role' => 'form',
	),
))?>

<div class="form-group">
	<div class="col-sm-offset-3 col-sm-5">
		<?php echo $form->radioButtonList($model, 'trainerType', Trainer::$trainerTypes);?>
	</div>
	<?php echo $form->error($model, 'trainerType', ['class' => 'col-sm-4 error-box']); ?>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'cityId', ['class' => 'col-sm-3 control-label']); ?>
	<div class="col-sm-5">
		<?php echo $form->dropDownList($model, 'cityId', CHtml::listData(City::model()->root()->findAll(), 'id', 'name'), ['class' => 'form-control']); ?>
	</div>
	<?php echo $form->error($model, 'cityId', ['class' => 'col-sm-4 error-box']); ?>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'categoryId', ['class' => 'col-sm-3 control-label']); ?>
	<div class="col-sm-5">
		<?php echo $form->dropDownList($model, 'categoryId', CHtml::listData(Category::model()->findAll(), 'id', 'dottedName'), ['class' => 'form-control']); ?>
	</div>
	<?php echo $form->error($model, 'categoryId', ['class' => 'col-sm-4 error-box']); ?>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'email', ['class' => 'col-sm-3 control-label']); ?>
	<div class="col-sm-5">
		<?php echo $form->textField($model, 'email', ['class' => 'form-control']); ?>
	</div>
	<?php echo $form->error($model, 'email', ['class' => 'col-sm-4 error-box error-box']); ?>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'password', ['class' => 'col-sm-3 control-label']); ?>
	<div class="col-sm-5">
		<?php echo $form->passwordField($model, 'password', ['class' => 'form-control']); ?>
	</div>
	<?php echo $form->error($model, 'password', ['class' => 'col-sm-4 error-box']); ?>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'passwordConfirm', ['class' => 'col-sm-3 control-label']); ?>
	<div class="col-sm-5">
		<?php echo $form->passwordField($model, 'passwordConfirm', ['class' => 'form-control']); ?>
	</div>
	<?php echo $form->error($model, 'passwordConfirm', ['class' => 'col-sm-4 error-box']); ?>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'firstname', ['class' => 'col-sm-3 control-label']); ?>
	<div class="col-sm-5">
		<?php echo $form->textField($model, 'firstname', ['class' => 'form-control']); ?>
	</div>
	<?php echo $form->error($model, 'firstname', ['class' => 'col-sm-4 error-box']); ?>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'lastname', ['class' => 'col-sm-3 control-label']); ?>
	<div class="col-sm-5">
		<?php echo $form->textField($model, 'lastname', ['class' => 'form-control']); ?>
	</div>
	<?php echo $form->error($model, 'lastname', ['class' => 'col-sm-4 error-box']); ?>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'phone', ['class' => 'col-sm-3 control-label']); ?>
	<div class="col-sm-5">
		<?php echo $form->textField($model, 'phone', ['class' => 'form-control']); ?>
	</div>
	<?php echo $form->error($model, 'phone', ['class' => 'col-sm-4 error-box']); ?>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'captcha', ['class' => 'col-sm-3 control-label']); ?>
	<div class="col-sm-3">
		<?php $this->widget('CCaptcha', array('showRefreshButton' => false));?>
		<a href="#" class="captcha-btn"><i class="glyphicon glyphicon-refresh"></i></a>
	</div>
	<div class="col-sm-2">
		<?php echo $form->textField($model, 'captcha', ['class' => 'form-control']); ?>
	</div>
	<?php echo $form->error($model, 'captcha', ['class' => 'col-sm-4 error-box']); ?>
</div>
<script type="text/javascript">
	$('a.captcha-btn').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$.getJSON('/site/captcha?refresh=1', function(data) {
			$this.parent().find('img').attr('src', data.url);
		});
	});
</script>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-5">
		<?php echo $form->checkBox($model, 'agree');?>
		<?php echo $form->label($model, 'agree');?>
	</div>
	<?php echo $form->error($model, 'agree', ['class' => 'col-sm-4 error-box']); ?>
</div>

<?php echo CHtml::submitButton('Зарегистрироваться', array('class' => 'btn btn-large btn-success')); ?>

<?php $this->endWidget(); ?>
