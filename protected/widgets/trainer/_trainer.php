<?php
/**
 * @var $data Trainer
 * @var $charCount integer
 */
if (!isset($charCount)) {
	$charCount = 500;
}
?>

<div class="thumbnail">
	<div class="col-sm-4 text-center">
		<?php if ($data->tariff && $data->tariff->recommend):?>
			<i class="glyphicon glyphicon-star-empty pull-left" style="position: absolute; margin-top: 10px; color: green" title="Мы рекомендуем этого тренера!"></i>
		<?php elseif ($data->tariff && $data->tariff->verify):?>
			<i class="glyphicon glyphicon-ok pull-left" style="position: absolute; margin-top: 10px; color: green" title="Это проверенный тренер!"></i>
		<?php endif?>
		<?=CHtml::link(
			CHtml::image($data->getThumb(220, 220, 'crop'), $data->image_alt, array('class' => 'img-circle', 'width' => 220, 'height' => 220)),
			$data->url,
			array('title' => $data->name . ' ' . $data->firstname . ' ' . $data->lastname)
		)?>
		<div class="caption">
			<p class=""><i class="glyphicon glyphicon-map-marker"></i>
				Россия, <?=$data->city->name?><?=$data->area ? ', ' . $data->area->name : ''?><?=$data->metro ? ', ' . $data->metro->name : ''?>
			</p>
			<?php if ($data->phone):?>
				<p><i class="glyphicon glyphicon-phone"></i> <?=$data->phone?></p>
			<?php endif?>
			<?php if ($data->phone_additional):?>
				<p><i class="glyphicon glyphicon-earphone"></i> <?=$data->phone_additional?></p>
			<?php endif?>
		</div>
	</div>
	<div class="col-sm-8">
		<div class="caption">
			<div class="row">
				<div class="col-xs-8">
					<h4><?=CHtml::link($data->name, $data->url, array('class' => ''))?></h4>
				</div>

				<div class="col-xs-4 text-right">
					<h4>
						<a href="#" class="text-muted text-info" title="Положительных отзывов: 0">
							<i class="glyphicon glyphicon-thumbs-up" style="margin-right: 3px"></i>0</a>
						<a href="#" class="text-muted text-danger" title="Отрицательных отзывов: 0" style="margin-left: 10px">
							<i class="glyphicon glyphicon-thumbs-down" style="margin-right: 3px"></i>0</a>
					</h4>
				</div>
			</div>


			<?php if ($data->firstname || $data->lastname):?>
				<p class="lead text-capitalize" style="margin-bottom: 10px"><?=$data->firstname?> <?=$data->middlename?> <?=$data->lastname?></p>
			<?php endif?>
			<p style="margin-bottom: 20px">
				<?=CHtml::link(
					'<strong>' . ($data->category->parent ? $data->category->parent->name . ' / ' : '') . $data->category->name . '</strong>',
					$data->category->url,
					array('class' => 'btn btn-xs btn-success disabled')
				)?>
				<?php if (!empty($data->categories)):?>
					<?php foreach ($data->categories as $category):?>
						<?=CHtml::link($category->name, $category->url, array('class' => 'btn btn-xs btn-default disabled'))?>
					<?php endforeach?>
				<?php endif?>
			</p>
			<dl>
			<?php if ($data->edu_level):?>
				<dt>Образование</dt>
				<dd><?=@Trainer::$eduLevels[$data->edu_level]?><?=$data->edu_college ? ', '.$data->edu_college : ''?><?=$data->edu_year ? ', '.$data->edu_year : ''?></dd>
			<?php endif?>
			</dl>
			<dl>
			<?php if ($data->experience):?>
				<dt>Опыт работы</dt>
				<dd><?=@Trainer::$experiences[$data->experience]?></dd>
			<?php endif?>
			</dl>
			<div class="row">
				<div class="col-xs-8">
					<a href="#" title="Позвонить" onclick="alert('Not implended yet!'); return false;" class="btn btn-default"><i class="glyphicon glyphicon-earphone"></i></a>
					<a href="#sms-dialog" data-toggle="modal" title="Написать SMS"  data-trainer="<?= $data->id; ?>" class="sendSms btn btn-default"><i class="glyphicon glyphicon-send"></i></a>
					<a href="<?=$data->url?>" class="btn btn-default" title="Смотреть полную анкету">...</a>
				</div>

				<div class="col-xs-4">
					<a href="<?=$data->url?>" class="btn btn-link pull-right" onclick="$(this).find(':first-child').toggleClass('glyphicon-heart-empty'); $(this).find(':first-child').toggleClass('glyphicon-heart');return false">
						<i class="glyphicon glyphicon-heart-empty small" style="margin-right: 5px"></i><span class="hidden-xs">Мне нравится</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

