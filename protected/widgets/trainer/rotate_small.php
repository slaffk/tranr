<?php
/**
 * @var $this TrainerWidget
 * @var $models Trainer[]
 */
?>
<?php foreach ($models as $data):?>
	<?php $this->render(TrainerWidget::VIEW_PATH . '._trainer_small', array('data' => $data))?>
<?php endforeach?>
