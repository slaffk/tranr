<?php
/**
 * @var $this TrainerWidget
 * @var $model Trainer
 */
?>

<?php $this->widget('zii.widgets.CListView', array(
	'id' => 'trainer-list',
	'dataProvider' => $model->search(),
	'itemView' => TrainerWidget::VIEW_PATH . '._trainer',
	'template' => "{items} {pager}",
	'emptyText' => 'Ничего не найдено',
	'pager' => array('class' => 'LinkPager', 'url' => '/'.$this->controller->page->path.'/', 'pageName' => 'Trainer_page'),
	'htmlOptions' => array('class' => ''),
));
?>

<?php if (array_key_exists('content', Yii::app()->request->getQuery('Trainer', array()))):?>
	<script type="text/javascript">
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('js/jquery.highlight.js'));?>
		var keyword = '<?=Yii::app()->request->getQuery('Trainer', array())['content']?>';
		$('#trainer-list').highlight(keyword);
	</script>
<?php endif?>
