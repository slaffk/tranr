<?php
/**
 * Class ArticleWidget
 */
class ArticleWidget extends AppWidget {

	const VIEW_PATH = 'application.widgets.article';

	/**
	 * @return string
	 */
	public static function getName() {

		return 'Статьи';
	}

	/**
	 * @return array
	 */
	public static function getActionList() {

		return array(
			'view' => 'Просмотр статьи (контент)',
			'index' => 'Список статей (контент)',
			'category' => 'Дерево категорий (сайдбар)',
			'new' => 'Полезные статьи (сайдбар)',
			'last' => 'Новые статьи (сайдбар)',
			'related' => 'Похожие статьи (сайдбар)',
			'relatedCategory' => 'Статьи категории (сайдбар)',
		);
	}

	/**
	 * @throws CHttpException
	 */
	public function actionView() {

		/** @var $c Controller */
		$c = $this->getController();

		$id = count($c->vars) == 4 ? $c->vars[2] : @$c->vars[3];
		$alias = count($c->vars) == 4 ? $c->vars[3] : @$c->vars[4];
		$categoryAlias = count($c->vars) == 4 ? $c->vars[1] : @$c->vars[2];
		$rootAlias = count($c->vars) == 4 ? null : @$c->vars[1];

		/** @var $article Article */
		$article = Article::model()->findByPk($id);
		if (!$article || $article->alias != $alias) {
			throw new CHttpException(404, 'Статья не найдена');
		}

		if ($article->category->parent) {
			if ($article->category->parent->alias != $rootAlias || $article->category->alias != $categoryAlias)
				throw new CHttpException(404, 'Статья не найдена');
		} else {
			if ($article->category->alias != $categoryAlias)
				throw new CHttpException(404, 'Статья не найдена');
		}

		/* крохи */
		$c->breadcrumbs = array('Статьи' => '/articles/');
		if ($article->category) {
			if ($article->category->parent) {
				$c->breadcrumbs[$article->category->parent->name] = $article->category->parent->getArticleUrl();
			}
			$c->breadcrumbs[$article->category->name] = $article->category->getArticleUrl();
		}
		$c->breadcrumbs[] = $article->title;

		/* немного SEO */
		$c->pageTitle = $article->meta_title ? $article->meta_title : $article->title;
		if ($article->meta_description) {
			Yii::app()->clientScript->registerMetaTag($article->meta_description, 'description');
		}
		if ($article->meta_keywords) {
			Yii::app()->clientScript->registerMetaTag($article->meta_keywords, 'keywords');
		}

		/* запоминаем что у нас открыто */
		$c->model = $article;
		$c->filterIds = array_merge($c->filterIds, array($article->id));

		$this->render(self::VIEW_PATH.'.view', array(
			'record' => $article,
		));
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new Article();
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Article');

		/** @var $c Controller */
		$c = $this->getController();

		if (@$c->vars[1]) {

			$categoryName = @$c->vars[2] ? $c->vars[2] : $c->vars[1];
			/** @var $category Category */
			$category = Category::model()->find('category.alias = :alias', array('alias' => $categoryName));
			if (!$category || ($category->parent && $category->parent->alias != $c->vars[1])) {
				throw new CHttpException(404, 'Категория статей не найдена');
			}
			$this->controller->breadcrumbs = array('Статьи' => '/articles/');
			if ($category->parent) {
				$this->controller->breadcrumbs[$category->parent->name] = $category->parent->getArticleUrl();
			}
			$this->controller->breadcrumbs[] = $category->name;

			$model->category_ids = array($category->id);
			if ($category->childs) {
				$model->category_ids = array_merge($model->category_ids, array_keys(CHtml::listData($category->childs, 'id', 'id')));
			}

			if ($category->a_meta_description) {
				Yii::app()->clientScript->registerMetaTag($category->a_meta_description, 'description', null, array(), null, true);
			}
			if ($category->a_meta_keywords) {
				Yii::app()->clientScript->registerMetaTag($category->a_meta_keywords, 'keywords', null, array(), null, true);
			}
			$c->pageTitle = $category->a_meta_title ? $category->a_meta_title : $c->pageTitle;

			$c->model = $category;

			$this->render(self::VIEW_PATH.'.category', array(
				'category' => $category,
				'model' => $model->published()->regional(),
			));
			return;
		}
		$c->breadcrumbs = array(
			'Статьи',
		);
		$this->render(self::VIEW_PATH.'.index', array(
			'model' => $model->published()->regional(),
		));
	}

	/**
	 * index
	 */
	public function actionCategory() {

		$this->render(self::VIEW_PATH.'.category_tree', array(
			'data' => Category::model()->root()->withArticles()->findAll(),
		));
	}

	public function actionNew() {

		/** @var $c Controller */
		$c = $this->getController();
		$articles = Article::model()->published()->regional()->notInArray(array('id' => $c->filterIds))->limit3()->findAll();
		$c->filterIds = array_merge($c->filterIds, array_keys(CHtml::listData($articles, 'id', 'id')));
		$this->render(self::VIEW_PATH.'.recent', array(
			'articles' => $articles,
		));
	}

	public function actionLast() {

		/** @var $c Controller */
		$c = $this->getController();
		$articles = Article::model()->published()->regional()->notInArray(array('id' => $c->filterIds))->limit3()->random()->findAll();
		$c->filterIds = array_merge($c->filterIds, array_keys(CHtml::listData($articles, 'id', 'id')));
		$this->render(self::VIEW_PATH.'.recent', array(
			'articles' => $articles,
		));
	}

	/**
	 * related похожие статьи
	 */
	public function actionRelated() {

		/** @var $c Controller */
		$c = $this->getController();
		if (!($c->model instanceof ActiveRecord)) {
			return;
		}
		$articles = $c->model->getRelatedByTags('Article', 3, array('not_in' => $c->filterIds));
		if (!empty($articles)) {
			$c->filterIds = array_merge($c->filterIds, array_keys(CHtml::listData($articles, 'id', 'id')));
			$this->render(self::VIEW_PATH.'.recent', array(
				'articles' => $articles
			));
		}
	}

	/**
	 * related похожие статьи в категории
	 */
	public function actionRelatedCategory() {

		/** @var $c Controller */
		$c = $this->getController();
		if (!($c->model instanceof Category)) {
			return;
		}
		$ids = array_merge([$c->model->id], CHtml::listData($c->model->childs, 'id', 'id'));
		$articles = Article::model()->published()->notInArray(array('id' => $c->filterIds))->limit(5)->random()->findAllByAttributes(['category_id' => $ids]);
		if (!empty($articles)) {
			$c->filterIds = array_merge($c->filterIds, array_keys(CHtml::listData($articles, 'id', 'id')));
			$this->render(self::VIEW_PATH.'.recent_category', array(
				'articles' => $articles,
				'category' => $c->model,
			));
		}
	}

}
