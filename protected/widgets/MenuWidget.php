<?php
/**
 * Class MenuWidget
 */
class MenuWidget extends AppWidget {

	const VIEW_PATH = 'application.widgets.menu';

	public static $adminMenu = array(
		'Глобально' => array(
			'/manager/city/index/' => 'Города',
			'/manager/area/index/' => 'Районы',
			'/manager/metro/index/' => 'Метро',
			'/manager/resource/index/' => 'Ресурсы',
			'/manager/role/index/' => 'Роли',
			'/manager/user/index/' => 'Юзеры',
//			'/manager/mailTemplate/index/' => 'Письма',
			'/manager/file/index/' => 'Файлы',
			'/manager/page/index/' => 'Страницы',
			'/manager/category/index/' => 'Виды спорта',
			'/manager/tariff/index/' => 'Тарифы',
			'/manager/trainer/index/' => 'Тренеры',
			'/manager/article/index/' => 'Статьи',
//			'/manager/review/index/' => 'Отзывы',
			//'/manager/news/index/' => 'Новости',
		),
	);

	public static $trainerMenu = array(
		'trainer' => array(
			'/cabinet/trainer/index' => '<i class="glyphicon glyphicon-user"></i> Кабинет тренера',
			'/cabinet/photo/index' => '<i class="glyphicon glyphicon-picture"></i> Мои фото',
			'/cabinet/video/index' => '<i class="glyphicon glyphicon-facetime-video"></i> Мои видео',
//			'/cabinet/rotate/index' => '<i class="glyphicon glyphicon-thumbs-up"></i> Отзывы',
		),
	);

	/**
	 * @return string
	 */
	public static function getName() {

		return 'Меню';
	}

	/**
	 * @return array
	 */
	public static function getActionList() {

		return array(
			'main' => 'Верхнее меню',
			'admin' => 'Меню администратора',
			'trainer' => 'Меню тренера',
		);
	}

	/**
	 * main
	 */
	public function actionMain() {

		$this->render(self::VIEW_PATH.'.top');
	}

	/**
	 * admin
	 */
	public function actionAdmin() {

		Yii::app()->user->requireAuthorized();
		if (!Yii::app()->user->checkAcl('manager', 'default')) {
			throw new CHttpException(403);
		}
		$this->render(self::VIEW_PATH.'.admin');
	}

	/**
	 * trainer
	 */
	public function actionTrainer() {

		Yii::app()->user->requireAuthorized();
		if (!Yii::app()->user->checkAcl('trainer', 'trainer')) {
			//throw new CHttpException(403);
		}
		$this->render(self::VIEW_PATH.'.trainer');
	}
}
