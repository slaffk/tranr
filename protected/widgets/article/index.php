<?php
/**
 * @var $this ArticleWidget
 * @var $model Article
 */
?>

<?php if ($this->caption):?>
	<h1><?=$this->caption?></h1>
<?php endif?>

<?php $this->widget('zii.widgets.CListView', array(
	'id' => 'article-list',
	'dataProvider' => $model->search(),
	'itemView' => ArticleWidget::VIEW_PATH . '._article',
	'template' => "{items} {pager}",
	'emptyText' => 'Ничего не найдено',
	'pager' => array('class' => 'LinkPager', 'url' => '/'.$this->controller->page->path.'/', 'pageName' => 'Article_page'),
	'htmlOptions' => array('class' => ''),
));
?>

<?php if (array_key_exists('content', Yii::app()->request->getQuery('Article', array()))):?>
	<script type="text/javascript">
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('js/jquery.highlight.js'));?>
		var keyword = '<?=Yii::app()->request->getQuery('Article', array())['content']?>';
		$('#article-list').highlight(keyword);
	</script>
<?php endif?>
