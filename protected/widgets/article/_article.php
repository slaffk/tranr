<?php
/**
 * @var $data Article
 * @var $charCount integer
 */
if (!isset($charCount)) {
	$charCount = 500;
}
?>

<div class="preview clearfix">
	<p class="title"><?=CHtml::link($data->title, $data->url)?></p>

	<?php if ($data->category):?>
		<span class="label label-info slim sticker">
			<?=$data->category->name?>
		</span>
	<?php endif?>
	<img class="pull-left img-thumbnail margin-right" width="120" height="120" src="<?=$data->getThumb(120, 120, 'crop');?>" alt="<?=$data->image_alt?>">

	<p><?=CommonHelper::truncate(strip_tags($data->content), $charCount)?></p>

	<?php /* if ($data->tags):?>
		<p class="article_tags">теги:
			<?php foreach ($data->tags as $tag):?>
				<a href="#"><?=$tag->name?></a>
			<?php endforeach?>
		</p>
	<?php endif */ ?>
</div>
