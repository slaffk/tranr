<?php
/**
 * @var $this ArticleWidget
 * @var $record Article
 */
Yii::app()->clientScript->registerLinkTag('canonical', null, $record->url);
Yii::app()->clientScript->registerLinkTag('image_src', null, $record->getThumb(200, 200, 'crop'));
Yii::app()->clientScript->registerMetaProperty('article', 'og:type', null, [], null, true);
Yii::app()->clientScript->registerMetaProperty($record->meta_description, 'og:description', null, [], null, true);
Yii::app()->clientScript->registerMetaProperty($record->url, 'og:url');
Yii::app()->clientScript->registerMetaProperty($record->getThumb(200, 200, 'crop'), 'og:image');
if ($record->meta_keywords) {
	foreach (explode(',', $record->meta_keywords) as $keyword) {
		if (trim($keyword))
			Yii::app()->clientScript->registerMetaProperty(trim($keyword), 'article:tag', null, [], null, true, true);
	}
}
if ($record->category) {
	Yii::app()->clientScript->registerMetaProperty($record->category->name, 'article:section');
}
?>

<div class="clearfix">
	<h1><?=$record->title?></h1>

	<?php if ($record->category):?>
		<span class="label label-info slim sticker sticker-big">
			<?=$record->category->name?>
		</span>
	<?php endif?>
	<img class="pull-left img-thumbnail margin-right" width="200" height="200" src="<?=$record->getThumb(200, 200, 'crop');?>" alt="<?=$record->image_alt?>">

	<?=$record->content?>

	<?php if ($record->tags):?>
		<p class="article_tags">
			Теги: <a><strong><?=implode('</strong></a>, <a><strong>', CHtml::listData($record->tags, 'name', 'name'))?></strong></a>
		</p>
	<?php endif ?>
</div>
<?php $this->widget('application.widgets.CommonWidget', array('action' => 'socialSharing'));?>
