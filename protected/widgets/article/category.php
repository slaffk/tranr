<?php
/**
 * @var $this ArticleWidget
 * @var $model Article
 * @var $category Category
 */
?>
<h1>
	<?php if ($category->image):?>
		<?=CHtml::image($category->getThumb(34, 34, 'crop'), $category->image_alt, array('class' => 'img-circle bordered lightblue-border', 'width' => 34, 'height' => 34))?>
	<?php endif?>
	<?=$category->a_h1 ? $this->controller->translate($category->a_h1) : 'Статьи на тему &laquo;' . $category->name . '&raquo;'?>
</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'id' => 'article-list',
	'dataProvider' => $model->search(),
	'itemView' => ArticleWidget::VIEW_PATH . '._article',
	'template' => "{items} {pager}",
	'emptyText' => '<p class="text-warning">К сожалению, сейчас в категории &laquo;'.$category->name.'&raquo; пусто :(<br/> Попробуйте сделать запрос позже, или выберите другую категорию в меню справа</p>',
	'pager' => array('class' => 'LinkPager', 'url' => '/'.$this->controller->page->path.'/', 'pageName' => 'Article_page'),
	'htmlOptions' => array('class' => ''),
))?>