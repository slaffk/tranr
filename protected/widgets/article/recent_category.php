<?php
/**
 * @var $category Category
 * @var $articles Article[]
 */
?>

<div class="well well-sm widget-title green-bg">
	<h4><i class="glyphicon glyphicon-thumbs-up"></i> Советы &mdash; &laquo;<?=$category->name?>&raquo;</h4>
</div>

<?php foreach ($articles as $article):?>
	<?php $this->render('application.widgets.article._article', array(
		'data' => $article,
		'charCount' => 250,
	))?>
<?php endforeach?>
