<?php
/**
 * @var $this ArticleWidget
 * @var $data Category[]
 */
$active = $this->getController()->model;
if ($active instanceof Article) {
	$active = $active->category;
}
if (!($active instanceof Category)) {
	$active = null;
}
?>

<div class="well well-sm widget-title green-bg">
	<h4><?=$this->caption ? $this->caption : 'У нас много статей :)'?></h4>
</div>
<ul class="nav nav-pills nav-stacked sidebar-menu">
	<?php foreach ($data as $category):?>
		<?php if ($active && ($active->id == $category->id || ($active->parent_id == $category->id))):?>
			<li <?=$active->id == $category->id ? 'class="active"' : ''?>>
				<?=CHtml::link(
					CHtml::image($category->getThumb(34, 34, 'crop'), $category->image_alt, array('class' => 'img-circle', 'width' => 34, 'height' => 34)).
						' ' . $category->name . ' <span class="text-muted small">('.$category->article_count.' '.CommonHelper::pluralForm($category->article_count, 'статья', 'статьи', 'статей').')</span>',
					$category->getArticleUrl()
				)?>
			</li>
			<?php if (!empty($category->childsWithArticles)):?>
				<ul class="nav nav-pills nav-stacked sidebar-submenu">
					<?php foreach ($category->childsWithArticles as $child):?>
						<li <?=$active->id == $child->id ? 'class="active"' : ''?>>
							<?=CHtml::link(
								CHtml::image($child->getThumb(34, 34, 'crop'), $child->image_alt, array('class' => 'img-circle', 'width' => 34, 'height' => 34)).
									' ' . $child->name . ' <span class="text-muted small">('.$child->article_count.' '.CommonHelper::pluralForm($child->article_count, 'статья', 'статьи', 'статей').')</span>',
								$child->getArticleUrl()
							)?>
						</li>
					<?php endforeach?>
				</ul>
			<?php endif?>
		<?php else:?>
			<li>
				<?=CHtml::link(
					CHtml::image($category->getThumb(34, 34, 'crop'), $category->image_alt, array('class' => 'img-circle', 'width' => 34, 'height' => 34)).
						' ' . $category->name . ' <span class="text-muted small">('.$category->article_count.' '.CommonHelper::pluralForm($category->article_count, 'статья', 'статьи', 'статей').')</span>',
					$category->getArticleUrl()
				)?>
			</li>
		<?php endif?>
	<?php endforeach?>
</ul>
