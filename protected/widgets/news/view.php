<?php
/**
 * @var $this NewsWidget
 * @var $record News
 */
?>
<div class="plain_content">
	<h1><?=$record->title?></h1>
	<p class="gray_date"><?=Yii::app()->dateFormatter->format('dd MMMM yyyy', $record->date)?></p>
	<br/>
	<?php if ($record->image):?>
		<img src="<?=$record->getThumb()?>" alt="<?=$record->image_alt?>" style="float: left; margin-right: 10px;"/>
	<?php endif;?>
	<?=$record->content?>
	<?php if ($record->tags):?>
		<p class="article_tags" style="margin-top: 10px">теги:
			<?php foreach ($record->tags as $tag):?>
				<a href="#"><?=$tag->name?></a>
			<?php endforeach?>
		</p>
	<?php endif?>

	<div class="clearfix"></div>
	<br/>
	<?php $this->widget('application.widgets.CommonWidget', array('action' => 'socialSharing'));?>
</div>