<?php
/**
 * @var $this NewsWidget
 * @var $data NewsCategory[]
 */
?>

<div class="left_content_left">
	<div class="left_content_border" style="background-color: transparent">
		<ul>
			<li><p>Новости</p></li>
			<?php foreach ($data as $category):?>
				<?php if (!empty($category->news)):?>
					<li><a href="<?=$category->url?>"><?=$category->name?></a></li>
				<?php endif?>
			<?php endforeach?>
		</ul>
	</div>
</div>
