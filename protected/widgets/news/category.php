<?php
/**
 * @var $this NewsWidget
 * @var $category NewsCategory
 */
?>
<?php if ($category->h1):?>
	<h1><?=$category->h1?></h1>
<?php endif?>
<div class="article_work article_work_top">
	<?php foreach ($category->news as $record):?>
		<?php $this->render(NewsWidget::VIEW_PATH . '._news', array('record' => $record));?>
	<?php endforeach;?>
</div>
