<?php
/**
 * @var $this NewsWidget
 * @var $records News[]
 */
?>

<div class="article_work article_work_top">
	<?php foreach ($records as $record):?>
		<?php $this->render(NewsWidget::VIEW_PATH . '._news', array('record' => $record));?>
	<?php endforeach;?>
</div>
