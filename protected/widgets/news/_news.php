<?php
/**
 * @var $record News
 */
?>

<p style="clear: left; font-weight: bold; margin-top: 10px; margin-bottom: 10px">
	<a href="<?=$record->getUrl()?>"><?=$record->title?></a>
</p>
<p class="gray_date"><?=Yii::app()->dateFormatter->format('dd MMMM yyyy', $record->date)?></p>
<?php if ($record->image):?>
	<img class="fl" src="<?=$record->getThumb(100, 100, 'crop');?>">
<?php endif?>
<p><?=CommonHelper::truncate(strip_tags($record->content), 300)?></p>
<?php if ($record->tags):?>
	<p class="article_tags">теги:
		<?php foreach ($record->tags as $tag):?>
			<a href="#"><?=$tag->name?></a>
		<?php endforeach?>
	</p>
<?php endif?>
<div class="empty"></div>
<br/>