<?php
/**
 * Class NewsWidget
 */
class NewsWidget extends AppWidget {

	const VIEW_PATH = 'application.widgets.news';

	/**
	 * @return string
	 */
	public static function getName() {

		return 'Новости';
	}

	/**
	 * @return array
	 */
	public static function getActionList() {

		return array(
			'view' => 'Просмотр новости (контент)',
			'index' => 'Список новостей (контент)',
			'category' => 'Дерево категорий (сайдбар)',
		);
	}

	/**
	 * @param News $model
	 */
	private function makeMetaTags($model) {

		if (!($model instanceof News)) {
			return;
		}
		$c = $this->getController();

		$c->pageTitle = ($model->meta_title ? $model->meta_title : $model->title) . ' | ' .
			Yii::app()->params['title'] . ' ' . Yii::app()->city->name_pril_where;

		Yii::app()->clientScript->registerMetaTag(
			$model->meta_keywords ? $model->meta_keywords : CommonHelper::truncate($model->content, 250),
			'keywords'
		);
		Yii::app()->clientScript->registerMetaTag(
			$model->meta_description ? $model->meta_description : CommonHelper::truncate($model->content, 250),
			'description'
		);
	}

	/**
	 * @throws CHttpException
	 */
	public function actionView() {

		/** @var $c Controller */
		$c = $this->getController();

		/** @var $news News */
		$news = News::model()->findByPk(@array_pop(explode('-', @$c->vars[2])));
		if (!$news) {
			throw new CHttpException(404, 'Новость не найдена');
		}
		$this->makeMetaTags($news);
		$this->controller->breadcrumbs = array(
			'Работа ' . Yii::app()->city->name_pril_where => '/',
			'Новости' => '/news/',
			$news->title,
		);
		$this->render(self::VIEW_PATH.'.view', array(
			'record' => $news,
		));
	}

	/**
	 * index
	 */
	public function actionIndex() {

		/** @var $c Controller */
		$c = $this->getController();

		if (@$c->vars[1]) {
			//Листинг категории новостей
			/** @var $category NewsCategory */
			$category = NewsCategory::model()->with('parent')->together()->find('concat(parent.alias, "-", t.alias) = :alias', array('alias' => $c->vars[1]));
			if (!$category) {
				$category = NewsCategory::model()->find('t.alias = :alias', array('alias' => $c->vars[1]));
			}
			if (!$category) {
				throw new CHttpException(404, 'Категория новостей не найдена');
			}

			if ($category->meta_description) {
				Yii::app()->clientScript->registerMetaTag($category->meta_description, 'description');
			}
			if ($category->meta_keywords) {
				Yii::app()->clientScript->registerMetaTag($category->meta_description, 'keywords');
			}

			if ($category->news) {
				$this->makeMetaTags(@$category->news[0]);
			} else {
				$c->pageTitle = 'Нет новостей';
			}
			$c->pageTitle = $category->meta_title ? $category->meta_title : $c->pageTitle;
			$this->controller->breadcrumbs = array(
				'Работа ' . Yii::app()->city->name_pril_where => '/',
				'Новости' => '/news/',
				$category->name,
			);
			$this->render(self::VIEW_PATH.'.category', array('category' => $category));
			return;
		}
		$this->controller->breadcrumbs = array(
			'Работа ' . Yii::app()->city->name_pril_where => '/',
			'Новости',
		);
		$records = News::model()->published()->findAll();
		if (@$records[0]) $this->makeMetaTags(@$records[0]);
		$this->render(self::VIEW_PATH.'.index', array('records' => $records));
	}

	/**
	 * index
	 */
	public function actionCategory() {

		$this->render(self::VIEW_PATH.'.category_tree', array(
			'data' => NewsCategory::model()->root()->findAll()
		));
	}
}
