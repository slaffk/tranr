<?php
$this->widget('zii.widgets.CMenu', array(
	'items' => array(
		array('label'=> 'Подбор тренера', 'url' => '/', 'itemOptions' => array('class' => 'active')),
		array('label'=> 'Статьи', 'url' => '/', 'itemOptions' => array()),
	),
	'htmlOptions' => array('class' => 'nav'),
));

$this->widget('zii.widgets.CMenu', array(
	'items' => array(
		array('label'=> 'Админка', 'url' => '/manager/page/index', 'itemOptions' => array('class' => '')),
		array('label'=> 'Войти', 'url' => '/site/login', 'itemOptions' => array('class' => '')),
	),
	'htmlOptions' => array('class' => 'nav pull-right'),
));
