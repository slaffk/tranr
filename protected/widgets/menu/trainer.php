	<?php $count = 0;?>
	<?php foreach(MenuWidget::$trainerMenu as $menu):?>
		<?php $count++?>
		<ul class="nav nav-tabs">
			<?php foreach($menu as $url => $title):?>
				<?php if (Yii::app()->user->checkAclInArray(array($url => $url))):?>
					<li <?=UriHelper::controller_exists(Yii::app()->request->getRequestUri(), array($url => $url)) ? 'class="active"' : ''?>>
						<a href="<?=$url?>" title="<?=strip_tags($title)?>"><?=$title?></a>
					</li>
				<?php endif?>
			<?php endforeach?>
		</ul>
	<?php endforeach?>
