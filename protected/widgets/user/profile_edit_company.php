<?php
/**
 * @var $this UserWidget
 * @var $company Company
 * @var $form CActiveForm
 * @var $formId string
 */
?>
<div class="wide form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'profile-form',
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)); ?>
	<h1>Профиль компании</h1>
	<div class="row">
		<?php echo $form->labelEx($company,'name'); ?>
		<?php echo $form->textField($company, 'name', array('placeholder' => 'Напишите название вашей компании'))?>
		<?php echo $form->error($company,'name'); ?>
	</div>

	<?php if ($company->company_type == Company::TYPE_COMPANY):?>
	<style>
		#Company_category_ids {
			width: 336px;
			height: 200px;
			overflow-y: scroll;
		}
	</style>
	<?php /**
	<div class="row">
		<?php echo $form->labelEx($company,'category_id'); ?>
		<?php echo $form->dropDownList($company, 'category_id', [null => 'Не выбрано'] + CHtml::listData(EmployerCategory::model()->findAll(), 'id', 'name'));?>
		<?php echo $form->error($company,'category_id'); ?>
	</div>
	*/ ?>
	<div class="row radio specs">
		<?php echo $form->labelEx($company,'category_ids'); ?>
		<?php echo $form->checkBoxList($company, 'category_ids', CHtml::listData(EmployerCategory::model()->findAll(), 'id', 'name'));?>
		<?php echo $form->error($company,'category_ids'); ?>
	</div>
	<script>
		$('.specs input').change(function(e) {
			if ($(this).attr('checked') && $('.specs input:checked').length > 10) {
				$(this).removeAttr('checked');
				alert('Вы можете выбрать максимум 10 дополнительных рубрик');
			}
		});
	</script>
	<?php endif?>

	<div class="row">
		<?php echo $form->labelEx($company,'ownership_type'); ?>
		<?php echo $form->dropDownList($company, 'ownership_type', Company::$ownershipTypes);?>
		<?php echo $form->error($company,'ownership_type'); ?>
	</div>

	<?php /*
	<div class="row">
		<?php echo $form->labelEx($company,'company_type'); ?>
		<span class="input_edit" style="width: 143px" id="span_company_type"><?php echo @Company::$companyTypes[$company->company_type]?></span>
		<?php echo $form->dropDownList($company, 'company_type', array(null => 'Соискатель') + Company::$companyTypes, array('style' => 'display: none; width: 150px;'));?>
		&nbsp;<a class="status-btn" href="#">сменить?</a>
		<?php echo $form->error($company,'company_type'); ?>
	</div>
	<script>
		$('.status-btn').click(function(e){
			e.preventDefault();
			if ($('#Company_company_type').is(':visible')) {
				$('#Company_company_type').hide();
				$('#span_company_type').show();
				$(this).text('сменить?');
				alert('Спасибо, ваша заявка будет расммотрена модератором.');
			} else {
				$('#Company_company_type').show();
				$('#span_company_type').hide();
				$(this).text('СОХРАНИТЬ');
			}
		});
	</script>
	*/ ?>
	<div class="row">
		<?php echo $form->labelEx($company,'image'); ?>
		<?php echo $form->fileField($company, 'image', array('style' => 'width: 230px'));?>
		<?php echo $form->error($company,'image'); ?>
	</div>
	<?php echo $form->hiddenField($company, 'deleteImage'); ?>
	<?php if ($company->image):?>
	<div class="row showImage">
		<label></label>
		<img src="<?php echo $company->getThumb(100, 100, 'crop');?>" style="float: left; margin: 5px 5px 5px 0" /> <br/>
		<a class="trash trash-btn" href="#">Удалить</a><br/>
	</div>
	<script>
		$('.trash-btn').click(function(e) {
			e.preventDefault();
			if (confirm('Вы уверены, что хотите удалить логотип?')) {
				$('#Company_deleteImage').val('1');
				$('.showImage').remove();
			}
		});
	</script>
	<?php endif?>
	<div class="row">
		<?php echo $form->labelEx($company,'description'); ?>
		<?php echo $form->textArea($company, 'description');?>
		<?php echo $form->error($company,'description'); ?>
	</div>
	<h3>Ответственное лицо</h3>
	<div class="row">
		<?php echo $form->labelEx($company,'responsible'); ?>
		<?php echo $form->textField($company, 'responsible');?>
		<?php echo $form->error($company,'responsible'); ?>
	</div>
	<div class="row radio">
		<?php echo $form->labelEx($company,'responsible_gender'); ?>
		<?php echo $form->radioButtonList($company, 'responsible_gender', User::$genders, array('separator' => '&nbsp;&nbsp;&nbsp;', 'class' => 'radio-group'));?>
		<?php echo $form->error($company,'responsible_gender'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'responsible_position'); ?>
		<?php echo $form->textField($company, 'responsible_position');?>
		<?php echo $form->error($company,'responsible_position'); ?>
	</div>
	<h3>Контактная информация</h3>
	<div class="row">
		<?php echo $form->labelEx($company,'city_id'); ?>
		<?php echo $form->dropDownList($company, 'city_id', array(null => 'Не указан') + CHtml::listData(City::model()->region()->findAll(), 'id', 'name'));?>
		<?php echo $form->error($company,'city_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'area_id'); ?>
		<?php echo $form->dropDownList($company, 'area_id', array(null => 'Не указан') + CHtml::listData(Area::model()->current()->findAll(), 'id', 'name'));?>
		<?php echo $form->error($company,'area_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'metro_id'); ?>
		<?php echo $form->dropDownList($company, 'metro_id', array(null => 'Не указана') + CHtml::listData(Metro::model()->current()->findAll(), 'id', 'name'));?>
		<?php echo $form->error($company,'metro_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'street'); ?>
		<?php echo $form->textField($company, 'street');?>
		<?php echo $form->error($company,'street'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'house'); ?>
		<?php echo $form->textField($company, 'house');?>
		<?php echo $form->error($company,'house'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'office'); ?>
		<?php echo $form->textField($company, 'office');?>
		<?php echo $form->error($company,'office'); ?>
	</div>
	<div class="row" style="margin-bottom: 0">
		<?php echo $form->labelEx($company,'email', array('label' => 'E-mail для уведомлений')); ?>
		<?php echo $form->textField($company, 'email');?>
		<?php echo $form->error($company,'email'); ?>
	</div>
	<div class="row" style="margin-top: 0">
		<?php echo $form->labelEx($company,'dummy'); ?>
		<?=$form->checkBox($company, 'hide_email')?>
		<?php echo $form->labelEx($company,'hide_email', array('class' => 'right')); ?>
		<?php echo $form->error($company,'hide_email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'phone'); ?>
		<?php echo $form->textField($company, 'phone');?>
		<?php echo $form->error($company,'phone'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'fax'); ?>
		<?php echo $form->textField($company, 'fax');?>
		<?php echo $form->error($company,'fax'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'website'); ?>
		<?php echo $form->textField($company, 'website');?>
		<?php echo $form->error($company,'website'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'skype'); ?>
		<?php echo $form->textField($company, 'skype');?>
		<?php echo $form->error($company,'skype'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($company,'icq'); ?>
		<?php echo $form->textField($company, 'icq');?>
		<?php echo $form->error($company,'icq'); ?>
	</div>
</div>
<div class="rform_footer" style="position: relative; width: 430px">
	<p>
		<span class="button"><input type="submit" value="Сохранить изменения"/></span>
	</p>
	<?php $this->endWidget(); ?>
</div>