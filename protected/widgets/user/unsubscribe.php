<?php
/**
 * @var $model Unsubscribe
 */
$this->controller->pageTitle = Yii::app()->name . ' - Отписаться от рассылки';
$this->controller->breadcrumbs = array(
	'Отписаться от рассылки',
);
?>
<h3>Отписаться от рассылки</h3>

<?php if (Yii::app()->user->hasFlash('success')):?>
	<div class="alert">
		<?=Yii::app()->user->getFlash('success')?>
		<a class="closeBtn flashCloseBtn" title="Закрыть">&nbsp;</a>
	</div>
<?php endif?>

<div class="wide form">

	<?php $form = $this->beginWidget('CActiveForm'); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

</div>
<div class="rform_footer" style="position: relative; width: 430px">
	<p>
		<span class="button"><?php echo CHtml::submitButton('Отписаться'); ?></span>
	</p>
	<?php $this->endWidget(); ?>
</div>

