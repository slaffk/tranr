<?php
/**
 * @var User $model
 */
?>

<?php if (Yii::app()->user->hasFlash('success')):?>
	<div class="alert">
		<?=Yii::app()->user->getFlash('success')?>
		<a class="closeBtn flashCloseBtn" title="Закрыть">&nbsp;</a>
	</div>
<?php endif?>

<h1>Подписка на обновления портала</h1>

<?php echo CHtml::beginForm(); ?>
<div class="right_div" style="float: right; margin-top: -16px; margin-right: 20px; position: relative">

</div>
<div class="left_div">
	<?php echo CHtml::activeCheckBoxList(
		$model,
		'subscribeIds',
		CHtml::listData(Category::model()->root()->findAll(), 'id', 'name') + Subscribe::$subs
	); ?>
</div>
<script type="text/javascript">
	var last = $('.left_div span').children().length / 2 + 1;
	var total = 0;
	$('.left_div span').prepend($('.right_div'));
	$('.left_div span').children().each(function(){
		if (total++ > last) {
			$('.right_div').append($(this));
		}
		if (total == 111) {
			$('.right_div').append("<br/>");
		}
	});
</script>
<div class="clearfix"></div>
<div class="rform_footer" style="padding-left: 300px">
	<p><span class="button"><?php echo CHtml::submitButton('Сохранить'); ?></span></p>
</div>
<?php echo CHtml::endForm(); ?>

<a name="sending"></a>
<h1>Рассылка уведомлений портала</h1>

<?php echo CHtml::beginForm(); ?>

<?php if ($model->subscribed):?>
	<p>Вы подписаны на рассылки уведомлений портала tranr.ru</p>
	<?php echo CHtml::activeHiddenField($model, 'subscribed', array('value' => 0)); ?>
	<div class="rform_footer" style="padding-left: 240px">
		<p><span class="button"><?php echo CHtml::submitButton('Приостановить рассылку'); ?></span></p>
	</div>
<?php else:?>
	<p>Вы отписаны от рассылок уведомлений портала tranr.ru</p>
	<p>Рассылка уведомлений на Ваш электронный адрес не производится.</p>
	<?php echo CHtml::activeHiddenField($model, 'subscribed', array('value' => 1)); ?>
	<div class="rform_footer" style="padding-left: 260px">
		<p><span class="button"><?php echo CHtml::submitButton('Возобновить рассылку'); ?></span></p>
	</div>
<?php endif?>

<?php echo CHtml::endForm(); ?>