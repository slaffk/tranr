<?php
/**
 * @var $this UserWidget
 * @var $user User
 * @var $form CActiveForm
 * @var $formId string
 */
?>
<div class="wide form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'profile-form',
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)); ?>
	<h1>Профиль соискателя</h1>
	<div class="row">
		<?php echo $form->labelEx($user,'lastname'); ?>
		<?php echo $form->textField($user, 'lastname');?>
		<?php echo $form->error($user,'lastname'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'firstname'); ?>
		<?php echo $form->textField($user, 'firstname');?>
		<?php echo $form->error($user,'firstname'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'middlename'); ?>
		<?php echo $form->textField($user, 'middlename');?>
		<?php echo $form->error($user,'middlename'); ?>
	</div>
	<div class="row radio">
		<?php echo $form->labelEx($user,'gender'); ?>
		<?php echo $form->radioButtonList($user, 'gender', User::$genders, array('separator' => '&nbsp;&nbsp;&nbsp;', 'class' => 'radio-group resume_gender'));?>
		<?php echo $form->error($user,'gender'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'birth_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $user,
			'name' => 'User[birth_date]',
			'language' => 'ru',
			'value' => $user->birth_date,
			'options' => array(
				'showAnim' => 'show',
				'showOn' => 'focus',
			),
			'htmlOptions' => array(
				'class' => 'inp_340',
			),
		));?>
		<style>.ui-datepicker-trigger {display: none}</style>
		<?php echo $form->error($user, 'birth_date'); ?>
	</div>
	<div class="row" style="margin-bottom: 0">
		<?php echo $form->labelEx($user,'marital_status'); ?>
		<?php echo $form->dropDownList($user, 'marital_status', [null => 'Выберите семейное положение'] + User::$maritalStatus);?>
		<?php echo $form->error($user,'marital_status'); ?>
	</div>
	<script>
		$('input.resume_gender').change(function(e) {
			if ($(this).val() != <?=User::GENDER_MEN?> && $(this).val() != <?=User::GENDER_WOMEN?>) return;
			var first = true;
			var notMarried = $(this).val() == <?=User::GENDER_MEN?> ? 'Не женат' : 'Не замужем';
			var married = $(this).val() == <?=User::GENDER_MEN?> ? 'Женат' : 'Замужем';
			$('#User_marital_status option').each(function(){
				if (!$(this).val()) return;
				$(this).text(first ? notMarried : married);
				first = false;
			});
		});
		$('input.resume_gender[checked=checked]').change();
	</script>
	<div class="row" style="margin-top: 0">
		<?php echo $form->labelEx($user,'dummy'); ?>
		<?=$form->checkBox($user, 'have_child')?>
		<?php echo $form->labelEx($user,'have_child', array('class' => 'right')); ?>
		<?php echo $form->error($user,'have_child'); ?>
	</div>
	<div class="row" style="margin-bottom: 0; margin-top: 15px">
		<?php echo $form->labelEx($user,'city_id'); ?>
		<?php echo $form->dropDownList($user, 'city_id', CHtml::listData(City::model()->region()->findAll(), 'id', 'name'));?>
		<?php echo $form->error($user,'city_id'); ?>
	</div>
	<div class="row" style="margin-top: 0">
		<?php echo $form->labelEx($user,'dummy'); ?>
		<?=$form->checkBox($user, 'ready_to_move')?>
		<?php echo $form->labelEx($user,'ready_to_move', array('class' => 'right')); ?>
		<?php echo $form->error($user,'ready_to_move'); ?>
	</div>
	<div class="row" style="margin-top: 0">
		<?php echo $form->labelEx($user,'dummy'); ?>
		<?=$form->checkBox($user, 'ready_to_trip')?>
		<?php echo $form->labelEx($user,'ready_to_trip', array('class' => 'right')); ?>
		<?php echo $form->error($user,'ready_to_trip'); ?>
	</div>

	<?php /*
	<div class="row" style="margin-top: 15px">
		<?php echo $form->labelEx($user,'user_type'); ?>
		<span class="input_edit" style="width: 143px" id="span_user_type">Соискатель</span>
		<?php echo $form->dropDownList($user, 'user_type', array(null => 'Соискатель') + Company::$companyTypes, array('style' => 'display: none; width: 150px;'));?>
		&nbsp;<a class="status-btn" href="#">сменить?</a>
		<?php echo $form->error($user,'user_type'); ?>
	</div>
	<script>
		$('.status-btn').click(function(e){
			e.preventDefault();
			if ($('#User_user_type').is(':visible')) {
				$('#User_user_type').hide();
				$('#span_user_type').show();
				$(this).text('сменить?');
				alert('Спасибо, ваша заявка будет расммотрена модератором.');
			} else {
				$('#User_user_type').show();
				$('#span_user_type').hide();
				$(this).text('СОХРАНИТЬ');
			}
		});
	</script>
	*/?>
	<div class="row">
		<?php echo $form->labelEx($user,'image'); ?>
		<?php echo $form->fileField($user, 'image', array('style' => 'width: 230px'));?>
		<?php echo $form->error($user,'image'); ?>
	</div>
	<?php echo $form->hiddenField($user, 'deleteImage'); ?>
	<?php if ($user->image):?>
		<div class="row showImage">
			<label></label>
			<img src="<?php echo $user->getThumb(100, 100, 'crop');?>" style="float: left; margin: 5px 5px 5px 0" /> <br/>
			<a class="trash trash-btn" href="#">Удалить</a><br/>
		</div>
		<script>
			$('.trash-btn').click(function(e) {
				e.preventDefault();
				if (confirm('Вы уверены, что хотите удалить аватар?')) {
					$('#User_deleteImage').val('1');
					$('.showImage').remove();
				}
			});
		</script>
	<?php endif?>

	<div class="row">
		<h3>Контактная информация</h3>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'email'); ?>
		<?php echo $form->textField($user, 'email');?>
		<?php echo $form->error($user,'email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'phone'); ?>
		<?php echo $form->textField($user, 'phone');?>
		<?php echo $form->error($user,'phone'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'skype'); ?>
		<?php echo $form->textField($user, 'skype');?>
		<?php echo $form->error($user,'skype'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'icq'); ?>
		<?php echo $form->textField($user, 'icq');?>
		<?php echo $form->error($user,'icq'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'portfolio'); ?>
		<?php echo $form->textField($user, 'portfolio');?>
		<?php echo $form->error($user,'portfolio'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'web'); ?>
		<?php echo $form->textField($user, 'web');?>
		<?php echo $form->error($user,'web'); ?>
	</div>

	<h3>Профили в социальных сетях</h3>
	<div class="row">
		<?php echo $form->labelEx($user,'social_vk'); ?>
		<?php echo $form->textField($user, 'social_vk');?>
		<?php echo $form->error($user,'social_vk'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'social_ok'); ?>
		<?php echo $form->textField($user, 'social_ok');?>
		<?php echo $form->error($user,'social_ok'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'social_fb'); ?>
		<?php echo $form->textField($user, 'social_fb');?>
		<?php echo $form->error($user,'social_fb'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'social_tw'); ?>
		<?php echo $form->textField($user, 'social_tw');?>
		<?php echo $form->error($user,'social_tw'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'social_pr'); ?>
		<?php echo $form->textField($user, 'social_pr');?>
		<?php echo $form->error($user,'social_pr'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'social_mk'); ?>
		<?php echo $form->textField($user, 'social_mk');?>
		<?php echo $form->error($user,'social_mk'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($user,'social_lj'); ?>
		<?php echo $form->textField($user, 'social_lj');?>
		<?php echo $form->error($user,'social_lj'); ?>
	</div>
</div>
<div class="rform_footer" style="position: relative; width: 430px">
	<p><span class="button"><input type="submit" value="Сохранить изменения"/></span></p>
	<?php $this->endWidget(); ?>
</div>