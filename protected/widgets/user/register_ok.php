<?php
/* @var $this UserWidget */
/* @var $model RegistrationForm */
/* @var $form CActiveForm  */

$this->controller->breadcrumbs = array(
	'Регистрация',
);
?>

<h1>Регистрация</h1>

<div class="form">
	<p style="margin: 10px 0">Регистрация прошла успешно!<br/>
	На Ваш адрес <?=$model->email?> отправлено письмо с инструкциями по активации аккаунта и дальнейшим действиям.</p>
	<p style="margin: 10px 0">Если вы не получили нашего письма, посмотрите, возможно оно попало в папку Спам или Сомнительные.</p>
	<p style="margin: 10px 0">Вы можете отправить письмо с кодом активации повторно, нажав <a href="/cabinet/sendAct">тут</a>.</p>
	<p style="margin: 10px 0">Зайдите в свой <a href="/cabinet/">Личный кабинет</a></p>
</div>

<script>
	$().ready(function() {
		$('.reg_right_app, .reg_right_emp, .reg_right_ka').hide();
		<?php if ($model instanceof HrRegistrationForm):?>
		$('.reg_right_ka').show();
		<?php elseif ($model instanceof CompanyRegistrationForm):?>
		$('.reg_right_emp').show();
		<?php else:?>
		$('.reg_right_app').show();
		<?php endif?>
	});
</script>