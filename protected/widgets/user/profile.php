<?php
/**
 * @var User $model
 */
?>
<h1>Профиль</h1>

<?php if (!$model->email):?>
	<p class="answer">
		Для получения откликов на свои резюме на электронную почту, пожалуйста <a href="#" class="addEmlBtn">добавьте свой e-mail</a> в профиле соискателя.
	</p>
	<script type="text/javascript">
		$('.addEmlBtn').click(function(e) {
			e.preventDefault();
			jPrompt('Добавьте свой e-mail', '', 'Добавьте свой e-mail', function(email) {
				if (email) {
					$.get('/ajax/updateEmail?email=' + email, function(text) {
						if (text == 'OK') {
							location.reload();
						}
					});
				}
			});
		})
	</script>
<?php endif?>

<?php if ((Yii::app()->user->hasRole(Role::ROLE_EMPLOYER) || Yii::app()->user->hasRole(Role::ROLE_HR)) && $model->company && !$model->company->moderate):?>
	<p class="answer">
		Ваша компания ожидает проверки модератором.
		После одобрения модератором, карточка вашей компании будет доступна в разделе
		<?php if ($model->company->company_type == Company::TYPE_COMPANY):?>
			<a href="/company/">Работодателей</a>,
		<?php else:?>
			<a href="/agency/">Кадровых агенств</a>,
		<?php endif?>
		а ваши вакансии появятся в списке <a href="/vacancy/">вакансий</a>
	</p>
<?php endif?>

<div class="my_vacancies">
	<p>Здравствуйте, <strong><?=$model->firstname?></strong>!</p>
	<p>Добро пожаловать на Tranr.ru!</p>

	<?php if (Yii::app()->user->hasFlash('success')):?>
		<div class="alert">
			<?=Yii::app()->user->getFlash('success')?>
			<a class="closeBtn flashCloseBtn" title="Закрыть">&nbsp;</a>
		</div>
	<?php endif?>

	<?php if (!$model->activated):?>
	<div class="alert stop">
		Ваш аккаунт не активирован. Перейдите по ссылке в вашем письме для его активации. Если вы не можете найти письмо -
		<a href="/cabinet/sendAct">нажмите сюда</a> и мы отправим его еще раз.
	</div>
	<?php endif?>

	<ul class="detail">
		<?php if ($model->company):?>
			<li><span>Ваша компания</span><span><?=$model->company->name?></span></li>
		<?php endif?>

		<?php if ($model->email):?>
			<li><span>Ваш логин</span><span><?=$model->email?></span></li>
		<?php else:?>
			<li><span>Ваш логин</span><span><?=$model->phone?></span></li>
		<?php endif?>

		<?php if ($model->company):?>
			<li><span>Адрес Вашей странички</span><span><a href="<?=$model->company->url?>"><?=$model->company->url?></a></span></li>
		<?php endif;?>

		<li><span>Последний вход c IP</span><span> <?=$model->last_login_ip?> (<?=Yii::app()->dateFormatter->format("dd.MM.yyyy в HH:mm", $model->last_login)?>)</span></li>
	</ul>
	<ul class="detail">
		<?php if (Yii::app()->user->hasRole(Role::ROLE_EMPLOYER) || Yii::app()->user->hasRole(Role::ROLE_HR)):?>
			<li><span>Тарифный план</span><span>Тестовый <!--a href="#" onclick="alert('пока не реализовано');return false">сменить тариф</a--></span></li>
			<li><span>Срок действия тарифа</span><span><?=Yii::app()->dateFormatter->format("dd.MM.yyyy в HH:mm", date('Y-m-d H:i:s', strtotime($model->begin_date) + 168*24*60*60))?><!--a href="#" onclick="alert('пока не реализовано');return false">продлить</a--></span></li>
		<?php endif?>
		<?php if (Yii::app()->user->hasRole(Role::ROLE_APPLICANT) || Yii::app()->user->hasRole(Role::ROLE_HR)):?>
			<li><span>Лимит на размещение резюме</span><span><?=Resume::MAX?>, еще можете разместить: <?=Resume::MAX - Resume::model()->alive()->my()->published()->count()?></span></li>
		<?php endif?>
		<?php if (Yii::app()->user->hasRole(Role::ROLE_EMPLOYER) || Yii::app()->user->hasRole(Role::ROLE_HR)):?>
			<li><span>Лимит на размещение вакансий</span><span><?=Vacancy::MAX?>, еще можете разместить: <?=Vacancy::MAX - Vacancy::model()->alive()->my()->published()->count()?></span></li>
		<?php endif?>
		<?php if (Yii::app()->user->hasRole(Role::ROLE_EMPLOYER) || Yii::app()->user->hasRole(Role::ROLE_HR)):?>
			<li><span>Лидирующая компания</span><span>нет</span></li>
		<?php endif?>
	</ul>
</div>
