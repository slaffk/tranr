<?php
/**
 * @var $this UserWidget
 * @var $model RemindForm
 * @var $form CActiveForm
 */
$this->controller->breadcrumbs = array(
	'Восстановление пароля' => '/remind/',
	'Установить новый пароль'
);
?>
<h2>Установить новый пароль</h2>

<?php if (Yii::app()->user->hasFlash('success')):?>
	<div class="alert">
		<?=Yii::app()->user->getFlash('success')?>
		<a class="closeBtn flashCloseBtn" title="Закрыть">&nbsp;</a>
	</div>
<?php endif?>

<form id="again-form" method="post" action="/remind/">
	<input type="hidden" name="RemindForm[required]" value="<?=md5(time())?>">
	<input type="hidden" name="RemindForm[email]" value="<?=Yii::app()->user->getFlash('info')?>">
</form>

<div class="wide form">
	<?php $form=$this->beginWidget('CActiveForm', array('id' => 'remind-password'));?>

	<?php if (strpos($model->email, '@') === false && $model->scenario !== 'step2'):?>
		<div class="row">
			<?php echo $form->labelEx($model,'resetCode', array('label' => 'Код из смс')); ?>
			<?php echo $form->textField($model,'resetCode', array('maxlength' => 256)); ?>
			<?php echo $form->error($model,'resetCode'); ?>
			<span><a href="/remind/" class="again-btn">выслать код повторно</a></span>
		</div>
	<?php else:?>
		<?php echo $form->hiddenField($model,'resetCode', array('maxlength' => 256)); ?>
		<?php echo $form->error($model,'resetCode'); ?>

		<?php if (strpos($model->email, '@') !== false):?>
			<div class="row">
				<?php echo $form->labelEx($model,'email'); ?>
				<?php echo $form->textField($model,'email', array('maxlength' => 256)); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		<?php endif?>
	<?php endif?>

	<?php if (strpos($model->email, '@') !== false || $model->scenario == 'step2'):?>
		<div class="row">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password', array('maxlength' => 256, 'autocomplete' => 'off')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'passwordConfirm'); ?>
			<?php echo $form->passwordField($model,'passwordConfirm', array('maxlength' => 256, 'autocomplete' => 'off')); ?>
			<?php echo $form->error($model,'passwordConfirm'); ?>
		</div>
	<?php endif?>

	<div class="rform_footer">
		<p><span class="button"><input type="submit" value="<?=$model->scenario == 'step2' ? 'Установить пароль' : 'Далее'?>"/></span></p>
	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$('.again-btn').click(function(e) {
		e.preventDefault();
		$('#again-form').submit();
	});
</script>