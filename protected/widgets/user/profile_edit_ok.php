<?php
/* @var $this UserWidget */
/* @var $model RegistrationForm */
/* @var $form CActiveForm  */

$this->controller->breadcrumbs = array(
	'Регистрация',
);
?>

<h1>Изменения в профиле</h1>

<?php if (Yii::app()->user->hasFlash('success')):?>
	<p class="answer"><?=Yii::app()->user->getFlash('success')?></p>
<?php endif?>

<div class="form">
	<p>Изменения в профиле были успешно применены. Просмотреть можете по ссылке <a href="/cabinet/settings/">Настройки профиля</a></p>
</div>
