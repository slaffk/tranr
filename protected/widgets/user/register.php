<?php
/**
 * @var $this UserWidget
 * @var $userForm RegistrationForm
 * @var $companyForm CompanyRegistrationForm
 * @var $hrForm HrRegistrationForm
 * @var $form CActiveForm
 * @var $tab integer
 */
$this->controller->breadcrumbs = array(
	'Регистрация',
);
?>
<div class="rform_usertype">
	<label class="rform_ut_soisk">
		<input type="radio" name="rad" class="reg-radio" data-url="reg_right_app" accesskey="user-registration-form"<?=$tab == 1 ? ' checked' : ''?>>
		Соискатель
	</label>
	<label class="rform_ut_soisk">
		<input type="radio" name="rad" class="reg-radio" data-url="reg_right_emp" accesskey="company-registration-form"<?=$tab == 2 ? ' checked' : ''?>>
		Работодатель
	</label>
	<label class="rform_ut_soisk">
		<input type="radio" name="rad" class="reg-radio" data-url="reg_right_ka" accesskey="hr-registration-form"<?=$tab == 3 ? ' checked' : ''?>>
		Кадровое агентство
	</label>
</div>
<script>
	$().ready(function() {
		$('.rform_usertype input[checked]').click();
	});
</script>

<?php $this->render(UserWidget::VIEW_PATH . '._register_user', array('model' => $userForm, 'visible' => $tab == 1)); ?>
<?php $this->render(UserWidget::VIEW_PATH . '._register_company', array('model' => $companyForm, 'formId' => 'company-registration-form', 'visible' => $tab == 2)); ?>
<?php $this->render(UserWidget::VIEW_PATH . '._register_company', array('model' => $hrForm, 'formId' => 'hr-registration-form', 'visible' => $tab == 3)); ?>

<script>
	$().ready(function(){
		$('.captcha-row a').click(function(e) {
			e.preventDefault();
			$.getJSON('/site/captcha?refresh=1', function(data) {
				$('.captcha-row img').attr('src', data.url);
			});
		});
		$('.ajaxCheckEmail').change(function(){
			var email = $(this).val();
			$.getJSON('/ajax/checkEmail?email=' + email, function(data) {
				if (data) {
					jAlert(email + ' уже зарегистрирован на сайте<br/>Для напоминания пароля, нажмите <a href="/remind/">тут</a>', 'Email занят');
				}
			});
		});
	});

	$('.reg-radio').click(function(e) {
		$('#user-registration-form, #company-registration-form, #hr-registration-form').hide();
		$('.reg_right_app, .reg_right_emp, .reg_right_ka').hide();
		$('#' + $(this).attr('accesskey')).show();
		$('.' + $(this).attr('data-url')).show();
	});
</script>
