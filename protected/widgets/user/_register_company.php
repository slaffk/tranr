<?php
/**
 * @var $this UserWidget
 * @var $model CompanyRegistrationForm
 * @var $form CActiveForm
 * @var $formId string
 */
?>

<div id="<?=$formId?>"<?=$visible === false ? ' style="display: none"': ''?>>
	<div class="wide form">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id' => 'reg-form-company',
			'enableClientValidation' => true,
			'htmlOptions' => array('autocomplete' => 'off'),
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
		)); ?>
		<div class="row">
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model, 'name', array('autocomplete' => 'on'))?>
			<?php echo $form->error($model,'name'); ?>
			<span style="display: block; margin-left: 188px; margin-bottom: 10px; color: #808080">Название компании без кавычек и указания организационно-правовой формы</span>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'cityId'); ?>
			<?=$form->dropDownList($model, 'cityId', CHtml::listData(City::model()->region()->findAll(), 'id', 'name'))?>
			<?php echo $form->error($model,'cityId'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'lastname'); ?>
			<?php echo $form->textField($model,'lastname', array('maxlength' => 256, 'data-notice' => 'Ваша фамилия', 'autocomplete' => 'on')); ?>
			<?php echo $form->error($model,'lastname'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'firstname'); ?>
			<?php echo $form->textField($model,'firstname', array('maxlength' => 256, 'data-notice' => 'Ваше имя', 'autocomplete' => 'on')); ?>
			<?php echo $form->error($model,'firstname'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'phoneNumber'); ?>
			<?php echo $form->textField($model,'phoneNumber', array('maxlength' => 20, 'autocomplete' => 'on')); ?>
			<?php echo $form->error($model,'phoneNumber'); ?>
			<span style="display: block; margin-left: 188px; margin-bottom: 10px; ">Пример:
				<span style="color: #808080">+79130000000</span>
			</span>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email', array('maxlength' => 256, 'class' => 'ajaxCheckEmail', 'autocomplete' => 'off')); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password', array('maxlength' => 256, 'autocomplete' => 'off')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'passwordConfirm'); ?>
			<?php echo $form->passwordField($model,'passwordConfirm', array('maxlength' => 256, 'autocomplete' => 'off')); ?>
			<?php echo $form->error($model,'passwordConfirm'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'hearFrom'); ?>
			<?=$form->dropDownList($model, 'hearFrom', [null => 'Не выбрано'] + User::$hearFroms)?>
			<?php echo $form->error($model,'hearFrom'); ?>
		</div>
		<div class="row captcha-row">
			<?php echo $form->labelEx($model,'captcha', array('style' => 'margin-top: 20px')); ?>
			<?php $this->widget('CCaptcha', array('showRefreshButton' => false, 'imageOptions' => array('style' => 'float: left;')));?>
			<?=$form->textField($model, 'captcha', array('style' => 'margin-top: 10px; width: 90px', 'autocomplete' => 'off'))?>

			<?php echo $form->error($model,'captcha'); ?>
			<br/><br/><a href="#" class="synop captcha-btn" style="margin-left: 180px">обновить картинку</a>
		</div>
	</div>
	<div class="rform_footer" style="position: relative; width: 430px">
		<dl>
			<dd>
				<?=$form->checkBox($model, 'agree', array('id' => 'agree', 'class' => 'required'))?>
				<?=$form->error($model, 'agree', array('style' => 'margin-left: 218px; top: 13px'));?>
				Я принимаю <a href="/rules/" target="_blank">Правила сайта</a>
				<?php /*и <a href="/agreement/" target="_blank">Условия соглашения (оферты)</a>*/?><br>
			</dd>
		</dl>
		<p>
			<span class="button button_registr"><input type="submit" value="зарегистрироваться"/></span>
		</p>
		<?php $this->endWidget(); ?>
	</div>
</div>
<script type="text/javascript">
	$('html, body').animate({
		scrollTop: $('input.error,textarea.error,select.error').first().offset().top
	}, 300);
</script>