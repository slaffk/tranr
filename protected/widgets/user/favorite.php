<?php
/**
 * @var $this UserWidget
 * @var $model Favorite
 */
?>

<p>Ссылок в избранном: <?=$model->search()->totalItemCount?></p>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'favorite-grid',
	'dataProvider' => $model->search(),
	'htmlOptions' => array('class' => 'grid'),
	'template' => "{items}{pager}",
	'pager' => array('class' => 'LinkPager', 'url' => '/'.$this->controller->page->path.'/', 'pageName' => 'Favorite_page'),
	'selectableRows' => 1024,
	'enableSorting' => false,
	'columns' => array(
		array(
			'name' => 'id',
			'class' => 'CCheckBoxColumn',
		),
		array(
			'name' => 'timestamp',
			'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm", $data->timestamp)',
			'htmlOptions' => array('style' => 'width: 110px; text-align: left'),
		),
		array(
			'name' => 'url',
			'type'=>'raw',
			'value' => 'CHtml::link($data->url, $data->url, array("target" => "_blank"))',
		),
		array(
			'name' => 'type',
			'value' => 'Favorite::$types[$data->type]',
			'htmlOptions' => array('style' => 'width: 100px; text-align: center'),
		),
//		array(
//			'class'=>'CButtonColumn',
//			'template'=>'{update}',
//		),
	),
)); ?>
<button type="submit" data-url="/cabinet/myfavorites/" class="small-button deleteBtn"><span>удалить</span></button>
<script>
	$('.deleteBtn').click(function(e) {
		e.preventDefault();
		var ids = [];
		$('.select-on-check:checked').each(function(index) {
			ids.push($(this).val());
		});
		if (ids.join() =='') return;

		if ($(this).hasClass('deleteBtn')) {
			if (confirm('Вы уверены, что хотите окончательно удалить ссылки из избранного?')) {
				$.get($(this).attr('data-url') + ids.join() + '/delete', '', function() {
					//document.location.href = '/cabinet/myfavorites/';
					$.fn.yiiGridView.update('favorite-grid', {url: document.location.href});
				});
			}
		}
	});
</script>