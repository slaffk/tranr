<?php
/**
 * @var $this UserWidget
 * @var $model RemindForm
 * @var $form CActiveForm
 */
$this->controller->breadcrumbs = array(
	'Восстановление пароля',
);
?>
<h2>Восстановление пароля</h2>

<?php if (Yii::app()->user->hasFlash('success')):?>
	<div class="alert">
		<?=Yii::app()->user->getFlash('success')?>
		<a class="closeBtn flashCloseBtn" title="Закрыть">&nbsp;</a>
	</div>
<?php endif?>

<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array('id' => 'remind-password'));?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email', array('maxlength' => 256)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row captcha-row">
		<?php echo $form->labelEx($model,'captcha', array('style' => 'margin-top: 20px')); ?>
		<?php $this->widget('CCaptcha', array('showRefreshButton' => false, 'imageOptions' => array('style' => 'float: left;')));?>
		<?=$form->textField($model, 'captcha', array('style' => 'margin-top: 10px; width: 90px', 'autocomplete' => 'off'))?>

		<?php echo $form->error($model,'captcha'); ?>
		<br/><br/><a href="#" class="synop captcha-btn" style="margin-left: 180px" ">обновить картинку</a>
	</div>

	<script>
		$().ready(function(){
			$('.captcha-row a').click(function(e) {
				e.preventDefault();
				$.getJSON('/site/captcha?refresh=1', function(data) {
					$('.captcha-row img').attr('src', data.url);
				});
			});
		});
	</script>
	<div class="rform_footer">
		<p><span class="button"><input type="submit" value="Прислать инструкции"/></span></p>
		<?php $this->endWidget(); ?>
	</div>
</div>