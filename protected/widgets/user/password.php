<?php
/**
 * @var $this UserWidget
 * @var $model RemindForm
 * @var $form CActiveForm
 */
$this->controller->breadcrumbs = array(
	'Личный кабинет' => '/cabinet/',
	'Изменение пароля',
);
?>
<h2>Изменение пароля учетной записи</h2>

<?php if (Yii::app()->user->hasFlash('success')):?>
	<div class="alert">
		<?=Yii::app()->user->getFlash('success')?>
		<a class="closeBtn flashCloseBtn" title="Закрыть">&nbsp;</a>
	</div>
<?php endif?>

<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array('id' => 'change-password'));?>

	<div class="row">
		<?php echo $form->labelEx($model,'oldPassword'); ?>
		<?php echo $form->passwordField($model,'oldPassword', array('maxlength' => 256, 'autocomplete' => 'off')); ?>
		<?php echo $form->error($model,'oldPassword'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password', array('maxlength' => 256, 'autocomplete' => 'off')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'passwordConfirm'); ?>
		<?php echo $form->passwordField($model,'passwordConfirm', array('maxlength' => 256, 'autocomplete' => 'off')); ?>
		<?php echo $form->error($model,'passwordConfirm'); ?>
	</div>

	<div class="rform_footer">
		<p><span class="button"><input type="submit" value="Изменить пароль"/></span></p>
		<?php $this->endWidget(); ?>
	</div>
</div>