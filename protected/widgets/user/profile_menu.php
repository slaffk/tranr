<div class="search_terms">
	<div class="left_content_border" style="background-color: transparent">
	<ul>
		<li><p>Основное</p></li>
		<?php if ((Yii::app()->user->hasRole(Role::ROLE_HR) || Yii::app()->user->hasRole(Role::ROLE_EMPLOYER)) && Yii::app()->user->getModel()->company):?>
			<li><a href="/cabinet/myvacancies/" title="Мои вакансии">Мои вакансии</a></li>
		<?php endif?>
		<?php if (Yii::app()->user->hasRole(Role::ROLE_APPLICANT)):?>
			<li><a href="/cabinet/myresume/" title="Мои резюме">Мои резюме</a></li>
		<?php endif?>
		<?php if (Yii::app()->user->hasRole(Role::ROLE_HR)):?>
			<li><a href="/cabinet/myresume/" title="Мои резюме">Мои резюме</a></li>
		<?php endif?>
		<!--li><a title="Мои запросы">Мои запросы</a></li-->
		<li><a href="/cabinet/myfavorites/" title="Избранное">Избранное</a></li>
		<li><a href="/cabinet/blacklist/" title="Избранное">Черный список</a></li>
	</ul>

	<!--ul>
		<li><p>Сообщения</p></li>
		<li><a>Входящие</a></li>
		<li><a>Отправленные</a></li>
		<li><a>Отложенные</a></li>
		<li><a>Архивные</a></li>
	</ul-->

	<ul>
		<li><p>Настройки</p></li>
		<li><?=CHtml::link('Подписка и рассылки', '/cabinet/subscribes/')?></li>
		<?php if ((Yii::app()->user->hasRole(Role::ROLE_HR) || Yii::app()->user->hasRole(Role::ROLE_EMPLOYER)) && Yii::app()->user->getModel()->company):?>
			<li><a href="/cabinet/settings/" title="Профиль компании">Профиль компании</a></li>
			<!--li><a title="Мои менеджеры">Мои менеджеры</a></li-->
		<?php endif?>
		<?php if (Yii::app()->user->hasRole(Role::ROLE_APPLICANT)):?>
			<li><a href="/cabinet/settings/" title="Профиль соискателя">Профиль соискателя</a></li>
		<?php endif;?>

		<!--li><a title="Мои настройки">Мои настройки</a></li-->
		<?php if ((Yii::app()->user->hasRole(Role::ROLE_HR) || Yii::app()->user->hasRole(Role::ROLE_EMPLOYER)) && Yii::app()->user->getModel()->company):?>
			<!--li><a href="/cabinet/rekviz/" title="Юридические реквизиты">Юридические реквизиты</a></li-->
		<?php endif?>
		<li><a href="/cabinet/pass/" title="Сменить пароль">Сменить пароль</a></li>
	</ul>

	</div>
</div>