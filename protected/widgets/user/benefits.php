<?php
/**
 * @var $app Benefit[]
 * @var $emp Benefit[]
 * @var $ka Benefit[]
 */
?>
<div class="possibilities__ad reg_right_app" style="width: auto;">
	<div class="possibilities__ad__title" style="font-weight: normal; font-size: 19px">Преимущества регистрации соискателя</div>
	<ul class="possibilities__ad__list">
		<?php foreach ($app as $benefit):?>
			<li class="possibilities__ad__list__item"><?=$benefit->title?></li>
		<?php endforeach?>
	</ul>
</div>

<div class="possibilities__ad reg_right_emp" style="display: none; width: auto;">
	<div class="possibilities__ad__title" style="font-weight: normal; font-size: 19px">Преимущества регистрации работодателя</div>
	<ul class="possibilities__ad__list">
		<?php foreach ($emp as $benefit):?>
			<li class="possibilities__ad__list__item"><?=$benefit->title?></li>
		<?php endforeach?>
	</ul>
</div>

<div class="possibilities__ad reg_right_ka" style="display: none; width: auto;">
	<div class="possibilities__ad__title" style="font-weight: normal; font-size: 19px">Преимущества регистрации агентства</div>
	<ul class="possibilities__ad__list">
		<?php foreach ($ka as $benefit):?>
			<li class="possibilities__ad__list__item"><?=$benefit->title?></li>
		<?php endforeach?>
	</ul>
</div>
