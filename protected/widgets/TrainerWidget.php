<?php
/**
 * Class TrainerWidget
 */
class TrainerWidget extends AppWidget {

	const VIEW_PATH = 'application.widgets.trainer';

	/**
	 * @return string
	 */
	public static function getName() {

		return 'Тренеры';
	}

	/**
	 * @return array
	 */
	public static function getActionList() {

		return array(
			'category' => 'Дерево категорий (сайдбар)',
			'related' => 'Похожие тренеры (сайдбар)',
			'index' => 'Список тренеров (контент)',
			'view' => 'Просмотр тренера (контент)',
			'rotateSmall' => 'Ротация на главной (контент)',
			'reviews' => 'Отзывы (контент)',
			'addReview' => 'Добавить отзыв (контент)',
			'registration' => 'Регистрация тренера (контент)',
		);
	}

	/**
	 * index
	 */
	public function actionIndex() {

		$model = new Trainer();
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('Trainer');

		/** @var $c Controller */
		$c = $this->getController();

		if (@$c->vars[1]) {

			$categoryName = @$c->vars[2] ? $c->vars[2] : $c->vars[1];
			/** @var $category Category */
			$category = Category::model()->find('category.alias = :alias', array('alias' => $categoryName));
			if (!$category || ($category->parent && $category->parent->alias != $c->vars[1])) {
				throw new CHttpException(404, 'Категория тренеров не найдена');
			}
			$c->breadcrumbs = array('Подбор тренера' => '/trainers/');
			if ($category->parent) {
				$c->breadcrumbs[$category->parent->name] = $category->parent->getArticleUrl();
			}
			$c->breadcrumbs[] = $category->name;

			$model->category_ids = array($category->id);
			if ($category->childs) {
				$model->category_ids = array_merge($model->category_ids, array_keys(CHtml::listData($category->childs, 'id', 'id')));
			}

			if ($category->meta_description) {
				Yii::app()->clientScript->registerMetaTag($category->meta_description, 'description', null, array(), null, true);
			}
			if ($category->meta_keywords) {
				Yii::app()->clientScript->registerMetaTag($category->meta_keywords, 'keywords', null, array(), null, true);
			}
			$c->pageTitle = $category->meta_title ? $category->meta_title : $c->pageTitle;

			$c->model = $category;

			$this->render(self::VIEW_PATH.'.category', array(
				'category' => $category,
				'model' => $model->active()->regional(),
			));
			return;
		}
		$c->breadcrumbs = array(
			'Подбор тренера',
		);
		$this->render(self::VIEW_PATH.'.index', array(
			'model' => $model->active()->regional(),
		));
	}

	/**
	 * @throws CHttpException
	 */
	public function actionView() {

		/** @var $c Controller */
		$c = $this->getController();

		$id = count($c->vars) == 4 ? $c->vars[2] : @$c->vars[3];
		$alias = count($c->vars) == 4 ? $c->vars[3] : @$c->vars[4];
		$categoryAlias = count($c->vars) == 4 ? $c->vars[1] : @$c->vars[2];
		$rootAlias = count($c->vars) == 4 ? null : @$c->vars[1];

		/** @var $trainer Trainer */
		$trainer = Trainer::model()->findByPk($id);
		if (!$trainer || $trainer->alias != $alias) {
			throw new CHttpException(404, 'Тренер не найден');
		}

		if ($trainer->category->parent) {
			if ($trainer->category->parent->alias != $rootAlias || $trainer->category->alias != $categoryAlias)
				throw new CHttpException(404, 'Статья не найдена');
		} else {
			if ($trainer->category->alias != $categoryAlias)
				throw new CHttpException(404, 'Статья не найдена');
		}

		/* крохи */
		$c->breadcrumbs = array('Статьи' => '/articles/');
		if ($trainer->category) {
			if ($trainer->category->parent) {
				$c->breadcrumbs[$trainer->category->parent->name] = $trainer->category->parent->getArticleUrl();
			}
			$c->breadcrumbs[$trainer->category->name] = $trainer->category->getArticleUrl();
		}
		$c->breadcrumbs[] = $trainer->name;

		/* немного SEO */
		$c->pageTitle = $trainer->meta_title ? $trainer->meta_title : $trainer->name;
		if ($trainer->meta_description) {
			Yii::app()->clientScript->registerMetaTag($trainer->meta_description, 'description');
		}
		if ($trainer->meta_keywords) {
			Yii::app()->clientScript->registerMetaTag($trainer->meta_keywords, 'keywords');
		}

		/* запоминаем что у нас открыто */
		$c->model = $trainer;
		$c->filterIds = array_merge($c->filterIds, array($trainer->id));

		$this->render(self::VIEW_PATH.'.view', array(
			'record' => $trainer,
		));
	}

	/**
	 * index
	 */
	public function actionCategory() {

		$this->render(self::VIEW_PATH.'.category_tree', array(
			'data' => Category::model()->root()->withTrainers()->findAll(),
		));
	}
	
	public function actionAddReview() {
		$c = $this->getController();
		$id = count($c->vars) == 4 ? $c->vars[2] : @$c->vars[3];
		
		$model = new TrainerReview();
		$model->setScenario('guest');
		$model->trainer_id = $id;
		
		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('TrainerReview');
			$model->attributes = $data;
			if($model->save()) {
				$c->refresh();
			}
		}
		
		$this->render(self::VIEW_PATH.'.add_review', array(
			'model' => $model,
		));
	}
	
	public function actionReviews() {
		$c = $this->getController();
		$id = count($c->vars) == 4 ? $c->vars[2] : @$c->vars[3];
		
		$model = new TrainerReview('search');
		$model->unsetAttributes();
		$model->attributes = Yii::app()->request->getQuery('TrainerReview');
		
		$this->render(self::VIEW_PATH.'.reviews', array(
			'dataProvider' => $model->search($id),
		));
	}

	/**
	 * rotateSmall
	 */
	public function actionRotateSmall() {

		/** @var Controller $c */
		$c = $this->getController();
		$models = Trainer::model()->active()->regional()->mainPage()->notInArray(['id' => $c->filterIds])->random()->limit(intval($this->caption))->findAll();
		$c->filterIds = array_merge($c->filterIds, CHtml::listData($models, 'id', 'id'));

		$this->render(self::VIEW_PATH . '.rotate_small', array(
			'models' => $models,
		));
	}

	/**
	 * related похожие тренеры
	 */
	public function actionRelated() {

		/** @var $c Controller */
		$c = $this->getController();
		if (!($c->model instanceof ActiveRecord)) {
			return;
		}
		$models = $c->model->getRelatedByTags('Trainer', 3, array('not_in' => $c->filterIds));
		if (!empty($models)) {
			$c->filterIds = array_merge($c->filterIds, array_keys(CHtml::listData($models, 'id', 'id')));
			$this->render(self::VIEW_PATH.'.related', array(
				'models' => $models
			));
		}
	}

	/**
	 * @throws CException
	 */
	public function actionRegistration() {

		$c = $this->getController();
		if (!Yii::app()->user->isGuest) {
			$c->redirect(array('/site/index'));
		}

		$model = new RegistrationForm();

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = Yii::app()->request->getPost('RegistrationForm');
			if ($model->register()) {
				$identity = new UserIdentity($model->email, $model->passwordConfirm);
				if ($identity->authenticate()){
					Yii::app()->user->login($identity);
				}
				if (!Yii::app()->user->isGuest) {
					Yii::app()->user->setFlash('success', 'Регистрация прошла успешно!<br/>'.
						'Предоставьте больше информации о вас, это можно сделать в  <a href="/cabinet/trainer">профиле тренера</a>, '.
						'чтобы потенциальные клинты могли быстрее вас отыскать среди других анкет!');

					$this->getController()->redirect('/cabinet/trainer');
				}
				$this->render(self::VIEW_PATH.'.register_ok', array('model' => $model));
				return;

			}
		}

		$this->render(self::VIEW_PATH.'.registration', array(
			'model' => $model,
		));
	}
}
