<h1>Контакты</h1>
<p>
	<?php if (Yii::app()->city->phone):?>
		<strong>Телефон:</strong>
		<?=Yii::app()->city->phone?><br>
		<br>
	<?php endif?>

	<?php if (Yii::app()->city->fax):?>
		<strong>Факс:</strong>
		<?=Yii::app()->city->fax?><br>
		<br>
	<?php endif?>

	<?php if (Yii::app()->city->email):?>
		<strong>E-mail:</strong>
		<?=Yii::app()->city->email?><br>
		<br>
	<?php endif ?>

	<?php if (Yii::app()->city->skype):?>
		<strong>Скайп:</strong>
		<?=Yii::app()->city->skype?><br>
		<br>
	<?php endif ?>

	<?php if (Yii::app()->city->icq):?>
		<strong>ICQ:</strong>
		<?=Yii::app()->city->icq?><br>
		<br>
	<?php endif ?>

	<?php if (Yii::app()->city->address):?>
		<strong>Адрес:</strong>
		<?=Yii::app()->city->address?><br>
		<br>
	<?php endif?>
</p>
