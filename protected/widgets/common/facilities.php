<?php
/**
 * @var $this CommonWidget
 * @var $sliders AdvertSlider[]
 * @var $left Benefit[]
 * @var $right Benefit[]
 */
?>
<div class="possibilities clearfix">
	<?php if (Yii::app()->user->isGuest):?>
	<table class="possibilities__reg-links">
		<tbody><tr>
			<td class="possibilities__leftcol">
				<span class="possibilities__text">Все уникальные возможности портала доступны только зарегистрированным пользователям.</span><br>
				<span class="button">
					<a href="/registration/">регистрация</a>
				</span>
			</td>
			<td class="possibilities__rightcol">
				<span class="possibilities__text">Если Вы зарегистрированы на портале, пройдите процедуру авторизации.</span><br>
				<span class="button">
					<a href="" onclick="$('.enter').click();return false;">авторизация</a>
				</span>
			</td>
		</tr>
		</tbody></table>
	<?php endif?>
	<div class="possibilities__main">
		<div class="possibilities__main__wrapper">
			<div class="possibilities__main__left-shadow"></div>
			<div class="possibilities__main__right-shadow"></div>
			<div id="slider-wrap">
				<div id="slider">
					<?php $i = 0;?>
					<?php foreach ($sliders as $slider):?>
						<div class="slide slide<?=$slider->id?>" <?=$i ? ' style="display: none"' : ''?>>
							<div class="slide__title"><?=$slider->title?></div>
							<div class="slide__content"><?=$slider->content?></div>
						</div>
						<?php $i++;?>
					<?php endforeach?>
				</div>
				<div class="sli-links">
					<?php $i = 0;?>
					<?php foreach ($sliders as $slider):?>
						<span class="control-slide control-slide<?=$i == 0 ? ' active' : ''?>" data-url=".slide<?=$slider->id?>">&nbsp;</span>
						<?php $i++;?>
					<?php endforeach?>
				</div>
				<script>
					$('.control-slide').click(function(e) {
						$('#slider .slide').hide();
						$($(this).attr('data-url')).show();
						$('.control-slide').removeClass('active');
						$(this).addClass('active');
					});
					function slideSwitch() {
						var $control = $('.control-slide.active').next().hasClass('control-slide') ? $('.control-slide.active').next() : $('.control-slide').first();
						$control.click();
					}

					$(function() {
						setInterval( "slideSwitch()", 5000);
					});
				</script>
			</div>
		</div>
	</div>
	<div class="possibilities__ad">
		<div class="possibilities__ad__title">Размещение вакансий</div>
		<ul class="possibilities__ad__list">
			<?php foreach ($left as $benefit):?>
				<li class="possibilities__ad__list__item"><?=$benefit->title?></li>
			<?php endforeach?>
		</ul>
		<span class="button button_margin-top button_orange">
			<a href="/cabinet/myvacancies/add/">разместить вакансию</a>
		</span>
	</div>
	<div class="possibilities__ad possibilities__ad_float-right">
		<div class="possibilities__ad__title possibilities__ad__title_blue">Поиск резюме</div>
		<ul class="possibilities__ad__list">
			<?php foreach ($right as $benefit):?>
				<li class="possibilities__ad__list__item"><?=$benefit->title?></li>
			<?php endforeach?>
		</ul>
		<span class="button button_margin-top button_blue">
			<a href="/resume/">начать поиск резюме</a>
		</span>
	</div>
</div>