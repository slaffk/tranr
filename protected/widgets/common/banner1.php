<div class="clearfix"></div>
<?php if (Yii::app()->user->isGuest):?>
	<!-- если мы неавторизованы -->
	<object width="220" height="400">
		<param name="movie" value="/images/2-01.swf">
		<param name="wmode" value="opaque" />
		<param name="allowscriptaccess" value="never">
		<embed src="/images/2-01.swf" wmode="opaque" width="220" height="400"></embed>
	</object>
<?php elseif (Yii::app()->user->hasRole(Role::ROLE_HR) || Yii::app()->user->hasRole(Role::ROLE_APPLICANT)):?>
	<!-- если hr или соискатель -->
	<object width="220" height="400">
		<param name="movie" value="/images/2-02.swf">
		<param name="wmode" value="opaque" />
		<param name="allowscriptaccess" value="never">
		<embed src="/images/2-02.swf" wmode="opaque" width="220" height="400"></embed>
	</object>
<?php endif?>
<div class="clearfix"></div>
