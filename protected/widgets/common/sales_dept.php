<div class="sidebar-contacts">
	<div class="sidebar-contacts__title">Отдел по работе с клиентами</div>
	<div class="hr hr_nomargin"></div>
	<ul class="sidebar-contacts__list">
		<li class="sidebar-contacts__list__item">
			<span class="icon icon_sidebar-contacts icon_sidebar-contacts_time"></span>
			<span class="sidebar-contacts__list__item__text">с 09.00 до 18.00 в будние дни, без обеда</span>
		</li>
		<?php if (Yii::app()->city->phone || Yii::app()->city->fax):?>
		<li class="sidebar-contacts__list__item">
			<span class="icon icon_sidebar-contacts icon_sidebar-contacts_phone"></span>
			<span class="sidebar-contacts__list__item__text">
				<?php $phones = array(
					Yii::app()->city->phone ? Yii::app()->city->phone : '',
					Yii::app()->city->fax ? 'факс ' . Yii::app()->city->fax : '',
				); ?>
				<?=implode(', ', array_filter($phones))?>
			</span>
		</li>
		<?php endif?>
		<?php if (Yii::app()->city->address):?>
		<li class="sidebar-contacts__list__item">
			<span class="icon icon_sidebar-contacts icon_sidebar-contacts_adress"></span>
			<span class="sidebar-contacts__list__item__text"><?=Yii::app()->city->address?><br><a target="_blank" href="http://maps.yandex.ru/?text=<?=Yii::app()->city->address?>">посмотреть на карте</a></span>
		</li>
		<?php endif?>
	</ul>
</div>