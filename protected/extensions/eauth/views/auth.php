<?php
/**
 * @var $id sstring
 * @var $services array
 * @var $action string
 */
?>
<div class="services">
	<ul class="auth-services">
		<?php foreach ($services as $name => $service):?>
			<li class="auth-service <?=$service->id?>">
			<?=CHtml::link('<span class="auth-icon ' . $service->id.'"><i></i></span>', array($action, 'service' => $name), array(
				'class' => 'auth-link ' . $service->id,
				'title' => 'Авторизация через ' . $service->title,
			));?>
			</li>
		<?php endforeach?>
	</ul>
</div>
