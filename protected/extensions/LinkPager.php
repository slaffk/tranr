<?php
/**
 * Class LinkPager
 */
class LinkPager extends CLinkPager {

	public $url;
	public $pageName = 'page';
	public $header = '';
	public $footer = '';
	public $prevPageLabel = 'предыдущая';
	public $nextPageLabel = 'следующая';
	public $cssFile = '/css/pager.css';
	public $showPageSize = true;

	/**
	 * @param integer $page the page that the URL should point to.
	 * @return string the created URL
	 * @see CPagination::createPageUrl
	 */
	protected function createPageUrl($page) {

		if (!$this->url) {
			$this->getPages()->createPageUrl($this->getController(),$page);
		}

		$this->url = preg_replace('/([?&])' . $this->pageName . '=\w+(&|$)/', '$1', $this->url);
		return CHtml::normalizeUrl($this->url) . ($page > 0 ? (strpos($this->url, '?') ? '&' : '?') . $this->pageName . '=' . ($page+1) : '');
	}

	/**
	 * @return array
	 */
	protected function createPageButtons() {

		$buttons = array();
		$pages = array();

		$pageCount = $this->getPageCount();
		list($beginPage, $endPage) = $this->getPageRange();
		$currentPage = $this->getCurrentPage(false);

		if (($page = $currentPage - 1) < 0) $page = 0;
		$buttons[] = $this->createPageButton($this->prevPageLabel, $page, $this->previousPageCssClass, $currentPage <= 0, false);

		$buttons[] = 'ctrl';

		if (($page = $currentPage + 1) >= $pageCount - 1) $page = $pageCount - 1;
		$buttons[] = $this->createPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);

		$buttons[] = $this->createPageButton($this->lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);

		if ($pageCount <= 1)
			return array('buttons' => $buttons, 'pages' => array($this->createPageButton(1, 0, $this->internalPageCssClass, false, true)));

		for ($i = $beginPage; $i <= $endPage; ++$i)
			$pages[] = $this->createPageButton($i + 1, $i, $this->internalPageCssClass, false, $i == $currentPage);

		return array('buttons' => $buttons, 'pages' => $pages);
	}

	public function run() {

		$this->registerClientScript();
		$all = $this->createPageButtons();

		if (in_array(@Yii::app()->session['pager_item_count'], array(null, 10)) && count($all['pages']) <= 1) return;
		echo '<div>';
		echo CHtml::tag('span', array('class' => 'pagerHeader'), 'Страниц: ' . (count($all['pages'])));
		echo CHtml::tag('ul', $this->htmlOptions, count($all['pages']) > 1 ? implode("\n", $all['buttons']) : '');
		if (count($all['pages']) > 1) {
			echo CHtml::tag('ul', array('class' => 'pages_number'), implode("\n", $all['pages']));
		}
		echo '</div>';
	}

}