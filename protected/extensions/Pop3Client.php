<?php

class Pop3Client {

	const TIMEOUT = 30;

	private $socket;
	private $errno;
	private $errstr;

	/**
	 * @param $answer
	 * @return bool
	 */
	private function answer($answer) {

		return ($answer[0] == '+') ? true : false;
	}

	/**
	 * @param $host
	 * @param $port
	 * @param $user
	 * @param $pass
	 * @throws Exception
	 */
	public function __construct($host, $port, $user, $pass) {

		$this->socket = fsockopen($host, $port, $this->errno, $this->errstr, self::TIMEOUT);
		if (!$this->socket) throw new Exception($this->errno . ' ' . $this->errstr);

		fgets($this->socket, 128);
		fputs($this->socket, "USER $user\n");
		fgets($this->socket, 128);
		fputs($this->socket, "PASS $pass\n");

		if (!$this->answer(fgets($this->socket, 128))) {
			throw new Exception('Auth failed');
		}
	}

	/**
	 * @return integer|bool
	 */
	public function count() {

		fputs($this->socket, "STAT\n");
		$answer = fgets($this->socket, 128);
		return $this->answer($answer) ? @explode(' ', $answer)[1] : false;
	}

	/**
	 * @return array|bool
	 */
	public function listMessages() {

		$msgCount = $this->count($this->socket);
		for (fputs($this->socket, "LIST\n"), $list = array(), $len = $msgCount + 1, $i = 0; $i <= $len; $i++) {
			$answer = fgets($this->socket, 128);
			if ($i == $len) break;
			if (!$i) {
				if (!$this->answer($answer)) return false;
				continue;
			}
			list($msgId, $msgLen) = explode(' ', $answer);
			$list[$msgId] = $msgLen;
		}
		return $list;
	}

	/**
	 * @param $msgId
	 * @return bool|string
	 */
	public function retrieve($msgId) {

		fputs($this->socket, "RETR $msgId\n");
		if ($this->answer(fgets($this->socket, 128))) {
			$msg = '';
			do {
				$tmp_str = fgets($this->socket, 128);
				$msg .= $tmp_str;
			} while (trim($tmp_str) != '.');
			return $msg;
		}
		return false;
	}

	/**
	 * @param $msgId
	 * @return bool
	 */
	function delete($msgId) {

		fputs($this->socket, "DELE $msgId\n");
		return $this->answer(fgets($this->socket, 128));
	}

	/**
	 * @return bool
	 */
	public function reset() {

		fputs($this->socket, "RSET\n");
		return $this->answer(fgets($this->socket, 128));
	}

	/**
	 * @param $msg
	 * @return bool
	 */
	public function boundary($msg) {

		@preg_match_all("|boundary=\"(.*)\"|U", $msg, $out, PREG_PATTERN_ORDER);
		if (@$out[1][0]) {
			return $out[1][0];
		}
		return false;
	}

	/**
	 * @return bool
	 */
	public function noop() {

		fputs($this->socket, "NOOP\n");
		return $this->answer(fgets($this->socket, 128));
	}

	/**
	 * @return bool
	 */
	public function quit() {

		fputs($this->socket, "QUIT\n");
		fclose($this->socket);
		return true;
	}
}
