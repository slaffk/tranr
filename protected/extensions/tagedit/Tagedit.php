<?php
/**
 * Class Tagedit
 */
class Tagedit extends CInputWidget {

	/**
	 * @var array select2 options
	 */
	public $options = array();

	/**
	 * @var array CHtml::dropDownList $data param
	 */
	public $data = array();

	/**
	 * @var string html element selector
	 */
	public $selector;

	/**
	 * @var array javascript event handlers
	 */
	public $events = array();

	protected $defaultOptions = array();

	public function init() {

		$this->defaultOptions = array(
			'typeahead' => true,
			'typeaheadAjaxSource' => '/site/tagAutoComplete',
			'hiddenTagListName' =>  get_class($this->model) . '[tag]'
		);
	}

	public function run() {

		$bu = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/');
		$cs = Yii::app()->clientScript;

		$cs->registerCssFile($bu . '/tagmanager.css');
		$cs->registerScriptFile($bu . '/tagmanager.js');

		$options = CJavaScript::encode(CMap::mergeArray($this->defaultOptions, $this->options));

		ob_start();
		echo "jQuery('.tm-input').tagsManager($options);";

		$cs->registerScript(__CLASS__ . '#' . $this->id, ob_get_clean() . ';');
	}
}
