<?php

if(!function_exists('w')) {
	function w($o) {
		$arguments = func_get_args();
		if (sizeof($arguments)>1) {
			foreach($arguments as $o) {
				w($o);
			}
			return;
		}
		if (PHP_SAPI != 'cli') {
			echo "<pre>";
		}

		print_r($o);

		if (PHP_SAPI != 'cli') {
			echo "</pre>";
		} else {
			echo "\n";
		}
	}
	
	function _w($o) {
		w($o); ob_flush();
	}
}

if(!function_exists('mem_used')) {

	function mem_used($t = 'm', $out = false) {
		$t = strtolower($t);
		if($t == 'b') {
			$t = 'b';
			$del = 1;
		} else if($t == 'k') {
			$t = 'Kb';
			$del = 1024;
		} else {
			$t = 'Mb';
			$del = 1024 * 1024;
		}
		$r = sprintf('usage memory: %.2f %s', (memory_get_usage() / $del), $t);
		if ($out) v($r);
		else return $r;
	}
}

if (!function_exists('camelize')) {

	/**
	 *  AAA_BBB => AaaaBbbb, AAA-BBB => AaaBbb
	 */
	function camelize($in, $startWithLowerCase = false) {
		$out = str_replace(' ', '', ucwords(str_replace(array('-', '_'), ' ', strtolower($in))));
		if ($startWithLowerCase) {
			$out{0} = strtolower($out{0});
		}
		return $out;
	}

}


if (!function_exists('uncamelize')) {

	/**
	 *  AaaaBbbb => aaa-bbb, AaaaBbbb => aaa_bbb
	 */
	function uncamelize($in, $splitter = "-") {
		return strtolower(preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter . '$0', $in)));
	}

}
