<?php

class m140815_130607_category_count extends CDbMigration {

	public function safeUp() {

		$this->addColumn('category', 'article_count', 'int(11) default null');
		$this->addColumn('category', 'trainer_count', 'int(11) default null');
		$this->execute('update article set status = 0');
		$this->execute('update trainer set active = 0');
		$this->execute('update category set article_count = 0');
		$this->execute('update category set trainer_count = 0');
	}

	public function safeDown() {

		$this->dropColumn('category', 'article_count');
		$this->dropColumn('category', 'trainer_count');
	}
}
