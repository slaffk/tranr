<?php
/**
 * Class m140218_051150_init_database
 */
class m140218_051150_init_database extends CDbMigration {

	public function safeUp() {

		$this->createTable(
			'alert',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'user_id' => 'int(11) not null COMMENT "Идентификатор получателя"',
				'read' => 'int(1) not null COMMENT "Прочитано"',
				'can_close' => 'int(1) not null COMMENT "Закрывабельно"',
				'by_user_id' => 'int(11) not null COMMENT "Идентификатор отправителя"',
				'PRIMARY KEY (`id`)',
				'KEY `user_id` (`user_id`) ',
				'KEY `read` (`read`) ',
				'KEY `can_close` (`can_close`) ',
				'KEY `by_user_id` (`by_user_id`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Алерты пользователей"'
		);

		$this->createTable(
			'area',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'city_id' => 'int(11) not null COMMENT "Идентификатор города"',
				'name' => 'varchar(256) not null COMMENT "Имя"',
				'PRIMARY KEY (`id`)',
				'KEY `city_id` (`city_id`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Районы"'
		);

		$this->createTable(
			'article',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'city_id' => 'int(11) default null COMMENT "Ид города"',
				'category_id' => 'int(11) not null COMMENT "Ид категории"',
				'user_id' => 'int(11) not null COMMENT "Ид пользователя"',
				'title' => 'varchar(256) not null COMMENT "Заголовок"',
				'alias' => 'varchar(256) not null COMMENT "Альяс"',
				'content' => 'TEXT not null COMMENT "Контент"',
				'meta_title' => 'varchar(256) default null COMMENT "Мета тайтл"',
				'meta_keywords' => 'varchar(256) default null COMMENT "Мета кейворды"',
				'meta_description' => 'varchar(256) default null COMMENT "Мета дескрипшн"',
				'image' => 'varchar(64) default null COMMENT "Пикча"',
				'image_alt' => 'varchar(256) default null COMMENT "Альт пикчи"',
				'status' => 'int(11) not null COMMENT "Статус"',
				'timestamp' => 'timestamp not null DEFAULT CURRENT_TIMESTAMP COMMENT "Штамп"',
				'PRIMARY KEY (`id`)',
				'KEY `city_id` (`city_id`) ',
				'KEY `category_id` (`category_id`) ',
				'KEY `user_id` (`user_id`) ',
				'KEY `status` (`status`) ',
				'KEY `timestamp` (`timestamp`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Статьи"'
		);

		$this->createTable(
			'article_to_tag',
			array(
				'article_id' => 'int(11) not null COMMENT "ИД статьи"',
				'tag_id' => 'int(11) not null COMMENT "ИД тега"',
				'PRIMARY KEY (`article_id`,`tag_id`) ',
			),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Связь статей с тегами"'
		);

		$this->createTable(
			'category',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'parent_id' => 'int(11) default null COMMENT "Идентификатор"',
				'name' => 'varchar(256) not null COMMENT "Имя"',
				'alias' => 'varchar(256) not null COMMENT "Альяс"',
				'h1' => 'varchar(256) default null COMMENT "Мета тайтл"',
				'meta_title' => 'varchar(256) default null COMMENT "Мета тайтл"',
				'meta_keywords' => 'varchar(256) default null COMMENT "Мета кейворды"',
				'meta_description' => 'varchar(256) default null COMMENT "Мета дескрипшн"',
				'image' => 'varchar(64) default null COMMENT "Пикча"',
				'image_alt' => 'varchar(256) default null COMMENT "Альт пикчи"',
				'sort' => 'int(11) default null COMMENT "Идентификатор"',
				'PRIMARY KEY (`id`)',
				'KEY `parent_id` (`parent_id`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Категории"'
		);

		$this->createTable('city', array(
			'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
			'parent_id' => 'int(11) default NULL COMMENT "Идентификатор"',
			'name' => 'varchar(128) not null COMMENT "Город"',
			'alias' => 'varchar(256) not null COMMENT "Альяс (nsk)"',
			'timezone' => 'varchar(256) default null COMMENT "Таймзона"',
			'default' => 'int(1) not null COMMENT "Дефолт-сити"',
			'metro_count' => 'int(11) default null COMMENT "Количество станций метро"',
			'area_count' => 'int(11) default null COMMENT "Количество районов"',
			'phone' => 'varchar(128) default null COMMENT "Телефон"',
			'email' => 'varchar(128) default null COMMENT "Емейл"',
			'name_pril' => 'varchar(128) default null COMMENT "Имя города в прил. (новосибирский)"',
			'name_pril_f' => 'varchar(128) default null COMMENT "Имя в прил женс род (новосибирская)"',
			'name_pril_mul' => 'varchar(128) default null COMMENT "Имя в прил мн. ч (новосибирские)"',
			'name_pril_where' => 'varchar(128) default null COMMENT "Имя в где (в новосибирске)"',
			'name_pril_on' => 'varchar(128) default null COMMENT "Имя на (на навосибирском)"',
			'active' => 'int(1) not null COMMENT "Флаг активности"',
			'timestamp' => 'timestamp not null DEFAULT CURRENT_TIMESTAMP COMMENT "Дата создания"',
			'PRIMARY KEY (`id`)',
			'KEY `active` (`active`) ',
			'KEY `default` (`default`) ',
			'KEY `timestamp` (`timestamp`) ',
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Города"');

		$this->createTable('favorite', array(
			'id' => 'int(11) NOT NULL AUTO_INCREMENT COMMENT "Идентификатор"',
			'type' => 'int(11) NOT NULL COMMENT "Тип"',
			'related_class' => 'varchar(128) DEFAULT NULL COMMENT "Связанный класс"',
			'related_id' => 'int(11) DEFAULT NULL COMMENT "Ид записи"',
			'url' => 'varchar(512) DEFAULT NULL COMMENT "Урл"',
			'user_id' => 'int(11) DEFAULT NULL COMMENT "Ид юзера"',
			'timestamp' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT "Время добавления"',
			'PRIMARY KEY (`id`)',
			'KEY `type` (`type`) ',
			'KEY `timestamp` (`timestamp`) ',
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Избранное"');

		$this->createTable(
			'file',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'city_id' => 'int(11) not null COMMENT "Идентификатор города"',
				'filename' => 'varchar(256) not null COMMENT "Имя файла"',
				'data' => 'mediumblob default null COMMENT "Двоичные данные"',
				'file_type' => 'varchar(128) not null COMMENT "Имя файла"',
				'file_size' => 'int(11) default null COMMENT "Идентификатор города"',
				'attach' => 'int(1) default null COMMENT "Идентификатор города"',
				'timestamp' => 'timestamp not null DEFAULT CURRENT_TIMESTAMP COMMENT "Дата создания"',
				'PRIMARY KEY (`id`)',
				'KEY `city_id` (`city_id`) ',
				'KEY `timestamp` (`timestamp`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Файлы городов"'
		);

		$this->createTable('mail_template', array(
			'id' => 'int(11) NOT NULL AUTO_INCREMENT COMMENT "Идентификатор"',
			'event' => 'int(11) NOT NULL COMMENT "Город"',
			'title' => 'varchar(256) DEFAULT NULL COMMENT "Заголовок"',
			'content' => 'text DEFAULT NULL COMMENT "Содержание"',
			'content_html' => 'text DEFAULT NULL COMMENT "Содержание"',
			'content_sms' => 'text DEFAULT NULL COMMENT "Содержание"',
			'PRIMARY KEY (`id`)',
			'KEY `event` (`event`) ',
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Темплейты рассылок"');

		$this->createTable(
			'metro',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'city_id' => 'int(11) not null COMMENT "Идентификатор города"',
				'name' => 'varchar(256) not null COMMENT "Имя"',
				'PRIMARY KEY (`id`)',
				'KEY `city_id` (`city_id`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Станции метрополитена"'
		);

		$this->createTable(
			'news',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'city_id' => 'int(11) default null COMMENT "Ид города"',
				'category_id' => 'int(11) not null COMMENT "Ид категории"',
				'user_id' => 'int(11) not null COMMENT "Ид пользователя"',
				'date' => 'timestamp DEFAULT "0000-00-00 00:00:00" COMMENT "Дата создания"',
				'title' => 'varchar(256) not null COMMENT "Заголовок"',
				'content' => 'TEXT not null COMMENT "Контент"',
				'meta_title' => 'varchar(256) default null COMMENT "Мета тайтл"',
				'meta_keywords' => 'varchar(256) default null COMMENT "Мета кейворды"',
				'meta_description' => 'varchar(256) default null COMMENT "Мета дескрипшн"',
				'image' => 'varchar(64) default null COMMENT "Пикча"',
				'image_alt' => 'varchar(256) default null COMMENT "Альт пикчи"',
				'status' => 'int(11) not null COMMENT "Статус"',
				'timestamp' => 'timestamp not null DEFAULT CURRENT_TIMESTAMP COMMENT "Штамп"',
				'PRIMARY KEY (`id`)',
				'KEY `city_id` (`city_id`) ',
				'KEY `category_id` (`category_id`) ',
				'KEY `user_id` (`user_id`) ',
				'KEY `status` (`status`) ',
				'KEY `date` (`date`) ',
				'KEY `timestamp` (`timestamp`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Новости"'
		);

		$this->createTable(
			'news_category',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'parent_id' => 'int(11) default null COMMENT "Идентификатор предка"',
				'name' => 'varchar(256) not null COMMENT "Имя"',
				'alias' => 'varchar(256) not null COMMENT "Альяс"',
				'h1' => 'varchar(256) default null COMMENT "Мета тайтл"',
				'meta_title' => 'varchar(256) default null COMMENT "Мета тайтл"',
				'meta_keywords' => 'varchar(256) default null COMMENT "Мета кейворды"',
				'meta_description' => 'varchar(256) default null COMMENT "Мета дескрипшн"',
				'image' => 'varchar(64) default null COMMENT "Пикча"',
				'image_alt' => 'varchar(256) default null COMMENT "Альт пикчи"',
				'PRIMARY KEY (`id`)',
				'KEY `parent_id` (`parent_id`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Категории новостей"'
		);

		$this->createTable(
			'news_to_tag',
			array(
				'news_id' => 'int(11) not null COMMENT "ИД новости"',
				'tag_id' => 'int(11) not null COMMENT "ИД тега"',
				'PRIMARY KEY (`news_id`,`tag_id`) ',
			),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Связь новостей с тегами"'
		);

		$this->createTable(
			'page',
			array(
				'id' => 'int(11) NOT NULL AUTO_INCREMENT COMMENT "Идентификатор"',
				'parent_id' => 'int(11) DEFAULT NULL COMMENT "Родительская страница"',
				'alias' => 'varchar(64) NOT NULL COMMENT "Альяс"',
				'path' => 'varchar(512) NOT NULL COMMENT "Путь"',
				'slash_id' => 'int(11) DEFAULT NULL COMMENT "Завершающий слеш"',
				'title' => 'varchar(256) DEFAULT NULL COMMENT "Тайтл"',
				'meta_keywords' => 'varchar(512) DEFAULT NULL COMMENT "Мета ключевики"',
				'meta_description' => 'varchar(512) DEFAULT NULL COMMENT "Мета описание"',
				'layout' => 'varchar(64) NOT NULL COMMENT "Лейаут"',
				'PRIMARY KEY (`id`)',
				'KEY `parent_id` (`parent_id`) ',
				'KEY `slash_id` (`slash_id`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Страницы"'
		);

		$this->createTable(
			'resource',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'module' => 'varchar(256) default null COMMENT "Модуль"',
				'controller' => 'varchar(256) default null COMMENT "Контроллер"',
				'action' => 'varchar(256) default null COMMENT "Действие"',
				'description' => 'varchar(256) default null COMMENT "Описание"',
				'PRIMARY KEY (`id`)',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Ресурсы"'
		);

		$this->createTable(
			'review',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'parent_id' => 'int(11) default null COMMENT "Идентификатор предка"',
				'user_id' => 'int(11) not null COMMENT "Ид пользователя"',
				'ndescription' => 'TEXT default null COMMENT "Плохие слова"',
				'pdescription' => 'TEXT default null COMMENT "Хорошие слова"',
				'active' => 'int(1) not null COMMENT "Активность"',
				'timestamp' => 'timestamp not null DEFAULT CURRENT_TIMESTAMP COMMENT "Штамп"',
				'PRIMARY KEY (`id`)',
				'KEY `parent_id` (`parent_id`) ',
				'KEY `user_id` (`user_id`) ',
				'KEY `active` (`active`) ',
				'KEY `timestamp` (`timestamp`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Отзывы"'
		);

		$this->createTable(
			'role',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'name' => 'varchar(256) not null COMMENT "Имя"',
				'alias' => 'varchar(256) not null COMMENT "Альяс"',
				'PRIMARY KEY (`id`)',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Роли"'
		);

		$this->createTable(
			'role_to_resource',
			array(
				'role_id' => 'int(11) not null COMMENT "ИД роли"',
				'resource_id' => 'int(11) not null COMMENT "ИД ресурса"',
				'PRIMARY KEY (`role_id`,`resource_id`) ',
			),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Связь ролей с ресурсами"'
		);

		$this->createTable(
			'setting',
			array(
				'id' => 'int(11) NOT NULL AUTO_INCREMENT COMMENT "Идентификатор"',
				'name' => 'varchar(256) NOT NULL COMMENT "Имя"',
				'value' => 'text NOT NULL COMMENT "Значение"',
				'PRIMARY KEY (`id`)',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Настройки"'
		);

		$this->createTable(
			'tag',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'name' => 'varchar(256) not null COMMENT "Тег"',
				'PRIMARY KEY (`id`)',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Теги"'
		);

		$this->createTable(
			'user',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT COMMENT "Идентификатор"',
				'email' => 'varchar(128) not null COMMENT "Емейл"',
				'firstname' => 'varchar(128) default null COMMENT "Имя"',
				'lastname' => 'varchar(128) default null COMMENT "Фамилия"',
				'middlename' => 'varchar(128) default null COMMENT "Отчество"',
				'phone' => 'varchar(128) default null COMMENT "Телефон"',
				'auth_phone' => 'varchar(128) default null COMMENT "Телефон"',
				'password' => 'varchar(128) not null COMMENT "Хеш пароля"',
				'begin_date' => 'timestamp not null DEFAULT CURRENT_TIMESTAMP COMMENT "Дата создания"',
				'end_date' => 'timestamp not null DEFAULT "0000-00-00 00:00:00" COMMENT "Дата удаления"',
				'last_login' => 'timestamp DEFAULT "0000-00-00 00:00:00" COMMENT "Штамп последнего логина"',
				'last_login_ip' => 'varchar(20) default null COMMENT "Телефон"',
				'image' => 'varchar(128) default null COMMENT "Телефон"',
				'gender' => 'int(1) default null COMMENT "Флаг активности"',
				'active' => 'int(1) not null COMMENT "Флаг активности"',
				'activated' => 'int(1) default null COMMENT "Флаг активности"',
				'PRIMARY KEY (`id`) ',
				'KEY `dates` (`begin_date`, `end_date`) ',
				'KEY `active` (`active`) ',
				'KEY `activated` (`activated`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Пользователи"'
		);

		$this->createTable(
			'user_to_role',
			array(
				'user_id' => 'int(11) not null COMMENT "Ид пользователя"',
				'role_id' => 'int(11) not null COMMENT "Ид роли"',
				'PRIMARY KEY (`user_id`,`role_id`) ',
			),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Связь пользователей и ролей"'
		);

		$this->createTable(
			'widget',
			array(
				'id' => 'int(11) NOT NULL AUTO_INCREMENT COMMENT "Идентификатор"',
				'page_id' => 'int(11) NOT NULL COMMENT "Идентификатор страницы"',
				'zone_id' => 'int(11) NOT NULL COMMENT "Зона"',
				'class' => 'varchar(256) DEFAULT NULL COMMENT "Класс виджета"',
				'text' => 'TEXT DEFAULT NULL COMMENT "Текст"',
				'sort' => 'int(11) DEFAULT NULL COMMENT "Сортировка"',
				'PRIMARY KEY (`id`)',
				'KEY `page_id` (`page_id`) ',
				'KEY `zone_id` (`zone_id`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Виджеты"'
		);

		$this->createTable(
			'widget_param',
			array(
				'id' => 'int(11) NOT NULL AUTO_INCREMENT COMMENT "Идентификатор"',
				'widget_id' => 'int(11) NOT NULL COMMENT "Идентификатор виджета"',
				'name' => 'varchar(128) NOT NULL COMMENT "Имя параметра"',
				'value' => 'varchar(512) DEFAULT NULL COMMENT "Значение параметра"',
				'PRIMARY KEY (`id`)',
				'KEY `widget_id` (`widget_id`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT="Параметры виджетов"'
		);

	}

	public function safeDown() {

		$this->dropTable('alert');
		$this->dropTable('area');
		$this->dropTable('article');
		$this->dropTable('article_to_tag');
		$this->dropTable('category');
		$this->dropTable('city');
		$this->dropTable('favorite');
		$this->dropTable('file');
		$this->dropTable('mail_template');
		$this->dropTable('metro');
		$this->dropTable('news');
		$this->dropTable('news_category');
		$this->dropTable('news_to_tag');
		$this->dropTable('page');
		$this->dropTable('resource');
		$this->dropTable('review');
		$this->dropTable('role');
		$this->dropTable('role_to_resource');
		$this->dropTable('setting');
		$this->dropTable('tag');
		$this->dropTable('user');
		$this->dropTable('user_to_role');
		$this->dropTable('widget');
		$this->dropTable('widget_param');
	}
}
