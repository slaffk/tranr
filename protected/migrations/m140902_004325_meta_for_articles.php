<?php
/**
 * Class m140902_004325_meta_for_articles
 */
class m140902_004325_meta_for_articles extends CDbMigration {

	public function safeUp() {

		$this->addColumn('category', 'a_h1', 'varchar(256) default null');
		$this->addColumn('category', 'a_meta_title', 'varchar(256) default null');
		$this->addColumn('category', 'a_meta_keywords', 'varchar(256) default null');
		$this->addColumn('category', 'a_meta_description', 'varchar(256) default null');
	}

	public function safeDown() {

		$this->dropColumn('category', 'a_h1');
		$this->dropColumn('category', 'a_meta_title');
		$this->dropColumn('category', 'a_meta_keywords');
		$this->dropColumn('category', 'a_meta_description');
	}
}
