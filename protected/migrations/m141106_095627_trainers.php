<?php
/**
 * Class m141106_095627_trainers
 */
class m141106_095627_trainers extends CDbMigration {

	public function safeUp() {

		$this->addColumn('trainer', 'parent_id', 'int(11) not null after id');
		$this->addColumn('trainer', 'tariff_id', 'int(11) default null');
		$this->addColumn('trainer', 'tariff_start', 'timestamp default "0000-00-00 00:00:00"');
		$this->addColumn('trainer', 'tariff_end', 'timestamp default "0000-00-00 00:00:00"');

		$this->createTable(
			'tariff',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT',
				'name' => 'varchar(100) default null',
				'stick' => 'int(1) default null',
				'main_page' => 'int(1) default null',
				'hide_related' => 'int(1) default null',
				'color' => 'int(1) default null',
				'recommend' => 'int(1) default null',
				'verify' => 'int(1) default null',
				'price_month' => 'int(11) default null',
				'price_quarter' => 'int(11) default null',
				'price_half_year' => 'int(11) default null',
				'price_year' => 'int(11) default null',
				'PRIMARY KEY (`id`)' ,
				'KEY `stick` (`stick`)',
				'KEY `main_page` (`main_page`)'
			),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8'
		);

		$this->execute('insert into tariff (name, price_month, price_quarter, price_half_year, price_year) values ("Базовый", 0, 0, 0, 0)');
		$this->execute('update trainer set tariff_id = 1');
	}

	public function safeDown() {

		$this->dropColumn('trainer', 'parent_id');
		$this->dropColumn('trainer', 'tariff_id');
		$this->dropColumn('trainer', 'tariff_start');
		$this->dropColumn('trainer', 'tariff_end');

		$this->dropTable('tariff');
	}
}
