<?php
/**
 * Class m140814_161749_trainer
 */
class m140814_161749_trainer extends CDbMigration {

	public function safeUp() {

		$this->createTable(
			'trainer',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT',
				'active' => 'int(11) not null',
				'moderated' => 'int(11) not null',
				'category_id' => 'int(11) not null',
				'user_id' => 'int(11) not null',
				'trainer_type' => 'int(1) not null',
				'city_id' => 'int(11) not null',
				'area_id' => 'int(11) default null',
				'metro_id' => 'int(11) default null',
				'address' => 'varchar(512) default null',

				'name' => 'varchar(512) default null',
				'alias' => 'varchar(512) default null',

				'firstname' => 'varchar(128) default null',
				'lastname' => 'varchar(128) default null',
				'middlename' => 'varchar(128) default null',

				'phone' => 'varchar(128) default null',
				'phone_additional' => 'varchar(128) default null',
				'email' => 'varchar(128) default null',
				'gender' => 'int(1) default null',
				'dob' => 'datetime default null',

				'edu_level' => 'int(11) default null',
				'edu_college' => 'varchar(128) default null',
				'edu_year' => 'varchar(10) default null',
				'edu_other' => 'text default null',
				'experience' => 'int(11) default null',
				'experience_text' => 'text default null',

				'additional' => 'text default null',

				'image' => 'varchar(128) default null',
				'image_alt' => 'varchar(128) default null',
				'meta_title' => 'varchar(256) default null COMMENT "Мета тайтл"',
				'meta_keywords' => 'varchar(256) default null COMMENT "Мета кейворды"',
				'meta_description' => 'varchar(256) default null COMMENT "Мета дескрипшн"',

				'timestamp' => 'timestamp not null default current_timestamp',
				'PRIMARY KEY (`id`)',
				'KEY `active` (`active`) ',
				'KEY `moderated` (`moderated`) ',
				'KEY `category_id` (`category_id`) ',
				'KEY `user_id` (`user_id`) ',
				'KEY `city_id` (`city_id`) ',
				'KEY `area_id` (`area_id`) ',
				'KEY `metro_id` (`metro_id`) ',
				'KEY `gender` (`gender`) ',
				'KEY `dob` (`dob`) ',
				'KEY `edu_level` (`edu_level`) ',
				'KEY `experience` (`experience`) ',
				'KEY `timestamp` (`timestamp`) ',
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8'
		);

		$this->createTable(
			'trainer_to_tag',
			array(
				'trainer_id' => 'int(11) not null',
				'tag_id' => 'int(11) not null',
				'PRIMARY KEY (`trainer_id`,`tag_id`) ',
			),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8'
		);

		$this->createTable(
			'trainer_to_category',
			array(
				'trainer_id' => 'int(11) not null',
				'category_id' => 'int(11) not null',
				'PRIMARY KEY (`trainer_id`,`category_id`) ',
			),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8'
		);
	}

	public function safeDown() {

		$this->dropTable('trainer_to_category');
		$this->dropTable('trainer_to_tag');
		$this->dropTable('trainer');
	}
}
