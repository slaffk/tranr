<?php

class m141031_062727_sms extends CDbMigration
{
	public function up()
	{
		$this->createTable(
		'sms',
		array(
			'id' => 'int(11) not null AUTO_INCREMENT',
			'user_fio' => 'VARCHAR(255) not null',
			'user_phone' => 'BIGINT not null',
			'sms_id' => 'int(11) null default null',
			'phone' => 'BIGINT not null',
			'text' => 'VARCHAR(255) not null',
			'check_date' => 'DATETIME null default null COMMENT "Время последней проверки статуса"',
			'status' => 'VARCHAR(100) NULL default null',
			'PRIMARY KEY (`id`)' ,
			'INDEX `fk_sms_id_1` (`sms_id` ASC)'
		),
		'ENGINE=InnoDB  DEFAULT CHARSET=utf8'
	    );
	}

	public function down()
	{
		$this->dropTable('sms');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}