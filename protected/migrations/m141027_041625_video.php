<?php

class m141027_041625_video extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			'trainer_video',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT',
				'trainer_id' => 'int(11) not null',
				'link' => 'VARCHAR(255) NOT NULL',
				'title' => 'VARCHAR(255) NOT NULL',
				'timestamp' => 'DATETIME NOT NULL',
				'PRIMARY KEY (`id`)' ,
				'INDEX `fk_trainer_video_1` (`trainer_id` ASC)'
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8'
		);
		$this->createTable(
			'trainer_photo',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT',
				'trainer_id' => 'int(11) not null',
				'photo' => 'VARCHAR(255) NOT NULL',
				'title' => 'VARCHAR(255) NOT NULL',
				'timestamp' => 'DATETIME NOT NULL',
				'PRIMARY KEY (`id`)' ,
				'INDEX `fk_trainer_photo_1` (`trainer_id` ASC)'
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8'
		);
	}

	public function down()
	{
		$this->dropTable('trainer_video');
		$this->dropTable('trainer_photo');
	}
}