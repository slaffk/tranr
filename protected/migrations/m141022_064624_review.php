<?php

class m141022_064624_review extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			'trainer_review',
			array(
				'id' => 'int(11) not null AUTO_INCREMENT',
				'trainer_id' => 'int(11) not null',
				'email' => 'VARCHAR(255) NOT NULL',
				'name' => 'VARCHAR(255) NOT NULL',
				'type' => 'TINYINT(1) NOT NULL DEFAULT 0',
				'date' => 'DATETIME NOT NULL',
				'text' => 'TEXT NOT NULL',
				'ip' => 'VARCHAR(15) NOT NULL',
				'PRIMARY KEY (`id`)' ,
				'INDEX `fk_trainer_review_1` (`trainer_id` ASC)'
			),
			'ENGINE=InnoDB  DEFAULT CHARSET=utf8'
		);
		
	}

	public function down()
	{
		$this->dropTable('trainer_review');
	}
}