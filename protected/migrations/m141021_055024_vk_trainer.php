<?php
/**
 * Class m141021_055024_vk_trainer
 */
class m141021_055024_vk_trainer extends CDbMigration {

	public function safeUp() {

		$this->addColumn('trainer', 'vk_link', 'varchar(255) default null');
		$this->addColumn('trainer', 'fb_link', 'varchar(255) default null');
		$this->addColumn('trainer', 'moderator_comment', 'text default null');
	}

	public function safeDown() {

		$this->dropColumn('trainer', 'vk_link');
		$this->dropColumn('trainer', 'fb_link');
		$this->dropColumn('trainer', 'moderator_comment');
	}
}
