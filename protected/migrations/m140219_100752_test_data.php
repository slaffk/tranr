<?php
/**
 * Class m140219_100752_test_data
 */
class m140219_100752_test_data extends CDbMigration {

	public function safeUp() {

		$this->insert('city', array(
			'id' => '1',
			'name' => 'Новосибирск',
			'alias' => 'nsk',
			'timezone' => '+07:00',
			'default' => '1',
			'active' => '1',
		));

		$this->insert('page', array(
			'id' => '1',
			'alias' => '-',
			'path' => '-',
			'slash_id' => '1',
			'title' => 'Главная {city}',
			'layout' => 'column3',
		));

		$this->insert('resource', array(
			'id' => '1',
			'module' => 'manager',
			'description' => 'Модуль администрирования',
		));

		$this->insert('role', array(
			'id' => '1',
			'name' => 'Администратор системы',
			'alias' => 'admin',
		));

		$this->insert('role_to_resource', array(
			'role_id' => '1',
			'resource_id' => '1',
		));

		$this->insert('user', array(
			'id' => '1',
			'email' => 'slava.volynets@gmail.com',
			'password' => crypt('hdh672h2g23gf32', substr('hdh672h2g23gf32', 0, 2)),
			'begin_date' => new CDbExpression('now()'),
			'end_date' => '2037-01-01 00:00:00',
			'active' => '1',
			'activated' => '1',
		));

		$this->insert('user_to_role', array(
			'user_id' => '1',
			'role_id' => '1',
		));

	}

	public function safeDown() {

		$this->delete('user_to_role', 'user_id = 1 and role_id = 1');
		$this->delete('user', 'id = 1');
		$this->delete('role_to_resource', 'resource_id = 1 and role_id = 1');
		$this->delete('page', 'id = 1');
		$this->delete('role', 'id = 1');
		$this->delete('resource', 'id = 1');
		$this->delete('city', 'id = 1');
	}
}