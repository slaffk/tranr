<?php

class AppWidget extends CWidget {

	/**
	 * @var string
	 */
	public $action;

	/**
	 * @var
	 */
	public $caption;

	/**
	 * run
	 */
	public function run() {

		$actionsList = static::getActionList();
		if (isset($actionsList[$this->action])) {
			$this->{'action'.$this->action}();
		}
	}
}
