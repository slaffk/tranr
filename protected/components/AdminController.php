<?php
/**
 * Class AdminController
 */
class AdminController extends Controller {

	const CTRL_NAME = 'Раздел администрирования';

	/**
	 * инициализация
	 */
	public function init() {

		parent::init();
		$this->page = new Page();
		$this->page->widgets = array(
			WidgetHelper::createWidget('MenuWidget', Widget::ZONE_HEADER_1, array('action' => 'admin'))
		);
		$this->layout = '//layouts/column1';
	}

	/**
	 * @param string $views
	 * @return bool
	 */
	protected function beforeRender($views) {

		$this->pageTitle = static::CTRL_NAME;
		return parent::beforeRender($views);
	}
}
