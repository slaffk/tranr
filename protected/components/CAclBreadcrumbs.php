<?php
/**
 * При выводе ссылок проверяем их в ACL
 */

Yii::import('zii.widgets.CBreadcrumbs');

class CAclBreadcrumbs extends CBreadcrumbs {

	public $activeLinkTemplate = '<li><a href="{url}">{label}</a></li>';
	public $inactiveLinkTemplate = '<li><span>{label}</span></li>';
	public $separator = '<li><span class="divider"> / </span></li>';
	public $tagName = 'ul';
	public $htmlOptions = array('class' => 'breadcrumbs breadcrumb');

	public function run() {

		if (empty($this->links)) {
			return;
		}

		$aclLinks = array();
		$user = Yii::app()->user;

		foreach($this->links as $label => $url) {

			$strUrl = is_array($url) ? CHtml::normalizeUrl($url) : $url;
			$arr = explode('/', $strUrl);

			$active = (is_string($label) || is_array($url)) &&
				count($arr) > 2 && $user &&
				(is_string($label) || is_array($url)) &&
				$user->checkAcl($arr[0], $arr[1], $arr[2]);

			$url = (!$active && is_string($label)) ? $label : $url;

			$aclLinks = array_merge($aclLinks, $active ? array($label => $url) : array($url));
		}

		$this->links = $aclLinks;

		parent::run();
	}
}