<?php
/**
 * Class ActiveRecord
 */
class ActiveRecord extends CActiveRecord {

	/** @var string */
	public $returnUrl;

	/**
	 * @return array|void
	 */
	public function rules() {

		return array(
			array('returnUrl', 'length', 'max' => 512),
		);
	}

	/**
	 * @param $limit
	 * @return $this
	 */
	public function limit($limit) {

		$this->getDbCriteria()->mergeWith(array(
			'limit' => $limit,
		));
		return $this;
	}

	/**
	 * при поиске добавляет условие ATTR != VALUE
	 * @param array $params
	 * @return $this
	 */
	public function not(array $params) {

		if (!is_array($params)) {
			return $this;
		}

		foreach ($params as $key => $value) {

			if (!in_array($key, $this->attributeNames()) || is_null($value)) continue;

			$this->getDbCriteria()->mergeWith(array(
				'condition' => '`' . $key . '` != :'.$key.'value',
				'params' => array($key.'value' => $value),
			));
		}
		return $this;
	}

	/**
	 * при поиске добавляет условие ATTR = VALUE
	 * @param array $params
	 * @return $this
	 */
	public function is(array $params) {

		if (!is_array($params)) {
			return $this;
		}

		foreach ($params as $key => $value) {

			if (!in_array($key, $this->attributeNames()) || is_null($value)) continue;

			$this->getDbCriteria()->mergeWith(array(
				'condition' => '`' . $key . '` = :'.$key.'value',
				'params' => array($key.'value' => $value),
			));
		}
		return $this;
	}

	/**
	 * при поиске добавляет условие ATTR in ARRAY
	 * @param array $params
	 * @return $this
	 */
	public function inArray(array $params) {

		if (!is_array($params)) {
			return $this;
		}

		foreach ($params as $key => $value) {

			if (!in_array($key, $this->attributeNames()) || !is_array($value)) continue;

			$criteria = $this->getDbCriteria();
			$criteria->addInCondition($key, $value);
			$this->setDbCriteria($criteria);
		}
		return $this;
	}

	/**
	 * при поиске добавляет условие ATTR not in ARRAY
	 * @param array $params
	 * @return $this
	 */
	public function notInArray(array $params) {

		if (!is_array($params)) {
			return $this;
		}

		foreach ($params as $key => $value) {

			if (!in_array($key, $this->attributeNames()) || !is_array($value)) continue;

			$criteria = $this->getDbCriteria();
			$criteria->addNotInCondition($key, $value);
			$this->setDbCriteria($criteria);
		}
		return $this;
	}

	/**
	 * @param $attr
	 * @param $value
	 * @return $this
	 */
	public function setAttr($attr, $value) {

		if (in_array($attr, $this->attributeNames())) {
			$this->$attr = $value;
		}
		return $this;
	}


	/**
	 * @var array маппинг классов для связанных записей
	 */
	protected $propertyMap = array(
		'Article' => 'article_id',
		'Trainer' => 'trainer_id',
	);

	/**
	 * @var array
	 */
	protected $propertyMapWith = array(
		'Article' => array('category'),
		'Trainer' => array('category'),
	);

	/**
	 * @var array
	 */
	protected $additionalConditions = array(
		'Article' => 'article.status = 1',
		'Trainer' => 'trainer.active = 1',
	);

	protected $aliasMap = array(
		'Article' => 'article',
		'Trainer' => 'trainer',
	);

	/**
	 * @param string $relatedClass
	 * @param int $count
	 * @param array $options
	 * @return bool|ArticleToTag[]|TrainerToTag[]|NewsToTag[]
	 */
	public function getRelatedByTags($relatedClass = 'Article', $count = 3, $options=array()) {

		$class = get_class($this);
		if (!array_key_exists($class, $this->propertyMap) || !array_key_exists($relatedClass, $this->propertyMap) ||
			!class_exists($class) || !class_exists($relatedClass) || !class_exists($class . 'ToTag') ||
			!class_exists($relatedClass . 'ToTag')) {

			return false;
		}
		/** @var $modelTag ArticleToTag|TrainerToTag|NewsToTag */
		$modelTag = call_user_func_array(array($class . 'ToTag', 'model'), array());
		/** @var $relatedModelTag ArticleToTag|TrainerToTag|NewsToTag */
		$relatedModelTag = call_user_func_array(array($relatedClass . 'ToTag', 'model'), array());
		//если запрашиваются релейтед на самих себя, например релейтед статьи для открытой статьи
		$subCondition = ($class == $relatedClass) ? ' AND t1.'.$this->propertyMap[$class].' <> t2.'.$this->propertyMap[$class] : '';
		$subCondition.= (isset($options['not_in']) && is_array($options['not_in']) && !empty($options['not_in'])) ? " AND t2.".$this->propertyMap[$relatedClass]." NOT IN (".implode(',', $options['not_in']).")" :"";

		$sql = 'SELECT t2.' . $this->propertyMap[$relatedClass] . ' as `id`, count(t2.' . $this->propertyMap[$relatedClass] . ') as `times`
                FROM `' . $modelTag->tableName() . '` as t1
                LEFT JOIN `' . $relatedModelTag->tableName() . '` as t2 ON t1.tag_id = t2.tag_id
                WHERE t1.' . $this->propertyMap[$class] . '=' . intval($this->id) . $subCondition . '
                GROUP BY t2.' . $this->propertyMap[$relatedClass] . ' ORDER BY `times` desc
                LIMIT ' . intval($count) . '
                ';

		$related = array();
		$results = Yii::app()->db->createCommand($sql)->query();
		if ($results) {
			$ids = array();
			foreach ($results as $result) {
				if ($result['id']) $ids[] = $result['id'];
			}

			if (!empty($ids)) {
				$object = call_user_func_array(array($relatedClass, 'model'), array());

				$criteria = new CdbCriteria();
				$criteria->addInCondition(@$this->aliasMap[$relatedClass] . '.id', $ids);
				if (array_key_exists($relatedClass, $this->additionalConditions)) {
					$criteria->addCondition($this->additionalConditions[$relatedClass]);
				}
				$criteria->order = 'Field ('.@$this->aliasMap[$relatedClass].'.id, ' . implode(',', $ids) . ')';
				$related = $object->with($this->propertyMapWith[$class])->findAll($criteria);
			}
		}
		return $related;
	}
}