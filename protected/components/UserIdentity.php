<?php

class UserIdentity extends CUserIdentity {

	private $_record;
	private $passCheck = true;

	/**
	 * @param string $username
	 * @param string $password
	 * @param bool $passCheck
	 */
	public function __construct($username, $password, $passCheck = true) {

		$this->passCheck = $passCheck;
		parent::__construct($username, $password);
	}

	public function authenticate() {

		/** @var $record User */
		$record = User::model()->active()->find('email = :email', array('email' => $this->username));

		if ($record === null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} else if ($record->password !== crypt($this->password, $record->password) && $this->passCheck) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			$record->last_login = new CDbExpression('now()');
			$record->last_login_ip = Yii::app()->request->getUserHostAddress();
			$record->save();
			$this->_record = $record;
			$this->errorCode = self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function getId() {

		return $this->_record->id;
	}
}