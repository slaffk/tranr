<?php
/**
 * Class ClientScript
 */
class ClientScript extends CClientScript {

	private $translate = array();

	/**
	 * метатеги вставляем перед </head>
	 * @param string $output the output to be inserted with scripts.
	 */
	public function renderHead(&$output) {

		$html='';
		foreach($this->metaTags as $meta)
			$html.=CHtml::metaTag($meta['content'],null,null,$meta)."\n";
		foreach($this->linkTags as $link)
			$html.=CHtml::linkTag(null,null,null,null,$link)."\n";
		foreach($this->cssFiles as $url=>$media)
			$html.=CHtml::cssFile($url,$media)."\n";
		foreach($this->css as $css)
			$html.=CHtml::css($css[0],$css[1])."\n";
		if($this->enableJavaScript)
		{
			if(isset($this->scriptFiles[self::POS_HEAD]))
			{
				foreach($this->scriptFiles[self::POS_HEAD] as $scriptFileValueUrl=>$scriptFileValue)
				{
					if(is_array($scriptFileValue))
						$html.=CHtml::scriptFile($scriptFileValueUrl,$scriptFileValue)."\n";
					else
						$html.=CHtml::scriptFile($scriptFileValueUrl)."\n";
				}
			}

			if(isset($this->scripts[self::POS_HEAD]))
				$html.=$this->renderScriptBatch($this->scripts[self::POS_HEAD]);
		}

		if($html!=='')
		{
			$count=0;
			$output=preg_replace('/(<\/head\b[^>]*>|<\\/head\s*>)/is','<###head###>$1',$output,1,$count);
			if($count)
				$output=str_replace('<###head###>',$html,$output);
			else
				$output=$html.$output;
		}
	}

	/**
	 * @param string $content
	 * @param null $name
	 * @param null $httpEquiv
	 * @param array $options
	 * @param null $id
	 * @param bool $replace
	 * @return ClientScript
	 */
	public function registerMetaTag($content, $name = null, $httpEquiv = null, $options = array(), $id = null, $replace = false) {

		$id = $id ? $id : $name;
		$this->hasScripts = true;
		if ($name !== null) $options['name'] = $name;
		if ($httpEquiv !== null) $options['http-equiv'] = $httpEquiv;
		$options['content'] = str_replace(array_keys($this->translate), array_values($this->translate), $content);
		if (!$id || !array_key_exists($id, $this->metaTags) || $replace) {
			$this->metaTags[null === $id ? count($this->metaTags) : $id] = $options;
			$params = func_get_args();
			$this->recordCachingAction('clientScript', 'registerMetaTag', $params);
		}
		return $this;
	}

	/**
	 * @param string $content
	 * @param null $name
	 * @param null $httpEquiv
	 * @param array $options
	 * @param null $id
	 * @param bool $replace
	 * @param bool $multiple
	 * @return ClientScript
	 */
	public function registerMetaProperty($content, $name = null, $httpEquiv = null, $options = array(), $id = null, $replace = false, $multiple = false) {

		$id = $id ? $id : $name;
		$this->hasScripts = true;
		if ($name !== null) $options['property'] = $name;
		if ($httpEquiv !== null) $options['http-equiv'] = $httpEquiv;
		$options['content'] = str_replace(array_keys($this->translate), array_values($this->translate), $content);
		if (!$id || !array_key_exists($id, $this->metaTags) || $replace) {
			$this->metaTags[null === $id || $multiple ? count($this->metaTags) : $id] = $options;
			$params = func_get_args();
			$this->recordCachingAction('clientScript', 'registerMetaTag', $params);
		}
		return $this;
	}

	/**
	 * @param array $array
	 */
	public function translate(array $array) {
		$this->translate = $array;
		foreach ($this->metaTags as &$tag) {
			$tag['content'] = str_replace(array_keys($array), array_values($array), $tag['content']);
		}
	}
}