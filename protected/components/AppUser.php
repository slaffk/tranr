<?php

class AppUser extends CWebUser {

	/* @property User $_model */
	private $_model = null;

	/** @var array */
	private $_resources = array();

	/**
	 * @return User|null
	 */
	public function getModel() {

		if (!$this->isGuest && $this->_model === null) {
			$this->_model = User::model()->active()
				->with(array(
						'roles' => array(
							'with' => 'resources',
							'together' => 1,
						)
				))
				->together()
				->findByPk($this->id);
		}

		if (!$this->_model && !$this->getIsGuest()) {
			$this->logout();
			header('Location: /site/login');
			die;
		}

		return $this->_model;
	}

	/**
	 * @return Role[]|bool
	 */
	private function getRoles() {
		if ($user = $this->getModel()) {
			return $user->roles;
		}
		return false;
	}

	/**
	 * @return Resource[]
	 */
	private function getResources() {
		if (empty($this->_resources)) {
			if ($roles = $this->getRoles()) {
				foreach ($roles as $role) {
					foreach ($role->resources as $resource) {
						$key = md5($resource->module . $resource->controller . $resource->action);
						$this->_resources[$key] = $resource;
					}
				}
			}
		}
		return $this->_resources;
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public function hasRole($id) {

		if ($model = $this->getModel()) {
			foreach ($model->roles as $role) {
				if ($role->id == $id) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param $module
	 * @param null $controller
	 * @param null $action
	 * @return bool
	 */
	public function checkAcl($module, $controller = null, $action = null) {

		return array_key_exists(md5($module), $this->getResources()) || //разрешен весь модуль?
			array_key_exists(md5(uncamelize($module)), $this->getResources()) || //разрешен весь модуль?
			array_key_exists(md5($module . $controller), $this->getResources()) || //можт весь контроллер?
			array_key_exists(md5(uncamelize($module) . uncamelize($controller)), $this->getResources()) || //можт весь контроллер?
			array_key_exists(md5($module . $controller . $action), $this->getResources()) || //ну хотя бы экшн?
			array_key_exists(md5(uncamelize($module) . uncamelize($controller) . uncamelize($action)), $this->getResources()); //ну хотя бы экшн?
	}

	/**
	 * @param array $array
	 * @return bool
	 */
	public function checkAclInArray(array $array) {

		foreach ($array as $acl) {
			if (!is_array($acl)) {
				$acl = array_filter(explode('/', $acl));
			}
			$module = current($acl);
			$controller = @next($acl);
			$action = @next($acl);
			if ($this->checkAcl($module, $controller, $action)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * проверка на авторизованность
	 */
	public function requireAuthorized() {

		if ($this->getIsGuest() || !$this->getModel()) {
			Yii::app()->user->returnUrl = Yii::app()->request->getUrl();
			Yii::app()->request->redirect(CHtml::normalizeUrl(Yii::app()->user->loginUrl));
			Yii::app()->end();
		}
	}

	/**
	 * @return DateTime
	 */
	public function getLastLogin() {

		if ($user = $this->getModel()) {
			return $user->last_login;
		}
		return false;
	}
}
