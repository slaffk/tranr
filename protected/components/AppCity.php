<?php
/**
 * Class AppCity
 * Работа со списком компаний, текущая компания
 */
class AppCity extends CApplicationComponent {

	/**
	 * @var City
	 */
	private $city = null;

	/**
	 * init
	 */
	public function init() {

		parent::init();

		$serverName = str_replace('www.', '', Yii::app()->request->getServerName());
		if ($serverName == Yii::app()->params['mainDomain']) {
			$this->city = City::model()->def()->find();
		} else {
			$this->city = City::model()->root()->find('concat(alias, :main) = :name', array(
				':main' => '.' . Yii::app()->params['mainDomain'],
				':name' => $serverName,
			));
		}
		if (!$this->city) {
			throw new CHttpException(500, 'Server Error', 500);
		}
		if ($this->city->timezone && in_array($this->city->timezone, City::$timezones)) {
			Yii::app()->db->createCommand('set time_zone = "'.$this->city->timezone.'"')->execute();
		}
	}

	/**
	 * @param string $name
	 * @return array|mixed|null
	 * @throws CException
	 */
	public function __get($name) {

		$getter = 'get' . $name;
		if (property_exists($this->city, $name) || $this->city->hasAttribute($name)) {
			return $this->city->$name;
		} elseif(method_exists($this->city, $getter)) {
			return $this->$getter();
		}
		throw new CException(Yii::t('yii','Property "{class}.{property}" is not defined.', array('{class}'=>get_class($this), '{property}'=>$name)));
	}

	public function getUrl() {

		return $this->city->url;
	}
}
