<?php
class Sms extends CApplicationComponent {
    
    const URL = 'https://gate.smsaero.ru';
    
    const PATH_BALANCE = '/balance/';
    
    const PATH_STATUS = '/status/';
    
    const PATH_SEND = '/send/';
    
    const PATH_SENDERS = '/senders/';
    
    const PATH_SIGN = '/sign/';
    
    const DEFAULT_SENDER = 'INFORM';
    
    public $user;

    public $password;
    
    public $params = array();
    
    private $errors = array();
    
    
    public function getErrors() {
		return $this->errors;
    }
    
    public function init() {
		$this->params['user'] = $this->user;
		$this->params['password'] = md5($this->password);
		$this->params['answer'] = 'json';
		return parent::init();
    }
    
	/**
	 * Отправляет сообщение
	 * @param int $phone
	 * @param string $message
	 * @return boolean or int
	 */
    public function send($phone, $message) {
		$from = self::DEFAULT_SENDER;
		$data = $this->api(self::PATH_SEND, array(
			'to'=>$phone, 
			'text'=>$message, 
			'from'=>$from,
		));
		if($data === false) {
			return false;
		}
		// TODO: Проверить что при нулевом балансе так будет.
		if(isset($data['no credits'])) {
			$this->errors[] = 'Недостаточно sms на балансе.';
			return false;
		}
		if(isset($data['result'])) {
			if($data['result'] == 'reject' and isset($data['reason'])) {
				switch($data['reason']) {
					case 'incorrect destination adress':
						$this->errors[] = 'Неверный номер.';
						break;
					case 'empty field':
						$this->errors[] = 'Неверный номер.';
						break;
					case 'incorrect user or password':
						$this->errors[] = 'Ошибка авторизации.';
						break;
					case 'incorrect sender name':
						$this->errors[] = 'Неверная '.$from.' (незарегистрированная) подпись отправителя.';
						break;
					case 'incorrect date':
						$this->errors[] = 'Неправильный формат даты.';
						break;
					case 'in blacklist':
						$this->errors[] = 'Телефон '.$phone.' находится в черном списке.';
						break;
					default:
						$this->errors[] = 'Произошла ошибка.';
						break;
				}
				return false;
			} else if($data['result'] == 'accepted' and isset($data['id'])) {
				return (int) $data['id'];
			}
		}
		return false;
    }
    
    public function getResponse($id) {
		$data = $this->api(self::PATH_STATUS, array('id'=>$id));
		return ($data === false) ? false : $data;
    }
    
    /**
     * 
     * @return int Баланс в сервисе https://gate.smsaero.ru или <b>FALSE</b> при ошибке.
     */
    public function getBalance() {
		$data = $this->api(self::PATH_BALANCE, array());
		return ($data === false) ? false : (int) $data['balance'];
    }
    /**
     * Подпись отправителя – это подпись адресанта, которую абоненты видят на 
     * своем телефоне при получении SMS-сообщения. SMS Aero передает сообщения 
     * по Прямому каналу доставки, а значит – вы сможете выбрать ту подпись, 
     * которая вам необходима. Размер подписи ограничен 11 символами. 
     * В качестве подписи по Прямому каналу можно использовать название компании, 
     * электронный почтовый адрес или адрес сайта.
     * @return array Список доступных подписей отправителя в сервисе https://gate.smsaero.ru или <b>FALSE</b> при ошибке.
     */
    public function getSenders() {
		return $this->api(self::PATH_SENDERS, array());
    }
    
    /**
     * Запрос новой подписи
     * @param string $sign
     * @return boolean
     */
    public function addSign($sign) {
		$data = $this->api(self::PATH_SIGN, array('sign'=>$sign));
		if($data === false or !isset($data['accepted'])) {
			return false;
		}
		$ret = false;
		switch($data['accepted']) {
			case 'approved':
				$ret = true;
				break;
			case 'rejected':
				$this->errors[] = 'Не удалось добавить новую подпись отправителя. Подпись отклонена.';
				$ret = false;
				break;
			case 'pending':
				$ret = true;
				break;
			default:
				break;
		}
		return $ret;
    }
    
    private function api($path, $params = array()) {
		$params = array_merge($this->params, $params);
		$query = http_build_query($params);
		$url = self::URL.$path.'?'.$query;
		$data = file_get_contents($url);
		if($data === false) {
			$this->errors[] = self::URL." не доступен.";
			return false;
		}
		return CJSON::decode($data);
    }
}
