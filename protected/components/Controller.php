<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

	const RECORDS_ON_PAGE = 20;

	/**
	 * @var string
	 */
	public $layout = '//layouts/column1';

	/**
	 * @var Page
	 */
	public $page;

	/**
	 * @var array
	 */
	public $menu = array();

	/**
	 * @var array
	 */
	public $breadcrumbs = array();

	/**
	 * @var array
	 */
	public $filterIds = array();

	/**
	 * @var array
	 */
	public $vars = array();

	/**
	 * @var null
	 */
	public $model = null;

	public $translate;

	/**
	 * инициализация
	 */
	public function init() {

		if (!Yii::app()->user->isGuest) {
			$this->layout = '//layouts/column2';
		}
		Yii::app()->city->id;
		if (get_class($this) != 'SiteController') {
			//Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish('css/form.css'));
			Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish('css/main.css'));
		}
	}

	/**
	 * @return array
	 */
	final public function filters() {

		return array('AccessControl');
	}

	/**
	 * @param CFilterChain $filterChain
	 */
	final public function filterAccessControl($filterChain) {

		$rules = $this->accessRules();

		$controller = $filterChain->controller;

		$action = $controller ? $controller->getAction() : null;
		$action = $action ? $action->getId() : null;

		$module = $controller ? $controller->getModule() : null;
		$module = $module ? $module->getId() : null;

		$controller = $controller ? $controller->getId() : null;

		if ($module) {
            $user = Yii::app()->user;
			$rules[] = $user->isGuest || !$user->checkAcl($module, $controller, $action) ? array('deny') : array('allow');
		} else {
			$rules[] = array('allow');
		}

		$filter = new CAccessControlFilter;
		$filter->setRules($rules);
		$filter->filter($filterChain);
	}

	protected function beforeRender($views) {

		$this->translate = array(
			'{city}' => Yii::app()->city->name,
			'{city_pril}' => Yii::app()->city->name_pril,
			'{city_pril_where}' => Yii::app()->city->name_pril_where,
		);
		Yii::app()->clientScript->translate($this->translate);
		$this->pageTitle = str_replace(array_keys($this->translate), array_values($this->translate), $this->pageTitle);

		return parent::beforeRender($views);
	}

	/**
	 * @return bool
	 */
	protected function beforeAction($action) {

		$token = Yii::app()->request->getQuery('token');
		if ($token) {
			CommonHelper::autoAuth($token);
		}
		return true;
	}

	/**
	 * @param $title
	 * @return string
	 */
	public function translate($title) {

		return str_replace(array_keys($this->translate), array_values($this->translate), $title);
	}

	/**
	 * @return string
	 */
	public function getTitle() {

		return $this->translate($this->pageTitle);
	}
}
