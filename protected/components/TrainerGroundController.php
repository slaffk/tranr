<?php
/**
 * Class TrainerGroundController
 */
class TrainerGroundController extends Controller {

	const CTRL_NAME = 'Личный кабинет тренера';

	/**
	 * инициализация
	 */
	public function init() {

		parent::init();
		$this->page = new Page();
		$this->page->widgets = array(
			WidgetHelper::createWidget('MenuWidget', Widget::ZONE_HEADER_1, array('action' => 'trainer'))
		);
		$this->layout = '//layouts/column1';
	}

	/**
	 * @param string $views
	 * @return bool
	 */
	protected function beforeRender($views) {

		$this->pageTitle = static::CTRL_NAME;
		return parent::beforeRender($views);
	}
}
