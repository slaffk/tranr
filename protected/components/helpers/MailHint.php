<?php

class MailHint {

	public static $params = array(

		MailTemplate::EVENT_USER_REG => array(
			'site_url' => 'URL сайта',
			'city' => 'Название города',
			'city_pril' => 'Название города (прилагательное)',
			'city_pril_where' => 'Название города (где?)',
			'email_support' => 'E-mail поддержки',
			'unsubscribe_url' => 'URL отписки',
			'subscribe_url' => 'URL управления подпиской',

			'email' => 'E-mail пользователя',
			'password' => 'Пароль пользователя',
			'firstname' => 'Имя',
			'lastname' => 'Фамилия',
			'middlename' => 'Отчество',
			'activation_code' => 'Код активации',
			'activation_url' => 'URL активации',
			'resume_add_url' => 'URL добавления резюме',
		),
		MailTemplate::EVENT_TRAINER_REG => array(
			'site_url' => 'URL сайта',
			'city' => 'Название города',
			'city_pril' => 'Название города (прилагательное)',
			'city_pril_where' => 'Название города (где?)',
			'email_support' => 'E-mail поддержки',
			'unsubscribe_url' => 'URL отписки',
			'subscribe_url' => 'URL управления подпиской',

			'email' => 'E-mail пользователя',
			'password' => 'Пароль пользователя',
			'firstname' => 'Имя',
			'lastname' => 'Фамилия',
			'middlename' => 'Отчество',
			'activation_code' => 'Код активации',
			'activation_url' => 'URL активации',
			'vacancy_add_url' => 'URL добавления вакансии',
		),
	);
}