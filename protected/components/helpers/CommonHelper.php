<?php
/**
 * Class CommonHelper
 */
class CommonHelper {

	/**
	 * @param $string
	 * @param int $length
	 * @param bool $breakWords
	 * @param string $postfix
	 * @return string
	 */
	public static function truncate($string, $length = 50, $breakWords = false, $postfix = '...') {
		$wordEndChars = ',.?!;:'; /* символы окончания слова */
		$truncated = trim($string);
		$length = (int)$length;
		if (!$string) {
			return $truncated;
		}
		$fullLength = iconv_strlen($truncated, 'UTF-8');
		if ($fullLength > $length) {
			$truncated = trim(iconv_substr($truncated, 0, $length, 'UTF-8'));
			if (!$breakWords) {
				$words = explode(' ', $truncated);
				$wordCount = sizeof($words);
				if (rtrim($words[$wordCount-1], $wordEndChars) == $words[$wordCount-1]) {
					unset($words[$wordCount-1]);
				}
				$wordCount = sizeof($words);
				if (!empty($words[$wordCount-1])) {
					$words[$wordCount-1] = rtrim($words[$wordCount-1], $wordEndChars);
				}
				$truncated = implode(' ', $words);
			}
			$truncated .= $postfix;
		}
		return $truncated;
	}

	/**
	 * @param $n
	 * @param $form1 'письмо'
	 * @param $form2 'письма'
	 * @param $form5 'писем'
	 * @return mixed
	 */
	public static function pluralForm($n, $form1, $form2, $form5) {
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $form5;
		if ($n1 > 1 && $n1 < 5) return $form2;
		if ($n1 == 1) return $form1;
		return $form5;
	}
	
	/**
	 * CommonHelper::createDirectory(Yii::getPathOfAlias('webroot'), '/uploads/user/somedir');
	 * @param string $from
	 * @param string $directory
	 */
	public static function createDirectory($from, $directory) {
		$dirs = explode(DIRECTORY_SEPARATOR, $directory);
		$path = $from;
		foreach($dirs as $d) {
			if($d == "") continue;
			$path .= DIRECTORY_SEPARATOR.$d;
			if(!is_dir($path)) {
				mkdir($path, 0777);
				chmod($path, 0777);
			}
		}
	}

	/**
	 * @param $phone
	 * @return string
	 */
	public static function formatPhone($phone) {

		$origin = $phone;
		$phone = str_replace(array('.', ',', '+7', '+', ' ', '-', '(', ')'), '', $phone);
		if (@$phone[0] == '8') {
			$phone[0] = ' ';
			$phone = trim($phone);
		}

		return @$phone[3] ? '+7 (' . @$phone[0] . @$phone[1] . @$phone[2] . ') ' . @$phone[3] . @$phone[4] . @$phone[5] . ' ' .
		@$phone[6] . @$phone[7] . ' ' .  @$phone[8] . @$phone[9] : $origin;
	}
}
