<?php
/**
 * Class Mailer
 */
class Mailer {

	/**
	 * @param $event
	 * @param $to
	 * @param $model
	 * @throws CHttpException
	 */
	public static function mail($event, $to, $model) {

		include_once('Mail.php');
		include_once('Mail/mime.php');

		/** @var $user User */
		$user = User::model()->findByAttributes(array(strpos($to, '@') === false ? 'auth_phone' : 'email' => $to));
//		if ($user && !$user->subscribed && !in_array($event, array_keys(MailTemplate::$bulkEvents))) {
//			return;
//		}

		$params = self::makeEventParams($event, $model, $to);

		/** @var $template MailTemplate */
		$template = MailTemplate::model()->findByAttributes(array('event' => $event));
		if (!$template) {
			return;
		}

		$crLf = "\r\n";

		$header = str_replace(array_keys($params), array_values($params),
			Setting::findByAttributesOrCreate(array('name' => Setting::MAIL_HEADER))->value
		);
		$header_html = str_replace(array_keys($params), array_values($params),
			Setting::findByAttributesOrCreate(array('name' => Setting::MAIL_HEADER_HTML))->value
		);
		$footer = str_replace(array_keys($params), array_values($params),
			Setting::findByAttributesOrCreate(array('name' => Setting::MAIL_FOOTER))->value
		);
		$footer_html = str_replace(array_keys($params), array_values($params),
			Setting::findByAttributesOrCreate(array('name' => Setting::MAIL_FOOTER_HTML))->value
		);

		if (strpos($to, '@') === false) {
			if ($template->content_sms) {
				require_once(Yii::getPathOfAlias('application.extensions') . '/smsc_api.php');
				//Yii::import('.');
				$content = str_replace(array_keys($params), array_values($params), $template->content_sms);
				send_sms($to, $content, 0,0 ,0,0, 'Tranr.ru');
			}
			return;
		}

		$subject = str_replace(array_keys($params), array_values($params), $template->title);
		$bodyTxt = $header . $crLf . str_replace(array_keys($params), array_values($params), $template->content) . $crLf . $footer . $crLf;
		$bodyHtml = '<html><head><title>'.$subject.'</title></head><body>'.
			$header_html . $crLf .
			str_replace(array_keys($params), array_values($params), $template->content_html) .
			$footer_html . $crLf .
			'</body></html>' . $crLf;

		$fromName = '=?UTF-8?B?'.base64_encode(Yii::app()->params['adminName']).'?=';
		$fromEmail = Yii::app()->params['adminEmail'];
		$headers = array(
			'To' => $to,
			'Message-ID' => time() .'-' . md5($fromEmail . $to) . '@mailer.tranr.ru',
			'From' => $fromName . ' <' . $fromEmail . '>',
			'Reply-To' => $fromEmail,
			'Subject' => $subject,
			'Precedence' => 'bulk',
		);
		$unsubscribeTxt = '{unsubscribe_url}';
		$unsubscribeTxt = str_replace(array_keys($params), array_values($params), $unsubscribeTxt);

		if ($unsubscribeTxt != '{unsubscribe_url}') {
			$headers['List-Unsubscribe'] = $unsubscribeTxt;
		}

		$mime = new Mail_mime($crLf);
		$mime->setTXTBody($bodyTxt);
		if ($template->content_html) {
			$mime->setHTMLBody($bodyHtml);
		}

		$body = $mime->get(array(
			'text_charset' => 'UTF-8',
			'html_charset' => 'UTF-8',
			'head_charset' => 'UTF-8',
		));
		$headers = $mime->headers($headers);

		$mail = Mail::factory('smtp');
		$mail->send($to, $headers, $body);
	}

	/**
	 * @param $event
	 * @param $model ActiveRecord
	 * @param $to
	 * @return array
	 */
	public static function makeEventParams($event, $model, $to) {

		$siteUrl = isset(Yii::app()->request) ? @Yii::app()->request->getHostInfo() : 'http://tranr.ru';
		$code = $model instanceof ActiveRecord && $model->hasAttribute('activation_code') ? @$model->activation_code : null;
		$code = $code ? $code : ($model instanceof ActiveRecord && $model->hasAttribute('user_id') && @$model->user ? $model->user->activation_code : null);
		$code = $code ? $code : ($model == 'test' ? User::model()->active()->find()->activation_code : null);

		$all = array(
			'{site_url}' => $siteUrl,
			'{city}' => isset(Yii::app()->city) ? @Yii::app()->city->name : 'Новосибирск',
			'{city_pril}' => isset(Yii::app()->city) ? @Yii::app()->city->name_pril : 'новосибирский',
			'{city_pril_where}' => isset(Yii::app()->city) ? @Yii::app()->city->name_pril_where : 'в Новосибирске',
			'{email_support}' => Yii::app()->params['adminEmail'],
			'{unsubscribe_url}' => $siteUrl . '/cabinet/subscribes/?u=1&c=' . $code,
			'{subscribe_url}' => $siteUrl . '/cabinet/subscribes/?c=' . $code,
		);

		if (in_array($event, array(MailTemplate::EVENT_USER_REG, MailTemplate::EVENT_TRAINER_REG,))) {
			$model = $model == 'test' ? User::model()->active()->find() : $model;
			/** @var $model User */
			return array_merge($all, array(
				'{email}' => $model->email,
				'{password}' => $model->plainPassword,
				'{firstname}' => $model->firstname,
				'{lastname}' => $model->lastname,
				'{middlename}' => $model->middlename,
				'{activation_code}' => $model->activation_code,
				'{resume_add_url}' =>  Yii::app()->request->getHostInfo() . '/cabinet/myresume/add/?token='.$model->activation_code,
				'{vacancy_add_url}' =>  Yii::app()->request->getHostInfo() . '/cabinet/myvacancies/add/?token='.$model->activation_code,
				'{activation_url}' =>  Yii::app()->request->getHostInfo() . '/site/activate/?code=' . $model->activation_code,
			));
		}
		return $all;
	}
}
