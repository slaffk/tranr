<?php
/**
 * Class WidgetHelper
 */
class WidgetHelper {

	const PATH_ALIAS = 'application.widgets.';
	private static $list = null;

	/**
	 * @return array
	 */
	public static function findAll() {

		if (is_null(static::$list)) {
			static::$list = array();
			foreach (glob(Yii::getPathOfAlias('application.widgets').'/*.php') as $filename) {
				$arr = explode('.', basename($filename));
				$widgetName = array_shift($arr);
				Yii::import(self::PATH_ALIAS . $widgetName);
				static::$list[$widgetName] = $widgetName::getName();
			}
		}
		return static::$list;
	}

	/**
	 * @param string $widget
	 * @return array
	 */
	public static function actions($widget) {

		if (in_array($widget, array_keys(static::findAll()))) {
			Yii::import(self::PATH_ALIAS . $widget);
			return $widget::getActionList();
		} else {
			return null;
		}
	}

	/**
	 * @param $widgetClass string
	 * @param $zone integer
	 * @param array $params
	 * @return Widget
	 */
	public static function createWidget($widgetClass, $zone, $params = array()) {

		$widget = new Widget();
		$widget->class = $widgetClass;
		$widget->zone_id = $zone;
		$paramsCollected = array();
		foreach ($params as $key => $value) {
			$param = new WidgetParam();
			$param->name = $key;
			$param->value = $value;
			$paramsCollected[] = $param;
		}
		$widget->params = $paramsCollected;
		return $widget;
	}
}
