<?php
/**
 * Class UriHelper
 */
class UriHelper {

	public static function controller_exists($uri, array $menu) {

		$uriAcl = array_filter(explode('/', $uri));
		$uriModule = current($uriAcl);
		$uriController = @next($uriAcl);
		foreach ($menu as $url => $title) {
			$acl = array_filter(explode('/', $url));
			$module = current($acl);
			$controller = @next($acl);
			if ($module == $uriModule && $controller == $uriController) {
				return true;
			}
		}
		return false;
	}
}