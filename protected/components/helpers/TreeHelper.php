<?php
/**
 * Class TreeHelper
 */
class TreeHelper {

	/**
	 * @param $model
	 * @param bool $expanded
	 * @param string $textAttr
	 * @param string $childAttr
	 * @return array
	 */
	public static function makeTreeCategory($model, $expanded = true, $textAttr = 'link', $childAttr = 'childs') {

		if (!$model) return null;

		$children = array();
		if (isset($model->$childAttr)) {
			foreach ($model->$childAttr as $child) {
				$children[] = static::makeTreeCategory($child, $expanded, $textAttr, $childAttr);
			}
		}

		return array(
			'text' => $model->$textAttr,
			'expanded' => $expanded,
			'children' => $children,
		);
	}
}

