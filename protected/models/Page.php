<?php
/**
 * Class Page
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $alias
 * @property string $path
 * @property integer $slash_id
 * @property string $title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $layout
 *
 * @property Widget[] $widgets
 * @property Page $parent
 * @property Page[] $childs
 */
class Page extends ActiveRecord {

	/**
	 * @var array
	 */
	public static $layouts = array(
		'column2' => 'Двуколоночный (сайдбар справа)',
		'column2l' => 'Двуколоночный (сайдбар слева)',
		'column3' => 'Трехколоночный (2 сайдбара)',
		'column1' => 'Одноколоночный (без сайдбара)',
		'empty' => 'Только контент (без верстки)',
	);

	const SLASH_ALLOWED = 1;
	const SLASH_REQUIRED = 2;
	const SLASH_NOT_ALLOWED = 3;

	/**
	 * @var array
	 */
	public static $slash = array(
		self::SLASH_ALLOWED => 'Возможен',
		self::SLASH_REQUIRED => 'Обязателен',
		self::SLASH_NOT_ALLOWED => 'Только без слеша',
	);

	public static $regexp = array(
		'/^articles\/([^\/]*)\/?$/' => '/articles/', # /articles/category/
		'/^articles\/([^\/]*)\/([^\/]*)\/([0-9]+)-([^\/]*)\/?$/' => '/articles/view/', # /articles/root/category/id-article_alias
		'/^articles\/([^\/]*)\/([0-9]+)-([^\/]*)\/?$/' => '/articles/view/', # /articles/root/id-article_alias
		'/^articles\/([^\/]*)\/([^\/]*)\/?$/' => '/articles/', # /articles/category/

		'/^trainers\/([^\/]*)\/?$/' => '/trainers/', # /trainers/category/
		'/^trainers\/([^\/]*)\/([^\/]*)\/([0-9]+)-([^\/]*)\/?$/' => '/trainers/view/', # /trainers/root/category/id-article_alias
		'/^trainers\/([^\/]*)\/([0-9]+)-([^\/]*)\/?$/' => '/trainers/view/', # /trainers/root/id-article_alias
		'/^trainers\/([^\/]*)\/([^\/]*)\/?$/' => '/trainers/', # /trainers/category/

		'/^news\/([^\/]*)\/?$/' => '/news/', # /news/category/
		'/^news\/([^\/]*)\/([^\/]*)\/?$/' => '/news/view/', # /news/category/article_alias

		'/^cabinet\/myvacancies\/add\/([^\/]*)\/?$/' => '/cabinet/myvacancies/add/',
		'/^cabinet\/myvacancies\/add\/([^\/]*)\/([^\/]*)\/?$/' => '/cabinet/myvacancies/add/',
		'/^cabinet\/blacklist\/([^\/]*)\/([^\/]*)\/?$/' => '/cabinet/blacklist/',
		'/^vacancy\/category([0-9]+)\/?$/' => '/vacancy/category/', # /vacancy/category[id]/
		'/^vacancy\/([^\/]*)\/?$/' => '/vacancy/view/', # /vacancy/id/
		'/^vacancy\/search\/new\/?$/' => '/vacancy/search/',
		'/^agency\/([0-9]+)\/?$/' => '/agency/view/', # /agency/id/
		'/^company\/([0-9]+)\/?$/' => '/company/view/', # /company/id/

		'/^cabinet\/myresume\/add\/([^\/]*)\/?$/' => '/cabinet/myresume/add/',
		'/^cabinet\/myresume\/add\/([^\/]*)\/([^\/]*)\/?$/' => '/cabinet/myresume/add/',
		'/^resume\/category([0-9]+)\/?$/' => '/resume/category/', # /resume/category[id]/
		'/^resume\/([^\/]*)\/?$/' => '/resume/view/', # /resume/[id]/
		'/^resume\/confirm\/([0-9]+)\/([^\/]*)\/?$/' => '/resume/confirm/', # /resume/confirm/[id]/[code]/
		'/^resume\/decline\/([0-9]+)\/([^\/]*)\/?$/' => '/resume/decline/', # /resume/decline/[id]/[code]/
		'/^resume\/search\/new\/?$/' => '/resume/search/',

		'/^cabinet\/([^\/]*)\/?$/' => '/cabinet/',
		'/^remind\/reset\/([^\/]*)\/([^\/]*)\/?$/' => '/remind/reset/',
	);

	/**
	 * @return string
	 */
	public function tableName() {
		return 'page';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('alias, layout', 'required'),
			array('path', 'unique'),
			array('alias', 'match', 'pattern' => '/^[A-Za-z0-9-]+$/'),
			array('slash_id', 'in', 'range' => array_keys(self::$slash)),
			array('layout', 'in', 'range' => array_keys(self::$layouts)),
			array('parent_id', 'exist', 'attributeName' => 'id', 'className' => 'Page'),
			array('parent_id, slash_id', 'numerical', 'integerOnly' => true),
			array('alias, layout', 'length', 'max' => 64),
			array('path, meta_keywords, meta_description', 'length', 'max' => 512),
			array('title', 'length', 'max' => 256),
			array('id, parent_id, alias, path, slash_id, title, meta_keywords, meta_description, layout', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'widgets' => array(self::HAS_MANY, 'Widget', 'page_id', 'order' => 'widgets.sort, widgets.id'),
			'parent' => array(self::BELONGS_TO, 'Page', 'parent_id'),
			'childs' => array(self::HAS_MANY, 'Page', 'parent_id'),
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['path'] = array(
			'condition' => 'page.path = :path',
		);
		return $scopes;
	}

	public function defaultScope() {

		return array(
			'alias' => 'page',
			'order' => 'page.path',
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'parent_id' => 'Родительская страница',
			'alias' => 'Альяс',
			'path' => 'Путь',
			'slash_id' => 'Завершающий слеш',
			'title' => 'Тайтл',
			'meta_keywords' => 'Мета ключевики',
			'meta_description' => 'Мета описание',
			'layout' => 'Лейаут',
		);
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		$path = array();
		$current = $this;
		while ($current) {
			$path[] = $current->alias;
			$current = $current->parent;
		}
		$this->path = implode('/', array_reverse($path));
		return parent::beforeValidate();
	}

	/**
	 * @return bool
	 */
	protected function beforeDelete() {

		foreach ($this->widgets as $widget) {
			$widget->delete();
		}
		return parent::beforeDelete();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('page.id',$this->id);
		$criteria->compare('page.parent_id',$this->parent_id);
		$criteria->compare('page.alias',$this->alias,true);
		$criteria->compare('page.path',$this->path,true);
		$criteria->compare('page.slash_id',$this->slash_id);
		$criteria->compare('page.title',$this->title,true);
		$criteria->compare('page.meta_keywords',$this->meta_keywords,true);
		$criteria->compare('page.meta_description',$this->meta_description,true);
		$criteria->compare('page.layout',$this->layout,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 100),
		));
	}

	/**
	 * @param string $className
	 * @return Page
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @param Controller $c
	 * @param $zone_id
	 */
	public function renderWidgets(Controller $c, $zone_id) {

		foreach ($this->widgets as $widget) {
			if ($widget->zone_id != $zone_id) {
				continue;
			}
			if ($widget->class && $widget->class != 'Text') {
				$c->widget(WidgetHelper::PATH_ALIAS . $widget->class, CHtml::listData($widget->params, 'name', 'value'));
			} else {
				if ($zone_id == Widget::ZONE_CONTENT) {
					echo '<div class="plain_content">' . $widget->text . '</div>';
				} else {
					echo $widget->text;
				}
			}
		}
	}
}
