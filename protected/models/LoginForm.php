<?php
/**
 * Class LoginForm
 */
class LoginForm extends CFormModel {

	public $username;
	public $password;
	public $rememberMe;

	private $_identity;

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('username, password', 'required'),
			array('rememberMe', 'boolean'),
			array('password', 'authenticate'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'username' => 'Email или телефон',
			'password' => 'Пароль',
			'rememberMe' => 'Запомнить меня',
		);
	}

	/**
	 * @param $attribute
	 * @param $params
	 */
	public function authenticate($attribute, $params) {

		if(!$this->hasErrors()) {
			$this->_identity = new UserIdentity($this->username,$this->password);
			if (!$this->_identity->authenticate()) {
				$this->addError(
					strpos($this->username, '@') === false ? 'username' : 'password',
					strpos($this->username, '@') === false ? 'Такой телефон не найден в базе, введите номер еще раз или авторизуйтесь используя e-mail' : 'Некорректное имя пользователя или пароль'
				);
			}
		}
	}

	/**
	 * @return bool
	 */
	public function login() {

		if($this->_identity === null) {
			$this->_identity = new UserIdentity($this->username, $this->password);
			$this->_identity->authenticate();
		}

		if($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
			$duration = $this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity, $duration);
			return true;
		} else {
			return false;
		}
	}
}
