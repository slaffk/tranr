<?php
/**
 * Class MailTemplate
 *
 * @property integer $id
 * @property integer $event
 * @property string $title
 * @property string $content
 * @property string $content_html
 * @property string $content_sms
 */
class MailTemplate extends ActiveRecord {

	const EVENT_ERROR = 10000;

	const EVENT_USER_REG = 1;
	const EVENT_TRAINER_REG = 2;

	/**
	 * @var array
	 */
	public static $events = array(
		self::EVENT_ERROR => 'Ошибка на сайте',

		self::EVENT_USER_REG => 'Регистрация пользователя',
		self::EVENT_TRAINER_REG => 'Регистрация тренера',
	);

	/**
	 * @var array уведомления, от которых невозможно отписаться
	 */
	public static $bulkEvents = array(
		self::EVENT_USER_REG,
		self::EVENT_TRAINER_REG,
	);

	/**
	 * @return string
	 */
	public function tableName() {

		return 'mail_template';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('event, title, content', 'required'),
			array('event', 'in', 'range' => array_keys(self::$events)),
			array('event', 'unique', 'message' => 'Событие используется другим шаблоном'),
			array('event', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max' => 256),
			array('content, content_html, content_sms', 'safe'),
			array('id, event, title, content, content_html', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'mail_template',
		);
	}
	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		return $scopes;
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'event' => 'Событие',
			'title' => 'Заголовок',
			'content' => 'Содержание',
			'content_html' => 'Содержание HTML',
			'content_sms' => 'Содержание SMS',
		);
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		return parent::beforeValidate();
	}

	/**
	 * @return bool
	 */
	protected function beforeDelete() {

		return parent::beforeDelete();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('event', $this->event);
		$criteria->compare('title',$this->title, true);
		$criteria->compare('content',$this->content, true);
		$criteria->compare('content_html',$this->content_html, true);
		$criteria->compare('content_sms',$this->content_sms, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return MailTemplate
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}
}
