<?php
/**
 * Class WidgetParam
 *
 * @property integer $id
 * @property integer $widget_id
 * @property string $name
 * @property string $value
 */
class WidgetParam extends CActiveRecord {

	/**
	 * @return string
	 */
	public function tableName() {
		return 'widget_param';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('widget_id, name', 'required'),
			array('widget_id', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 128),
			array('value', 'length', 'max' => 512),
			array('id, widget_id, name, value', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'widget' => array(self::BELONGS_TO, 'Widget', 'widget_id'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'id' => 'Идентификатор',
			'widget_id' => 'Виджет',
			'name' => 'Имя параметра',
			'value' => 'Значение параметра',
		);
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('widget_id',$this->widget_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string
	 * @return WidgetParam
	 */
	public static function model($className=__CLASS__) {

		return parent::model($className);
	}
}
