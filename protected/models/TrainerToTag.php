<?php
/**
 * Class TrainerToTag
 *
 * @property integer $trainer_id
 * @property integer $tag_id
 *
 * @property Trainer $trainer
 * @property Tag $tag
 */
class TrainerToTag extends ActiveRecord {

	/**
	 * @return string
	 */
	public function tableName() {

		return 'trainer_to_tag';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('trainer_id, tag_id', 'required'),
			array('trainer_id, tag_id', 'numerical', 'integerOnly' => true),
			array('trainer_id, tag_id', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'trainer' => array(self::BELONGS_TO, 'Trainer', 'trainer_id'),
			'tag' => array(self::BELONGS_TO, 'Tag', 'tag_id'),
		);
	}

	/**
	 * @param string $className
	 * @return TrainerToTag
	 */
	public static function model($className=__CLASS__) {

		return parent::model($className);
	}
}
