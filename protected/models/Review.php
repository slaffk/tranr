<?php

/**
 * This is the model class for table "review".
 *
 * The followings are the available columns in table 'review':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $city_id
 * @property integer $company_id
 * @property integer $category_id
 * @property integer $user_id
 * @property string $ndescription
 * @property string $pdescription
 * @property integer $specialist_id
 * @property integer $product_id
 * @property integer $service_id
 * @property integer $active
 * @property string $timestamp
 */
class Review extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'review';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('city_id, company_id, category_id, user_id, specialist_id, product_id, service_id, active, timestamp', 'required'),
			array('parent_id, city_id, company_id, category_id, user_id, specialist_id, product_id, service_id, active', 'numerical', 'integerOnly'=>true),
			array('ndescription, pdescription', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_id, city_id, company_id, category_id, user_id, ndescription, pdescription, specialist_id, product_id, service_id, active, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Идентификатор',
			'parent_id' => 'Идентификатор предка',
			'city_id' => 'Ид города',
			'company_id' => 'Ид компании',
			'category_id' => 'Ид категории',
			'user_id' => 'Ид пользователя',
			'ndescription' => 'Плохие слова',
			'pdescription' => 'Хорошие слова',
			'specialist_id' => 'Ид спеца',
			'product_id' => 'Ид товара',
			'service_id' => 'Ид услуги',
			'active' => 'Активность',
			'timestamp' => 'Штамп',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('ndescription',$this->ndescription,true);
		$criteria->compare('pdescription',$this->pdescription,true);
		$criteria->compare('specialist_id',$this->specialist_id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('service_id',$this->service_id);
		$criteria->compare('active',$this->active);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Review the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
