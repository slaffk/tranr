<?php
/**
 * Class Metro
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 *
 * @property City $city
 * @property Vacancy[] $vacancies
 *
 * @method Metro current()
 *
 */
class Metro extends ActiveRecord {

	/**
	 * @return string
	 */
	public function tableName() {

		return 'metro';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('city_id, name', 'required'),
			array('city_id', 'numerical', 'integerOnly' => true),
			array('city_id', 'exist', 'attributeName' => 'id', 'className' => 'City'),
			array('name', 'length', 'max' => 256),
			array('id, city_id, name', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'vacancies' => array(self::HAS_MANY, 'Vacancy', 'area_id'),
		);
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'metro',
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['current'] = array(
			'condition' => '`metro`.`city_id` = ' . Yii::app()->city->id,
		);
		return $scopes;
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'city_id' => 'Город',
			'name' => 'Станция метро',
		);
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('city_id', $this->city_id);
		$criteria->compare('name', $this->name, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return Metro
	 */
	public static function model($className=__CLASS__) {

		return parent::model($className);
	}
}
