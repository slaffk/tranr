<?php
/**
 * Class Tariff
 *
 * @property integer $id
 * @property string $name
 * @property integer $stick
 * @property integer $main_page
 * @property integer $hide_related
 * @property integer $color
 * @property integer $recommend
 * @property integer $verify
 * @property integer $price_month
 * @property integer $price_quarter
 * @property integer $price_half_year
 * @property integer $price_year
 *
 * @property Trainer[] $trainers
 */
class Tariff extends ActiveRecord {

	/**
	 * @return string
	 */
	public function tableName() {

		return 'tariff';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('name, price_month, price_quarter, price_half_year, price_year', 'required'),
			array('stick, main_page, hide_related, color, recommend, verify', 'boolean'),
			array('price_month, price_quarter, price_half_year, price_year', 'numerical'),
			array('name', 'length', 'max' => 256),
			array('id, name, price_month, price_quarter, price_half_year, price_year, stick, main_page, hide_related, color, master, professional', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'trainers' => array(self::HAS_MANY, 'Trainer', 'tariff_id'),
		);
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'tariff',
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'name' => 'Наименование',
			'stick' => 'Закрепить сверху',
			'hide_related' => 'Скрыть похожих конкурентов',
			'color' => 'Покрасить в золото',
			'recommend' => 'Рекомендуемый',
			'verify' => 'Проверенный',
			'main_page' => 'Ротация на главной',
			'price_month' => 'Цена за месяц',
			'price_quarter' => 'Цена за квартал',
			'price_half_year' => 'Цена за пол года',
			'price_year' => 'Цена за год',
		);
	}

	/**
	 * @return array
	 */
	public function getOptions() {

		$options = array();
		if ($this->stick) {
			$options['stick'] = $this->attributeLabels()['stick'];
		}
		if ($this->hide_related) {
			$options['hide_related'] = $this->attributeLabels()['hide_related'];
		}
		if ($this->color) {
			$options['color'] = $this->attributeLabels()['color'];
		}
		if ($this->recommend) {
			$options['recommend'] = $this->attributeLabels()['recommend'];
		}
		if ($this->verify) {
			$options['verify'] = $this->attributeLabels()['verify'];
		}
		if ($this->main_page) {
			$options['main_page'] = $this->attributeLabels()['main_page'];
		}
		return $options;
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('stick', $this->stick);
		$criteria->compare('hide_related', $this->hide_related);
		$criteria->compare('color', $this->color);
		$criteria->compare('recommend', $this->recommend);
		$criteria->compare('verify', $this->verify);
		$criteria->compare('price_month', $this->price_month);
		$criteria->compare('price_quarter', $this->price_quarter);
		$criteria->compare('price_half_year', $this->price_half_year);
		$criteria->compare('price_year', $this->price_year);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return Tariff
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}
}
