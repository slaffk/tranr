<?php
/**
 * Class Setting
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 */
class Setting extends ActiveRecord {

	const MAIL_HEADER = 'MAIL_HEADER';
	const MAIL_HEADER_HTML = 'MAIL_HEADER_HTML';
	const MAIL_FOOTER = 'MAIL_FOOTER';
	const MAIL_FOOTER_HTML = 'MAIL_FOOTER_HTML';

	/**
	 * @return string
	 */
	public function tableName() {

		return 'setting';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('name, value', 'required'),
			array('name', 'unique'),
			array('id, name, value', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
		);
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'setting',
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'name' => 'Имя',
			'value' => 'Значение',
		);
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('value',$this->value, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return Setting
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @param $attributes
	 * @return Setting
	 */
	public static function findByAttributesOrCreate($attributes) {

		$row = static::model()->findByAttributes($attributes);
		if (!$row) {
			$row = new static();
			$row->attributes = $attributes;
		}
		return $row;
	}

}
