<?php
/**
 * Class Tag
 *
 * @property integer $id
 * @property string $name
 */
class Tag extends CActiveRecord {

	/**
	 * @return string
	 */
	public function tableName() {

		return 'tag';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('name', 'required'),
			array('name', 'unique'),
			array('name', 'length', 'max' => 256),
			array('id, name', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'name' => 'Тег',
		);
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param $name
	 * @return Tag
	 */
	public function findOrCreate($name) {

		$model = $this->findByAttributes(array('name' => $name));
		if ($model) {
			return $model;
		} else {
			$model = new Tag;
			$model->name = $name;
			$model->save();
			return $model;
		}
	}

	/**
	 * @param string $className
	 * @return Tag
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}
}
