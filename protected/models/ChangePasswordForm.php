<?php
/**
 * Class ChangePasswordForm
 */
class ChangePasswordForm extends CFormModel {

	public $oldPassword;
	public $password;
	public $passwordConfirm;

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('oldPassword, password, passwordConfirm', 'required'),
			array('password', 'compare', 'compareAttribute' => 'passwordConfirm'),
			array('passwordConfirm', 'compare', 'compareAttribute' => 'password'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'oldPassword' => 'Старый пароль',
			'password' => 'Новый пароль',
			'passwordConfirm' => 'Подтверждение пароля',
		);
	}

	/**
	 * @return bool
	 */
	public function change() {

		if ($this->validate()) {
			/** @var $model User */
			$model = Yii::app()->user->getModel();
			if ($model->password != crypt($this->oldPassword, substr($this->oldPassword, 0, 2))) {
				$this->addError('oldPassword', 'Неверный пароль');
				return false;
			}
			$model->password = crypt($this->password, substr($this->password, 0, 2));
			$model->save();
			return true;
		}
		return false;
	}
}