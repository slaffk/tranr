<?php
/**
 * Class Role
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 *
 * @property User[] $users
 * @property Resource[] $resources
 * @property RoleToResource[] $roleToResources
 *
 */
class Role extends CActiveRecord {

	const ROLE_ADMIN = 1;
	const ROLE_ICON = 2;
	const ROLE_TRAINER = 3;

	/**
	 * @var string
	 */
	public $resourcesImplode;

	/**
	 * @var array
	 */
	public $resourcesIds;

	/**
	 * @return string
	 */
	public function tableName() {

		return 'role';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('name, alias', 'required'),
			array('name, alias', 'length', 'max'=>256),
			array('id, name, alias', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {

		return array(
			'resources' => array(self::MANY_MANY, 'Resource', 'role_to_resource(role_id, resource_id)'),
			'roleToResources' => array(self::HAS_MANY, 'RoleToResource', 'role_id'),
			'users' => array(self::MANY_MANY, 'User', 'user_to_role(role_id, user_id)'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'name' => 'Имя',
			'alias' => 'Альяс',
		);
	}

	/**
	 * @return void
	 */
	protected function afterFind() {

		if ($this->scenario == 'update') {
			$this->resourcesImplode = array();
			foreach ($this->resources as $resource) {
				$this->resourcesImplode[] = implode('/', array_filter(array($resource->module, $resource->controller, $resource->action)));
			}
		}

		$this->resourcesIds = array_values(CHtml::listData($this->roleToResources, 'resource_id', 'resource_id'));
		parent::afterFind();
	}


	/**
	 * @return void
	 */
	protected function afterSave() {

		foreach ($this->roleToResources as $rtr) {
			$rtr->delete();
		}

		if (is_array($this->resourcesIds)) {
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $this->resourcesIds);
			foreach (Resource::model()->findAll($criteria) as $resource) {
				/** @var Resource $resource  */
				$rtr = new RoleToResource();
				$rtr->role_id = $this->id;
				$rtr->resource_id = $resource->id;
				$rtr->save();
			}
		}

		parent::afterSave();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('alias', $this->alias, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return Role
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @param $alias
	 * @param string $name
	 * @return int
	 * @throws CHttpException
	 */
	public static function getRoleIdByAlias($alias, $name = '') {

		/** @var Role $model */
		$model = self::model()->findByAttributes(['alias' => $alias]);
		if ($model === null) {
			$model = new self();
			$model->alias = $alias;
			$model->name = $name;
			if (!$model->save()) {
				throw new CHttpException(403, 'Ошибка при создании роли');
			}
		}
		return $model->id;
	}
}
