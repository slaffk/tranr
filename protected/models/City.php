<?php
/**
 * Class City
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $alias
 * @property string $timezone
 * @property integer $default
 * @property integer $metro_count
 * @property integer $area_count
 * @property string $phone
 * @property string $email
 * @property string $name_pril
 * @property string $name_pril_f
 * @property string $name_pril_mul
 * @property string $name_pril_where
 * @property string $name_pril_on
 * @property integer $active
 * @property string $timestamp
 *
 * @property Area[] $areas
 * @property Metro[] $metros
 * @property City[] $childs
 * @property City $parent
 *
 * @method City def()
 * @method City root()
 */
class City extends ActiveRecord {

	public static $timezones = array(
		'+01:00' => '+01:00',
		'+02:00' => '+02:00',
		'+03:00' => '+03:00',
		'+04:00' => '+04:00',
		'+05:00' => '+05:00',
		'+06:00' => '+06:00',
		'+07:00' => '+07:00',
		'+08:00' => '+08:00',
		'+09:00' => '+09:00',
		'+10:00' => '+10:00',
		'+11:00' => '+11:00',
		'+12:00' => '+12:00',
	);

	private $currentDefault;

	/**
	 * @return string
	 */
	public function tableName() {

		return 'city';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('name, alias, default, active', 'required'),
			array('alias', 'match', 'pattern' => '/^[A-Za-z0-9-]+$/'),
			array('default, metro_count, area_count, active, parent_id', 'numerical', 'integerOnly' => true),

			array('parent_id', 'exist', 'attributeName' => 'id', 'className' => 'City'),

			array('name, phone, email, name_pril, name_pril_f, name_pril_mul, name_pril_where, name_pril_on', 'length', 'max' => 128),
			array('alias, timezone', 'length', 'max' => 256),
			array('id, name, alias, timezone, default, seo_text, metro_count, area_count, phone, email, name_pril, name_pril_f, name_pril_mul, name_pril_where, name_pril_on, active, timestamp', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'areas' => array(self::HAS_MANY, 'Area', 'city_id'),
			'metros' => array(self::HAS_MANY, 'Metro', 'city_id'),
			'vacancies' => array(self::HAS_MANY, 'Vacancy', 'city_id'),
			'resumes' => array(self::HAS_MANY, 'Resume', 'city_id'),
			'parent' => array(self::BELONGS_TO, 'City', 'parent_id'),
			'childs' => array(self::HAS_MANY, 'City', 'parent_id'),
		);
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		if ($this->parent) {
			$this->active = $this->parent->active;
			$this->timezone = $this->parent->timezone;
			$this->alias = $this->parent->alias;
		}
		if ($this->currentDefault && !$this->default) {
			$this->addError('default', 'Чтобы убрать этот флаг - установите его у другого города');
			return false;
		}
		return parent::beforeValidate();
	}

	/**
	 * @return void
	 */
	protected function afterSave() {

		foreach ($this->childs as $child) {
			$child->validate();
			$child->save();
		}
		parent::afterSave();
	}

	/**
	 * @return void
	 */
	protected function afterFind() {

		$this->currentDefault = $this->default;
		parent::afterFind();
	}

	/**
	 * @return bool
	 */
	protected function beforeSave() {

		if ($this->default) {
			City::model()->updateAll(array('default' => 0));
		}
		return parent::beforeSave();
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'parent_id' => 'Родительский город',
			'name' => 'Город',
			'alias' => 'Альяс',
			'timezone' => 'Таймзона',
			'default' => 'Город по умолчанию',
			'metro_count' => 'Количество станций метро',
			'area_count' => 'Количество районов',
			'phone' => 'Телефон',
			'email' => 'Емейл',
			'name_pril' => 'Имя города в прил. (новосибирский)',
			'name_pril_f' => 'Имя в прил женс род (новосибирская)',
			'name_pril_mul' => 'Имя в прил мн. ч (новосибирские)',
			'name_pril_where' => 'Имя в где (в новосибирске)',
			'name_pril_on' => 'Имя на (на навосибирском)',
			'active' => 'Активен',
			'timestamp' => 'Дата создания',
			'dummy' => '',
		);
	}

	public function defaultScope() {

		return array(
			'alias' => 'city',
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['def'] = array(
			'condition' => '`city`.`default` = 1',
		);
		$scopes['root'] = array(
			'condition' => '`city`.`parent_id` is null and city.active = 1',
		);
		$scopes['hasResume'] = array(
			'condition' => '`city`.`id` in (select distinct city_id from resume)',
		);
		$scopes['hasVacancy'] = array(
			'condition' => '`city`.`id` in (select distinct city_id from vacancy)',
		);
		return $scopes;
	}

	/**
	 * @param $id
	 * @return $this
	 */
	public function region($id = null) {

		if (!$id) {
			$id = Yii::app()->city->id;
		}
		$this->getDbCriteria()->mergeWith(array(
			'condition' => '(`city`.`id` = ' . $id . ' OR `city`.`parent_id` = ' . $id . ')  and city.active = 1',
			'order' => '`city`.`parent_id`, `city`.`name`',
		));
		return $this;
	}

	/**
	 * @param $id
	 * @return $this
	 */
	public function byRegions($id = null) {

		if (!$id) {
			$id = Yii::app()->city->id;
		}
		$this->getDbCriteria()->mergeWith(array(
			'select' =>  'city.id, if (city.parent_id is null, city.id, city.parent_id) as parent_id, city.name',
			'condition' => 'city.active = 1',
			'order' => 'if (`city`.`id` = '.$id.', 0, 1), if (`city`.`parent_id` is null, city.id, city.parent_id), `city`.`name`',
		));
		return $this;
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('timezone',$this->timezone,true);
		$criteria->compare('default',$this->default);
		$criteria->compare('metro_count',$this->metro_count);
		$criteria->compare('area_count',$this->area_count);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name_pril',$this->name_pril,true);
		$criteria->compare('name_pril_f',$this->name_pril_f,true);
		$criteria->compare('name_pril_mul',$this->name_pril_mul,true);
		$criteria->compare('name_pril_where',$this->name_pril_where,true);
		$criteria->compare('name_pril_on',$this->name_pril_on,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string
	 * @return City
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @return string
	 */
	public function getUrl() {

		return 'http://' . $this->alias  . '.' . Yii::app()->params['mainDomain'];
	}
}
