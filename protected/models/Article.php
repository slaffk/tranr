<?php
/**
 * Class Article
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $category_id
 * @property integer $user_id
 * @property string $title
 * @property string $content
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $meta_title
 * @property string $image
 * @property string $image_alt
 * @property string $alias
 * @property integer $status
 * @property string $timestamp
 *
 * @property User $user
 * @property City $city
 * @property Category $category
 * @property Tag[] $tags
 *
 * @method Article published()
 * @method Article limit3()
 * @method Article regional()
 * @method Article random()
 */
class Article extends ActiveRecord {

	const STATUS_ON_REVIEW = 0;
	const STATUS_PUBLISHED = 1;
	const STATUS_HIDDEN = 2;
	const STATUS_DRAFT = 3;
	const STATUS_DECLINED = 4;

	public $tag;

	public $category_ids;

	/**
	 * @var bool
	 */
	public $deleteImage;
	private $currentStatus;
	private $currentCategoryId;

	/**
	 * @var array
	 */
	public static $statuses = array(
		self::STATUS_ON_REVIEW => 'На проверке',
		self::STATUS_PUBLISHED => 'Опубликован',
		self::STATUS_HIDDEN => 'Скрыт',
		self::STATUS_DRAFT => 'Черновик',
		self::STATUS_DECLINED => 'Отклонен',
	);

	/**
	 * @return string
	 */
	public function tableName() {

		return 'article';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('status', 'in', 'range' => array_keys(self::$statuses), 'allowEmpty' => false),
			array('category_id', 'exist', 'attributeName' => 'id', 'className' => 'Category'),
			array('user_id', 'exist', 'attributeName' => 'id', 'className' => 'User'),
			array('city_id', 'exist', 'attributeName' => 'id', 'className' => 'City'),
			array('deleteImage', 'boolean'),
			array('timestamp', 'date', 'format' => 'yyyy-MM-dd HH:mm:ss'),
			array('category_id, user_id, title, content, status', 'required'),
			array('title, meta_keywords, meta_description, meta_title, image_alt', 'length', 'max' => 256),
			array('image', 'file', 'types' => 'jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG', 'allowEmpty' => true),
			array('id, city_id, category_id, user_id, date, title, content, meta_keywords, meta_description, status, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'tags' => array(self::MANY_MANY, 'Tag', 'article_to_tag(article_id, tag_id)'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'city_id' => 'Город',
			'category_id' => 'Категория',
			'user_id' => 'Пользователь',
			'title' => 'Заголовок',
			'content' => 'Контент',
			'image' => 'Основное изображение',
			'image_alt' => 'ALT изображения',
			'alias' => 'Альяс',
			'status' => 'Статус',
			'timestamp' => 'Штамп',
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['published'] = array(
			'condition' => '`article`.`status` = ' . self::STATUS_PUBLISHED,
		);
		$scopes['limit3'] = array(
			'limit' => 3
		);
		$scopes['regional'] = array(
			'condition' => '(article.city_id is null or article.city_id = ' . (@Yii::app()->city->id ? Yii::app()->city->id : 0) . ')',
		);
		$scopes['random'] = array(
			'order' => 'rand()',
		);
		return $scopes;
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'article',
			'order' => '`article`.`timestamp` desc',
		);
	}

	/**
	 * @return void
	 */
	protected function afterFind() {

		$this->tag = implode(',', CHtml::listData($this->tags, 'id', 'name'));
		$this->currentStatus = $this->status;
		$this->currentCategoryId = $this->category_id;
		parent::init();
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		Yii::import('application.extensions.ETranslitFilter');

		$this->user_id = Yii::app()->user->id;
		$this->alias = ETranslitFilter::cyrillicToLatin($this->title);
		return parent::beforeValidate();
	}

	/**
	 * @return bool
	 */
	protected function beforeSave() {

		if ($this->deleteImage) {
			$this->image = null;
		}

		$image = CUploadedFile::getInstance($this, 'image');
		if ($image) {
			$this->image = $image->getExtensionName();
		}

		return parent::beforeSave();
	}

	/**
	 * @return void
	 */
	protected function afterSave() {

		ArticleToTag::model()->deleteAllByAttributes(array('article_id' => $this->id));
		if ($this->tag) {
			$tag = explode(',', $this->tag);

			if (is_array($tag) && !empty($tag)) {
				foreach ($tag as $keyword) {
					if (trim($keyword) != '') {
						$tag = Tag::model()->findOrCreate($keyword);
						$att = new ArticleToTag();
						$att->article_id = $this->id;
						$att->tag_id = $tag->id;
						$att->save();
					}
				}
			}
		}

		$image = CUploadedFile::getInstance($this, 'image');
		if ($image) {
			$filename = $this->id . '.original.' . $image->getExtensionName();
			@mkdir('uploads/articles/');
			@chmod('uploads/articles/', 0777);
			@unlink('uploads/articles/'.$filename);
			@array_map('unlink', @glob('uploads/articles/'.$this->id.'.w*.h*'));
			$image->saveAs('uploads/articles/'.$filename);
			@chmod('uploads/articles/'.$filename, 0777);
			$this->image = $image->getExtensionName();
		}

		if ($this->getIsNewRecord()) {
			/** добавляется новая статья в опубликованном виде в категорию */
			if ($this->category && $this->status == self::STATUS_PUBLISHED) {
				$this->category->saveCounters(array('article_count' => 1));
			}
		} else {
			/** @var $currentCategory Category */
			$currentCategory = Category::model()->findByPk($this->currentCategoryId);
			/** редактируется статья, задана категория */
			if ($this->category) {
				/* заданная категория не равна предыдущей категории, а статус был и есть опубликован*/
				if ($currentCategory && $currentCategory->id != $this->category->id && $this->currentStatus == self::STATUS_PUBLISHED && $this->status == self::STATUS_PUBLISHED) {
					$this->category->saveCounters(array('article_count' => 1));
					$currentCategory->saveCounters(array('article_count' => -1));
				} else
				/* заданная категория не равна предыдущей категории, а статус был неопубликован, а стал опубликован*/
				if ($currentCategory && $currentCategory->id != $this->category->id && $this->currentStatus != self::STATUS_PUBLISHED && $this->status == self::STATUS_PUBLISHED) {
					$this->category->saveCounters(array('article_count' => 1));
				} else
				/* заданная категория не равна предыдущей категории, а статус был опубликован, а стал неопубликован*/
				if ($currentCategory && $currentCategory->id != $this->category->id && $this->currentStatus == self::STATUS_PUBLISHED && $this->status != self::STATUS_PUBLISHED) {
					$currentCategory->saveCounters(array('article_count' => -1));
				} else
				/* раньше небыло категории, а сейчас задана категория и статус неопубликован*/
				if (!$currentCategory && $this->status == self::STATUS_PUBLISHED) {
					$this->category->saveCounters(array('article_count' => 1));
				} else
				/** категория прежняя но у статьи поменялся статус */
				if ($currentCategory->id == $this->category->id && $this->status != $this->currentStatus && in_array(self::STATUS_PUBLISHED, array($this->status, $this->currentStatus))) {
					$this->category->saveCounters(array('article_count' => $this->status == self::STATUS_PUBLISHED ? 1 : -1));
				}
			} else if ($currentCategory && $this->currentStatus == self::STATUS_PUBLISHED) {
				/** редактируется статья, убрали категорию, которая была в статусе опубликована */
				$currentCategory->saveCounters(array('article_count' => -1));
			}
		}
		parent::afterSave();
	}

	/**
	 * @return void
	 */
	protected function afterDelete() {

		if ($this->currentStatus == self::STATUS_PUBLISHED && $this->currentCategoryId && $category = Category::model()->findByPk($this->currentCategoryId)) {
			$category->saveCounters(array('article_count' => -1));
		}
		ArticleToTag::model()->deleteAllByAttributes(array('article_id' => $this->id));
		parent::afterDelete();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('city_id', $this->city_id);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('meta_keywords', $this->meta_keywords, true);
		$criteria->compare('meta_description', $this->meta_description, true);
		$criteria->compare('meta_title', $this->meta_title, true);
		$criteria->compare('image', $this->image, true);
		$criteria->compare('image_alt', $this->image_alt, true);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('timestamp', $this->timestamp, true);

		if (is_array($this->category_ids) && !empty($this->category_ids)) {
			$criteria->addCondition('(category_id in ('.implode(',', $this->category_ids).') or category_id in (select id from category where parent_id in ('.implode(',', $this->category_ids).')))');
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 50),
		));
	}

	/**
	 * @param string $className
	 * @return Article
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @param null $width
	 * @param null $height
	 * @param string $mode
	 * @return string
	 */
	public function getThumb($width = null, $height = null, $mode = 'origin') {

		$dir = 'uploads/articles/';
		$originFile = $dir . $this->id . '.original.' . $this->image;

		if (!is_file($originFile)) {
			return "http://www.placehold.it/{$width}x{$height}/EFEFEF/AAAAAA&text=Tranr";
		}

		if ($mode == 'origin') {
			return '/' . $originFile;
		}

		$filePath = $dir . $this->id . '.w' . $width . '.h' . $height . '.' . $this->image;
		if (!is_file($filePath)) {
			if ($mode == 'resize') {
				Yii::app()->image->load($originFile)->resize($width, $height)->save($filePath);
			} else {
				Yii::app()->image->cropSave($originFile, $width, $height, $filePath);
				@chmod($filePath, 0777);
			}
		}

		return '/' . $filePath;
	}

	/**
	 * @return string
	 */
	public function getUrl() {

		return ($this->category ? $this->category->getArticleUrl() : '') . $this->id . '-' . $this->alias;
	}
}
