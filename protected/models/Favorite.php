<?php
/**
 * Class Favorite
 *
 * @property integer $id
 * @property integer $type
 * @property string $related_class
 * @property integer $related_id
 * @property integer $user_id
 * @property string $url
 * @property string $timestamp
 *
 * @property User $user
 *
 * @method Favorite my()
 */
class Favorite extends ActiveRecord {

	const TYPE_VACANCY = 1;
	const TYPE_RESUME = 2;
	const TYPE_AGENCY = 3;
	const TYPE_EMPLOYER = 4;

	/**
	 * @var array
	 */
	public static $types = array(
		self::TYPE_VACANCY => 'Вакансия',
		self::TYPE_RESUME => 'Резюме',
		self::TYPE_AGENCY => 'Агенство',
		self::TYPE_EMPLOYER => 'Работодатель',
	);

	/**
	 * @var array
	 */
	public static $classes = array(
		self::TYPE_VACANCY => 'Vacancy',
		self::TYPE_RESUME => 'Resume',
		self::TYPE_AGENCY => 'Company',
		self::TYPE_EMPLOYER => 'Company',
	);

	/**
	 * @return string
	 */
	public function tableName() {

		return 'favorite';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('url, type, user_id', 'required'),
			array('type', 'in', 'range' => array_keys(self::$types)),
			array('related_class', 'in', 'range' => self::$classes),
			array('user_id', 'exist', 'attributeName' => 'id', 'className' => 'User'),
			array('related_id, user_id, type', 'numerical', 'integerOnly' => true),
			array('url', 'length', 'max' => 512),
			array('related_class', 'length', 'max' => 128),
			array('id, url, type, related_class, related_id, timestamp', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'favorite',
		);
	}
	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['my'] = array(
			'condition' => '`favorite`.`user_id` = ' . (Yii::app()->user->id ? Yii::app()->user->id : 0),
		);
		return $scopes;
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'type' => 'Тип',
			'url' => 'URL',
			'timestamp' => 'Дата добавления',
		);
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		return parent::beforeValidate();
	}

	/**
	 * @return bool
	 */
	protected function beforeDelete() {

		return parent::beforeDelete();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('type', $this->type);
		$criteria->compare('url',$this->url, true);
		$criteria->compare('timestamp',$this->timestamp, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return Favorite
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}
}
