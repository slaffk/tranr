<?php
/**
 * Class TrainerPhoto
 *
 * @property integer $id
 * @property integer $trainer_id
 * @property string $photo
 * @property string $title
 * @property string $timestamp
 *
 * @property Trainer $trainer
 */
class TrainerPhoto extends ActiveRecord {

	/**
	 * @var bool
	 */
	public $deletePhoto;

	/**
	 * @return string
	 */
	public function tableName() {

		return 'trainer_photo';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('trainer_id, title', 'required'),
			array('trainer_id', 'numerical', 'integerOnly' => true),
			array('photo', 'file', 'types' => 'jpg,png,gif,jpeg', 'allowEmpty' => false, 'on' => 'create'),
			array('photo', 'file', 'types' => 'jpg,png,gif,jpeg', 'allowEmpty' => true, 'on' => 'update'),
			array('deletePhoto', 'boolean'),
			array('id, trainer_id, photo, title, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'trainer' => array(self::BELONGS_TO, 'Trainer', 'trainer_id'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'photo' => 'Файл с фотографией',
			'title' => 'Подпись к фотографии',
			'timestamp' => 'Timestamp',
		);
	}

	/**
	 * @return void
	 */
	protected function afterSave() {

		$image = CUploadedFile::getInstance($this, 'photo');
		if ($image) {
			$filename = $this->id . '.original.' . $image->getExtensionName();
			@mkdir('uploads/'.$this->tableName().'/');
			@chmod('uploads/'.$this->tableName().'/', 0777);
			@unlink('uploads/'.$this->tableName().'/'.$filename);
			@array_map('unlink', @glob('uploads/'.$this->tableName().'/'.$this->id.'.w*.h*'));
			$image->saveAs('uploads/'.$this->tableName().'/'.$filename);
			@chmod('uploads/'.$this->tableName().'/'.$filename, 0777);
			$this->photo = $image->getExtensionName();
		}

		parent::afterSave();
	}
	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('trainer_id', $this->trainer_id);
		$criteria->compare('photo', $this->photo, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('timestamp', $this->timestamp, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string
	 * @return TrainerPhoto
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @return bool
	 */
	public function beforeSave() {

		if ($this->isNewRecord) {
			$this->timestamp = new CDbExpression('NOW()');
		}
		if ($this->deletePhoto) {
			$this->photo = null;
		}
		$image = CUploadedFile::getInstance($this, 'photo');
		if ($image) {
			$this->photo = $image->getExtensionName();
		}
		return parent::beforeSave();
	}

	/**
	 * @param null $width
	 * @param null $height
	 * @param string $mode
	 * @return string
	 */
	public function getThumb($width = null, $height = null, $mode = 'origin') {

		$dir = 'uploads/'.$this->tableName().'/';
		$originFile = $dir . $this->id . '.original.' . $this->photo;

		if (!is_file($originFile)) {
			return "http://www.placehold.it/{$width}x{$height}/EFEFEF/AAAAAA&text=Tranr";
		}

		if ($mode == 'origin') {
			return '/' . $originFile;
		}

		$filePath = $dir . $this->id . '.w' . $width . '.h' . $height . '.' . $this->photo;
		if (!is_file($filePath)) {
			if ($mode == 'resize') {
				Yii::app()->image->load($originFile)->resize($width, $height)->save($filePath);
			} else {
				Yii::app()->image->cropSave($originFile, $width, $height, $filePath);
				@chmod($filePath, 0777);
			}
		}

		return '/' . $filePath;
	}
}
