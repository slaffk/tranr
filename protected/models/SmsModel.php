<?php

/**
 * This is the model class for table "sms".
 *
 * The followings are the available columns in table 'sms':
 * @property integer $id
 * @property integer $sms_id
 * @property integer $phone
 * @property string $text
 * @property string $check_date
 * @property integer $status
 */
class SmsModel extends CActiveRecord
{
	const STATUS_SUCCESS = 'delivery success';
	const STATUS_FAILURE = 'delivery failure';
	const STATUS_SMSC_SUBMIT = 'smsc submit';
	const STATUS_REJECT = 'smsc reject';
	const STATUS_QUEUE = 'queue';
	const STATUS_WAIT = 'wait status';
	const STATUS_INCORRECT_ID = 'incorrect id';
	const STATUS_FIELD = 'empty field';
	const STATUS_AUTH_FAIL = 'incorrect user or password';
	
	
	public static function getStatusList() {
		return array(
			self::STATUS_SUCCESS => 'Сообщение доставлено',
			self::STATUS_FAILURE => 'Ошибка доставки SMS (абонент в течение времени доставки находился вне зоны действия сети или номер абонента заблокирован)',
			self::STATUS_SMSC_SUBMIT => 'Сообщение доставлено в SMSC',
			self::STATUS_REJECT => 'отвергнуто SMSC',
			self::STATUS_QUEUE  => 'Ожидает отправки',
			self::STATUS_WAIT => 'Ожидание статуса (запросите позднее)',
			self::STATUS_INCORRECT_ID => 'Неверный идентификатор сообщения',
			self::STATUS_FIELD => 'Не все обязательные поля заполнены',
			self::STATUS_AUTH_FAIL => 'Ошибка авторизации',
		);
	}




	public $trainer_id;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sms';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phone, text, user_fio, user_phone', 'required'),
			array('sms_id, status', 'numerical', 'integerOnly'=>true),
			//1 SMS – это 70 символов на кириллице или 160 символов на латинице.
			array('text', 'length', 'max'=>70),
			array('check_date, trainer_id', 'safe'),
			array('phone, user_phone', 'normalizePhone'),
			/*
			 * В SMS сообщении из системы SMSAero Вы можете использовать любые буквы 
			 * Русского и Английского алфавита, а также знаки препинания и символы + - * ( ) &
			 */
			array('text', 'match', 'pattern' => '/^[a-zA-Zа-яА-Я0-9 \-\+\*\(\)\&[:punct:]]+$/imu'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sms_id, phone, text, check_date, status', 'safe', 'on'=>'search'),
		);
	}
	
	public function normalizePhone($attribute) {
		$this->$attribute = preg_replace('/[^\d]/', "", $this->$attribute);
		if(!preg_match('/^7[\d]{10}$/', $this->$attribute)) {
			$labels = $this->attributeLabels();
			$this->addError($attribute, $labels[$attribute].' не верен.');
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sms_id' => 'Sms',
			'phone' => 'Номер телефона',
			'text' => 'Сообщение',
			'check_date' => 'Check Date',
			'status' => 'Status',
			'user_phone' => 'Телефон',
			'user_fio' => 'Ваше имя'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sms_id',$this->sms_id);
		$criteria->compare('phone',$this->phone);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('check_date',$this->check_date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SmsModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
