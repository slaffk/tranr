<?php
/**
 * Class RemindForm
 */
class RemindForm extends CFormModel {

	public $email;
	public $captcha;

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('email', 'required'),
			array('captcha', 'required', 'on' => 'step1'),
			array('captcha', 'captcha', 'captchaAction' => 'site/captcha', 'message' => 'Не корректный код проверки', 'on' => 'step1'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'email' => 'E-mail или телефон',
			'captcha' => 'Проверочный код',
		);
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		if (strpos($this->email, '@') === false ) {
			$this->email = CommonHelper::filterPhone($this->email);
		}
		return parent::beforeValidate();
	}

	/**
	 * @return bool
	 */
	public function remind() {

		if ($this->validate()) {
			/** @var $model User */
			if (strpos($this->email, '@') === false ) {
				$model = User::model()->findByAttributes(array('auth_phone' => $this->email));
			} else {
				$model = User::model()->findByAttributes(array('email' => $this->email));
			}
			if ($model) {
				//w($model);
				//die;
				$model->activation_code = strpos($this->email, '@') === false ? rand(100000, 999999) : md5(date('YmdHis'));
				$model->save();
				Mailer::mail(MailTemplate::EVENT_USER_REMIND, $this->email, $model);
				return true;
			} else {
				if (strpos($this->email, '@') === false) {
					$this->addError('email', 'Номер телефона '.$this->email.' на нашем сайте не обнаружен, уточните номер телефона');
				} else {
					$this->addError('email', 'Адрес '.$this->email.' на нашем сайте не обнаружен, уточните адрес e-mail');
				}
			}
		}
		return false;
	}
}