<?php
/**
 * Class Trainer
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $active
 * @property integer $moderated
 * @property integer $category_id
 * @property integer $user_id
 * @property integer $trainer_type
 * @property integer $city_id
 * @property integer $area_id
 * @property integer $metro_id
 * @property string $address
 *
 * @property integer $tariff_id
 * @property string $tariff_start
 * @property string $tariff_end
 *
 * @property string $name
 * @property string $alias
 * @property string $firstname
 * @property string $lastname
 * @property string $middlename
 * @property string $phone
 * @property string $phone_additional
 * @property string $email
 * @property integer $gender
 * @property string $dob
 * @property string $vk_link
 * @property string $fb_link
 * @property string $moderator_comment
 *
 * @property integer $edu_level
 * @property string $edu_college
 * @property string $edu_year
 * @property string $edu_other
 * @property integer $experience
 * @property string $experience_text
 * @property string $additional
 * @property string $image
 * @property string $image_alt
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $meta_title
 * @property string $timestamp
 *
 * @property Category $category
 * @property Tariff $tariff
 * @property User $user
 * @property City $city
 * @property Area $area
 * @property Metro $metro
 * @property Category[] $categories
 * @property TrainerToCategory[] $trainerToCategories
 * @property Tag[] $tags
 *
 * @method Trainer active()
 * @method Trainer notActive()
 * @method Trainer regional()
 * @method Trainer club()
 * @method Trainer mainPage()
 * @method Trainer random()
 */
class Trainer extends ActiveRecord {

	/**
	 * @var bool
	 */
	public $deleteImage;
	/**
	 * @var string
	 */
	public $tag;

	/**
	 * @var array
	 */
	public $category_ids;

	/**
	 * @var boolean
	 */
	private $currentActive;

	/**
	 * @var integer
	 */
	private $currentCategoryId;

	/**
	 * @var array
	 */
	public $categoryIds;

	const TYPE_PERSONAL = 1;
	const TYPE_LEGAL = 2;

	public static $trainerTypes = array(
		self::TYPE_PERSONAL => 'Тренер',
		self::TYPE_LEGAL => 'Организация',
	);

	const EDU_LEVEL_HIGH = 1;
	const EDU_LEVEL_PART_HIGH = 2;

	public static $eduLevels = array(
		self::EDU_LEVEL_HIGH => 'Высшее',
		self::EDU_LEVEL_PART_HIGH => 'Неполное высшее',
	);

	const EXPERIENCE_UNDER_ONE = 1;
	const EXPERIENCE_ONE_TWO = 2;
	const EXPERIENCE_TWO_FIVE = 3;
	const EXPERIENCE_FIVE_TEN = 4;
	const EXPERIENCE_UPPER_TEN = 5;

	public static $experiences = array(
		self::EXPERIENCE_UNDER_ONE => 'Менее 1 года',
		self::EXPERIENCE_ONE_TWO => 'От 1 до 2 лет',
		self::EXPERIENCE_TWO_FIVE => 'От 2 до 5 лет',
		self::EXPERIENCE_FIVE_TEN => 'От 5 до 10 лет',
		self::EXPERIENCE_UPPER_TEN => 'Более 10 лет',
	);

	/**
	 * @return string
	 */
	public function tableName() {

		return 'trainer';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('category_id, user_id, trainer_type, city_id, phone, email', 'required'),
			array('active, moderated', 'boolean'),
			array('category_id', 'exist', 'attributeName' => 'id', 'className' => 'Category'),
			array('parent_id', 'exist', 'attributeName' => 'id', 'className' => 'Trainer', 'criteria' => ['condition' => 'trainer_type = ' . self::TYPE_LEGAL]),
			array('tariff_id', 'exist', 'attributeName' => 'id', 'className' => 'Tariff'),
			array('user_id', 'exist', 'attributeName' => 'id', 'className' => 'User'),
			array('trainer_type', 'in', 'range' => array_keys(self::$trainerTypes), 'allowEmpty' => false),
			array('city_id', 'exist', 'attributeName' => 'id', 'className' => 'City'),
			array('area_id', 'exist', 'attributeName' => 'id', 'className' => 'Area'),
			array('metro_id', 'exist', 'attributeName' => 'id', 'className' => 'Metro'),
			array('address, name, alias', 'length', 'max' => 512),
			array('vk_link, fb_link, firstname, middlename, lastname, phone, phone_additional, email, edu_college, image, image_alt', 'length', 'max' => 128),
			array('meta_keywords, meta_description, meta_title', 'length', 'max' => 256),
			array('gender', 'in', 'range' => array_keys(User::$genders)),
			//array('dob', 'date', 'format' => 'yyyy-MM-dd'),
			array('edu_level', 'in', 'range' => array_keys(self::$eduLevels)),
			array('edu_year', 'length', 'max' => 4),
			array('edu_other, experience_text, additional', 'safe'),
			array('experience', 'in', 'range' => array_keys(self::$experiences)),
			array('categoryIds', 'type', 'type' => 'array'),
			array('image', 'file', 'types' => 'jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG', 'allowEmpty' => true),
			array('deleteImage', 'boolean'),
			array('moderator_comment', 'safe'),

			array('email', 'email'),

			array('id,active,moderated,category_id,user_id,trainer_type,city_id,area_id,metro_id,address,name,firstname,lastname,middlename,phone,phone_additional,email,gender,dob,edu_level,edu_college,edu_year,edu_other,experience,experience_text,additional,image,image_alt,meta_keywords,meta_description,meta_title,timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'tariff' => array(self::BELONGS_TO, 'Tariff', 'tariff_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'area' => array(self::BELONGS_TO, 'Area', 'area_id'),
			'metro' => array(self::BELONGS_TO, 'Metro', 'metro_id'),
			'trainerToCategories' => array(self::HAS_MANY, 'TrainerToCategory', 'trainer_id'),
			'categories' => array(self::MANY_MANY, 'Category', 'trainer_to_category(trainer_id, category_id)'),
			'tags' => array(self::MANY_MANY, 'Tag', 'trainer_to_tag(trainer_id, tag_id)'),
			'reviewsAmount'=>array(self::STAT, 'TrainerReview', 'trainer_id')
		);
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'trainer',
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['active'] = array(
			'condition' => '`trainer`.`active` = 1',
		);
		$scopes['notActive'] = array(
			'condition' => '`trainer`.`active` != 1',
		);
		$scopes['regional'] = array(
			'condition' => '(trainer.city_id is null or trainer.city_id = ' . (@Yii::app()->city->id ? Yii::app()->city->id : 0) . ')',
		);
		$scopes['club'] = array(
			'condition' => 'trainer.trainer_type = ' . self::TYPE_LEGAL,
		);
		$scopes['mainPage'] = array(
			'condition' => 'trainer.tariff_id in (select id from tariff where main_page = 1)',
		);
		$scopes['random'] = array(
			'order' => 'rand()',
		);
		return $scopes;
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'parent_id' => 'Входит в клуб',
			'tariff_id' => 'Текущий тариф',
			'active' => 'Активен',
			'moderated' => 'Промодерирован',
			'category_id' => 'Основная рубрика',
			'user_id' => 'Привязка к пользователю',
			'trainer_type' => 'Тип тренера',
			'city_id' => 'Город',
			'area_id' => 'Район',
			'metro_id' => 'Станция метро',
			'address' => 'Адрес',
			'name' => 'Наименование',
			'alias' => 'Альяс',
			'firstname' => 'Имя',
			'lastname' => 'Фамилия',
			'middlename' => 'Отчество',
			'phone' => 'Телефон',
			'phone_additional' => 'Доп. телефон',
			'email' => 'E-mail',
			'gender' => 'Пол',
			'dob' => 'Дата рождения',
			'edu_level' => 'Уровень образования',
			'edu_college' => 'Учебное заведение',
			'edu_year' => 'Год окончания УЗ',
			'edu_other' => 'Степени, звания, дипломы',
			'experience' => 'Опыт работы',
			'experience_text' => 'Описание опыта работы',
			'additional' => 'Дополнительная информация',
			'image' => 'Аватар',
			'image_alt' => 'ALT аватара',
			'dummy' => '',
			'categoryIds' => 'Дополнительные рубрики',
			'timestamp' => 'Штамп',
			'vk_link' => 'Ссылка вконтакте',
			'fb_link' => 'Ссылка фейсбук',
			'moderator_comment' => 'Примечания (не публ)',
		);
	}

	/**
	 * @return void
	 */
	protected function afterConstruct() {

		$this->active = 1;
		$this->moderated = 0;
		parent::afterConstruct();
	}

	/**
	 * @return void
	 */
	protected function afterFind() {

		$this->tag = implode(',', CHtml::listData($this->tags, 'id', 'name'));
		$this->currentActive = $this->active;
		$this->currentCategoryId = $this->category_id;
		$this->categoryIds = array_values(CHtml::listData($this->trainerToCategories, 'category_id', 'category_id'));
		parent::afterFind();
	}

	/**
	 * @return void
	 */
	protected function afterSave() {

		TrainerToTag::model()->deleteAllByAttributes(array('trainer_id' => $this->id));
		if ($this->tag) {
			$tag = explode(',', $this->tag);
			if (is_array($tag) && !empty($tag)) {
				foreach ($tag as $keyword) {
					if (trim($keyword) != '') {
						$tag = Tag::model()->findOrCreate($keyword);
						$att = new TrainerToTag();
						$att->trainer_id = $this->id;
						$att->tag_id = $tag->id;
						$att->save();
					}
				}
			}
		}

		$image = CUploadedFile::getInstance($this, 'image');
		if ($image) {
			$filename = $this->id . '.original.' . $image->getExtensionName();
			@mkdir('uploads/'.$this->tableName().'/');
			@chmod('uploads/'.$this->tableName().'/', 0777);
			@unlink('uploads/'.$this->tableName().'/'.$filename);
			@array_map('unlink', @glob('uploads/'.$this->tableName().'/'.$this->id.'.w*.h*'));
			$image->saveAs('uploads/'.$this->tableName().'/'.$filename);
			@chmod('uploads/'.$this->tableName().'/'.$filename, 0777);
			$this->image = $image->getExtensionName();
		}

		foreach ($this->trainerToCategories as $ttc) {
			$ttc->delete();
		}

		if (is_array($this->categoryIds)) {
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $this->categoryIds);
			foreach (Category::model()->findAll($criteria) as $category) {
				/** @var $category Category */
				$utr = new TrainerToCategory();
				$utr->trainer_id = $this->id;
				$utr->category_id = $category->id;
				$utr->save();
			}
		}

		if ($this->getIsNewRecord()) {
			/** добавляется новый тренер в опубликованном виде в категорию */
			if ($this->category && $this->active) {
				$this->category->saveCounters(array('trainer_count' => 1));
			}
		} else {
			/** @var $currentCategory Category */
			$currentCategory = Category::model()->findByPk($this->currentCategoryId);
			/** редактируется тренер, задана категория */
			if ($this->category) {
				/* заданная категория не равна предыдущей категории, а статус был и есть опубликован*/
				if ($currentCategory && $currentCategory->id != $this->category->id && $this->currentActive && $this->active) {
					$this->category->saveCounters(array('trainer_count' => 1));
					$currentCategory->saveCounters(array('trainer_count' => -1));
				} else
					/* заданная категория не равна предыдущей категории, а статус был неопубликован, а стал опубликован*/
					if ($currentCategory && $currentCategory->id != $this->category->id && !$this->currentActive && $this->active) {
						$this->category->saveCounters(array('trainer_count' => 1));
					} else
						/* заданная категория не равна предыдущей категории, а статус был опубликован, а стал неопубликован*/
						if ($currentCategory && $currentCategory->id != $this->category->id && $this->currentActive && !$this->active) {
							$currentCategory->saveCounters(array('trainer_count' => -1));
						} else
							/* раньше небыло категории, а сейчас задана категория и статус неопубликован*/
							if (!$currentCategory && $this->active) {
								$this->category->saveCounters(array('trainer_count' => 1));
							} else
								/** категория прежняя но у тренера поменялся статус */
								if ($currentCategory->id == $this->category->id && $this->active != $this->currentActive) {
									$this->category->saveCounters(array('trainer_count' => $this->active ? 1 : -1));
								}
			} else if ($currentCategory && $this->currentActive) {
				/** редактируется тренер, убрали категорию, которая была в статусе опубликована */
				$currentCategory->saveCounters(array('trainer_count' => -1));
			}
		}

		parent::afterSave();
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		if (!$this->alias) {
			Yii::import('application.extensions.ETranslitFilter');
			$this->alias = ETranslitFilter::cyrillicToLatin($this->name);
		}
		return parent::beforeValidate();
	}

	/**
	 * @return bool
	 */
	protected function beforeSave() {

		if ($this->deleteImage) {
			$this->image = null;
		}
		$image = CUploadedFile::getInstance($this, 'image');
		if ($image) {
			$this->image = $image->getExtensionName();
		}
		return parent::beforeSave();
	}

	/**
	 * @return void
	 */
	protected function afterDelete() {

		foreach ($this->trainerToCategories as $utr) {
			$utr->delete();
		}
		TrainerToTag::model()->deleteAllByAttributes(array('trainer_id' => $this->id));
		if ($this->currentActive && $this->currentCategoryId && $category = Category::model()->findByPk($this->currentCategoryId)) {
			$category->saveCounters(array('trainer_count' => -1));
		}
		parent::afterDelete();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		if ($this->id) {
			$criteria->addCondition('`trainer`.`id` in (select `trainer_id` from `trainer_to_category` where `category_id` = :category)');
			$criteria->params = array('category' => $this->id);
		}
		if ($this->name) {
			$criteria->compare('name', $this->name, true, 'OR');
			$criteria->compare('email', $this->name, true, 'OR');
			$criteria->compare('firstname', $this->name, true, 'OR');
			$criteria->compare('middlename', $this->name, true, 'OR');
			$criteria->compare('lastname', $this->name, true, 'OR');
		}
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('city_id', $this->city_id);
		$criteria->compare('trainer_type', $this->trainer_type);
		$criteria->compare('tariff_id', $this->tariff_id);
		$criteria->compare('active', $this->active);
		$criteria->compare('moderated', $this->moderated);

		if (is_array($this->category_ids) && !empty($this->category_ids)) {
			$criteria->addCondition('(category_id in ('.implode(',', $this->category_ids).') or category_id in (select id from category where parent_id in ('.implode(',', $this->category_ids).')))');
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return Trainer
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @param null $width
	 * @param null $height
	 * @param string $mode
	 * @return string
	 */
	public function getThumb($width = null, $height = null, $mode = 'origin') {

		$dir = 'uploads/'.$this->tableName().'/';
		$originFile = $dir . $this->id . '.original.' . $this->image;

		if (!is_file($originFile)) {
			return "http://www.placehold.it/{$width}x{$height}/EFEFEF/AAAAAA&text=Tranr";
		}

		if ($mode == 'origin') {
			return '/' . $originFile;
		}

		$filePath = $dir . $this->id . '.w' . $width . '.h' . $height . '.' . $this->image;
		if (!is_file($filePath)) {
			if ($mode == 'resize') {
				Yii::app()->image->load($originFile)->resize($width, $height)->save($filePath);
			} else {
				Yii::app()->image->cropSave($originFile, $width, $height, $filePath);
				@chmod($filePath, 0777);
			}
		}

		return '/' . $filePath;
	}

	/**
	 * @return string
	 */
	public function getUrl() {

		return ($this->category ? $this->category->getUrl() : Yii::app()->getBaseUrl(true) . '/trainers/') . $this->id . '-' . $this->alias;
	}
	
	public function getReviewAmount($type) {
		$validTypes = array_keys(TrainerReview::getTypeList());
		if(!in_array($type, $validTypes)) {
			throw new Exception("type must be in ".implode(", ", $validTypes));
		}
		return $this->reviewsAmount(array('condition'=>'type=:t', 'params'=>array(':t'=>$type)));
	}
	
	public function getFullName() {
		return $this->firstname.' '.$this->lastname;
	}
}
