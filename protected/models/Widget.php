<?php
/**
 * Class Widget
 *
 * @property integer $id
 * @property integer $page_id
 * @property integer $zone_id
 * @property string $class
 * @property string $text
 * @property integer $sort
 *
 * @property WidgetParam[] $params
 * @property Page $page
 */
class Widget extends CActiveRecord {

	const ZONE_HEADER_1 = 1;
	const ZONE_SIDEBAR_1 = 3;
	const ZONE_SIDEBAR_2 = 7;
	const ZONE_CONTENT = 4;
	const ZONE_FOOTER_1 = 5;
	const ZONE_SEARCH = 6;

	/**
	 * @var array
	 */
	public static $zone = array(
		self::ZONE_HEADER_1 => 'Header 1',
		self::ZONE_SIDEBAR_1 => 'Sidebar 1',
		self::ZONE_SIDEBAR_2 => 'Sidebar 2 (opt)',
		self::ZONE_CONTENT => 'Content',
		self::ZONE_FOOTER_1 => 'Footer 1',
	);

	public $action;
	public $caption;

	/**
	 * @return string
	 */
	public function tableName() {

		return 'widget';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('page_id, zone_id', 'required'),
			array('zone_id', 'in', 'range' => array_keys(self::$zone)),
			array('page_id', 'exist', 'attributeName' => 'id', 'className' => 'Page'),
			array('page_id, zone_id, sort', 'numerical', 'integerOnly' => true),
			array('class', 'length', 'max' => 256),
			array('text', 'safe'),
			array('id, page_id, zone_id, class, text, sort', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
			'params' => array(self::HAS_MANY, 'WidgetParam', 'widget_id'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'page_id' => 'Идентификатор страницы',
			'zone_id' => 'Зона размещения',
			'class' => 'Виджет',
			'text' => 'Текст',
			'sort' => 'Сортировка',
		);
	}

	/**
	 * @return void
	 */
	protected function afterFind() {

		$params = CHtml::listData($this->params, 'name', 'value');
		$this->action = @$params['action'] ? $params['action'] : 'index';
		$this->caption = @$params['caption'] ? $params['caption'] : null;
		parent::afterFind();
	}

	/**
	 * @return void
	 */
	protected function afterSave() {

		$modified = false;
		$modifiedCaption = false;
		foreach ($this->params as $param) {
			if ($param->name == 'action') {
				$param->value = $this->action ? $this->action : 'index';
				$param->save();
				$modified = true;
			}
			if ($param->name == 'caption') {
				$param->value = $this->caption ? $this->caption : '';
				$param->save();
				$modifiedCaption = true;
			}
		}

		if (!$modified) {
			$action = new WidgetParam();
			$action->widget_id = $this->id;
			$action->name = 'action';
			$action->value = $this->action ? $this->action : 'index';
			$action->save();
		}

		if (!$modifiedCaption) {
			$caption = new WidgetParam();
			$caption->widget_id = $this->id;
			$caption->name = 'caption';
			$caption->value = $this->caption ? $this->caption : '';
			$caption->save();
		}

		parent::afterSave();
	}

	/**
	 * @return bool
	 */
	protected function beforeDelete() {

		foreach ($this->params as $param) {
			$param->delete();
		}
		return parent::beforeDelete();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('page_id',$this->page_id);
		$criteria->compare('zone_id',$this->zone_id);
		$criteria->compare('class',$this->class,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('sort',$this->sort);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string
	 * @return Widget
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}
}
