<?php
/**
 * Class Resource
 *
 * @property integer $id
 * @property string $module
 * @property string $controller
 * @property string $action
 * @property string $description
 *
 * @property Role[] $roles
 *
 * @method Role sortAcl()
 */
class Resource extends CActiveRecord {

	/**
	 * @return string
	 */
	public function tableName() {

		return 'resource';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('module, controller, action, description', 'length', 'max' => 256),
			array('id, module, controller, action, description', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'roles' => array(self::MANY_MANY, 'Role', 'role_to_resource(resource_id, role_id)'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'module' => 'Модуль',
			'controller' => 'Контроллер',
			'action' => 'Действие',
			'description' => 'Описание',
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['sortAcl'] = array('order' => 'module, controller, action');
		return $scopes;
	}

	/**
	 * @param array $options
	 * @return CActiveDataProvider
	 */
	public function search($options = array()) {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('module', $this->module, true);
		$criteria->compare('controller', $this->controller, true);
		$criteria->compare('action', $this->action, true);
		$criteria->compare('description', $this->description, true);

		return new CActiveDataProvider($this, array_merge(array('criteria' => $criteria), $options));
	}

	/**
	 * @param string $className
	 * @return Resource
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @return array
	 */
	static public function getAllCodeResources() {

		$resources = array();
		$modules = Yii::app()->metadata->getModules();

		foreach ($modules as $module) {
			$moduleArr = array();
			$controllers = Yii::app()->metadata->getControllers($module);
			foreach ($controllers as $controller) {
				$cntrl = uncamelize(str_replace('Controller', '', $controller));
				$controllerArr = array();
				$actions = Yii::app()->metadata->getActions($controller, $module);
				foreach ($actions as $action) {
					$action = uncamelize($action);
					array_push($controllerArr, $action);
				}
				$moduleArr[$cntrl] = $controllerArr;
			}
			$resources[$module] = $moduleArr;
		}

		return $resources;
	}

	/**
	 * @return array
	 */
	static public function getAllDbResources() {

		$resources = array();
		$dbResources = Resource::model()->findAll(array('order' => 'module, controller, action'));

		foreach ($dbResources as $resource) {
			$module = $resource->module;
			$controller = $resource->controller;
			$action = $resource->action;

			if ($module && !array_key_exists($module, $resources)) {
				$resources[$module] = array();
			}

			if ($module && $controller && !array_key_exists($controller, $resources[$module])) {
				$resources[$module][$controller] = array();
			}

			if ($module && $controller && $action) {
				array_push($resources[$module][$controller], $action);
			}
		}

		return $resources;
	}
}
