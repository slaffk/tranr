<?php
/**
 * Class ResetPasswordForm
 */
class ResetPasswordForm extends CFormModel {

	public $email;
	public $resetCode;
	public $password;
	public $passwordConfirm;

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('resetCode', 'required'),
			array('password, passwordConfirm', 'required', 'on' => 'step2'),
//			array('email', 'email'),
			array('email', 'exist', 'className' => 'User', 'attributeName' => 'email'),
			array('resetCode', 'exist', 'className' => 'User', 'attributeName' => 'activation_code'),
			array('password', 'compare', 'compareAttribute' => 'passwordConfirm', 'on' => 'step2'),
			array('passwordConfirm', 'compare', 'compareAttribute' => 'password', 'on' => 'step2'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'email' => 'E-mail',
			'resetCode' => 'Код сброса пароля',
			'password' => 'Новый пароль',
			'passwordConfirm' => 'Подтверждение пароля',
		);
	}

	/**
	 *
	 */
	public function validateStep1() {

		if ($this->validate()) {
			/** @var $model User */
			if ($this->email) {
				$model = User::model()->findByAttributes(array('email' => $this->email, 'activation_code' => $this->resetCode));
			} else {
				$model = User::model()->findByAttributes(array('activation_code' => $this->resetCode));
			}
			if (!$model) {
				$this->addError('resetCode', 'Проверочный код не подходит');
				return false;
			}
			return $model;
		}
		return false;
	}

	/**
	 * @return bool
	 */
	public function reset() {

		if ($model = $this->validateStep1()) {
			$model->plainPassword = $this->password;
			$model->password = crypt($this->password, substr($this->password, 0, 2));
			$model->activated = 1; #ну мы же данные кода из емейла взяли.. сразу подтвердим емейл, если что)
			$model->save();
			Mailer::mail(MailTemplate::EVENT_PASS_CHANGE, $model->email ? $model->email : $model->auth_phone, $model);
			return true;
		}
		return false;
	}
}