<?php
/**
 * Class TrainerToCategory
 *
 * @property integer $trainer_id
 * @property integer $category_id
 *
 * @property Trainer $trainer
 * @property Category $category
 */
class TrainerToCategory extends ActiveRecord {

	/**
	 * @return string
	 */
	public function tableName() {

		return 'trainer_to_category';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('trainer_id, category_id', 'required'),
			array('trainer_id, category_id', 'numerical', 'integerOnly' => true),
			array('trainer_id, category_id', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'trainer' => array(self::BELONGS_TO, 'Trainer', 'trainer_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}

	/**
	 * @param string $className
	 * @return TrainerToCategory
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}
}
