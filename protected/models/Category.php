<?php
/**
 * Class Category
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $alias
 * @property integer $sort
 *
 * @property string $h1
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property string $a_h1
 * @property string $a_meta_title
 * @property string $a_meta_keywords
 * @property string $a_meta_description
 *
 * @property string $image
 * @property string $image_alt
 *
 * @property integer $trainer_count
 * @property integer $article_count
 *
 * @property Category $parent
 * @property Category[] $childs
 * @property Category[] $childsWithTrainers
 * @property Category[] $childsWithArticles
 *
 * @method Category root()
 * @method Category notRoot()
 * @method Category sortByName()
 * @method Category withTrainers()
 * @method Category withArticles()
 */
class Category extends ActiveRecord {

	/**
	 * @var bool
	 */
	public $deleteImage;

	/**
	 * @var string
	 */
	public $dottedName;

	/**
	 * @return string
	 */
	public function tableName() {

		return 'category';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array_merge(parent::rules(), array(
			array('name', 'required'),
			array('alias', 'match', 'pattern' => '/^[A-Za-z0-9-]+$/'),
			array('sort, trainer_count, article_count', 'numerical', 'integerOnly' => true),
			array('parent_id', 'exist', 'attributeName' => 'id', 'className' => 'Category'),
			array('name, alias', 'length', 'max' => 256),

			array('deleteImage', 'boolean'),
			array('image', 'file', 'types' => 'jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG', 'allowEmpty' => true),

			array('h1, meta_keywords, meta_title, image_alt', 'length', 'max' => 255),
			array('a_h1, a_meta_keywords, a_meta_title', 'length', 'max' => 255),
			array('meta_description', 'length', 'max' => 512),
			array('a_meta_description', 'length', 'max' => 512),
			array('returnUrl', 'length', 'max' => 512),

			array('id, parent_id, name, alias', 'safe', 'on' => 'search'),
		));
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'parent' => array(self::BELONGS_TO, 'Category', 'parent_id'),
			'childs' => array(self::HAS_MANY, 'Category', 'parent_id', 'order' => 'category.sort asc'),
			'childsWithArticles' => array(self::HAS_MANY, 'Category', 'parent_id', 'condition' => 'category.article_count > 0', 'order' => 'category.sort asc'),
			'childsWithTrainers' => array(self::HAS_MANY, 'Category', 'parent_id', 'condition' => 'category.trainer_count > 0', 'order' => 'category.sort asc'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'parent_id' => 'Родитель',
			'name' => 'Наименование',
			'alias' => 'Наименование транслитом',
			'sort' => 'Сортировка',
			'trainer_count' => 'Тренеров',
			'article_count' => 'Статей',

			'image' => 'Иконка',
			'image_alt' => 'ALT иконки',

			'h1' => 'h1',
			'meta_title' => 'Title <span class="titlecount"></span>',
			'meta_keywords' => 'Keywords',
			'meta_description' => 'Description <span class="descriptioncount"></span>',

			'a_h1' => 'h1',
			'a_meta_title' => 'Title <span class="atitlecount"></span>',
			'a_meta_keywords' => 'Keywords',
			'a_meta_description' => 'Description <span class="adescriptioncount"></span>',
		);
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'category',
			'order' => 'category.sort asc',
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['root'] = array(
			'condition' => 'category.parent_id is null',
		);
		$scopes['notRoot'] = array(
			'condition' => 'category.parent_id is not null',
		);
		$scopes['sortByName'] = array(
			'order' => 'category.name asc',
		);
		$scopes['withTrainers'] = array(
			'condition' => 'category.trainer_count > 0',
		);
		$scopes['withArticles'] = array(
			'condition' => 'category.article_count > 0',
		);
		return $scopes;
	}

	/**
	 * @return void
	 */
	protected function afterConstruct() {

		$this->article_count = 0;
		$this->trainer_count = 0;
		parent::afterConstruct();
	}

	/**
	 * @return void
	 */
	protected function afterFind() {

		$this->dottedName = ($this->parent_id ? '...' : '') . $this->name;
		parent::afterFind();
	}

	/**
	 * @return bool
	 */
	protected function beforeSave() {

		if ($this->deleteImage) {
			$this->image = null;
		}

		$image = CUploadedFile::getInstance($this, 'image');
		if ($image) {
			$this->image = $image->getExtensionName();
		}

		return parent::beforeSave();
	}

	/**
	 * @return void
	 */
	protected function afterSave() {

		$image = CUploadedFile::getInstance($this, 'image');
		if ($image) {
			$filename = $this->id . '.original.' . $image->getExtensionName();
			@mkdir('uploads/category/');
			@chmod('uploads/category/', 0777);
			@unlink('uploads/category/'.$filename);
			@array_map('unlink', @glob('uploads/category/'.$this->id.'.w*.h*'));
			$image->saveAs('uploads/category/'.$filename);
			@chmod('uploads/category/'.$filename, 0777);
			$this->image = $image->getExtensionName();
		}

		parent::afterSave();
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		if (!$this->alias) {
			Yii::import('application.extensions.ETranslitFilter');
			$this->alias = ETranslitFilter::cyrillicToLatin($this->name);
		}
		return parent::beforeValidate();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('alias', $this->alias, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 100),
		));
	}

	/**
	 * @param string
	 * @return Category
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @param null $width
	 * @param null $height
	 * @param string $mode
	 * @return string
	 */
	public function getThumb($width = null, $height = null, $mode = 'origin') {

		$dir = 'uploads/category/';
		$originFile = $dir . $this->id . '.original.' . $this->image;

		if (!is_file($originFile)) {
			return "http://www.placehold.it/{$width}x{$height}/ffffff/428bca&text=TRANR";
		}

		if ($mode == 'origin') {
			return '/' . $originFile;
		}

		$filePath = $dir . $this->id . '.w' . $width . '.h' . $height . '.' . $this->image;
		if (!is_file($filePath)) {
			if ($mode == 'resize') {
				Yii::app()->image->load($originFile)->resize($width, $height)->save($filePath);
			} else {
				Yii::app()->image->cropSave($originFile, $width, $height, $filePath);
				@chmod($filePath, 0777);
			}
		}

		return '/' . $filePath;
	}

	/**
	 * @return string
	 */
	public function getLink() {

		return $this->name; //$this->childs ? $this->name : CHtml::link($this->name, $this->getUrl());
	}

	/**
	 * @return string
	 */
	public function getUrl() {

		return Yii::app()->getBaseUrl(true) . '/trainers/' . ($this->parent ? $this->parent->alias . '/' : '') . $this->alias . '/';
	}

	/**
	 * @return string
	 */
	public function getArticleUrl() {

		return Yii::app()->getBaseUrl(true) . '/articles/' . ($this->parent ? $this->parent->alias . '/' : '') . $this->alias . '/';
	}
	/**
	 * @return string
	 */
	public function getAurl() {

		return CHtml::link($this->name, $this->url);
	}

	/**
	 * @param array $counters
	 * @return bool
	 */
	public function saveCounters($counters) {

		if ($this->parent) {
			$this->parent->saveCounters($counters);
		}
		return parent::saveCounters($counters);
	}
}
