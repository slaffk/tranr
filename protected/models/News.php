<?php
/**
 * Class News
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $category_id
 * @property integer $company_id
 * @property integer $service_id
 * @property integer $product_id
 * @property integer $user_id
 * @property string $date
 * @property string $title
 * @property string $content
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $meta_title
 * @property string $image
 * @property string $image_alt
 * @property string $alias
 * @property integer $status
 * @property string $timestamp
 *
 * @property User $user
 * @property City $city
 * @property NewsCategory $category
 * @property Company $company
 * @property Service $service
 * @property Product $product
 * @property Tag[] $tags
 *
 * @method News published()
 */
class News extends ActiveRecord {

	const STATUS_ON_REVIEW = 0;
	const STATUS_PUBLISHED = 1;
	const STATUS_HIDDEN = 2;
	const STATUS_DRAFT = 3;
	const STATUS_DECLINED = 4;

	public $tag;

	/**
	 * @var bool
	 */
	public $deleteImage;

	/**
	 * @var array
	 */
	public static $statuses = array(
		self::STATUS_ON_REVIEW => 'На проверке',
		self::STATUS_PUBLISHED => 'Опубликован',
		self::STATUS_HIDDEN => 'Скрыт',
		self::STATUS_DRAFT => 'Черновик',
		self::STATUS_DECLINED => 'Отклонен',
	);

	/**
	 * @return string
	 */
	public function tableName() {

		return 'news';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('status', 'in', 'range' => array_keys(self::$statuses), 'allowEmpty' => false),
			array('category_id', 'exist', 'attributeName' => 'id', 'className' => 'NewsCategory'),
			array('user_id', 'exist', 'attributeName' => 'id', 'className' => 'User'),
			array('city_id', 'exist', 'attributeName' => 'id', 'className' => 'City'),
			array('company_id', 'exist', 'attributeName' => 'id', 'className' => 'Company'),
			array('service_id', 'exist', 'attributeName' => 'id', 'className' => 'Service'),
			array('product_id', 'exist', 'attributeName' => 'id', 'className' => 'Product'),
			array('deleteImage', 'boolean'),
			array('category_id, user_id, title, content, status', 'required'),
			array('title, meta_keywords, meta_description, meta_title, image_alt', 'length', 'max' => 256),
			array('image', 'file', 'types' => 'jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG', 'allowEmpty' => true),
			array('date', 'safe'),
			array('id, city_id, category_id, company_id, service_id, product_id, user_id, date, title, content, meta_keywords, meta_description, status, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'category' => array(self::BELONGS_TO, 'NewsCategory', 'category_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'service' => array(self::BELONGS_TO, 'Service', 'service_id'),
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'tags' => array(self::MANY_MANY, 'Tag', 'news_to_tag(news_id, tag_id)'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'city_id' => 'Город',
			'category_id' => 'Категория',
			'company_id' => 'Компания',
			'service_id' => 'Услуга',
			'product_id' => 'Товар',
			'user_id' => 'Пользователь',
			'date' => 'Дата',
			'title' => 'Заголовок',
			'content' => 'Контент',
			'meta_keywords' => 'Ключевые слова (KEYWORDS)',
			'meta_description' => 'Метаописание (DESCRIPTION)',
			'meta_title' => 'Заголовок (TITLE)',
			'image' => 'Основное изображение',
			'image_alt' => 'Ключевые изображения (ALT)',
			'alias' => 'Альяс',
			'status' => 'Статус',
			'timestamp' => 'Штамп',
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['published'] = array(
			'condition' => '`news`.`status` = ' . self::STATUS_PUBLISHED,
		);
		return $scopes;
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'news',
			'order' => '`news`.`timestamp` desc',
		);
	}

	/**
	 * @return void
	 */
	protected function afterConstruct() {

		$this->date = date('Y-m-d H:i:s');
		parent::afterConstruct();
	}

	/**
	 * @return void
	 */
	protected function afterFind() {

		$this->tag = implode(',', CHtml::listData($this->tags, 'id', 'name'));
		parent::init();
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		Yii::import('application.extensions.ETranslitFilter');

		$this->user_id = Yii::app()->user->id;
		$this->alias = ETranslitFilter::cyrillicToLatin($this->title);
		return parent::beforeValidate();
	}

	/**
	 * @return bool
	 */
	protected function beforeSave() {

		if ($this->deleteImage) {
			$this->image = null;
		}

		$image = CUploadedFile::getInstance($this, 'image');
		if ($image) {
			$this->image = $image->getExtensionName();
		}

		return parent::beforeSave();
	}

	/**
	 * @return void
	 */
	protected function afterSave() {

		NewsToTag::model()->deleteAllByAttributes(array('news_id' => $this->id));
		if ($this->tag) {
			$tag = explode(',', $this->tag);

			if (is_array($tag) && !empty($tag)) {
				foreach ($tag as $keyword) {
					if (trim($keyword) != '') {
						$tag = Tag::model()->findOrCreate($keyword);
						$att = new NewsToTag();
						$att->news_id = $this->id;
						$att->tag_id = $tag->id;
						$att->save();
					}
				}
			}
		}

		$image = CUploadedFile::getInstance($this, 'image');
		if ($image) {
			$filename = $this->id . '.original.' . $image->getExtensionName();
			@mkdir('uploads/news/');
			@chmod('uploads/news/', 0777);
			@unlink('uploads/news/'.$filename);
			$image->saveAs('uploads/news/'.$filename);
			$this->image = $image->getExtensionName();
		}

		parent::afterSave();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('city_id', $this->city_id);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('service_id', $this->service_id);
		$criteria->compare('product_id', $this->product_id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('meta_keywords', $this->meta_keywords, true);
		$criteria->compare('meta_description', $this->meta_description, true);
		$criteria->compare('meta_title', $this->meta_title, true);
		$criteria->compare('image', $this->image, true);
		$criteria->compare('image_alt', $this->image_alt, true);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('timestamp', $this->timestamp, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 50),
		));
	}

	/**
	 * @param string $className
	 * @return News
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @param null $width
	 * @param null $height
	 * @param string $mode
	 * @return string
	 */
	public function getThumb($width = null, $height = null, $mode = 'origin') {

		$dir = 'uploads/news/';
		$originFile = $dir . $this->id . '.original.' . $this->image;

		if (!is_file($originFile)) {
			return "http://www.placehold.it/{$width}x{$height}/EFEFEF/AAAAAA";
		}

		if ($mode == 'origin') {
			return '/' . $originFile;
		}

		$filePath = $dir . $this->id . '.w' . $width . '.h' . $height . '.' . $this->image;
		if (!is_file($filePath)) {
			if ($mode == 'resize') {
				Yii::app()->image->load($originFile)->resize($width, $height)->save($filePath);
			} else {
				Yii::app()->image->cropSave($originFile, $width, $height, $filePath);
			}
		}

		return '/' . $filePath;
	}

	/**
	 * @return string
	 */
	public function getUrl() {

		return $this->category->getUrl() . $this->alias . '-' . $this->id;
	}
}
