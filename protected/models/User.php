<?php
/**
 * Class User
 *
 * @property integer $id
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property string $middlename
 * @property string $phone
 * @property string $auth_phone
 * @property string $password
 * @property string $begin_date
 * @property string $end_date
 * @property string $last_login
 * @property string $last_login_ip
 * @property integer $active
 * @property integer $activated
 *
 * @property integer $gender
 * @property string $birth_date
 * @property string $image
 *
 * @property Role[] $roles
 * @property UserToRole[] $userToRoles
 *
 * @method User active()
 * @method User notActive()
 * @method User trainers()
 */
class User extends ActiveRecord {

	/**
	 * @var bool
	 */
	public $deleteImage;

	/**
	 * полы
	 */
	const GENDER_MEN = 1;
	const GENDER_WOMEN = 2;

	/**
	 * @var array
	 */
	public static $genders = array(
		self::GENDER_MEN => 'Мужской',
		self::GENDER_WOMEN => 'Женский',
	);

	public static $genderNames = array(
		self::GENDER_MEN => 'Мужчина',
		self::GENDER_WOMEN => 'Женщина',
	);

	/**
	 * @var array
	 */
	public $roleIds;

	/**
	 * @return string
	 */
	public function tableName() {

		return 'user';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('lastname, password, begin_date, active', 'required'),
			array('gender', 'numerical', 'integerOnly' => true),
			array('active, activated', 'boolean'),
			array('email', 'unique'),

			array('image', 'file', 'types' => 'jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG', 'allowEmpty' => true),
			array('deleteImage', 'boolean'),
			array('roleIds', 'type', 'type' => 'array'),

			array('email', 'email'),

			array('email, firstname, lastname, middlename, password, phone', 'length', 'max'=>128),
			array('last_login_ip', 'length', 'max' => 16),
			array('end_date, last_login, birth_date', 'safe'),
			array('id, image, email, firstname, lastname, middlename, password, begin_date, end_date, last_login, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['active'] = array(
			'condition' => '`user`.`active` = 1',
		);
		$scopes['notActive'] = array(
			'condition' => '`user`.`active` != 1',
		);
		$scopes['trainers'] = array(
			'condition' => '`user`.`id` in (select user_id from user_to_role where role_id = '.Role::ROLE_TRAINER.')',
		);
		return $scopes;
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'user',
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'roles' => array(self::MANY_MANY, 'Role', 'user_to_role(user_id, role_id)'),
			'userToRoles' => array(self::HAS_MANY, 'UserToRole', 'user_id'),
			'trainer' => array(self::HAS_ONE, 'Trainer', 'user_id'),
			'blacklist' => array(self::HAS_ONE, 'Blacklist', 'related_id', 'condition' => 'user_id = '.((isset(Yii::app()->user) && Yii::app()->user->id) ? Yii::app()->user->id : 0).' and related_class = "' . __CLASS__.'"'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'email' => 'E-mail',
			'firstname' => 'Имя',
			'lastname' => 'Фамилия',
			'middlename' => 'Отчество',
			'password' => 'Пароль',
			'phone' => 'Телефон',
			'begin_date' => 'Дата создания',
			'end_date' => 'Дата удаления',
			'last_login' => 'Штамп последнего логина',
			'last_login_ip' => 'IP последнего логина',
			'active' => 'Активен',
			'activated' => 'E-mail подтвержден',
			'gender' => 'Пол',
			'birth_date' => 'Дата рождения',
			'dummy' => '',
			'image' => 'Аватар',
		);
	}

	/**
	 * @return void
	 */
	protected function afterFind() {

		$this->roleIds = array_values(CHtml::listData($this->userToRoles, 'role_id', 'role_id'));
		parent::afterFind();
	}

	/**
	 * @return void
	 */
	protected function afterSave() {

		foreach ($this->userToRoles as $utr) {
			$utr->delete();
		}

		if (is_array($this->roleIds)) {
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $this->roleIds);
			foreach (Role::model()->findAll($criteria) as $role) {
				$utr = new UserToRole();
				$utr->user_id = $this->id;
				$utr->role_id = $role->id;
				$utr->save();
			}
		}

		parent::afterSave();
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		if ($this->getIsNewRecord()) {
			$this->begin_date = new CDbExpression('now()');
			$this->end_date = '2037-01-01 00:00:00';
			$this->last_login = '0000-00-00 00:00:00';
			$this->last_login_ip = Yii::app()->request->getUserHostAddress();
		}
		return parent::beforeValidate();
	}

	/**
	 * @return bool
	 */
	protected function beforeSave() {

		if ($this->deleteImage) {
			$this->image = null;
		}

		$image = CUploadedFile::getInstance($this, 'image');
		if ($image) {
			$filename = $this->id . '.original.' . $image->getExtensionName();
			@mkdir('uploads/user/');
			@chmod('uploads/user/', 0777);
			@unlink('uploads/user/'.$filename);
			@array_map('unlink', @glob('uploads/user/'.$this->id.'.w*.h*'));
			$image->saveAs('uploads/user/'.$filename);
			$this->image = $image->getExtensionName();
		}

		return parent::beforeSave();
	}

	/**
	 * @throws CDbException
	 */
	protected function afterDelete() {

		foreach ($this->userToRoles as $utr) {
			$utr->delete();
		}
		parent::afterDelete();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		if ($this->id) {
			$criteria->addCondition('`user`.`id` in (select `user_id` from `user_to_role` where `role_id` = :role)');
			$criteria->params = array('role' => $this->id);
		}
		if ($this->lastname) {
			$criteria->compare('email', $this->lastname, true, 'OR');
			$criteria->compare('firstname', $this->lastname, true, 'OR');
			$criteria->compare('lastname', $this->lastname, true, 'OR');
		}
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('begin_date', $this->begin_date, true);
		$criteria->compare('end_date', $this->end_date, true);
		$criteria->compare('last_login', $this->last_login, true);
		$criteria->compare('active', $this->active);
		$criteria->compare('activated', $this->activated);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return User
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @param null $width
	 * @param null $height
	 * @param string $mode
	 * @return string
	 */
	public function getThumb($width = null, $height = null, $mode = 'origin') {

		$dir = 'uploads/user/';
		$originFile = $dir . $this->id . '.original.' . $this->image;

		if (!is_file($originFile)) {
			return "http://www.placehold.it/{$width}x{$height}/EFEFEF/AAAAAA";
		}

		if ($mode == 'origin') {
			return '/' . $originFile;
		}

		$filePath = $dir . $this->id . '.w' . $width . '.h' . $height . '.' . $this->image;
		if (!is_file($filePath)) {
			if ($mode == 'resize') {
				Yii::app()->image->load($originFile)->resize($width, $height)->save($filePath);
			} else {
				Yii::app()->image->cropSave($originFile, $width, $height, $filePath);
				@chmod($filePath, 0777);
			}
		}

		return '/' . $filePath;
	}

	/**
	 * @return string
	 */
	public function getUrl() {

		return Yii::app()->getBaseUrl(true) . '/user/' . $this->id;
	}

	/**
	 * @param $password
	 * @return string
	 */
	public static function cryptPassword($password) {

		return crypt($password, substr($password, 0, 2));
	}
}
