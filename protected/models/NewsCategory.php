<?php
/**
 * Class NewsCategory
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $section_id
 * @property string $name
 * @property string $alias
 *
 * @property string $h1
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property News[] $news
 * @property Section $section
 * @property NewsCategory $parent
 * @property NewsCategory[] $childs
 *
 * @method NewsCategory root()
 * @method NewsCategory notRoot()
 */
class NewsCategory extends ActiveRecord {

	/**
	 * @return string
	 */
	public function tableName() {

		return 'news_category';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('name', 'required'),
			array('alias', 'match', 'pattern' => '/^[A-Za-z0-9-]+$/'),
			//array('section_id', 'exist', 'attributeName' => 'id', 'className' => 'Section'),
			array('parent_id', 'exist', 'attributeName' => 'id', 'className' => 'NewsCategory'),
			array('name, alias', 'length', 'max' => 256),
			array('h1, meta_keywords, meta_title', 'length', 'max' => 255),
			array('meta_description', 'length', 'max' => 512),
			array('id, parent_id, section_id, name, alias', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'news' => array(self::HAS_MANY, 'News', 'category_id', 'condition' => '`news`.`status` = ' . News::STATUS_PUBLISHED),
			'parent' => array(self::BELONGS_TO, 'NewsCategory', 'parent_id'),
			'childs' => array(self::HAS_MANY, 'NewsCategory', 'parent_id'),
			'section' => array(self::BELONGS_TO, 'Section', 'section_id'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'id' => 'Идентификатор',
			'parent_id' => 'Родитель',
			'section_id' => 'Секция',
			'name' => 'Наименование',
			'alias' => 'Наименование транслитом',
			'h1' => 'h1',
			'meta_title' => 'Title',
			'meta_keywords' => 'Keywords',
			'meta_description' => 'Description',
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['root'] = array(
			'condition' => 't.parent_id is null',
		);
		$scopes['notRoot'] = array(
			'condition' => '((t.parent_id is not null) or (t.id not in (select parent_id from news_category where parent_id is not null)))',
		);
		return $scopes;
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		if (!$this->alias) {
			Yii::import('application.extensions.ETranslitFilter');
			$this->alias = ETranslitFilter::cyrillicToLatin($this->name);
		}
		$this->section_id = 1;
		return parent::beforeValidate();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->compare('section_id', $this->section_id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('alias', $this->alias, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 50),
		));
	}

	/**
	 * @param string $className
	 * @return NewsCategory
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @return string
	 */
	public function getLink() {

		return $this->childs ? $this->name : CHtml::link($this->name, $this->getUrl());
	}

	/**
	 * @return string
	 */
	public function getUrl() {

		return Yii::app()->getBaseUrl(true) . '/news/' . ($this->parent ? $this->parent->alias . '-' : '') . $this->alias . '/';
	}
}
