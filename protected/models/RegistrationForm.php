<?php
/**
 * Class RegistrationForm
 */
class RegistrationForm extends CFormModel {

	public $cityId;
	public $categoryId;
	public $trainerType;
	public $email;
	public $password;
	public $passwordConfirm;
	public $firstname;
	public $lastname;
	public $phone;
	public $agree;
	public $captcha;

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'cityId' => 'Ваш город',
			'categoryId' => 'Вид деятельности',
			'trainerType' => '',
			'email' => 'E-mail',
			'password' => 'Придумайте пароль',
			'passwordConfirm' => 'Повторите пароль',
			'firstname' => 'Имя',
			'lastname' => 'Фамилия',
			'phone' => 'Мобильный телефон',
			'agree' => 'Я согласен с правилами сайта',
			'captcha' => 'Проверочный код',
		);
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('cityId, categoryId, trainerType, firstname, lastname, email, password, passwordConfirm, phone', 'required'),
			array('categoryId', 'exist', 'attributeName' => 'id', 'className' => 'Category'),
			array('cityId', 'exist', 'attributeName' => 'id', 'className' => 'City'),
			array('password', 'compare', 'compareAttribute' => 'passwordConfirm'),
			array('passwordConfirm', 'compare', 'compareAttribute' => 'password'),
			array('email', 'email'),

			array('trainerType', 'in', 'range' => array_keys(Trainer::$trainerTypes)),

			array('phone', 'length', 'max' => 20),
			array('phone', 'match', 'pattern' => '/^((8|\+7)[\- ]?)?((9)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', 'message' => 'Данный номер мобильного не характерен для России'),
			array('email','unique', 'attributeName' => 'email', 'className' => 'User'),
			array('agree', 'compare', 'compareValue' => '1',  'message' => 'Вы не согласились с правилами сайта'),
			array('captcha', 'captcha', 'captchaAction' => 'site/captcha', 'message' => 'Не корректный код проверки'),
		);
	}

	/**
	 * @throws CHttpException
	 */
	public function init() {

		if (!Yii::app()->user->isGuest) {
			throw new CHttpException(403, 'Доступ запрещен', 403);
		}

		parent::init();
	}

	/**
	 * @return void
	 */
	protected function afterConstruct() {

		$this->trainerType = Trainer::TYPE_PERSONAL;
		parent::afterConstruct();
	}

	/**
	 * @return bool
	 * @throws CHttpException
	 */
	public function register() {

		if ($this->validate()) {

			$transaction = Yii::app()->db->beginTransaction();

			$user = new User('create');
			$user->firstname = $this->firstname;
			$user->lastname = $this->lastname;
			$user->email = $this->email;
			$user->password = User::cryptPassword($this->password);
			$user->phone = CommonHelper::formatPhone($this->phone);
			$user->active = 1;
			$user->activated = 1;
			$user->roleIds = array(Role::getRoleIdByAlias('trainer', 'Тренер'));

			if ($user->save()) {

				$trainer = new Trainer('create');
				$trainer->email = $user->email;
				$trainer->phone = $user->phone;
				$trainer->trainer_type = $this->trainerType;
				$trainer->city_id = $this->cityId;
				$trainer->user_id = $user->id;
				$trainer->firstname = $user->firstname;
				$trainer->lastname = $user->lastname;
				$trainer->category_id = $this->categoryId;
				if ($trainer->save()) {
					$transaction->commit();
					#Mailer::mail(MailTemplate::EVENT_USER_REG, $user->email, $user);
					return true;
				}
			}
			$transaction->rollback();
		}
		return false;
	}
}
