<?php
/**
 * Class File
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $filename
 * @property string $file_type
 * @property int $file_size
 * @property string $data
 * @property int $attach
 * @property string $timestamp
 *
 * @property City $city
 *
 * @method File thisCity()
 */
class File extends ActiveRecord {

	/**
	 * @return string
	 */
	public function tableName() {

		return 'file';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('city_id', 'required'),
			array('city_id', 'exist', 'attributeName' => 'id', 'className' => 'City'),
			array('file_size', 'numerical', 'integerOnly' => true),
			array('attach', 'boolean'),
			array('file_type', 'length', 'max' => 128),
			array('data', 'file', 'allowEmpty' => false),
			array('id, city_id, filename, data, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'dummy' => '',
			'id' => 'Идентификатор',
			'city_id' => 'Город',
			'filename' => 'Имя файла',
			'file_type' => 'Тип',
			'file_size' => 'Размер',
			'data' => 'Файл',
			'attach' => 'Отдавать аттачментом',
			'timestamp' => 'Дата загрузки',
		);
	}

	/**
	 * @return array
	 */
	public function scopes() {

		$scopes = parent::scopes();
		$scopes['thisCity'] = array(
			'condition' => 'file.city_id = '.Yii::app()->city->id,
		);
		return $scopes;
	}

	/**
	 * @return array
	 */
	public function defaultScope() {

		return array(
			'alias' => 'file',
			'order' => 'file.city_id, file.timestamp desc',
		);
	}

	/**
	 * @return bool
	 */
	protected function beforeValidate() {

		$file = CUploadedFile::getInstance($this, 'data');
		if ($file) {
			$this->filename = $file->name;
			$this->file_type = $file->type;
			$this->file_size = $file->size;
			$this->data = file_get_contents($file->tempName);
		}

		if (File::model()->findByAttributes(array('city_id' => $this->city_id, 'filename' => $this->filename))) {
			$this->addError('data', 'Файл с таким именем уже существует в этом городе');
			return false;
		}

		return parent::beforeValidate();
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('city_id', $this->city_id);
		$criteria->compare('filename', $this->filename, true);
		$criteria->compare('file_type', $this->file_type, true);
		$criteria->compare('file_size', $this->file_size);
		$criteria->compare('timestamp', $this->timestamp, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string
	 * @return File
	 */
	public static function model($className=__CLASS__) {

		return parent::model($className);
	}

	public function getUrl() {
		return $this->city->getUrl() . '/' . $this->filename;
	}
}
