<?php
/**
 * Class TrainerVideo
 *
 * @property integer $id
 * @property integer $trainer_id
 * @property string $link
 * @property string $title
 * @property string $timestamp
 *
 * @property Trainer $trainer
 */
class TrainerVideo extends ActiveRecord {

	const SCREENSHOT_SIZE_SMALL = 0;
	const SCREENSHOT_SIZE_BIG = 1;

	/**
	 * @return string
	 */
	public function tableName() {

		return 'trainer_video';
	}

	/**
	 * @return array
	 */
	public function rules() {

		return array(
			array('trainer_id, link, title', 'required'),
			array('trainer_id', 'numerical', 'integerOnly' => true),
			array('link, title', 'length', 'max' => 255),
			array('link', 'url'),
			array('link', 'checkUrl'),
			array('id, trainer_id, link, title, timestamp', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @param $attribute
	 */
	public function checkUrl($attribute) {

		$url = $this->$attribute;
		if (!preg_match('#^http://www.youtube.com/watch\?v=[\d\D]+$#', $url)) {
			$labels = $this->attributeLabels();
			$this->addError($attribute, $labels[$attribute] . ' должен быть формата http://www.youtube.com/watch?v=*');
		}
	}


	/**
	 * @return array
	 */
	public function relations() {

		return array(
			'trainer' => array(self::BELONGS_TO, 'Trainer', 'trainer_id'),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels() {

		return array(
			'link' => 'Url видео',
			'title' => 'Название видео',
		);
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('trainer_id', $this->trainer_id);
		$criteria->compare('link', $this->link, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('timestamp', $this->timestamp, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string
	 * @return TrainerVideo
	 */
	public static function model($className = __CLASS__) {

		return parent::model($className);
	}

	/**
	 * @return string
	 */
	public function getVideoId() {

		$matches = array();
		preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $this->link, $matches);
		return isset($matches[1]) ? $matches[1] : 'XXXXXXXXXXX';
	}

	/**
	 * @param $size
	 * @return string
	 * @throws Exception
	 */
	public function getVideoScreenshot($size) {

		$screenshot = 'http://img.youtube.com/vi/XXXXXXXXXXX/1.jpg';
		switch($size) {
			case self::SCREENSHOT_SIZE_SMALL:
				$screenshot = 'http://img.youtube.com/vi/'.$this->getVideoId().'/1.jpg';
				break;
			case self::SCREENSHOT_SIZE_BIG:
				$screenshot = 'http://img.youtube.com/vi/'.$this->getVideoId().'/0.jpg';
				break;
			default:
				throw new Exception("Укажите размер скриншота.");
		}
		return $screenshot;
	}

	/**
	 * @return bool
	 */
	public function beforeSave() {

		if($this->isNewRecord) {
			$this->timestamp = new CDbExpression('NOW()');
		}
		return parent::beforeSave();
	}
}
