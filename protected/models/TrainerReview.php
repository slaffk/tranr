<?php

/**
 * This is the model class for table "trainer_review".
 *
 * The followings are the available columns in table 'trainer_review':
 * @property integer $id
 * @property integer $trainer_id
 * @property string $email
 * @property string $name
 * @property integer $type
 * @property string $date
 * @property string $text
 * @property string $ip
 *
 * The followings are the available model relations:
 * @property Trainer $trainer
 */
class TrainerReview extends ActiveRecord
{
	
	const TYPE_POSITIVE = 1;
	
	const TYPE_NEGATIVE = 0;
	
	public $verifyCode;
	
	public function beforeSave() {
		if($this->isNewRecord) {
			$this->date = new CDbExpression('NOW()');
			$this->ip = Yii::app()->request->getUserHostAddress();
		}
		return parent::beforeSave();
	}
	
	public static function getTypeList() {
		return array(
			self::TYPE_POSITIVE => 'Положительный',
			self::TYPE_NEGATIVE => 'Отрицательный'
		);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trainer_review';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trainer_id, email, name, text, type', 'required'),
			array('verifyCode', 'required', 'on'=>'guest'),
			array('trainer_id, type', 'numerical', 'integerOnly'=>true),
			array('email, name', 'length', 'max'=>255),
			array('ip', 'length', 'max'=>15),
			array('email', 'email'),
			array('type', 'unsafe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, trainer_id, email, name, type, date, text, ip', 'safe', 'on'=>'search'),
			array(
                'verifyCode',
                'captcha',
                'on'=>'guest'
            ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trainer' => array(self::BELONGS_TO, 'Trainer', 'trainer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trainer_id' => 'Тренер',
			'email' => 'Email',
			'name' => 'Имя',
			'type' => 'Тип отзыва',
			'date' => 'Дата',
			'text' => 'Текст отзыва',
			'ip' => 'Ip',
			'verifyCode' => 'Код проверки',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id = null)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($id != null) {
			$criteria->addCondition('trainer_id=:t');
			$criteria->params[':t'] = $id;
		}
		
		$criteria->compare('id',$this->id);
		$criteria->compare('trainer_id',$this->trainer_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('ip',$this->ip,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>20,
			)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrainerReview the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
