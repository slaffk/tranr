<?php

class SmsCommand extends CConsoleCommand {
	
	public function run() {
		$criteria = new CDbCriteria();
		$criteria->addCondition('status IS NULL', 'OR');
		$criteria->addInCondition('status', array(SmsModel::STATUS_WAIT, SmsModel::STATUS_SMSC_SUBMIT, SmsModel::STATUS_QUEUE), 'OR');
		$models = SmsModel::model()->findAll($criteria);
		
		if(empty($models)) {
			echo 'Ничего не было сделано.'.PHP_EOL;
		}
		
		foreach($models as $model) {
			$data = Yii::app()->sms->getResponse($model->sms_id);
			if($data === false) {
				print_r(Yii::app()->sms->getErrors());
			}
			$save = false;
			if(isset($data['result'])) {
				if($data['result'] == 'reject') {
					switch($data['reason']) {
						case SmsModel::STATUS_INCORRECT_ID:
							$save = true;
							$model->status = SmsModel::STATUS_INCORRECT_ID;
							break;
						case SmsModel::STATUS_FIELD:
							$save = true;
							$model->status = SmsModel::STATUS_FIELD;
							break;
						case SmsModel::STATUS_AUTH_FAIL:
							$save = true;
							$model->status = SmsModel::STATUS_AUTH_FAIL;
							break;
						default:
							break;
					}
				} else {
					switch($data['result']) {
						case SmsModel::STATUS_SUCCESS:
							$save = true;
							$model->status = SmsModel::STATUS_SUCCESS;
							break;
						case SmsModel::STATUS_FAILURE:
							$save = true;
							$model->status = SmsModel::STATUS_FAILURE;
							break;
						case SmsModel::STATUS_SMSC_SUBMIT:
							$save = true;
							$model->status = SmsModel::STATUS_SMSC_SUBMIT;
							break;
						case SmsModel::STATUS_REJECT:
							$save = true;
							$model->status = SmsModel::STATUS_REJECT;
							break;
						case SmsModel::STATUS_QUEUE:
							$save = true;
							$model->status = SmsModel::STATUS_QUEUE;
							break;
						case SmsModel::STATUS_WAIT:
							$save = true;
							$model->status = SmsModel::STATUS_WAIT;
							break;
						default:
							break;
					}
				}
			}
			$statuses = SmsModel::getStatusList();
			if($save === true) {
				$model->check_date = new CDbExpression('NOW()');
				$model->save(false, array('status', 'check_date'));
				echo $statuses[$model->status].PHP_EOL;
			}
		}
	}
}
