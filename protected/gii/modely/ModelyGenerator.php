<?php
/**
 * Class ModelyGenerator
 */
class ModelyGenerator extends CCodeGenerator {

	public $codeModel='application.gii.modely.ModelyCode';

	/**
	 * @param $db
	 * @throws CHttpException
	 */
	public function actionGetTableNames($db) {

		if(Yii::app()->getRequest()->getIsAjaxRequest()) {
			$all = array();
			if(!empty($db) && Yii::app()->hasComponent($db)!==false && (Yii::app()->getComponent($db) instanceof CDbConnection))
				$all=array_keys(Yii::app()->{$db}->schema->getTables());
			echo json_encode($all);
		} else {
			throw new CHttpException(404,'The requested page does not exist.');
		}
	}
}