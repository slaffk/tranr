/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'ru';
	// config.uiColor = '#AADC6E';
	config.extraPlugins = 'youtube,filebrowser,imageuser';
	config.filebrowserUploadUrl = '/site/upload';

	//не очищать <script>**</script>
	config.protectedSource.push( /<script[\s\S]*?script>/g ); /* script tags */
	config.allowedContent = true; /* all tags */
	config.youtube_related = false;
	config.youtube_older = false;
	config.youtube_privacy = false;
};
