$(document).ready(function() {
    $(document).on("click", ".sendSms", function() {
	var trainer_id = $(this).data("trainer");
	$.post(
	    "/site/sms",
	    {trainer_id: trainer_id},
	    function(data) {
		data = JSON.parse(data);
		if(data.success == true) {
		    $("#smsContent").html(data.content);
		}
	    }
	);
	return false;
    });
    $(document).on("click", "#smsSubmitButton", function() {
	var data = $("#sms-form").serialize();
	$.post(
	    "/site/sms",
	    {data: data},
	    function(data) {
		data = JSON.parse(data);
		if(data.success == false && data.content != undefined) {
		    $("#smsContent").html(data.content);
		}
		if(data.success == false && data.message != undefined) {
		    $(".smsInfo").html(data.message);
		}
		if(data.success == true && data.message != undefined) {
		    $(".smsInfo").html(data.message);
		    $("#sms-form")[0].reset();
		}
	    }
	);
    });
    
});