<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
$yii = dirname(__FILE__) . '/../yii/framework/yii.php'; //lite?
$config = dirname(__FILE__) . '/protected/config/web.php';
require_once(dirname(__FILE__) . '/protected/extensions/global_functions.php');
require_once($yii);
Yii::createWebApplication($config)->run();
